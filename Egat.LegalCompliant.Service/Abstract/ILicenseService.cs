﻿using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Service.Abstract
{
    interface ILicenseService
    {
        void GetLicenseTypes();
        void GetLicenseTypesByArtifactId();
        void GetLicenseTypesByExpireInPeriod();
        License RenewLicense(long id, License license);
    }
}

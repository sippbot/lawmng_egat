﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface IViewRendererService
    {
        Task<string> RenderToStringAsync(string viewName, object model);
    }
}

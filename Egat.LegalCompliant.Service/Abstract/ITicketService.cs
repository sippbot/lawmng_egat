﻿using System.Collections.Generic;
using Egat.LegalCompliant.Service.Complaint;
using Egat.LegalCompliant.Model.Tickets;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface ITicketService
    {
        void SubmitTicket(TicketInfo ticket);
        IEnumerable<Ticket> GetAllTickets();
        IEnumerable<Ticket> GetAllTickets(TicketStatus status);
        IEnumerable<Ticket> GetAllTicketsByDepartmentId(long departmentId);
        IEnumerable<Ticket> GetAllTicketsByDepartmentId(long departmentId, TicketStatus status);
        Ticket GetTicket(long id);
        void ResubmitTicket(long id);
        bool IsTaskAssignmentHasTicket(long taskAssignmentId, int overduePeriod);
    }
}

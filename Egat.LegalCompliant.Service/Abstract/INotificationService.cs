﻿using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Notification;
using System.Collections.Generic;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface INotificationService
    {
        IEnumerable<NotificationInfo> ResolveNotificationInfo(string messageConfig, params object[] models);
        void PushNotification(NotificationInfo info);
        void SendNotification(long id);
        void PushNotifications(IEnumerable<NotificationInfo> notificationList);
        void PushNotificationsFromTaskAssignmentSummary(IEnumerable<TaskAssignmentSummary> taskSummary);
        IEnumerable<MailNotification> GetAllNotifications();
        IEnumerable<MailNotification> GetNotificationByUserId(long userId);
    }
}

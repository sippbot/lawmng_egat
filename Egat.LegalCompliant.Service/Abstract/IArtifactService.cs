﻿using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Artifacts;
using System.Collections.Generic;
using System;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface IArtifactService : IBaseService
    {
        Artifact GetArtifact(long id);
        IEnumerable<Artifact> GetArtifacts();
        ArtifactSource GetArtifactSource(long id);
        ArtifactSource GetArtifactSource(string source);
        Artifact CreateArtifact(Artifact artifact);
        Artifact CreateArtifact(Artifact artifact, ArtifactSource source);
        void CancleArtifact(long id);
        ArtifactSource CreateArtifactSource(string sourceName);

        //* add by Toey 21/04/2018
        ArtifactSource UpdateArtifactSource(string sourceName, long? id);
        IEnumerable<ArtifactAttachment> UpdateArtifactDocument(long artifactId, List<long> documentId);
        //*

        ArtifactAttachment CreateArtifactDocument(long artifactId, long documentId);
        ArtifactNodeGroup CreateArtifactNodeGroup(long artifactId, ArtifactNodeGroup artifactNodeGroup);
        void DeleteArtifactNodeGroup(long artifactId);
        ArtifactNode CreateArtifactNode(long artifactId, long artifactNodeGroupId, ArtifactNode artifactNodes);
        void DeleteArtifactNode(long artifactNodeId);
        Artifact UpdateArtifact(long artifactId, Artifact artifact);
        void DeleteArtifactAttachment(long artifactId, long documentId);
        ArtifactNodeGroup UpdateArtifactNodeGroup(long artifactNodeGroupId, ArtifactNodeGroup artifactNodeGroup);
        ArtifactNode UpdateArtifactNode(long artifactNodeId, ArtifactNode artifactNodes);
        IEnumerable<ArtifactNode> CreateArtifactNodes(long artifactId, long artifactNodeGroupId, ICollection<ArtifactNode> artifactNode);
        IEnumerable<ArtifactNodeLicenseType> CreateArtifactNodeRequireLicenseType(long artifactNodeId, ICollection<long> licenseTypeIds);
        IEnumerable<ArtifactNodeLicenseType> UpdateArtifactNodeRequireLicenseType(long artifactNodeId, ICollection<long> licenseTypeIds);
        ArtifactAssignment CreateArtifactAssignment(long artifactNodeId, long departmentId);
        IEnumerable<ArtifactAssignment> CreateArtifactAssignments(long artifactNodeId, ICollection<long> departmentIds);
        IEnumerable<ArtifactAssignment> UpdateArtifactAssignments(long artifactNodeId, ICollection<long> departmentIds);
        void CancleArtifactAssignment(long artifactNodeId, long departmentId);
        IEnumerable<ArtifactAssignment> CreateArtifactAssignmentsAllDepartment(long artifactNodeId);
        int CountArtifactNumberByStatus();
        IEnumerable<ArtifactSummary> GetArtifactSummary(ArtifactStatus status);
        IEnumerable<ArtifactSummary> GetArtifactSummary(long departmentId, ArtifactStatus status);
        IEnumerable<TaskAssignment> CreateTaskAssignmentFromArtifactAssignment(long artifactId, DateTime dueDate);
        int CreateTaskAssignmentFromInitialArtifactAssignment(string token, long artifactId);
    }
}
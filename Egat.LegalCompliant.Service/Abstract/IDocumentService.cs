﻿using Egat.LegalCompliant.Model.Documents;
using System;
using System.Collections.Generic;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface IDocumentService
    {
        Document GetDocument(long id);
        IEnumerable<Document> GetDocumentsByUserId(long userId);
        string GetFileStoragePath(long id);
        string GetFileExtension(long id);
        Document CreateDocument(string title, long fileDataId, bool isPublic, DocumentType type);
        Document UpdateDocument(Document document);
        License CreateLicense(License license);
        License RenewLicense(long id, License license);
        License AttachLicenseToTaskAssignment(long licenseId, long taskAssignmentId);
        IEnumerable<License> GetAllLicenses(long userId);
        IEnumerable<License> GetAllLicensesByDueDate(DateTime begin, DateTime end);
    }
}

﻿using Egat.LegalCompliant.Model.Authorizations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface IAccessControlService
    {
        AccountRoles GetRoles(long userId);
        void SetGroupRole(long groupId, RolesConstant role);
        void SetUserRole(long userId, RolesConstant role);
        void DeleteGroupRole(long groupId, RolesConstant role);
        void DeleteRole(long groupId, RolesConstant role);
    }
}

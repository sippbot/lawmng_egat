﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Abstract
{
    public interface IDailyJobExecutorService
    {
        void SendAssigndTaskAssignment();
        void SendOverdueTicket();
        void SendLicenseExpireAlert();
        void SendLicenseExpiredNotification();
        void AnalyseSystem();
    }
}

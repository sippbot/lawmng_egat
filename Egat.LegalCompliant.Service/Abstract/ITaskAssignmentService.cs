﻿using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Tickets;
using Egat.LegalCompliant.Service.ServiceModels;
using System.Collections.Generic;
using System;

namespace Egat.LegalCompliant.Service.Abstract
{
    /// <summary>
    /// It strongly recommended that you should name method with meanful verb
    /// Use Create Get Update or Delete 
    /// </summary>
    public interface ITaskAssignmentService : IBaseService
    {
        TaskAssignment GetTaskAssignment(long id);
        TaskAssignment GetTaskAssignmentFullDetail(long id);
        IEnumerable<TaskAssignment> GetTaskAssignmentsByDepartmentId(long departmentId);
        IEnumerable<TaskAssignment> GetTaskAssignmentsByArtifactId(long artifactId);
        IEnumerable<TaskAssignment> GetTaskAssignmentsByStatus(TaskAssignmentStatus status, long artifactId, long departmentId);
        IEnumerable<TaskAssignment> GetTaskAssignmentsByStatusExclude(TaskAssignmentStatus status, long artifactId, long departmentId);
        IEnumerable<TaskAssignmentSummary> GetTaskAssignmentsSummaryByArtifactId(long artifactId);
        IEnumerable<TaskAssignmentSummary> GetTaskAssignmentsSummaryByArtifactId(long artifactId, TaskAssignmentStatus status);
        IEnumerable<TaskAssignmentSummary> GetTaskAssignmentsSummaryForProposed(long artifactId);
        IEnumerable<TaskAssessment> GetTaskAssessmentsByTaskAssignmentId(long taskAssignmentId);
        void CreateTaskAssignment(TaskAssignment taskAssignment);
        TaskAssessment CreateTaskAssessment(AssessmentInfoServiceModel info);
        TaskComment CreateTaskComment(TaskComment taskComment);
        void CreateTaskAssignmentProposal(TaskAssignmentProposalServiceModel proposal);
        void ApproveTaskGroup(long taskGroupId, long approverId);
        TaskAssessment GetTaskAssessment(long tasAssignmentId);
        TaskAssessment ReviewTaskAssessnment(AssessmentReviewServiceModel review);
        void UpdateTaskAssessment(long taskAssignmentId, long taskAssessmentId, TaskAssessment taskAssessment);
        IEnumerable<long> GetTaskAssignmentIdListByArtifactId(long artifactId, long departmentId, TaskAssignmentStatus statusFlag);
        IEnumerable<long> GetTaskAssignmentIdListByArtifactIdExclude(long artifactId, long departmentId, TaskAssignmentStatus statusFlag);
        void ApproveTaskAssignment(TaskAssignmentApprovalServiceModel approvalInfo);
        void CloseTicket(long taskAssignmentId, long ticketId, long closeById, string detail);
        Ticket CreateTicket(long taskAssignmentId, Ticket ticket);
        TaskAssignmentAttachment CreateTaskAssignmentAttachment(long taskAssignmentId, long creatorId, long documentId);
        void DeleteTaskAssignmentAttachment(long taskAssignmentId, long documentId);
        IEnumerable<TaskAssignment> CreateTaskAssignmentFromApprovedTaskAssignments(long artifactId, IEnumerable<TaskAssignment> taskAssignments);
        IEnumerable<TaskAssignment> GetInitialTaskAssignmentsInPeriod(DateTime begin, DateTime end);


    }
}

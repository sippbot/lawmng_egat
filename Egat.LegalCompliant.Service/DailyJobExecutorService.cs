﻿using Egat.LegalCompliant.API.Models.Configurations;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Service.Complaint;
using Egat.LegalCompliant.Service.Configurations;
using Egat.LegalCompliant.Service.Utilities;
using Hangfire;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Egat.LegalCompliant.Service
{
    public class DailyJobExecutorService : IDailyJobExecutorService, IDisposable
    {
        private readonly ITaskAssignmentRepository _taskAssignmentRepository;
        private readonly ILicenseRepository _licenseRepository;
        private readonly INotificationService _notificationService;
        private readonly ITicketService _ticketService;
        private readonly IBackgroundJobClient _backgroundJob;
        private readonly DailyJobExecutorServiceConfiguration _dailyJobConfig;
        private readonly TicketServiceConfiguration _ticketServiceConfig;
        private readonly ILogger<DailyJobExecutorService> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public DailyJobExecutorService(ITaskAssignmentRepository taskAssignmentRepository, IUnitOfWork unitOfWork,
            IOptions<DailyJobExecutorServiceConfiguration> dailyJobConfig, IOptions<TicketServiceConfiguration> ticketServiceConfig,
            INotificationService notificationService,ILicenseRepository licenseRepository,
            ITicketService ticketService, IBackgroundJobClient backgroundJob, ILogger<DailyJobExecutorService> logger)
        {
            _taskAssignmentRepository = taskAssignmentRepository;
            _licenseRepository = licenseRepository;
            _dailyJobConfig = dailyJobConfig.Value;
            _notificationService = notificationService;
            _ticketService = ticketService;
            _ticketServiceConfig = ticketServiceConfig.Value;
            _unitOfWork = unitOfWork;
            _backgroundJob = backgroundJob;
            _logger = logger;
        }

        public void AnalyseSystem()
        {
            throw new NotImplementedException();
        }

        public void SendAssigndTaskAssignment()
        {
            var today = DateTime.Today;
            var begin = today;
            var end = today.AddDays(_dailyJobConfig.AutoAssignThreshold);
            var taskAssignmentList = _taskAssignmentRepository.GetInitialTaskAssignmentsInPeriod(begin, end);

            if(taskAssignmentList.Count() == 0)
            {
                _logger.LogInformation("DailyJobExecutorService.SendAssigndTaskAssignment(): no taskassignment be assigned!");
            }

            foreach(var taskAssignment in taskAssignmentList)
            {
                taskAssignment.Assign();
                _taskAssignmentRepository.Update(taskAssignment);
            }

            Save();

            // Email Handling
            if (_dailyJobConfig.EnableSendMail)
            {
                IEnumerable<TaskAssignmentSummary> summary = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(taskAssignmentList);
                _notificationService.PushNotificationsFromTaskAssignmentSummary(summary);
                _logger.LogInformation("DailyJobExecutorService.SendAssigndTaskAssignment(): submitted {0} email notifications", summary.Count());
            }
            Save();
        }

        public void SendLicenseExpireAlert()
        {
            var today = DateTime.Today;
            var begin = today;
            var end = today.AddDays(_dailyJobConfig.AutoLicenseAlertThershold);
            var licenseList = _licenseRepository.GetLicensesByDueDate(begin, end);

            if (licenseList.Count() == 0)
            {
                _logger.LogInformation("DailyJobExecutorService.SendLicenseExpireAlert(): no license be assigned!");
            }

            foreach (var license in licenseList)
            {
                // Email Handling
                if (_dailyJobConfig.EnableSendMail && license.Alert != LicenseAlert.Alert)
                {
                    // LicenseAlertNotification
                    var notificationInfos = _notificationService.ResolveNotificationInfo("LicenseAlertNotification", license);
                    _notificationService.PushNotifications(notificationInfos);
                    license.Alert = LicenseAlert.Alert;
                    _licenseRepository.Update(license);
                }
            }

            Save();

        }

        public void SendLicenseExpiredNotification()
        {
            var licenseList = _licenseRepository.GetExpiredLicense();

            if (licenseList.Count() == 0)
            {
                _logger.LogInformation("DailyJobExecutorService.SendLicenseExpiredNotification(): no license expired today!");
            }

            foreach (var license in licenseList)
            {
                // Email Handling
                if (_dailyJobConfig.EnableSendMail && license.Alert != LicenseAlert.Alert)
                {
                    // LicenseAlertNotification
                    var notificationInfos = _notificationService.ResolveNotificationInfo("LicenseExpiredNotification", license);
                    _notificationService.PushNotifications(notificationInfos);
                    license.Alert = LicenseAlert.Alert;
                    _licenseRepository.Update(license);
                }
            }

            Save();
        }

        public void SendOverdueTicket()
        {
            var taskAssignmentList = _taskAssignmentRepository.GetOverDueTaskAssignments();

            if (taskAssignmentList.Count() == 0)
            {
                _logger.LogInformation("DailyJobExecutorService.SendOverdueTicket(): no taskassignment overdue!");
            }

            if (!_dailyJobConfig.EnableComplaint)
            {
                return;
            }

            foreach (var taskAssignment in taskAssignmentList)
            {
                if (_ticketService.IsTaskAssignmentHasTicket(taskAssignment.Id, 15))
                {
                    // if it already has ticket that create in 15 period we will ignore it
                    continue;
                }
                _logger.LogInformation("DailyJobExecutorService.SendOverdueTicket(): no ticket for taskassignment within period, so create new ticket!");
                // Submit ticket here
                // TODO: Create Ticket and assign to TaskAssignment
                TicketInfo ticketInfo = new TicketInfo();

                ticketInfo.ISO = (int)taskAssignment.Artifact.Type;
                ticketInfo.DepartmentId = taskAssignment.DepartmentId;
                if (taskAssignment.Type == TaskAssignmentType.Initial)
                {
                    ticketInfo.OwnerId = _ticketServiceConfig.InitialPhase.UserId;
                }
                else if (taskAssignment.Type == TaskAssignmentType.FollowUp)
                {
                    ticketInfo.OwnerId = _ticketServiceConfig.FollowUpPhase.UserId;
                }

                ticketInfo.TaskAssignmentId = taskAssignment.Id;
                ticketInfo.TaskAssignmentDescription = taskAssignment.ArtifactNode.Content;
                // We utilize comment to be duedate, so we don't need to add new field for overdue ticket
                ticketInfo.TaskAssessmentComment = DateTimeUtil.ConvertDateTimeToStringDate(taskAssignment.DueDate);
                ticketInfo.ArtifactCode = taskAssignment.Artifact.Code;
                ticketInfo.Department = taskAssignment.Department.Name;

                // Sunmit ticket to ticket service
                _ticketService.SubmitTicket(ticketInfo);

            }

            Save();

        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

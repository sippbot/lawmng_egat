﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ViewRenderer
{
    public class PlaceholderElement
    {
        public string StringFormat { get; set; }
        public Dictionary<string, string> Params { get; set; }

        public PlaceholderElement()
        {
            Params = new Dictionary<string, string>();
        }
    }
}

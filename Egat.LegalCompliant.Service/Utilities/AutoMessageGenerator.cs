﻿using Egat.LegalCompliant.Service.Configurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;

namespace Egat.LegalCompliant.Service.Utilities
{
    public class AutoMessageGenerator : IAutoMessageGenerator
    {
        private readonly IConfigurationRoot Configuration;
        private readonly ServiceCommonConfiguration _serviceConfig;

        public AutoMessageGenerator(IOptions<ServiceCommonConfiguration> config)
        {
            _serviceConfig = config.Value;
            IConfigurationBuilder builder;
            if (_serviceConfig.BasePath != "")
            {
                builder = new ConfigurationBuilder()
                .SetBasePath(_serviceConfig.BasePath)
                .AddJsonFile(_serviceConfig.AutoMessageDictionaryFileName)
                .AddEnvironmentVariables();
            }
            else
            {
                builder = new ConfigurationBuilder()
                .AddJsonFile(_serviceConfig.AutoMessageDictionaryFileName)
                .AddEnvironmentVariables();
            }

            Configuration = builder.Build();
        }

        public string GetMessage(Type Type, string MessageKey, params object[] Arguments)
        {
            if (Type == null || MessageKey == null)
            {
                return String.Empty;
            }
            var fullname = Type.FullName;
            var section = Configuration.GetSection(fullname.Substring(fullname.LastIndexOf(".") + 1));
            var templateString = section.GetValue<string>(MessageKey);

            return String.Format(templateString, Arguments);
        }
    }
}



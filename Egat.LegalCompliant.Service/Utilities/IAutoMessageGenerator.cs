﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Utilities
{
    public interface IAutoMessageGenerator
    {
        string GetMessage(Type Type, string MessageKey, params Object [] Arguments);
    }
}

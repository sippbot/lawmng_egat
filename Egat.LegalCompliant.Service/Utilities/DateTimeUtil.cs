﻿using System;
using System.Globalization;

namespace Egat.LegalCompliant.Service.Utilities
{
    public static class DateTimeUtil
    {
        private static string _contentRootPath = string.Empty;

        public static string ConvertDateTimeToStringDate(DateTime dateTime)
        {
            if (dateTime != DateTime.MinValue)
            {
                CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
                return dateTime.ToString("dd/M/yyyy", _cultureTHInfo);

            }
            return "";
        }

        public static DateTime ConvertStringToDateTime(string dateTime)
        {
            if (dateTime != "")
            {
                CultureInfo _cultureTHInfo = new CultureInfo("th-TH");
                return DateTime.ParseExact(dateTime, "dd/M/yyyy", _cultureTHInfo);

            }
            return DateTime.MinValue;
        }
    }
}

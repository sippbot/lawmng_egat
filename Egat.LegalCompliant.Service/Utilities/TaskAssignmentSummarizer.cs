﻿using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Egat.LegalCompliant.Service.Utilities
{
    public static class TaskAssignmentSummarizer
    {
        public static IEnumerable<TaskAssignmentSummary> CalculateTaskAssignmentSummary(IEnumerable<TaskAssignment> assignments)
        {
            Dictionary<long, Dictionary<long, TaskAssignmentSummary>> result = new Dictionary<long, Dictionary<long, TaskAssignmentSummary>>();

            foreach (var assignment in assignments)
            {
                var summary = new TaskAssignmentSummary();
                if (result.ContainsKey(assignment.ArtifactId))
                {
                    if (result[assignment.ArtifactId].ContainsKey(assignment.DepartmentId))
                    {
                        result[assignment.ArtifactId][assignment.DepartmentId].NumberOfTotalTask++;
                        result[assignment.ArtifactId][assignment.DepartmentId].Status = SummarizeTaskStatus(result[assignment.ArtifactId][assignment.DepartmentId].Status, assignment.Status);
                        if (!assignment.IsReady())
                        {
                            result[assignment.ArtifactId][assignment.DepartmentId].NumberOfInprogressTask++;
                        }
                        result[assignment.ArtifactId][assignment.DepartmentId].Flag |= assignment.Flag;
                        result[assignment.ArtifactId][assignment.DepartmentId].DueDate = CalculateDueDate(result[assignment.ArtifactId][assignment.DepartmentId].DueDate, assignment.DueDate);
                        result[assignment.ArtifactId][assignment.DepartmentId].IsApplicable &= assignment.IsApplicable;
                        result[assignment.ArtifactId][assignment.DepartmentId].IsCompliant &= assignment.IsCompliant;

                        continue;
                    }
                    // First element ( assignment does not exist in list)
                    summary.ArtifactId = assignment.ArtifactId;
                    summary.ArtifactTitle = assignment.Artifact.Title;
                    summary.DepartmentId = assignment.DepartmentId;
                    summary.Department = assignment.Department;
                    summary.NumberOfTotalTask = 1;
                    if (!assignment.IsReady())
                    {
                        summary.NumberOfInprogressTask = 1;
                    }
                    summary.Status = assignment.Status;
                    summary.Flag = 0;
                    summary.IsApplicable = assignment.IsApplicable;
                    summary.IsCompliant = assignment.IsCompliant;
                    summary.DueDate = assignment.DueDate;
                    result[assignment.ArtifactId].Add(assignment.DepartmentId, summary);
                }
                else
                {
                    // First element ( assignment does not exist in list) 
                    summary.ArtifactId = assignment.ArtifactId;
                    summary.ArtifactTitle = assignment.Artifact.Title;
                    summary.DepartmentId = assignment.DepartmentId;
                    summary.Department = assignment.Department;
                    summary.NumberOfTotalTask = 1;
                    if (!assignment.IsReady())
                    {
                        summary.NumberOfInprogressTask = 1;
                    }
                    summary.Status = assignment.Status;
                    summary.Flag = 0;
                    summary.IsApplicable = assignment.IsApplicable;
                    summary.IsCompliant = assignment.IsCompliant;
                    summary.DueDate = assignment.DueDate;
                    var entry = new Dictionary<long, TaskAssignmentSummary>();
                    entry.Add(assignment.DepartmentId, summary);
                    result.Add(assignment.ArtifactId, entry);
                }
            }

            var list = result.Select(kvp => kvp.Value).ToList();
            List<TaskAssignmentSummary> returnList = new List<TaskAssignmentSummary>();
            foreach(var item in list)
            {
                returnList.AddRange(item.Select(kvp => kvp.Value).ToList());
            }

            return returnList;
        }

        public static IEnumerable<TaskAssignmentSummary> CalculateTaskAssignmentSummaryForProposed(IEnumerable<TaskAssignment> assignments)
        {
            Dictionary<long, Dictionary<long, TaskAssignmentSummary>> result = new Dictionary<long, Dictionary<long, TaskAssignmentSummary>>();

            foreach (var assignment in assignments)
            {
                var summary = new TaskAssignmentSummary();
                if (result.ContainsKey(assignment.ArtifactId))
                {
                    if (result[assignment.ArtifactId].ContainsKey(assignment.DepartmentId))
                    {
                        var key = assignment.DepartmentId;
                        result[assignment.ArtifactId][key].NumberOfTotalTask++;
                        result[assignment.ArtifactId][key].Status = SummarizeTaskStatus(result[assignment.ArtifactId][key].Status, assignment.Status);
                        if (!assignment.IsReady())
                        {
                            result[assignment.ArtifactId][key].NumberOfInprogressTask++;
                        }
                        result[assignment.ArtifactId][key].Flag |= assignment.Flag;
                        result[assignment.ArtifactId][key].DueDate = CalculateDueDate(result[assignment.ArtifactId][key].DueDate, assignment.DueDate);
                        result[assignment.ArtifactId][key].IsApplicable &= assignment.IsApplicable;
                        result[assignment.ArtifactId][key].IsCompliant &= assignment.IsCompliant;
                        continue;
                    }
                    // First element ( assignment does not exist in list)
                    summary.ArtifactId = assignment.ArtifactId;
                    summary.ArtifactTitle = assignment.Artifact.Title;
                    summary.DepartmentId = assignment.DepartmentId;
                    summary.Department = assignment.Department;
                    summary.NumberOfTotalTask = 1;
                    if (!assignment.IsReady())
                    {
                        summary.NumberOfInprogressTask = 1;
                    }
                    summary.Status = assignment.Status;

                    summary.Flag = assignment.Flag;

                    summary.IsApplicable = assignment.IsApplicable;
                    summary.IsCompliant = assignment.IsCompliant;
                    summary.DueDate = assignment.DueDate;

                    result[assignment.ArtifactId].Add(assignment.DepartmentId, summary);
                }
                else
                {
                    // First element ( assignment does not exist in list) 
                    summary.ArtifactId = assignment.ArtifactId;
                    summary.ArtifactTitle = assignment.Artifact.Title;
                    summary.DepartmentId = assignment.DepartmentId;
                    summary.Department = assignment.Department;
                    summary.NumberOfTotalTask = 1;
                    if (!assignment.IsReady())
                    {
                        summary.NumberOfInprogressTask = 1;
                    }
                    summary.Status = assignment.Status;

                    summary.Flag = assignment.Flag;
                    
                    summary.IsApplicable = assignment.IsApplicable;
                    summary.IsCompliant = assignment.IsCompliant;
                    summary.DueDate = assignment.DueDate;
                    var entry = new Dictionary<long, TaskAssignmentSummary>();

                    entry.Add(assignment.DepartmentId, summary);

                    result.Add(assignment.ArtifactId, entry);
                }
            }
            var list = result.Select(kvp => kvp.Value).ToList();
            List<TaskAssignmentSummary> returnList = new List<TaskAssignmentSummary>();
            foreach (var item in list)
            {
                returnList.AddRange(item.Select(kvp => kvp.Value).ToList());
            }
            return returnList;
        }

        public static IEnumerable<TaskAssignmentSummary> CalculateTaskAssignmentSummary(IEnumerable<TaskAssignment> assignments, TaskGroup taskGroup)
        {
            Dictionary<long, TaskAssignmentSummary> result = new Dictionary<long, TaskAssignmentSummary>();

            foreach (var assignment in assignments)
            {
                if (!taskGroup.IsInGroup(assignment.Id))
                {
                    // TODO log somthing here
                    continue;
                }
                var summary = new TaskAssignmentSummary();
                if (result.ContainsKey(assignment.DepartmentId))
                {
                    result[assignment.DepartmentId].NumberOfTotalTask++;
                    result[assignment.DepartmentId].Status = SummarizeTaskStatus(result[assignment.DepartmentId].Status, assignment.Status);
                    if (!assignment.IsReady())
                    {
                        result[assignment.DepartmentId].NumberOfInprogressTask++;
                    }
                    summary.Flag &= assignment.Flag;

                    summary.IsApplicable &= assignment.IsApplicable;
                    summary.IsCompliant &= assignment.IsCompliant;

                    continue;
                }

                // First element ( assignment does not exist in list) 
                summary.DepartmentId = assignment.DepartmentId;
                summary.Department = assignment.Department;
                summary.NumberOfTotalTask = 1;
                if (!assignment.IsReady())
                {
                    summary.NumberOfInprogressTask = 1;
                }
                summary.Status = assignment.Status;
                summary.Flag = 0;

                summary.IsApplicable = assignment.IsApplicable;
                summary.IsCompliant = assignment.IsCompliant;

                result.Add(assignment.DepartmentId, summary);
            }

            return result.Select(kvp => kvp.Value).ToList();
        }

        private static TaskAssignmentStatus SummarizeTaskStatus(TaskAssignmentStatus a, TaskAssignmentStatus b)
        {
            var c = Math.Min((int)a, (int)b);
            return (TaskAssignmentStatus)c;
        }
        
        private static DateTime CalculateDueDate(DateTime a,DateTime b)
        {
            return DateTime.Compare(a, b) < 0 ? a : b;
        }

        /*
        public static IEnumerable<TaskAssignment> ConsolidateTaskAssignment(IEnumerable<TaskAssignment> assignments)
        {
            Dictionary<long, TaskAssignment> result = new Dictionary<long, TaskAssignment>();

            foreach (var assignment in assignments)
            {
                var summary = new TaskAssignmentSummary();
                if (result.ContainsKey(assignment.DepartmentId))
                {
                    if (assignment.Assessments == null)
                    {
                        summary.IsApplicable = false;
                        summary.IsCompliant = false;
                        // TODO handle status mapping here
                        summary.SummaryResult = "รอการประเมิน";
                    }
                    else
                    {
                        foreach (var assessment in assignment.Assessments)
                        {
                            summary.IsApplicable &= (assessment.IsApplicable ?? false);
                            summary.IsCompliant &= (assessment.IsCompliant ?? false);
                        }

                    }
                    continue;
                }

                summary.DepartmentId = assignment.DepartmentId;
                summary.Department = assignment.Department;
                summary.NumberOfArtifactNode = 1;
                if (assignment.Assessments == null)
                {
                    summary.IsApplicable = false;
                    summary.IsCompliant = false;
                }
                else
                {
                    foreach (var assessment in assignment.Assessments)
                    {
                        summary.IsApplicable &= (assessment.IsApplicable ?? false);
                        summary.IsCompliant &= (assessment.IsCompliant ?? false);
                    }

                }

                result.Add(assignment.DepartmentId, summary);
            }

            return result.Select(kvp => kvp.Value).ToList();
        }
        */
    }
}

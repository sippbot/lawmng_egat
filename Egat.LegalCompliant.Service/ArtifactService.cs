﻿using Egat.LegalCompliant.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Service.ServiceModels;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Service.Utilities;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Documents;
using System.Linq;
using AutoMapper;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Exceptions;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Service.Configurations;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Egat.LegalCompliant.Data;
using Microsoft.EntityFrameworkCore;

namespace Egat.LegalCompliant.Service
{
    public class ArtifactService : IArtifactService, IDisposable
    {
        private readonly IArtifactSourceRepository _artifactSourceRepository;
        private readonly IDocumentRepository _documentRepository;
        private readonly IArtifactRepository _artifactRepository;
        private readonly IArtifactNodeGroupRepository _artifactNodeGroupRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly ILicenseTypeRepository _licenseTypeRepository;
        private readonly IArtifactNodeRepository _artifactNodeRepository;
        private readonly IArtifactAssignmentRepository _artifactAssignmentRepository;
        private readonly ITaskAssignmentRepository _taskAssignmentRepository;
        private readonly IAutoMessageGenerator _autoMessage;
        private readonly INotificationService _notificationService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private readonly ArtifactServiceConfiguration _config;

        private readonly ILogger<ArtifactService> _logger;

        private readonly IArtifactNodeLicenseTypeRepository _LicenseTypeRepository;
        private readonly IArtifactAttachmentRepository _AttachmentRepository;

        private readonly IArtifactAttachmentRepository _artifactAttachment;
        private readonly IArtifactCategoryRepository _categoryRepository;

        public ArtifactService(IArtifactSourceRepository artifactSourceRepository, IDocumentRepository documentRepository, IArtifactNodeGroupRepository artifactNodeGroupRepository, IDepartmentRepository departmentRepository,
            IArtifactRepository artifactRepository, ILicenseTypeRepository licenseTypeRepository, IArtifactNodeRepository artifactNodeRepository,
            IArtifactAssignmentRepository artifactAssignmentRepository, ITaskAssignmentRepository taskAssignmentRepository, IUnitOfWork unitOfWork,
            IAutoMessageGenerator autoMessage, INotificationService notificationService, IOptions<ArtifactServiceConfiguration> config, IMapper mapper, ILogger<ArtifactService> logger
            , IArtifactNodeLicenseTypeRepository LicenseTypeRepository, IArtifactAttachmentRepository artifactAttachment
            , IArtifactAttachmentRepository AttachmentRepository, IArtifactCategoryRepository categoryRepository)
        {
            _artifactSourceRepository = artifactSourceRepository;
            _documentRepository = documentRepository;
            _artifactNodeGroupRepository = artifactNodeGroupRepository;
            _departmentRepository = departmentRepository;
            _artifactRepository = artifactRepository;
            _licenseTypeRepository = licenseTypeRepository;
            _artifactNodeRepository = artifactNodeRepository;
            _artifactAssignmentRepository = artifactAssignmentRepository;
            _taskAssignmentRepository = taskAssignmentRepository;
            _unitOfWork = unitOfWork;
            _config = config.Value;
            _mapper = mapper;
            _notificationService = notificationService;
            _autoMessage = autoMessage;
            _logger = logger;

            _LicenseTypeRepository = LicenseTypeRepository;
            _AttachmentRepository = AttachmentRepository;
            _artifactAttachment = artifactAttachment;
            _categoryRepository = categoryRepository;
        }


        public Artifact CreateArtifact(Artifact artifact)
        {
            // comment this line because we will allow user to pick any value
            // artifact.CreateDate = DateTime.Now; 
            artifact.NodeGroups = new HashSet<ArtifactNodeGroup>();
            artifact.Nodes = new HashSet<ArtifactNode>();
            _artifactRepository.Add(artifact);

            Save();

            var category = _categoryRepository.GetSingle(artifact.CategoryId);
            var categoryId = category != null ? category.ExternalId : artifact.CategoryId;

            if (artifact.Code == null)
            {
                artifact.Code = String.Format("{0:00} {1:00}/{2}", categoryId, artifact.Id, (DateTime.Today.Year + 543) % 2500);
                _artifactRepository.Update(artifact);
            }

            Save();

            return artifact;
        }

        public ArtifactSource CreateArtifactSource(string sourceName)
        {
            ArtifactSource source = _artifactSourceRepository.FindBy(s => s.Name == sourceName.Trim()).FirstOrDefault();
            if (source == null)
            {
                ArtifactSource _source = new ArtifactSource();
                _source.Name = sourceName.Trim();
                _artifactSourceRepository.Add(_source);
                Save();
                return _source;
            }
            return source;
        }

        public ArtifactSource UpdateArtifactSource(string sourceName, long? id)
        {
            IEnumerable<ArtifactSource> sourcelist = _artifactSourceRepository.FindBy(s => s.Id == id).ToList();
            if (sourcelist.Count() > 0)
            {
                ArtifactSource _source = sourcelist.FirstOrDefault();
                _source.Name = sourceName.Trim();
                _artifactSourceRepository.Update(_source);
                Save();
                return _source;
            }
            return sourcelist.FirstOrDefault();
        }

        public ArtifactSource GetArtifactSource(long id)
        {
            return _artifactSourceRepository.GetSingle(id);
        }

        public ArtifactSource GetArtifactSource(string sourceName)
        {
            return _artifactSourceRepository.FindBy(s => s.Name == sourceName.Trim()).FirstOrDefault();
        }

        public Artifact CreateArtifact(Artifact artifact, ArtifactSource source)
        {
            if (source == null)
            {
                _logger.LogWarning("ArtifactService.CreateArtifact() : cannot find artifact source {0} - by pass to CreateArtifact(Artifact artifact)", source);

                return CreateArtifact(artifact);
            }
            artifact.CreateDate = DateTime.Now;
            artifact.NodeGroups = new HashSet<ArtifactNodeGroup>();
            artifact.Nodes = new HashSet<ArtifactNode>();
            artifact.SourceId = source.Id;
            _artifactRepository.Add(artifact);

            Save();

            var category = _categoryRepository.GetSingle(artifact.CategoryId);
            var categoryId = category != null ? category.ExternalId : artifact.CategoryId;

            if (artifact.Code == null)
            {
                artifact.Code = String.Format("{0:00} {1:00}/{2}", categoryId, artifact.Id, (DateTime.Today.Year + 543) % 2500);
                _artifactRepository.Update(artifact);
            }

            Save();

            return artifact;
        }

        public ArtifactAttachment CreateArtifactDocument(long artifactId, long documentId)
        {
            var _artifact = _artifactRepository.GetSingle(artifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactDocument() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            var _document = _documentRepository.GetSingle(d => d.Id == documentId, d => d.File);
            if (_document == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactDocument() : cannot find document id {0} - throw exception.", documentId);
                throw new ArtifactServiceException("cannot find document");
            }

            var artifactAttachment = new ArtifactAttachment(artifactId, documentId, _document.File.OwnerId);
            _artifact.Documents.Add(artifactAttachment);
            Save();

            return artifactAttachment;
        }

        public IEnumerable<ArtifactAttachment> UpdateArtifactDocument(long artifactId, List<long> documentId)
        {
            var _artifact = _artifactRepository.GetFullDetail(artifactId);

            ICollection<ArtifactAttachment> _afn = _artifact.Documents;

            var previousDoc = _afn.Select(l => l.DocumentId).ToList();

            var includeList = previousDoc.Intersect(documentId); // หาตัวที่ซ้ำ

            var excludeList = previousDoc.Except(includeList); // หาตัวที่ลบออก

            ICollection<ArtifactAttachment> ArtifactAttachmentList = new HashSet<ArtifactAttachment>();

            foreach (var docId in documentId)
            {
                if (includeList.Contains(docId))
                {
                    ArtifactAttachmentList.Add(_afn.Where(l => l.DocumentId == docId).First());
                    continue;
                }
                var _document = _documentRepository.GetSingle(d => d.Id == docId, d => d.File);

                var artifactAttachment = new ArtifactAttachment(artifactId, docId, _document.File.OwnerId);
                _artifact.Documents.Add(artifactAttachment);

                ArtifactAttachmentList.Add(artifactAttachment);
            }

            foreach (var ecl in excludeList)
            {
                var _ecl = _afn.Where(l => l.DocumentId == ecl).First();

                _AttachmentRepository.Delete(_ecl);
            }

            Save();

            return ArtifactAttachmentList;
        }

        public void CancleArtifact(long id)
        {
            Artifact _artifact = _artifactRepository.GetSingle(a => a.Id == id, a => a.Nodes, a => a.NodeGroups, a => a.Documents);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CancleArtifact() : cannot find artifact id {0} - throw exception.", id);
                throw new ArtifactServiceException("cannot find artifact");
            }

            foreach (var group in _artifact.NodeGroups.ToList())
            {
                // Update NodeGroup or Delete NodeGroup
                DeleteArtifactNodeGroup(group.Id);
            }

            bool hasTaskAssignment = false;

            foreach (var node in _artifact.Nodes.ToList())
            {
                // Check Draft
                hasTaskAssignment |= _artifactNodeRepository.InArtifactNodeHasTaskAssignment(node.Id);
            }

            if (hasTaskAssignment)
            {
                // No Draft >>> Cancle law
                _artifact.Cancle();
                Save();
                return;
            }

            // Delete Draft
            foreach (var doc in _artifact.Documents.ToList())
            {
                _artifactAttachment.Delete(doc);
                Save();
            }
            _artifactRepository.Delete(_artifact);
            Save();
        }

        public ArtifactNodeGroup CreateArtifactNodeGroup(long artifactId, ArtifactNodeGroup artifactNodeGroup)
        {
            var _artifact = _artifactRepository.GetSingle(a => a.Id == artifactId, a => a.NodeGroups);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactNodeGroup() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            int seq = _artifact.NodeGroups.Count + 1;

            artifactNodeGroup.ArtifactId = artifactId;
            artifactNodeGroup.Nodes = new HashSet<ArtifactNode>();
            artifactNodeGroup.Seq = seq;

            //* add by Toey 16/04/2018
            artifactNodeGroup.IsActive = true;

            _artifactNodeGroupRepository.Add(artifactNodeGroup);
            Save();

            return artifactNodeGroup;
        }

        public void DeleteArtifactNodeGroup(long artifactNodeGroupId)
        {
            ArtifactNodeGroup _artifactNodeGroup = _artifactNodeGroupRepository.GetSingle(artifactNodeGroupId);
            if (_artifactNodeGroup == null)
            {
                _logger.LogError("ArtifactService.DeleteArtifactNodeGroup() : cannot find artifact node id {0} - throw exception.", artifactNodeGroupId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            var _artifact = _artifactRepository.GetSingle(_artifactNodeGroup.ArtifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.DeleteArtifactNodeGroup() : cannot find artifact id {0} - throw exception.", _artifactNodeGroup.ArtifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }
            bool hasTaskAssignment = false;

            ICollection<ArtifactNode> Nodes = _artifactNodeRepository.FindBy(o => o.GroupId == artifactNodeGroupId).ToList();

            foreach (var node in Nodes.ToList())
            {
                // Check Draft
                hasTaskAssignment |= _artifactNodeRepository.InArtifactNodeHasTaskAssignment(node.Id);

                // Update Node or Delete Node
                DeleteArtifactNode(node.Id);
            }

            if (hasTaskAssignment)
            {
                // No Draft >>> Update NodeGroup
                _artifactNodeGroup.IsActive = false;
                _artifactNodeGroupRepository.Update(_artifactNodeGroup);
                Save();
                return;
            }

            // Delete Draft
            _artifactNodeGroupRepository.Delete(_artifactNodeGroup);
            Save();
        }

        public ArtifactNode CreateArtifactNode(long artifactId, long artifactNodeGroupId, ArtifactNode artifactNode)
        {
            var _artifact = _artifactRepository.GetSingle(artifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactNode() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }
            var _artifactNodeGroup = _artifactNodeGroupRepository.GetSingle(g => g.Id == artifactNodeGroupId, g => g.Nodes);
            if (_artifactNodeGroup == null || _artifactNodeGroup.ArtifactId != artifactId)
            {
                _logger.LogError("ArtifactService.CreateArtifactNode() : cannot find artifact node group id {0} or node group not belong to artifact id {1} - throw exception.", artifactNodeGroupId, artifactId);
                throw new ArtifactServiceException("cannot find artifact node group");
            }

            int seq = _artifactNodeGroup.Nodes.Count + 1;

            if (artifactNode.FollowUpPeriodMonth == 0 && artifactNode.FollowUpPeriodYear == 0)
            {
                artifactNode.FollowUpPeriodMonth = _config.DefaultFollowUpPeriod;
            }
            artifactNode.ArtifactId = artifactId;
            artifactNode.GroupId = artifactNodeGroupId;
            artifactNode.Seq = seq;
            artifactNode.Departments = new HashSet<ArtifactAssignment>();

            //* add by Toey 16/04/2018
            artifactNode.IsActive = true;

            _artifactNodeRepository.Add(artifactNode);
            Save();

            return artifactNode;
        }

        public void DeleteArtifactNode(long artifactNodeId)
        {
            ArtifactNode _artifactNode = _artifactNodeRepository.GetSingle(artifactNodeId);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.DeleteArtifactNode() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            var _artifact = _artifactRepository.GetSingle(_artifactNode.ArtifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.DeleteArtifactNode() : cannot find artifact id {0} - throw exception.", _artifactNode.ArtifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            if (_artifactNodeRepository.InArtifactNodeHasTaskAssignment(artifactNodeId))
            {
                // No Draft >>> Update Node & TaskAssignment

                ICollection<TaskAssignment> taskAssignment = _taskAssignmentRepository.FindBy(a => a.ArtifactId == _artifactNode.ArtifactId && a.ArtifactNodeId == artifactNodeId).ToList();
                foreach (var TAss in taskAssignment.ToList())
                {
                    TAss.IsActive = false;
                    _taskAssignmentRepository.Update(TAss);
                    Save();
                }
                _artifactNode.IsActive = false;
                _artifactNodeRepository.Update(_artifactNode);
                Save();
                return;
            }

            // Delete Draft
            _artifactNodeRepository.DeleteArtifactAssignment(artifactNodeId);
            _artifactNodeRepository.DeleteRequiredLicense(artifactNodeId);
            _artifactNodeRepository.Delete(_artifactNode);
            Save();
        }

        public IEnumerable<ArtifactNode> CreateArtifactNodes(long artifactId, long artifactNodeGroupId, ICollection<ArtifactNode> artifactNodes)
        {
            var _artifact = _artifactRepository.GetSingle(artifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactNodes() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }
            var _artifactNodeGroup = _artifactNodeGroupRepository.GetSingle(artifactNodeGroupId);
            if (_artifactNodeGroup == null || _artifactNodeGroup.ArtifactId != artifactId)
            {
                _logger.LogError("ArtifactService.CreateArtifactNodes() : cannot find artifact node group id {0} or node group not belong to artifact id {1} - throw exception.", artifactNodeGroupId, artifactId);
                throw new ArtifactServiceException("cannot find artifact node group");
            }

            foreach (var artifactNode in artifactNodes)
            {
                artifactNode.ArtifactId = artifactId;
                artifactNode.GroupId = artifactNodeGroupId;
                _artifactNodeRepository.Add(artifactNode);
            }
            Save();

            return artifactNodes;
        }

        public IEnumerable<ArtifactNodeLicenseType> CreateArtifactNodeRequireLicenseType(long artifactNodeId, ICollection<long> licenseTypeIds)
        {
            var _artifactNode = _artifactNodeRepository.GetSingle(artifactNodeId);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactNodeRequireLicenseType() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact node");
            }
            ICollection<ArtifactNodeLicenseType> licenseTypeList = new HashSet<ArtifactNodeLicenseType>();
            foreach (var licenseTypeId in licenseTypeIds)
            {
                var _license = _licenseTypeRepository.GetSingle(licenseTypeId);
                if (_license == null)
                {
                    _logger.LogError("ArtifactService.CreateArtifactNodeRequireLicenseType() : cannot find license type id {0} - throw exception.", licenseTypeId);
                    throw new ArtifactServiceException("cannot find lecense type");
                }
                ArtifactNodeLicenseType artifactNodeLicenseType = new ArtifactNodeLicenseType
                {
                    ArtifactNodeId = artifactNodeId,
                    LicenseTypeId = licenseTypeId
                };
                _artifactNode.RequiredLicenses.Add(artifactNodeLicenseType);
                licenseTypeList.Add(artifactNodeLicenseType);
            }
            Save();

            return licenseTypeList;
        }

        public ArtifactAssignment CreateArtifactAssignment(long artifactNodeId, long departmentId)
        {
            var _artifactNode = _artifactNodeRepository.GetSingle(artifactNodeId);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactAssignment() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact node");
            }

            var _department = _departmentRepository.GetSingle(departmentId);
            if (_department != null && _department.IsActive)
            {
                ArtifactAssignment _assignment = new ArtifactAssignment
                {
                    DepartmentId = _department.Id,
                    ArtifactId = _artifactNode.ArtifactId,
                    ArtifactNodeId = _artifactNode.Id,
                    AssignDate = DateTime.Now
                };
                _artifactAssignmentRepository.Add(_assignment);
                Save();

                return _assignment;
            }

            return null;
        }

        public IEnumerable<ArtifactAssignment> CreateArtifactAssignments(long artifactNodeId, ICollection<long> departmentIds)
        {
            var _artifactNode = _artifactNodeRepository.GetSingle(artifactNodeId);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactAssignments() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact node");
            }
            ICollection<ArtifactAssignment> artifactAssignmentList = new HashSet<ArtifactAssignment>();
            foreach (var departmentId in departmentIds)
            {
                var _department = _departmentRepository.GetSingle(departmentId);
                if (_department != null && _department.IsActive)
                {
                    ArtifactAssignment assignment = new ArtifactAssignment
                    {
                        DepartmentId = _department.Id,
                        ArtifactId = _artifactNode.ArtifactId,
                        ArtifactNodeId = _artifactNode.Id,
                        AssignDate = DateTime.Now,
                        ValidFrom = DateTime.Now
                    };
                    _artifactAssignmentRepository.Add(assignment);
                    artifactAssignmentList.Add(assignment);
                }
            }
            Save();

            return artifactAssignmentList;
        }

        public IEnumerable<ArtifactAssignment> CreateArtifactAssignmentsAllDepartment(long artifactNodeId)
        {
            var _artifactNode = _artifactNodeRepository.GetSingle(artifactNodeId);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.CreateArtifactAssignments() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact node");
            }

            ICollection<ArtifactAssignment> artifactAssignmentList = new HashSet<ArtifactAssignment>();
            var Departments = _departmentRepository.FindBy(d => d.IsActive == true);
            foreach (var department in Departments)
            {
                ArtifactAssignment assignment = new ArtifactAssignment
                {
                    DepartmentId = department.Id,
                    ArtifactId = _artifactNode.ArtifactId,
                    ArtifactNodeId = _artifactNode.Id,
                    AssignDate = DateTime.Now,
                    ValidFrom = DateTime.Now
                };
                _artifactAssignmentRepository.Add(assignment);
                artifactAssignmentList.Add(assignment);
            }
            _artifactNode.IsAllDepartment = true;
            _artifactNodeRepository.Update(_artifactNode);

            Save();

            return artifactAssignmentList;
        }

        public void CancleArtifactAssignment(long artifactNodeId, long departmentId)
        {
            var _artifactAssignments = _artifactAssignmentRepository.FindBy(
                aa => aa.ArtifactNodeId == artifactNodeId &&
                    aa.DepartmentId == departmentId).ToList();

            if (_artifactAssignments.Count == 0)
            {
                _logger.LogWarning("ArtifactService.CancleArtifactAssignment() : cannot find artifact assignment artifactNodeId {0} and departmentNodeId {1}", artifactNodeId, artifactNodeId);
                // throw new ArtifactServiceException("cannot find artifact assignment");
                return;
            }
            foreach (var artifactAssignment in _artifactAssignments)
            {
                if (!artifactAssignment.IsActive) { continue; }
                artifactAssignment.IsActive = false;
                artifactAssignment.ValidTo = DateTime.Now;
            }
            Save();
        }

        public IEnumerable<TaskAssignment> CreateTaskAssignmentFromArtifactAssignment(long artifactId, DateTime dueDate)
        {
            Artifact _artifact = _artifactRepository.GetAllArtifactNodes(artifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CreateTaskAssignmentFromArtifactAssignment() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }
            DateTime _dueDate = dueDate;
            if (dueDate <= DateTime.Today)
            {
                _dueDate = DateTime.Today.AddDays(7);
            }
            else if (dueDate > DateTime.Today.AddDays(20))
            {
                _dueDate = DateTime.Today.AddDays(20);
            }
            var currentTaskAssignment = _taskAssignmentRepository.FindBy(t => t.ArtifactId == artifactId && t.Type == TaskAssignmentType.Initial);
            if (currentTaskAssignment.Count() > 0)
            {
                // TODO handle case that partially assign
                _logger.LogError("ArtifactService.CreateTaskAssignmentFromArtifactAssignment() : cannot re assign artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot reassign artifact");
            }
            var taskAssignmentList = _taskAssignmentRepository.CreateTaskAssigment(_artifact, _dueDate);
            Save();

            foreach (var task in taskAssignmentList)
            {
                task.Assign();
                _taskAssignmentRepository.Update(task);
            }
            _artifact.Assign();
            Save();

            var tasks = _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifactId);
            // Email Handling
            if (_config.EnableSendMail)
            {
                IEnumerable<TaskAssignmentSummary> summary = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                _notificationService.PushNotificationsFromTaskAssignmentSummary(summary);
                _logger.LogInformation("ArtifactService.CreateTaskAssignmentFromArtifactAssignment(): submitted {0} email notifications of artifact id {1}", summary.Count(), artifactId);
            }
            Save();

            return tasks;
        }

        public int CreateTaskAssignmentFromInitialArtifactAssignment(string token, long artifactId)
        {
            int count = 0;
            if (token != _config.Token)
            {
                return 0;
            }

            _logger.LogCritical("ArtifactService -  CreateTaskAssignmentFromArtifactAssignment(): enable create task with token");

            DateTime initialDate = DateTime.Parse(_config.InitialDate);

            _logger.LogCritical("ArtifactService -  CreateTaskAssignmentFromArtifactAssignment(): initial date is {0}", initialDate.ToString());

            Artifact _artifact = _artifactRepository.GetAllArtifactNodes(artifactId);

            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.CreateTaskAssignmentFromArtifactAssignment() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            if (!_artifact.IsManualImport())
            {
                return 0;
            }

            var currentTaskAssignment = _taskAssignmentRepository.FindBy(t => t.ArtifactId == artifactId && t.Type == TaskAssignmentType.Initial);
            if (currentTaskAssignment.Count() > 0)
            {
                // TODO handle case that partially assign
                _logger.LogError("ArtifactService.CreateTaskAssignmentFromArtifactAssignment() : cannot re assign artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot reassign artifact");
            }

            var taskAssignmentList = _taskAssignmentRepository.CreateTaskAssigment(_artifact, initialDate);
            // Make sure save before move to next operation
            Save();

            foreach (var task in taskAssignmentList)
            {
                task.Assign();
                _taskAssignmentRepository.Update(task);
                count++;
            }
            // _artifact.Assign();
            Save();

            var tasks = _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifactId);
            // Email Handling
            if (_config.EnableSendMail)
            {
                IEnumerable<TaskAssignmentSummary> summary = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                _notificationService.PushNotificationsFromTaskAssignmentSummary(summary);
                _logger.LogInformation("ArtifactService.CreateTaskAssignmentFromArtifactAssignment(): submitted {0} email notifications of artifact id {1}", summary.Count(), artifactId);
            }
            Save();


            return count;
        }

        [System.Obsolete("This method is deprecated, please use CreateArtifact(Artifact artifact) instead")]
        public Artifact CreateArtifact(ArtifactServiceModel artifactServiceModel, Artifact artifact)
        {
            Artifact _artifact = artifact;
            // Artifact _artifact = new Artifact();
            /*
            _artifact.CategoryId = artifact.CategoryId;
            _artifact.CreateDate = artifact.CreateDate;
            _artifact.EffectiveDate = artifact.EffectiveDate;
            _artifact.Introduction = _artifact.Introduction;
            _artifact.CreateDate = DateTime.Now;
            */
            ArtifactSource source = _artifactSourceRepository.FindBy(s => s.Name == artifactServiceModel.Source.Trim()).FirstOrDefault();
            if (source != null)
            {
                _artifact.Source = source;
                _artifact.SourceId = source.Id;
            }
            else
            {
                ArtifactSource _source = new ArtifactSource();
                _source.Name = artifactServiceModel.Source.Trim();
                _artifactSourceRepository.Add(_source);
                _artifactSourceRepository.Commit();

                _artifact.Source = _source;
                _artifact.SourceId = _source.Id;
            }

            if (artifactServiceModel.Documents != null)
            {
                _artifact.Documents = new HashSet<ArtifactAttachment>();
                foreach (var document in artifactServiceModel.Documents)
                {
                    /*
                    var _document = _documentRepository.GetSingle(document.Id);
                    if (_document != null)
                    {
                        var att = new ArtifactAttachment(_artifact.)
                        _artifact.Documents.Add(_document);
                    }
                    */
                }
            }
            var _tempNodeGroups = _artifact.NodeGroups;

            _artifact.NodeGroups = new HashSet<ArtifactNodeGroup>();
            _artifact.Nodes = new HashSet<ArtifactNode>();

            _artifactRepository.Add(_artifact);
            _artifactRepository.Commit();

            for (int i = 0; i < _tempNodeGroups.Count; i++)
            {
                var _group = _tempNodeGroups.ElementAt<ArtifactNodeGroup>(i);

                var _tempNodes = _group.Nodes;

                _group.Nodes = new HashSet<ArtifactNode>();

                _group.ArtifactId = _artifact.Id;
                _artifact.NodeGroups.Add(_group);
                _artifactNodeGroupRepository.Add(_group);
                _artifactNodeGroupRepository.Commit();

                for (int j = 0; j < _tempNodes.Count; j++)
                {
                    var _node = _tempNodes.ElementAt<ArtifactNode>(j);
                    if (_node.IsRequiredLicense)
                    {
                        var reqLicense = artifactServiceModel.NodeGroups.ElementAt<ArtifactNodeGroupServiceModel>(i).Nodes.ElementAt<ArtifactNodeServiceModel>(j).RequiredLicenses;
                        foreach (var license in reqLicense)
                        {
                            var _license = _licenseTypeRepository.GetSingle(license.Id);
                            if (_license != null)
                            {
                                ArtifactNodeLicenseType artifactNodeLicenseType = new ArtifactNodeLicenseType { ArtifactNodeId = _node.Id, LicenseTypeId = _license.Id };
                                _node.RequiredLicenses.Add(artifactNodeLicenseType);

                            }
                        }
                    }

                    _node.Departments = new HashSet<ArtifactAssignment>();

                    var departments = artifactServiceModel.NodeGroups.ElementAt<ArtifactNodeGroupServiceModel>(i).Nodes.ElementAt<ArtifactNodeServiceModel>(j).Departments;

                    if (_node.IsAllDepartment && departments.Count == 1 && (departments.OfType<DepartmentAssignServiceModel>().First().Id == 0))
                    {
                        var Departments = _departmentRepository.FindBy(d => d.IsActive == true);
                        foreach (var department in Departments)
                        {
                            ArtifactAssignment assignment = new ArtifactAssignment
                            {
                                DepartmentId = department.Id,
                                ArtifactId = _artifact.Id,
                                ArtifactNodeId = _node.Id,
                                AssignDate = DateTime.Now
                            };

                            _node.Departments.Add(assignment);
                        }
                        // Set artifact node IsAllDepartment flag to true
                        _node.IsAllDepartment = true;
                    }
                    else
                    {
                        foreach (var department in departments)
                        {
                            var _department = _departmentRepository.GetSingle(department.Id);
                            if (_department != null)
                            {
                                ArtifactAssignment assignment = new ArtifactAssignment
                                {
                                    DepartmentId = _department.Id,
                                    ArtifactId = _artifact.Id,
                                    ArtifactNodeId = _node.Id,
                                    AssignDate = DateTime.Now
                                };

                                _node.Departments.Add(assignment);

                            }
                        }
                    }

                    _node.ArtifactId = _artifact.Id;
                    _node.GroupId = _group.Id;

                    _artifactNodeRepository.Add(_node);
                    _artifactNodeRepository.Commit();
                }

            }
            _artifactRepository.Update(_artifact);
            _artifactRepository.Commit();
            _artifact.Code = String.Format("{0:00} {1:00}/{2}", _artifact.CategoryId, _artifact.Id, (DateTime.Today.Year + 543) % 2500);
            _artifactRepository.Update(_artifact);
            _artifactRepository.Commit();

            return _artifact;
        }

        public Artifact GetArtifact(long id)
        {
            return _artifactRepository.GetFullDetail(id);
        }

        public IEnumerable<Artifact> GetArtifacts()
        {
            //TODO enable this after add delete and clean db
            // return _artifactRepository.AllIncluding(a => a.IsActive, a => a.Category, a => a.Level, a => a.Source);
            return _artifactRepository.AllIncluding(a => a.Category, a => a.Level, a => a.Source);
        }

        public IEnumerable<ArtifactSummary> GetArtifactSummary(ArtifactStatus status)
        {
            switch (status)
            {
                case ArtifactStatus.Inprogress:
                    return GetArtifactByStatusFromTaskAssignment((Artifact.InProgressFlag | ArtifactStatus.Approved));
                case ArtifactStatus.Approved:
                case ArtifactStatus.Canceled:
                    return GetArtifactByStatusFromTaskAssignment(status);
                case ArtifactStatus.Created:
                case ArtifactStatus.Draft:
                    return GetArtifactByStatusFromArtifactAssignment(status);
                default:
                    return GetAll();
            }
        }


        public IEnumerable<ArtifactSummary> GetArtifactSummary(long departmentId, ArtifactStatus status)
        {
            switch (status)
            {
                case ArtifactStatus.Inprogress:
                    return GetArtifactByStatusFromTaskAssignment((Artifact.InProgressFlag | ArtifactStatus.Approved));
                case ArtifactStatus.Approved:
                case ArtifactStatus.Canceled:
                    return GetArtifactByStatusFromTaskAssignment(status);
                case ArtifactStatus.Created:
                case ArtifactStatus.Draft:
                    return GetArtifactByStatusFromArtifactAssignment(status);
                default:
                    return GetAll(departmentId);
            }
        }

        private IEnumerable<ArtifactSummary> GetArtifactByStatusFromTaskAssignment(ArtifactStatus status)
        {
            IEnumerable<Artifact> artifacts = _artifactRepository.GetAllByStatus(status);
            ICollection<ArtifactSummary> artifactSummaryList = new HashSet<ArtifactSummary>();

            foreach (var artifact in artifacts)
            {
                var _artifactSummary = new ArtifactSummary(artifact);
                IEnumerable<TaskAssignment> tasks = _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifact.Id);
                IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                foreach (var summary in summaryList)
                {
                    var departmentSummary = new DepartmentSummary
                    {
                        Id = summary.DepartmentId,
                        Name = summary.Department.Name,
                        AliasName = summary.Department.AliasName,
                        Status = (ArtifactStatus)summary.Status
                    };
                    _artifactSummary.Departments.Add(departmentSummary);
                }
                artifactSummaryList.Add(_artifactSummary);
            }

            return artifactSummaryList;
        }

        private IEnumerable<ArtifactSummary> GetArtifactByStatusFromArtifactAssignment(ArtifactStatus status)
        {
            IEnumerable<Artifact> artifacts = _artifactRepository.GetAllByStatus(status);
            ICollection<ArtifactSummary> artifactSummaryList = new HashSet<ArtifactSummary>();

            foreach (var artifact in artifacts)
            {
                var _artifactSummary = new ArtifactSummary(artifact);
                var _departments = _departmentRepository.GetDepartmentByArtifactId(artifact.Id);
                foreach (var department in _departments)
                {
                    var departmentSummary = new DepartmentSummary
                    {
                        Id = department.Id,
                        Name = department.Name,
                        AliasName = department.AliasName,
                        Status = status
                    };
                    _artifactSummary.Departments.Add(departmentSummary);
                }
                artifactSummaryList.Add(_artifactSummary);
            }

            return artifactSummaryList;
        }

        private IEnumerable<ArtifactSummary> GetAll()
        {
            #region BeforePerfTunning
            /*
             * This block of code is previous version
             * We found this block quite slow
             * After change to utilise ParallelFor compute time reduce from 10000 to 100.
             * 
             * 
            IEnumerable<long> artifactIds = _artifactRepository.GetAll().Select(a => a.Id).ToArray();
            IEnumerable<Artifact> artifacts = _artifactRepository.GetArtifactsFromArtifactAssignment();
            IEnumerable<Artifact> excludeList = new IEnumerable<Artifact>();
            ICollection<ArtifactSummary> artifactSummaryList = new HashSet<ArtifactSummary>();
            foreach (var artifactId in artifactIds)
            {
                var _artifact = _artifactRepository.GetSingle(a => a.Id == artifactId, a => a.Source, a => a.Category, a => a.Level);
                var _artifactSummary = new ArtifactSummary(_artifact);
                var _departments = _departmentRepository.GetDepartmentByArtifactId(artifactId);
                foreach (var department in _departments)
                {
                    var departmentSummary = new DepartmentSummary {
                        Id = department.Id,
                        Name = department.Name,
                        AliasName = department.AliasName,
                        Status = _artifact.Status
                    };
                    _artifactSummary.Departments.Add(departmentSummary);
                }
                artifactSummaryList.Add(_artifactSummary);
            }
            */
            #endregion

            IEnumerable<Artifact> artifacts = _artifactRepository.GetAllArtifacts();
            ConcurrentStack<Artifact> excludeList = new ConcurrentStack<Artifact>();
            ConcurrentBag<ArtifactSummary> artifactSummaryList = new ConcurrentBag<ArtifactSummary>();
            Parallel.ForEach(artifacts,
                _artifact =>
                {
                    if (((_artifact.Status != ArtifactStatus.Created && _artifact.Status != ArtifactStatus.Draft))
                     || (_artifact.Status == ArtifactStatus.Approved && _artifact.IsManualImport())
                    )
                    {
                        excludeList.Push(_artifact);
                        return;
                    }
                    var _artifactSummary = new ArtifactSummary(_artifact);
                    _artifactSummary.TaskAssignmentType = TaskAssignmentType.Initial;
                    var bag = new HashSet<long>();

                    foreach (var node in _artifact.Nodes)
                    {
                        if (node.IsAllDepartment)
                        {
                            var departmentSummary = new DepartmentSummary
                            {
                                Id = 0,
                                // TODO add configuration here 
                                Name = "ทุกหน่วยงาน",
                                AliasName = "ทุกหน่วยงาน",
                                Status = _artifact.Status
                            };
                            _artifactSummary.Departments = new HashSet<DepartmentSummary>();
                            _artifactSummary.Departments.Add(departmentSummary);

                            //artifactSummaryList.Add(_artifactSummary);
                            break;
                        }

                        foreach (var asignment in node.Departments)
                        {
                            if (bag.Contains(asignment.Department.Id))
                            {
                                continue;
                            }

                            var departmentSummary = new DepartmentSummary
                            {
                                Id = asignment.Department.Id,
                                Name = asignment.Department.Name,
                                AliasName = asignment.Department.AliasName,
                                Status = _artifact.Status
                            };
                            _artifactSummary.Departments.Add(departmentSummary);
                            bag.Add(asignment.Department.Id);
                        }
                    }
                    artifactSummaryList.Add(_artifactSummary);
                });

            ConcurrentBag<TaskAssignment> initialTaskAssignments = new ConcurrentBag<TaskAssignment>(_taskAssignmentRepository.GetTaskAssignments(TaskAssignmentType.Initial));
            Parallel.ForEach(excludeList,
                _artifact =>
                {
                    var _artifactSummary = new ArtifactSummary(_artifact);
                    _artifactSummary.TaskAssignmentType = TaskAssignmentType.Initial;
                    //var tasks = initialTaskAssignments.Where(t => t.ArtifactId == _artifact.Id).ToList();

                    //* Edit by Toey 15/04/2018
                    var tasks = initialTaskAssignments.Where(t => t.ArtifactId == _artifact.Id && t.IsActive == true).ToList();
                    if (tasks.Count == 0)
                    {
                        return;
                    }
                    //*
                    IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                    foreach (var summary in summaryList)
                    {
                        var departmentSummary = new DepartmentSummary
                        {
                            Id = summary.DepartmentId,
                            Name = summary.Department.Name,
                            AliasName = summary.Department.AliasName,
                            Status = (ArtifactStatus)summary.Status
                        };
                        _artifactSummary.Departments.Add(departmentSummary);

                        if (_artifactSummary.Departments.Count > 1)
                        {
                            _artifactSummary.TaskAssignmentStatus = SummarizeTaskStatus(_artifactSummary.TaskAssignmentStatus, (TaskAssignmentStatus)summary.Status);
                        }
                        else
                        {
                            _artifactSummary.TaskAssignmentStatus = (TaskAssignmentStatus)summary.Status;
                        }
                    }
                    artifactSummaryList.Add(_artifactSummary);
                });

            ConcurrentBag<TaskAssignment> followupTaskAssignments = new ConcurrentBag<TaskAssignment>(_taskAssignmentRepository.GetTaskAssignments(TaskAssignmentType.FollowUp));
            Parallel.ForEach(excludeList,
                _artifact =>
                {
                    var _artifactSummary = new ArtifactSummary(_artifact);
                    _artifactSummary.TaskAssignmentType = TaskAssignmentType.FollowUp;
                    //var tasks = followupTaskAssignments.Where(t => t.ArtifactId == _artifact.Id).ToList();

                    //* Edit by Toey 15/04/2018
                    var tasks = followupTaskAssignments.Where(t => t.ArtifactId == _artifact.Id && t.IsActive == true).ToList();
                    if (tasks.Count == 0)
                    {
                        return;
                    }
                    //*
                    IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                    if (summaryList.Count() > 0)
                    {
                        _artifactSummary.DueDate = summaryList.First().DueDate;
                    }
                    foreach (var summary in summaryList)
                    {
                        var departmentSummary = new DepartmentSummary
                        {
                            Id = summary.DepartmentId,
                            Name = summary.Department.Name,
                            AliasName = summary.Department.AliasName,
                            Status = (ArtifactStatus)summary.Status
                        };
                        _artifactSummary.Departments.Add(departmentSummary);

                        if (_artifactSummary.DueDate.CompareTo(summary.DueDate) > 0)
                        {
                            _artifactSummary.DueDate = summary.DueDate;
                        }
                        //* create by Toey 02/04/2018
                        if (_artifactSummary.Departments.Count > 1)
                        {
                            _artifactSummary.TaskAssignmentStatus = SummarizeTaskStatus(_artifactSummary.TaskAssignmentStatus, (TaskAssignmentStatus)summary.Status);
                        }
                        else
                        {
                            _artifactSummary.TaskAssignmentStatus = (TaskAssignmentStatus)summary.Status;
                        }
                        //*
                    }
                    artifactSummaryList.Add(_artifactSummary);
                });

            artifactSummaryList.OrderBy(a => a.Id);
            _logger.LogInformation("Number of return ArtifactSummaryNode = {0}", artifactSummaryList.Count);

            return artifactSummaryList;
        }

        private static TaskAssignmentStatus SummarizeTaskStatus(TaskAssignmentStatus a, TaskAssignmentStatus b)
        {
            var c = Math.Max((int)a, (int)b);
            return (TaskAssignmentStatus)c;
        }

        private IEnumerable<ArtifactSummary> GetAll(long departmentId)
        {
            IEnumerable<Artifact> artifacts = _artifactRepository.GetAllArtifacts();
            ConcurrentStack<Artifact> excludeList = new ConcurrentStack<Artifact>();
            ConcurrentBag<ArtifactSummary> artifactSummaryList = new ConcurrentBag<ArtifactSummary>();
            Parallel.ForEach(artifacts,
                _artifact =>
                {
                    if (_artifact.Status != ArtifactStatus.Created && _artifact.Status != ArtifactStatus.Draft
                    || (_artifact.Status == ArtifactStatus.Approved && _artifact.IsManualImport()) )
                    {
                        excludeList.Push(_artifact);
                        return;
                    }
                    var _artifactSummary = new ArtifactSummary(_artifact);
                    _artifactSummary.TaskAssignmentType = TaskAssignmentType.Initial;
                    var bag = new HashSet<long>();

                    foreach (var node in _artifact.Nodes)
                    {
                        if (node.Departments.Where(n => n.DepartmentId == departmentId).ToList().Count() == 0)
                        {
                            // if not match department then filter it out
                            continue;
                        }

                        if (node.IsAllDepartment)
                        {
                            var departmentSummary = new DepartmentSummary
                            {
                                Id = 0,
                                // TODO add configuration here 
                                Name = "ทุกหน่วยงาน",
                                AliasName = "ทุกหน่วยงาน",
                                Status = _artifact.Status
                            };
                            _artifactSummary.Departments = new HashSet<DepartmentSummary>();
                            _artifactSummary.Departments.Add(departmentSummary);
                            artifactSummaryList.Add(_artifactSummary);

                            break;
                        }

                        foreach (var asignment in node.Departments)
                        {
                            if (bag.Contains(asignment.Department.Id))
                            {
                                continue;
                            }

                            var departmentSummary = new DepartmentSummary
                            {
                                Id = asignment.Department.Id,
                                Name = asignment.Department.Name,
                                AliasName = asignment.Department.AliasName,
                                Status = _artifact.Status
                            };
                            _artifactSummary.Departments.Add(departmentSummary);
                            bag.Add(asignment.Department.Id);
                        }
                    }
                    artifactSummaryList.Add(_artifactSummary);
                });

            ConcurrentBag<TaskAssignment> initialTaskAssignments = new ConcurrentBag<TaskAssignment>(_taskAssignmentRepository.GetTaskAssignments(TaskAssignmentType.Initial));
            Parallel.ForEach(excludeList,
                _artifact =>
                {
                    var _artifactSummary = new ArtifactSummary(_artifact);
                    _artifactSummary.TaskAssignmentType = TaskAssignmentType.Initial;
                    //var tasks = initialTaskAssignments.Where(t => t.ArtifactId == _artifact.Id).ToList();

                    //* Edit by Toey 15/04/2018
                    var tasks = initialTaskAssignments.Where(t => t.ArtifactId == _artifact.Id && t.IsActive == true).ToList();

                    if (!tasks.Select(t => t.DepartmentId).ToList().Any(t => t == departmentId))
                    {
                        return;
                    }
                    IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                    foreach (var summary in summaryList)
                    {
                        var departmentSummary = new DepartmentSummary
                        {
                            Id = summary.DepartmentId,
                            Name = summary.Department.Name,
                            AliasName = summary.Department.AliasName,
                            Status = (ArtifactStatus)summary.Status
                        };
                        _artifactSummary.Departments.Add(departmentSummary);
                        
                        if (_artifactSummary.Departments.Count > 1)
                        {
                            _artifactSummary.TaskAssignmentStatus = SummarizeTaskStatus(_artifactSummary.TaskAssignmentStatus, (TaskAssignmentStatus)summary.Status);
                        }
                        else
                        {
                            _artifactSummary.TaskAssignmentStatus = (TaskAssignmentStatus)summary.Status;
                        }
                        
                    }
                    artifactSummaryList.Add(_artifactSummary);
                });

            ConcurrentBag<TaskAssignment> followupTaskAssignments = new ConcurrentBag<TaskAssignment>(_taskAssignmentRepository.GetTaskAssignments(TaskAssignmentType.FollowUp));
            Parallel.ForEach(excludeList,
                _artifact =>
                {
                    var _artifactSummary = new ArtifactSummary(_artifact);
                    _artifactSummary.TaskAssignmentType = TaskAssignmentType.FollowUp;
                    //var tasks = followupTaskAssignments.Where(t => t.ArtifactId == _artifact.Id).ToList();

                    //* Edit by Toey 15/04/2018
                    var tasks = followupTaskAssignments.Where(t => t.ArtifactId == _artifact.Id && t.IsActive == true).ToList();

                    if (!tasks.Select(t => t.DepartmentId).ToList().Any(t => t == departmentId))
                    {
                        return;
                    }
                    IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                    if (summaryList.Count() > 0)
                    {
                        _artifactSummary.DueDate = summaryList.First().DueDate;
                    }

                    foreach (var summary in summaryList)
                    {
                        var departmentSummary = new DepartmentSummary
                        {
                            Id = summary.DepartmentId,
                            Name = summary.Department.Name,
                            AliasName = summary.Department.AliasName,
                            Status = (ArtifactStatus)summary.Status
                        };
                        _artifactSummary.Departments.Add(departmentSummary);

                        if (_artifactSummary.DueDate.CompareTo(summary.DueDate) > 0)
                        {
                            _artifactSummary.DueDate = summary.DueDate;
                        }

                        if (_artifactSummary.Departments.Count > 1)
                        {
                            _artifactSummary.TaskAssignmentStatus = SummarizeTaskStatus(_artifactSummary.TaskAssignmentStatus, (TaskAssignmentStatus)summary.Status);
                        }
                        else
                        {
                            _artifactSummary.TaskAssignmentStatus = (TaskAssignmentStatus)summary.Status;
                        }
                        
                    }
                    // comment out and add logic to calculate correct taskAssignmentStatus
                    //* create by Toey 02/04/2018
                    //_artifactSummary.TaskAssignmentStatus = summaryList.Where(s => s.DepartmentId == departmentId).Select(a => a.Status).FirstOrDefault();
                    //*
                    artifactSummaryList.Add(_artifactSummary);
                });

            var returnList = artifactSummaryList.OrderBy(a => a.Id);
            _logger.LogInformation("Number of return ArtifactSummaryNode = {0}", artifactSummaryList.Count);

            return returnList;
        }

        public Artifact UpdateArtifact(long artifactId, Artifact artifact)
        {
            var _artifact = _artifactRepository.GetSingle(artifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.UpdateArtifact() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            _artifact.Update(artifact);
            if (artifact.Status == ArtifactStatus.Created)
            {
                _artifact.Create();
            }

            Save();

            return _artifact;
        }

        public void DeleteArtifactAttachment(long artifactId, long documentId)
        {
            if (artifactId != 0)
            {
                var _artifact = _artifactRepository.GetSingle(artifactId);
                if (_artifact == null)
                {
                    _logger.LogError("ArtifactService.RemoveArtifactDocument() : cannot find artifact id {0} - throw exception.", artifactId);
                    throw new ArtifactServiceException("cannot find artifact");
                }

                var _document = _documentRepository.GetSingle(d => d.Id == documentId, d => d.File);
                if (_document == null)
                {
                    _logger.LogError("ArtifactService.RemoveArtifactDocument() : cannot find document id {0} - throw exception.", documentId);
                    throw new ArtifactServiceException("cannot find document");
                }

                var attachment = _artifact.Documents.Where(a => a.DocumentId == documentId).FirstOrDefault();
                _artifact.Documents.Remove(attachment);
                Save();
            }
            else
            {
                var _document = _documentRepository.GetSingle(d => d.Id == documentId, d => d.File);
                if (_document == null)
                {
                    _logger.LogError("ArtifactService.RemoveArtifactDocument() : cannot find document id {0} - throw exception.", documentId);
                    throw new ArtifactServiceException("cannot find document");
                }
                _documentRepository.Delete(_document);

                Save();
            }

        }

        public ArtifactNodeGroup UpdateArtifactNodeGroup(long artifactNodeGroupId, ArtifactNodeGroup artifactNodeGroup)
        {
            var _artifact = _artifactRepository.GetSingle(artifactNodeGroup.ArtifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.UpdateArtifactNodeGroup() : cannot find artifact id {0} - throw exception.", artifactNodeGroup.ArtifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }
            var _artifactNodeGroup = _artifactNodeGroupRepository.GetSingle(artifactNodeGroupId);
            _artifactNodeGroup.Update(artifactNodeGroup);

            Save();

            return _artifactNodeGroup;
        }

        public ArtifactNode UpdateArtifactNode(long artifactNodeId, ArtifactNode artifactNodes)
        {
            var _artifact = _artifactRepository.GetSingle(artifactNodes.ArtifactId);
            if (_artifact == null)
            {
                _logger.LogError("ArtifactService.UpdateArtifactNode() : cannot find artifact id {0} - throw exception.", artifactNodes.ArtifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            var _artifactNode = _artifactNodeRepository.GetSingle(artifactNodeId);
            _artifactNode.Update(artifactNodes);

            Save();

            return _artifactNode;
        }

        public IEnumerable<ArtifactNodeLicenseType> UpdateArtifactNodeRequireLicenseType(long artifactNodeId, ICollection<long> licenseTypeIds)
        {
            var _artifactNode = _artifactNodeRepository.GetSingle(n => n.Id == artifactNodeId, n => n.RequiredLicenses);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.UpdateArtifactNodeRequireLicenseType() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact node");
            }

            //var previousLicenseType = _artifactNode.RequiredLicenses.Select(l => l.Id).ToList();

            //* Edit by Toey 17/03/2018
            ICollection<ArtifactNodeLicenseType> _afn = _LicenseTypeRepository.FindBy(a => a.ArtifactNodeId == artifactNodeId).ToList();

            var previousLicenseType = _afn.Select(l => l.LicenseTypeId).ToList();
            //*
            var includeList = previousLicenseType.Intersect(licenseTypeIds);

            var excludeList = previousLicenseType.Except(includeList);


            ICollection<ArtifactNodeLicenseType> licenseTypeList = new HashSet<ArtifactNodeLicenseType>();
            foreach (var licenseTypeId in licenseTypeIds)
            {
                var _license = _licenseTypeRepository.GetSingle(licenseTypeId);

                if (_license == null)
                {
                    _logger.LogError("ArtifactService.UpdateArtifactNodeRequireLicenseType() : cannot find license type id {0} - throw exception.", licenseTypeId);
                    throw new ArtifactServiceException("cannot find lecense type");
                }

                if (includeList.Contains(licenseTypeId))
                {
                    //licenseTypeList.Add(_artifactNode.RequiredLicenses.Where(l => l.Id == licenseTypeId).First());

                    //* Edit by Toey 17/03/2018
                    licenseTypeList.Add(_artifactNode.RequiredLicenses.Where(l => l.LicenseTypeId == licenseTypeId).First());
                    //*
                    continue;
                }

                ArtifactNodeLicenseType artifactNodeLicenseType = new ArtifactNodeLicenseType
                {
                    ArtifactNodeId = artifactNodeId,
                    LicenseTypeId = licenseTypeId
                };
                _artifactNode.RequiredLicenses.Add(artifactNodeLicenseType);
                licenseTypeList.Add(artifactNodeLicenseType);
            }

            foreach (var licenseTypeId in excludeList)
            {
                var _license = _licenseTypeRepository.GetSingle(licenseTypeId);
                if (_license == null)
                {
                    _logger.LogError("ArtifactService.UpdateArtifactNodeRequireLicenseType() : cannot find license type id {0} - throw exception.", licenseTypeId);
                    throw new ArtifactServiceException("cannot find lecense type");
                }
                var _licenseTypes = _artifactNode.RequiredLicenses.Where(l => l.LicenseTypeId == licenseTypeId).ToList();
                foreach (var _licenseType in _licenseTypes)
                {
                    //_licenseType.IsActive = false;
                    //_artifactNode.RequiredLicenses.Remove(_licenseType);

                    //* Edit by Toey 17/03/2018
                    _LicenseTypeRepository.Delete(_licenseType);
                    //*
                }
            }
            Save();

            return licenseTypeList;
        }

        public IEnumerable<ArtifactAssignment> UpdateArtifactAssignments(long artifactNodeId, ICollection<long> departmentIds)
        {
            var _artifactNode = _artifactNodeRepository.GetSingle(n => n.Id == artifactNodeId, n => n.Departments);
            if (_artifactNode == null)
            {
                _logger.LogError("ArtifactService.UpdateArtifactAssignments() : cannot find artifact node id {0} - throw exception.", artifactNodeId);
                throw new ArtifactServiceException("cannot find artifact node");
            }

            var previousDepartmentList = _artifactNode.Departments.Select(l => l.Id).ToList();

            var includeList = previousDepartmentList.Intersect(departmentIds);

            var excludeList = previousDepartmentList.Except(includeList);

            // TODO Check whether to handel all department case

            ICollection<ArtifactAssignment> departmentList = new HashSet<ArtifactAssignment>();
            foreach (var departmentId in departmentIds)
            {
                var _department = _departmentRepository.GetSingle(departmentId);

                if (_department == null)
                {
                    _logger.LogError("ArtifactService.UpdateArtifactAssignments() : cannot find department id {0} - throw exception.", departmentId);
                    throw new ArtifactServiceException("cannot find department");
                }
                if (includeList.Contains(departmentId))
                {
                    departmentList.Add(_artifactNode.Departments.Where(l => l.DepartmentId == departmentId).First());
                    continue;
                }
                if (_department != null && _department.IsActive)
                {
                    ArtifactAssignment artifactAssignment = new ArtifactAssignment
                    {
                        //ArtifactNodeId = artifactNodeId,
                        //DepartmentId = departmentId

                        //* edit by Toey 11/03/2018
                        DepartmentId = _department.Id,
                        ArtifactId = _artifactNode.ArtifactId,
                        ArtifactNodeId = _artifactNode.Id,
                        AssignDate = DateTime.Now,
                        ValidFrom = DateTime.Now
                        //*
                    };
                    _artifactNode.Departments.Add(artifactAssignment);
                    departmentList.Add(artifactAssignment);
                }
            }
            Save();

            foreach (var departmentId in excludeList)
            {
                //var _department = _departmentRepository.GetSingle(departmentId);

                //* edit by Toey 11/03/2018
                var _department = _artifactAssignmentRepository.GetSingle(departmentId);
                //*
                if (_department == null)
                {
                    _logger.LogError("ArtifactService.UpdateArtifactAssignments() : cannot find department id {0} - throw exception.", departmentId);
                    throw new ArtifactServiceException("cannot find artifact assignment"); //cannot find lecense type
                }
                //var _departments = _artifactNode.Departments.Where(l => l.DepartmentId == departmentId).ToList();
                var _departments = _artifactNode.Departments.Where(l => l.Id == departmentId).ToList();
                foreach (var department in _departments)
                {
                    //_department.IsActive = false;
                    //_artifactNode.Departments.Remove(department);

                    //* edit by Toey 11/03/2018
                    _artifactAssignmentRepository.Delete(department);
                    //*
                }
            }
            Save();

            return departmentList;
        }

        public int CountArtifactNumberByStatus()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

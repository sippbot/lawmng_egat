﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models.Configurations
{
    public class TicketServiceConfiguration
    {
        public string ServiceEndPoint { get; set; }
        public string RequestMethod { get; set; }
        public string TitleNCFormat { get; set; }
        public string DescriptionNCFormat { get; set; }
        public string TitleODFormat { get; set; }
        public string DescriptionODFormat { get; set; }
        public UserInfoConfiguration InitialPhase { get; set; }
        public UserInfoConfiguration FollowUpPhase { get; set; }
    }

    public class UserInfoConfiguration
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
    }
}

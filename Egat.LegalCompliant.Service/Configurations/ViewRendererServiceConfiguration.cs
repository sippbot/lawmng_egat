﻿using System.Collections.Generic;

namespace Egat.LegalCompliant.Service.Configurations
{
    public class ViewRendererServiceConfiguration
    {
        public string TemplatePath { get; set; }
        public string ViewEngine { get; set; }
    }
}

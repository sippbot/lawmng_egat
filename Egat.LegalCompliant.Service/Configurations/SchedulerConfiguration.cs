﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.Service.Configurations
{
    public class SchedulerConfiguration
    {
        public bool EnableSchedulder { get; set; }
        public string DashboradPath { get; set; }
        public string ConfigurationPath { get; set; }

        public bool IsEnable {
            get { return EnableSchedulder; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.Service.Configurations
{
    public class EmailNotificationServiceConfiguration
    {
        public string Template { get; set; }
        public string Identity { get; set; }
        public string Credential { get; set; }
        public string SenderName { get; set; }
        public string SenderAddress { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool IsUseSSL { get; set; }

        public string ConfigurationPath { get; set; }
        public ICollection<string> ConfigurationFileNames { get; set; }
        public string RecipientConfigurationFileName { get; set; }

        public string EmailLogoPath { get; set; }
    }
}

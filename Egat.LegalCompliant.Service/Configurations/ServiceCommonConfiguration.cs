﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Configurations
{
    public class ServiceCommonConfiguration
    {
        public string EndPointURL { get; set; }
        public string BasePath { get; set; }
        public string AutoMessageDictionaryFileName { get; set; }
        public bool EnableDemoMode { get; set; }
        public bool EnableMaintainaceMode { get; set; }
        public bool EnableByPassUser { get; set; }
        public string EnableByPassPassword { get; set; }
        public ICollection<long> ByPassUserIdList { get; set; }
        public string JwtKey { get; set; }
        public string JwtIssuer { get; set; }
        public string JwtAudience { get; set; }
        public int JwtExpireDays { get; set; }

    }
}

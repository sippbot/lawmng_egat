﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Configurations
{
    public class ArtifactServiceConfiguration
    {
        public bool EnableSendMail { get; set; }
        public int DefaultFollowUpPeriod { get; set; }
        public string Token { get; set; }
        public string InitialDate { get; set; }
    }
}

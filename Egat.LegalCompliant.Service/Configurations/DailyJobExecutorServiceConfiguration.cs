﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Configurations
{
    public class DailyJobExecutorServiceConfiguration
    {
        public bool Enable { get; set; }
        public string CronStringAutoAssign { get; set; }
        public string CronStringTicketOverDue { get; set; }
        public string CronStringLicenseExpired { get; set; }
        public int AutoAssignThreshold { get; set; }
        public int AutoTicketDueThreshold { get; set; }
        public int AutoLicenseAlertThershold { get; set; }
        public bool EnableSendMail { get; set; }
        public bool EnableComplaint { get; set; }


    }
}

﻿using Egat.LegalCompliant.Service.Abstract;
using System;
using System.Linq;
using System.Collections.Generic;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Service.ServiceModels;
using Egat.LegalCompliant.Service.Utilities;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Service.Exceptions;
using Egat.LegalCompliant.Model.Tickets;
using Egat.LegalCompliant.API.Models.Configurations;
using Microsoft.Extensions.Options;
using Egat.LegalCompliant.Service.Complaint;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Service
{
    public class TaskAssignmentService : ITaskAssignmentService, IDisposable
    {
        private readonly ITaskAssignmentRepository _taskAssignmentRepository;
        private readonly ITaskAssessmentRepository _taskAssessmentRepository;
        private readonly IArtifactRepository _artifactRepository;
        private readonly IArtifactNodeRepository _artifactNodeRepository;
        private readonly ITaskGroupRepository _taskGroupRepository;
        private readonly IUserRepository _userRepository;
        private readonly ITaskCommentRepository _taskCommentRepository;
        private readonly IArtifactAssignmentRepository _artifactAssignmentRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly IDocumentRepository _documentRepository;
        private readonly IFileDataRepository _fileDataRepository;
        private readonly TicketServiceConfiguration _ticketServiceConfig;
        private readonly IUnitOfWork _unitOfWork;
        private readonly INotificationService _notificationService;
        private readonly IAutoMessageGenerator _autoMessage;
        private readonly ITicketService _ticketService;

        private readonly ILogger<TaskAssignmentService> _logger;

        public TaskAssignmentService(ITaskAssignmentRepository taskAssignmentRepository, ITaskAssessmentRepository taskAssessmentRepository, IDocumentRepository documentRepository, IFileDataRepository fileDataRepository,
            IArtifactRepository artifactRepository, ITaskGroupRepository taskGroupRepository, IUserRepository userRepository, ITaskCommentRepository taskCommentRepository, IArtifactNodeRepository artifactNodeRepository,
            IArtifactAssignmentRepository artifactAssignmentRepository, ITicketRepository ticketRepository, IUnitOfWork unitOfWork, IOptions<TicketServiceConfiguration> ticketServiceConfig,
            INotificationService notificationService, IAutoMessageGenerator autoMessage, ITicketService ticketService, ILogger<TaskAssignmentService> logger)
        {
            _taskAssignmentRepository = taskAssignmentRepository;
            _taskAssessmentRepository = taskAssessmentRepository;
            _artifactRepository = artifactRepository;
            _artifactNodeRepository = artifactNodeRepository;
            _taskGroupRepository = taskGroupRepository;
            _userRepository = userRepository;
            _taskCommentRepository = taskCommentRepository;
            _artifactAssignmentRepository = artifactAssignmentRepository;
            _unitOfWork = unitOfWork;
            _ticketServiceConfig = ticketServiceConfig.Value;
            _ticketRepository = ticketRepository;
            _documentRepository = documentRepository;
            _fileDataRepository = fileDataRepository;
            _notificationService = notificationService;
            _autoMessage = autoMessage;
            _ticketService = ticketService;
            _logger = logger;
        }

        public TaskAssessment CreateTaskAssessment(AssessmentInfoServiceModel info)
        {
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(t => t.Id == info.TaskAssignmentId, t => t.Assessments, t => t.ArtifactNode, t => t.Department);
            if (taskAssignment == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssessment(): task assignment id {0} not found", info.TaskAssignmentId);
                throw new TaskAssignmentServiceException(String.Format("task assignment id {0} not found", info.TaskAssignmentId));
            }

            if (!taskAssignment.IsAssessable())
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssessment(): task assignment id {0} is not assessable", info.TaskAssignmentId);
                throw new TaskAssignmentServiceException("task assignment is not assessable");
            }

            if (taskAssignment.DepartmentId != info.DepartmentId)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssessment(): task assignment id {0} is not for department id {1}", info.TaskAssignmentId, info.DepartmentId);
                throw new TaskAssignmentServiceException("task assignment is not for department");
            }

            User creator = _userRepository.GetSingle(info.CreatorId);
            if (creator == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssessment(): user id {0} not found", info.CreatorId);
                throw new TaskAssignmentServiceException("user not found");
            }

            // Before Add new TaskAssessment, close all previous TaskAssesment
            var previousAssessment = _taskAssessmentRepository.FindBy(a => a.TaskAssignmentId == info.TaskAssignmentId);
            foreach (var _assessment in previousAssessment)
            {
                // TODO get config of auto user and message
                var msg = _autoMessage.GetMessage(typeof(TaskAssessment), "CLOSE_TASKASSESSMENT_MSG");
                _assessment.Close(0, msg);
                _logger.LogInformation("TaskAssignmentService.CreateTaskAssessment(): close task assessment id {0}", _assessment.Id);
                _taskAssessmentRepository.Update(_assessment);
            }

            // Create new TaskAssignment
            TaskAssessment taskAssessment = new TaskAssessment(creator.Id, info.TaskAssignmentId);
            taskAssignment.Assess(taskAssessment.Assess(info.IsApplicable, info.IsCompliant, info.Detail));
            _taskAssessmentRepository.Add(taskAssessment);


            // Submit email notification
            // create new object
            // We send email to different mail box according to type of taskassignement
            if (taskAssignment.Type == TaskAssignmentType.Initial)
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("ArtifactAssessmentSubmitInitialNotification", taskAssignment);
                _notificationService.PushNotifications(notificationInfos);
            }
            else if (taskAssignment.Type == TaskAssignmentType.FollowUp)
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("ArtifactAssessmentSubmitFollowUpNotification", taskAssignment);
                _notificationService.PushNotifications(notificationInfos);
            }

            // submit ticket
            if (taskAssessment.IsApplicable && !taskAssessment.IsCompliant) // IsApplicable = true && IsCompliant == false
            {
                // Submit ticket here
                // TODO: Create Ticket and assign to TaskAssignment
                TicketInfo ticketInfo = new TicketInfo();

                var _artifact = _artifactRepository.GetSingle(taskAssignment.ArtifactId);

                ticketInfo.ISO = (int)_artifact.Type;
                ticketInfo.DepartmentId = info.DepartmentId;
                if (taskAssignment.Type == TaskAssignmentType.Initial)
                {
                    ticketInfo.OwnerId = _ticketServiceConfig.InitialPhase.UserId;

                }
                else if (taskAssignment.Type == TaskAssignmentType.FollowUp)
                {
                    ticketInfo.OwnerId = _ticketServiceConfig.FollowUpPhase.UserId;
                }

                ticketInfo.TaskAssignmentId = taskAssignment.Id;
                ticketInfo.TaskAssignmentDescription = taskAssignment.ArtifactNode.Content;
                ticketInfo.TaskAssessmentComment = info.Detail;
                ticketInfo.ArtifactCode = _artifact.Code;
                ticketInfo.Department = taskAssignment.Department.Name;
                ticketInfo.Type = TicketType.NotCompliant;

                // Sunmit ticket to ticket service
                _ticketService.SubmitTicket(ticketInfo);

                string msg = _autoMessage.GetMessage(typeof(TaskAssessment), "TICKET_TASKASSESSMENT_MSG");
                taskAssessment.Close(0, msg);
                _taskAssessmentRepository.Update(taskAssessment);

            }

            // Propagate Change to Artifact if TaskAssignment is Initial type
            if (taskAssignment.Type == TaskAssignmentType.Initial)
            {
                // TODO Call Update Artifact Here
                Artifact _artifact = _artifactRepository.GetSingle(a => a.Id == taskAssignment.ArtifactId, a => a.TaskAssignments);
                _artifact.UpdateState();
                _artifactRepository.Update(_artifact);
            }

            _taskAssignmentRepository.Update(taskAssignment);
            Save();

            return taskAssessment;
        }

        public TaskComment CreateTaskComment(TaskComment taskComment)
        {
            taskComment.CreateDate = DateTime.Now;
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(taskComment.TaskAssignmentId);

            if (taskAssignment == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskComment(): task assignment id {0} not found", taskComment.TaskAssignmentId);
                throw new TaskAssignmentServiceException("task assignment not found");
            }

            User creator = _userRepository.GetSingle(taskComment.CreatorId);
            if (creator == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskComment(): user id {0} not found", taskComment.CreatorId);
                throw new TaskAssignmentServiceException("user not found");
            }

            _taskCommentRepository.Add(taskComment);
            Save();

            return _taskCommentRepository.GetSingle(c =>
                c.Id == taskComment.Id,
                c => c.Creator);
        }

        public void CreateTaskAssignment(TaskAssignment taskAssignment)
        {
            _taskAssignmentRepository.Add(taskAssignment);
        }

        public IEnumerable<long> GetTaskAssignmentIdListByArtifactId(long artifactId, long departmentId, TaskAssignmentStatus statusFlag)
        {
            var _taskAssignments = _taskAssignmentRepository.FindBy(t => t.DepartmentId == departmentId && t.ArtifactId == artifactId && ((t.Status & statusFlag) != 0));

            return _taskAssignments.Select(a => a.Id).ToList();
        }

        public Ticket CreateTicket(long taskAssignmentId, Ticket ticket)
        {
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(taskAssignmentId);
            if (taskAssignment == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTicket(): task assignment id {0} not found", taskAssignmentId);
                throw new TaskAssignmentServiceException("task assignment not found");
            }
            ticket.TaskAssignmentId = taskAssignmentId;
            ticket.CreateDate = DateTime.Now;
            _ticketRepository.Add(ticket);

            Save();

            // send email notification for create ticket
            if (taskAssignment.Type == TaskAssignmentType.Initial)
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("TaskAssignmentCreateInitialTicketNotification", taskAssignment, ticket);
                _notificationService.PushNotifications(notificationInfos);
            } else
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("TaskAssignmentCreateFollowUpTicketNotification", taskAssignment, ticket);
                _notificationService.PushNotifications(notificationInfos);
            }


            return ticket;
        }

        public void CloseTicket(long taskAssignmentId, long ticketId, long closeById, string detail)
        {
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(t=> t.Id == taskAssignmentId);
            if (taskAssignment == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTicket(): task assignment id {0} not found", taskAssignmentId);
                throw new TaskAssignmentServiceException("task assignment not found");
            }

            Ticket ticket = _ticketRepository.GetSingle(ticketId);
            if (ticket == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTicket(): ticket id {0} not found", taskAssignmentId);
                throw new TaskAssignmentServiceException("ticket not found");
            }

            ticket.CloseDate = DateTime.Now;
            ticket.CloseById = closeById;
            ticket.ActionDetail = detail;
            ticket.Status = TicketStatus.Closed;
            _ticketRepository.Update(ticket);

            taskAssignment.CloseTicket();

            Save();
            _logger.LogInformation("TaskAssignmentService.CreateTicket(): closed ticket id {0} by user id {1}", taskAssignmentId, closeById);

            var notificationInfos = _notificationService.ResolveNotificationInfo("TaskAssignmentCloseTicketNotification", taskAssignment, ticket);
            _notificationService.PushNotifications(notificationInfos);

        }

        public IEnumerable<long> GetTaskAssignmentIdListByArtifactIdExclude(long artifactId, long departmentId, TaskAssignmentStatus statusFlag)
        {
            var _taskAssignments = _taskAssignmentRepository.FindBy(t => t.DepartmentId == departmentId && t.ArtifactId == artifactId && ((t.Status & statusFlag) == 0));

            return _taskAssignments.Select(a => a.Id).ToList();
        }

        public void CreateTaskAssignmentProposal(TaskAssignmentProposalServiceModel proposal)
        {
            var taskGroup = new TaskGroup();
            taskGroup.CreatorId = proposal.CreatorId;
            taskGroup.CreateDate = DateTime.Now;
            taskGroup.Assignments = new HashSet<TaskAssignmentGroup>();
            _taskGroupRepository.Add(taskGroup);

            Save();
            var flag = TaskAssignmentStatus.Proposed & TaskAssignmentStatus.Approved;
            var _taskAssignments = _taskAssignmentRepository.GetTaskAssignmentsExclude(proposal.TaskAssignments, flag);
            bool isFollowUpTask = true;
            foreach (var _taskAssignment in _taskAssignments)
            {
                if (!_taskAssignment.IsReady())
                {
                    continue;
                }

                _taskAssignment.Propose();

                if (_taskAssignment.Type == TaskAssignmentType.Initial)
                {
                    var _artifact = _artifactRepository.GetSingle(a => a.Id == proposal.ArtifactId, a => a.Nodes, a => a.TaskAssignments);
                    if (_artifact.Status != ArtifactStatus.Proposed && _artifact.Status != ArtifactStatus.Approved &&
                        IsArtifactReadyToPropose(_artifact, _taskAssignments))
                    {
                        _artifact.Propose();

                        // Send email notification to MR
                        var notificationInfo = _notificationService.ResolveNotificationInfo("InitialProposalNotification", _artifact);
                        _notificationService.PushNotifications(notificationInfo);
                        isFollowUpTask = false;
                    }
                }
                var taskAssignmetGroup = new TaskAssignmentGroup(_taskAssignment.Id, taskGroup.Id);
                taskGroup.Assignments.Add(taskAssignmetGroup);
            }

            Save();

            // send email for followup task
            if (isFollowUpTask)
            {
                var _artifact = _artifactRepository.GetSingle(a => a.Id == proposal.ArtifactId, a => a.Nodes, a => a.TaskAssignments);
                var notificationInfos = _notificationService.ResolveNotificationInfo("FollowUpProposalNotification", _artifact);
                _notificationService.PushNotifications(notificationInfos);
            }

        }

        private bool IsArtifactReadyToPropose(Artifact artifact, IEnumerable<TaskAssignment> taskAssignments)
        {
            //* Comment By Toey 08/05/2018
            //var departmentIdList = taskAssignments.Select(t => t.DepartmentId).ToList().Distinct();
            //var artifactAssignmentList = _artifactAssignmentRepository.FindBy(aa => aa.IsActive && aa.ArtifactId == artifact.Id);
            //foreach (var departmentId in departmentIdList)
            //{
            //    var artifactNodeList = taskAssignments.Where(t => t.DepartmentId == departmentId).Select(t => t.ArtifactNodeId).ToList();       
            //    bool flag = true;
            //    foreach (var _artifactNodeId in artifactAssignmentList.Where(aa => aa.DepartmentId == departmentId).Select(aa => aa.ArtifactNodeId))
            //    {
            //        if (!artifactNodeList.Any(artifactNodeId => artifactNodeId == _artifactNodeId))
            //        {
            //            flag = false;
            //            break;
            //        }
            //    }
            //    if (flag)
            //    {
            //        return true;
            //    }
            //}

            bool flag = true;
            var TASM_Type = taskAssignments.Select(t => t.Type).FirstOrDefault();

            if (TASM_Type == TaskAssignmentType.Initial)
            {
                //*** Initial ***//
                IEnumerable<TaskAssignment> artifactNodeList = _taskAssignmentRepository
                    .FindBy(t => t.ArtifactId == artifact.Id && t.IsActive == true && t.Type == TaskAssignmentType.Initial).ToList();

                foreach (var item in artifactNodeList)
                {
                    if (item.Status != TaskAssignmentStatus.Proposed)
                    {
                        flag = false;
                        break;
                    }
                }
            }
            else
            {
                //*** FollowUp ***//
                var departmentIdList = taskAssignments.Select(t => t.DepartmentId).ToList().Distinct();

                var artifactAssignmentList = _artifactAssignmentRepository.FindBy(aa => aa.IsActive && aa.ArtifactId == artifact.Id);

                foreach (var departmentId in departmentIdList)
                {
                    var artifactNodeList = taskAssignments.Where(t => t.DepartmentId == departmentId).Select(t => t.ArtifactNodeId).ToList();

                    foreach (var _artifactNodeId in artifactAssignmentList.Where(aa => aa.DepartmentId == departmentId).Select(aa => aa.ArtifactNodeId))
                    {
                        if (!artifactNodeList.Any(artifactNodeId => artifactNodeId == _artifactNodeId))
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }

            return flag;
        }

        public void ApproveTaskAssignment(TaskAssignmentApprovalServiceModel approvalInfo)
        {
            var flag = TaskAssignmentStatus.Proposed;
            var _taskAssignments = _taskAssignmentRepository.GetTaskAssignments(approvalInfo.TaskAssignments, flag);
            List<TaskAssignment> _actuaList = new List<TaskAssignment>();
            var _artifact = _artifactRepository.GetSingle(approvalInfo.ArtifactId);
            bool isInitialApprove = false;
            foreach (var _taskAssignment in _taskAssignments)
            {
                _taskAssignment.Approve(approvalInfo.ApproverId);

                if (_taskAssignment.Status == TaskAssignmentStatus.Approved)
                {
                    _actuaList.Add(_taskAssignment);
                }

                if (_artifact.Status != ArtifactStatus.Approved && _taskAssignment.Type == TaskAssignmentType.Initial)
                {
                    _artifact.Approve();
                    isInitialApprove = true;
                }
            }
            Save();

            // We will create next due task
            CreateTaskAssignmentFromApprovedTaskAssignments(approvalInfo.ArtifactId, _actuaList);
            Save();
            // ArtifactInitialApproveNotification
            if (isInitialApprove)
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("ArtifactInitialApproveNotification", _artifact);
                _notificationService.PushNotifications(notificationInfos);
            }
            else
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("ArtifactFollowupApproveNotification", _artifact);
                _notificationService.PushNotifications(notificationInfos);
            }

        }


        public TaskAssessment GetTaskAssessment(long tasAssignmentId)
        {
            throw new NotImplementedException();
        }

        public TaskAssignment GetTaskAssignment(long id)
        {
            return _taskAssignmentRepository.GetSingle(id);
        }

        public TaskAssignment GetTaskAssignmentFullDetail(long id)
        {
            return _taskAssignmentRepository.GetFullDetail(id);
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentsByArtifactId(long artifactId)
        {
            return _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifactId);
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentsByDepartmentId(long departmentId)
        {
            throw new NotImplementedException();
        }

        public void UpdateTaskAssessment(long taskAssignmentId, long taskAssessmentId, TaskAssessment taskAssessment)
        {
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(taskAssignmentId);
            if (taskAssignment == null)
            {
                throw new Exception(String.Format("Task assignment id {0} not found", taskAssignmentId));
            }

            TaskAssessment _taskAssessment = _taskAssessmentRepository.GetSingle(taskAssessmentId);
            if (_taskAssessment.TaskAssignmentId != taskAssignmentId)
            {
                return;
            }
            // _taskAssessment.
            _taskAssessmentRepository.Update(taskAssessment);

        }

        public TaskAssessment ReviewTaskAssessnment(AssessmentReviewServiceModel review)
        {
            var assessment = _taskAssessmentRepository.GetSingle(review.TaskAssessmentId);
            if (assessment == null)
            {
                throw new TaskAssignmentServiceException("cannot find task assessment");
            }

            if (assessment.TaskAssignmentId != review.TaskAssignmentId)
            {
                throw new TaskAssignmentServiceException("invalid task assignment id");
            }

            var taskAssignment = _taskAssignmentRepository.GetSingle(a => a.Id == review.TaskAssignmentId, a => a.Assessments);

            if (taskAssignment == null)
            {
                throw new TaskAssignmentServiceException("invalid task assignment id");
            }

            assessment.Review(review.ReviewerId, review.ReviewComment, review.IsConfirm);
            taskAssignment.Review();

            if(review.IsConfirm && !assessment.IsApplicable)
            {
                var artifactassigment = _artifactAssignmentRepository.GetSingle(aa => aa.DepartmentId == taskAssignment.DepartmentId && aa.ArtifactNodeId == taskAssignment.ArtifactNodeId && aa.IsActive);
                artifactassigment.IsActive = false;
                _artifactAssignmentRepository.Update(artifactassigment);
                Save();
            }

            bool isInitialApprove = taskAssignment.Type == TaskAssignmentType.Initial;

            if (isInitialApprove)
            {
                var _artifact = _artifactRepository.GetSingle(taskAssignment.ArtifactId);
                _artifact.UpdateState();
                _artifactRepository.Update(_artifact);
            }

            // email notification to department to react with review comment
            bool isConfirm = review.IsConfirm;
            if (!isConfirm)
            {
                var notificationInfos = _notificationService.ResolveNotificationInfo("ArtifactAssessmentRejectNotification", assessment, taskAssignment);
                _notificationService.PushNotifications(notificationInfos);
            }

            _taskAssessmentRepository.Update(assessment);
            _taskAssignmentRepository.Update(taskAssignment);
            Save();

            return assessment;
        }

        public IEnumerable<TaskAssignmentSummary> GetTaskAssignmentsSummaryByArtifactId(long artifactId)
        {
            IEnumerable<TaskAssignment> tasks = _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifactId);
            IEnumerable<TaskAssignmentSummary> summary = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);

            return summary;
        }

        public IEnumerable<TaskAssignmentSummary> GetTaskAssignmentsSummaryForProposed(long artifactId)
        {
            IEnumerable<TaskAssignment> tasks = _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifactId, true);
            IEnumerable<TaskAssignmentSummary> summary = TaskAssignmentSummarizer.CalculateTaskAssignmentSummaryForProposed(tasks);

            return summary;
        }

        public IEnumerable<TaskAssessment> GetTaskAssessmentsByTaskAssignmentId(long taskAssignmentId)
        {
            IEnumerable<TaskAssessment> taskAssignments = _taskAssessmentRepository
                .GetTaskAssessmentByTaskAssignmentId(taskAssignmentId);

            return taskAssignments;
        }

        public void ApproveTaskGroup(long taskGroupId, long approverId)
        {
            var _taskGroup = _taskGroupRepository.GetSingleIncludeTaskAssignment(taskGroupId);
            if (_taskGroup == null)
            {
                throw new Exception("Cannot find task group");
            }
            foreach (var _taskAssignment in _taskGroup.Assignments)
            {
                _taskAssignment.TaskAssignment.Approve(approverId);
            }
        }

        public IEnumerable<TaskAssignmentSummary> GetTaskAssignmentsSummaryByArtifactId(long artifactId, TaskAssignmentStatus status)
        {
            IEnumerable<TaskAssignment> tasks = _taskAssignmentRepository.GetTaskAssignmentByArtifactId(artifactId, status);
            IEnumerable<TaskAssignmentSummary> summary = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);

            return summary;
        }

        public IEnumerable<TaskAssignment> GetInitialTaskAssignmentsInPeriod(DateTime begin, DateTime end)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentsByStatus(TaskAssignmentStatus status, long artifactId, long departmentId)
        {
            return _taskAssignmentRepository.GetTaskAssignmentByStatus(status, artifactId, departmentId);
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentsByStatusExclude(TaskAssignmentStatus status, long artifactId, long departmentId)
        {
            return _taskAssignmentRepository.GetTaskAssignmentByStatusExclude(status, artifactId, departmentId);
        }

        public TaskAssignmentAttachment CreateTaskAssignmentAttachment(long taskAssignmentId, long creatorId, long documentId)
        {
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(taskAssignmentId);
            if (taskAssignment == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssignmentAttachment(): task assignment id {0} not found", taskAssignmentId);
                throw new TaskAssignmentServiceException("task assignment not found");
            }

            Document document = _documentRepository.GetSingle(documentId);
            if (document == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssignmentAttachment(): document id {0} not found", documentId);
                throw new TaskAssignmentServiceException("document not found");
            }

            var previousAttachment = taskAssignment.Documents.Where(d => d.DocumentId == documentId).FirstOrDefault();
            if (previousAttachment != null)
            {
                return previousAttachment;
            }

            var attachment = new TaskAssignmentAttachment(taskAssignmentId, document.Id, creatorId);
            taskAssignment.Documents.Add(attachment);

            _taskAssignmentRepository.Update(taskAssignment);

            Save();

            var _attachment = _taskAssignmentRepository.GetTaskAssignmentAttachment(attachment.Id);

            return attachment;

        }

        public void DeleteTaskAssignmentAttachment(long taskAssignmentId, long documentId)
        {
            TaskAssignment taskAssignment = _taskAssignmentRepository.GetSingle(t => t.Id == taskAssignmentId, t => t.Documents);
            if (taskAssignment == null)
            {
                _logger.LogError("TaskAssignmentService.DeleteTaskAssignmentAttachment(): task assignment id {0} not found", taskAssignmentId);
                throw new TaskAssignmentServiceException("task assignment not found");
            }

            Document document = _documentRepository.GetSingle(documentId);
            if (document == null)
            {
                _logger.LogError("TaskAssignmentService.DeleteTaskAssignmentAttachment(): document id {0} not found", documentId);
                throw new TaskAssignmentServiceException("document not found");
            }

            _taskAssignmentRepository.DeleteTaskAssignmentAttachment(taskAssignmentId, documentId);

            Save();
        }

        public IEnumerable<TaskAssignment> CreateTaskAssignmentFromApprovedTaskAssignments(long artifactId, IEnumerable<TaskAssignment> taskAssignments)
        {
            Artifact _artifact = _artifactRepository.GetAllArtifactNodes(artifactId);
            if (_artifact == null)
            {
                _logger.LogError("TaskAssignmentService.CreateTaskAssignmentFromApprovedTaskAssignments() : cannot find artifact id {0} - throw exception.", artifactId);
                throw new ArtifactServiceException("cannot find artifact");
            }

            List<TaskAssignment> taskAssignmentList = new List<TaskAssignment>();
            foreach (var prevoiusTaskAssignment in taskAssignments)
            {
                var artifactTaskAssignment = _artifactAssignmentRepository.GetSingle(aa =>
                    aa.DepartmentId == prevoiusTaskAssignment.DepartmentId && aa.ArtifactNodeId == prevoiusTaskAssignment.ArtifactNodeId);

                if (!artifactTaskAssignment.IsActive)
                {
                    continue;
                }

                int followUpOffsetMonth;
                int followUpOffsetYear;
                if (prevoiusTaskAssignment.ArtifactNode == null)
                {
                    var artifactNode = _artifactNodeRepository.GetSingle(prevoiusTaskAssignment.ArtifactNodeId);
                    if (!artifactNode.IsActive) { continue; }
                    followUpOffsetMonth = artifactNode.FollowUpPeriodMonth;
                    followUpOffsetYear = artifactNode.FollowUpPeriodYear;
                }
                else
                {
                    followUpOffsetMonth = prevoiusTaskAssignment.ArtifactNode.FollowUpPeriodMonth;
                    followUpOffsetYear = prevoiusTaskAssignment.ArtifactNode.FollowUpPeriodYear;
                }
                if (!prevoiusTaskAssignment.ArtifactNode.IsActive) { continue; }

                // We will ignore time part of approve date because next due date suppose to be a full date
                var _dueDate = prevoiusTaskAssignment.ApproveDate.Date.AddMonths(followUpOffsetMonth);
                _dueDate = _dueDate.Date.AddYears(followUpOffsetYear);
                var taskAssignment = _taskAssignmentRepository.CreateTaskAssigment(prevoiusTaskAssignment, _dueDate, TaskAssignmentType.FollowUp);

                //* add by Toey 17/04/2018
                taskAssignment.IsActive = true;

                taskAssignmentList.Add(taskAssignment);
            }
            _taskAssignmentRepository.AddTaskAssignments(taskAssignmentList);
            Save();

            // move Assessments, Comments, Documents to New TaskAssignment create by Toey 31/03/2018
            foreach (var NewtaskAssign in taskAssignmentList)
            {
                TaskAssignment OldtaskAssign = taskAssignments.Where(c => c.ArtifactNodeId == NewtaskAssign.ArtifactNodeId && c.DepartmentId == NewtaskAssign.DepartmentId).FirstOrDefault();

                foreach (var Ass in OldtaskAssign.Assessments)
                {
                    TaskAssessment NewTaskAssessment = new TaskAssessment();
                    NewTaskAssessment.CreateDate = Ass.CreateDate;
                    NewTaskAssessment.CreatorId = Ass.CreatorId;
                    NewTaskAssessment.Detail = Ass.Detail;
                    NewTaskAssessment.IsApplicable = Ass.IsApplicable;
                    NewTaskAssessment.IsCompliant = Ass.IsCompliant;
                    NewTaskAssessment.ReviewComment = Ass.ReviewComment;
                    NewTaskAssessment.ReviewDate = Ass.ReviewDate;
                    NewTaskAssessment.ReviewerId = Ass.ReviewerId;
                    NewTaskAssessment.Status = Ass.Status;
                    NewTaskAssessment.TaskAssignmentId = NewtaskAssign.Id;
                    _taskAssessmentRepository.Add(NewTaskAssessment);
                    Save();
                }
                foreach (var Com in OldtaskAssign.Comments)
                {
                    TaskComment NewTaskComment = new TaskComment();
                    NewTaskComment.Content = Com.Content;
                    NewTaskComment.CreateDate = Com.CreateDate;
                    NewTaskComment.CreatorId = Com.CreatorId;
                    NewTaskComment.TaskAssignmentId = NewtaskAssign.Id;
                    _taskCommentRepository.Add(NewTaskComment);
                    Save();
                }
                foreach (var Doc in OldtaskAssign.Documents)
                {
                    TaskAssignmentAttachment NewAttachment = new TaskAssignmentAttachment();
                    NewAttachment.AttachDate = Doc.AttachDate;
                    NewAttachment.CreatorId = Doc.CreatorId;
                    NewAttachment.DocumentId = Doc.DocumentId;
                    NewAttachment.TaskAssignmentId = NewtaskAssign.Id;
                    NewtaskAssign.Documents.Add(NewAttachment);
                    _taskAssignmentRepository.Update(NewtaskAssign);
                    Save();
                }
            }
            return taskAssignmentList;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

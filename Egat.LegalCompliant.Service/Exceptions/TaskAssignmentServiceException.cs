﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Exceptions
{
    public class TaskAssignmentServiceException : Exception
    {
        public TaskAssignmentServiceException() { }
        public TaskAssignmentServiceException(string message) : base(message) { }
    }
}

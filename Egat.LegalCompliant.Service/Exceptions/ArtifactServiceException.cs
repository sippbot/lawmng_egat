﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Exceptions
{
    public class ArtifactServiceException : Exception
    {
        public ArtifactServiceException() { }
        public ArtifactServiceException(string message) : base(message) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.Service.Exceptions
{
    public class TicketServiceException : Exception
    {
        public TicketServiceException() : base() { }
        public TicketServiceException(string msg) : base(msg) { }
        public TicketServiceException(string msg, Exception e) : base(msg, e) { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.Service.Exceptions
{
    public class EmailNotificationServiceException : Exception
    {
        public EmailNotificationServiceException() { }
        public EmailNotificationServiceException(string message) : base(message) { }
    }
}

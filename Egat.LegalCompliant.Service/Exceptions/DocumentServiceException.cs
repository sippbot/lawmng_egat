﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Exceptions
{
    public class DocumentServiceException : Exception
    {
        public DocumentServiceException() { }
        public DocumentServiceException(string message) : base(message) { }
    }
}

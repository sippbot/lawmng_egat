﻿using Egat.LegalCompliant.Model.Party;
using System;

namespace Egat.LegalCompliant.Service.Complaint
{
    public class TicketInfo
    {
        public string ArtifactCode { get; set; }
        public long TaskAssignmentId { get; set; }
        public string TaskAssignmentDescription { get; set; }
        public string TaskAssessmentComment { get; set; }
        public int ISO { get; set; }
        public long DepartmentId { get; set; }
        public string Department { get; set; }
        public long OwnerId { get; set; }
        public TicketType Type { get; set; }
    }

    public enum TicketType
    {
        NotCompliant    = 0,
        OverDue         = 1
    }
}

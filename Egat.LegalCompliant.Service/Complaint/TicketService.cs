﻿using Egat.LegalCompliant.API.Models.Configurations;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Tickets;
using Hangfire;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Egat.LegalCompliant.Service.Exceptions;
using Microsoft.AspNetCore.WebUtilities;
using Egat.LegalCompliant.Service.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Service.Configurations;
using System.Globalization;

namespace Egat.LegalCompliant.Service.Complaint
{
    public class TicketService : ITicketService, IDisposable
    {
        private readonly TicketServiceConfiguration _ticketServiceConfig;
        private readonly SchedulerConfiguration _schedulerConfig;
        private readonly ITicketRepository _ticketRepository;
        private readonly IBackgroundJobClient _backgroundJob;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<TicketService> _logger;

        public TicketService(IOptions<TicketServiceConfiguration> ticketServiceConfig, IOptions<SchedulerConfiguration> schedulerConfig,
            ITicketRepository ticketRepository, IBackgroundJobClient backgroudJob, IUnitOfWork unitOfWork, ILogger<TicketService> logger)
        {
            _ticketServiceConfig = ticketServiceConfig.Value;
            _schedulerConfig = schedulerConfig.Value;
            _ticketRepository = ticketRepository;
            _backgroundJob = backgroudJob;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public void ResubmitTicket(long id)
        {
            _backgroundJob.Enqueue
            (
                () => SendCreateRequest(id, true)
            );
        }

        public void SubmitTicket(TicketInfo ticketInfo)
        {
            // Prepare ticket model
            Ticket ticket = PrepareModel(ticketInfo);

            _ticketRepository.Add(ticket);

            Save();

            if (!_schedulerConfig.IsEnable)
                return;

            if (ticket.Id != 0)
            {
                _backgroundJob.Enqueue
                (
                    () => SendCreateRequest(ticket.Id, false)
                );
            }
            else
            {
                _logger.LogError("TicketService.SubmitTicket(): cannot submit ticket");
                throw new TicketServiceException("can not submit Ticket");
            }

        }

        public bool IsTaskAssignmentHasTicket(long taskAssignmentId, int overduePeriod)
        {
            bool ret = false;
            var tickets = _ticketRepository.FindBy(t => t.TaskAssignmentId == taskAssignmentId);
            DateTime offsetDate = DateTime.Today.AddDays(-overduePeriod);
            foreach (var ticket in tickets)
            {
                //  CASE I: return true
                //  |-------------X-----------------|
                //  |             |                 |
                // offset       create date        Today
                //
                //  CASE II: return false
                //  -----X-----------------|-----------|
                //       |                 |           |
                //    create date        offset      Today

                if (offsetDate < ticket.CreateDate)
                {
                    ret = true;
                }
            }
            return ret;
        }

        public void SendCreateRequest(long id, bool isResubmit)
        {
            // 0. Ensure that ticket id exist and not mark as created
            var ticket = _ticketRepository.GetSingle(t => t.Id == id, t => t.Owner, t => t.Department);
            if (ticket == null)
            {
                _logger.LogError("TicketService.SendCreateRequest(): ticket id {0} not found", id);
                throw new TicketServiceException("ticket not found");
            }

            if (!isResubmit && ticket.Status == TicketStatus.Submitted)
            {
                return;
            }

            try
            {
                // 1. send request to external system
                var msg = SendHttpRequest(ticket).Result;

                // 2. update ticket to store external ticket id
                var ticketExternalId = ProcessResponse(msg);
                ticket.TicketExternalId = ticketExternalId;
                ticket.Status = TicketStatus.Submitted;
                _ticketRepository.Update(ticket);

                Save();

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        private async Task<string> SendHttpRequest(Ticket ticket)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    HttpResponseMessage response;
                    switch (_ticketServiceConfig.RequestMethod.ToUpper())
                    {
                        case "GET":
                            string queryStr = PrepareRequestString(ticket);
                            response = await client.GetAsync(queryStr);
                            break;
                        case "POST":
                        default:
                            FormUrlEncodedContent formContent = PrepareFormContent(ticket);
                            response = await client.PostAsync(_ticketServiceConfig.ServiceEndPoint, formContent);
                            break;
                    }
                    
                    if (response.IsSuccessStatusCode)
                    {
                        Encoding encoding = Encoding.GetEncoding(874);
                        var stringTask = await response.Content.ReadAsStreamAsync();
                        var reader = new StreamReader(stringTask, encoding);
                        var returnStr = reader.ReadToEnd();
                        return returnStr;
                    }
                    else
                    {
                        throw new Exception("Complaint Service request not success");
                    }
                }
            }
            catch (TimeoutException e)
            {
                throw new TicketServiceException(String.Format("Timeout! Sending HttpRequest to {0} timeout!", _ticketServiceConfig.ServiceEndPoint), e);
            }
            catch (Exception e)
            {
                throw new TicketServiceException("Cannot connect to Complaint Service!", e);
            }
        }

        private string PrepareRequestString(Ticket ticket)
        {
            var parametersDictionary = new Dictionary<string, string> {
                { "name", ticket.Title },
                { "user_name", ticket.Owner.ToString() },
                { "user_id", ticket.Owner.Id.ToString("G") },
                { "discription", ticket.Description },
                { "iso_ref", ticket.ISO.ToString() },
                { "department_id",ticket.DepartmentId.ToString("G") },
                { "date_time", GetDateTimeNowString() }
            };

            var serviceUrl = _ticketServiceConfig.ServiceEndPoint;

            return QueryHelpers.AddQueryString(serviceUrl, parametersDictionary);
        }

        private FormUrlEncodedContent PrepareFormContent(Ticket ticket)
        {
            var parametersDictionary = new Dictionary<string, string> {
                { "name", ticket.Title },
                { "user_name", ticket.Owner.ToString() },
                { "user_id", ticket.Owner.Id.ToString() },
                { "discription", ticket.Description },
                { "iso_ref", ticket.ISO.ToString() },
                { "department_id",ticket.DepartmentId.ToString() },
                { "date_time", GetDateTimeNowString() }
            };

            return new FormUrlEncodedContent(parametersDictionary);
        }

        private string GetDateTimeNowString()
        {
            return DateTime.Now.ToString("dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Process response return from Compliant Service.
        /// This method will process response by substring
        /// and replace '\t', '\n' and '\r' with ''.
        /// 
        /// We need to do this because return result from Compliant Service is encoded with Window-874 charset.
        ///
        /// <u>Example.</u>
        /// 200OK12:60\r\r\t\n\n  -->  12:60
        /// </summary>
        /// <param name="response"></param>    
        /// 
        private string ProcessResponse(string response)
        {
            string templateStr = "200OK";
            int index = response.ToUpper().IndexOf(templateStr);
            if (index > -1)
            {
                return Regex.Replace(
                    response.Substring(index + templateStr.Length).Trim(),
                    @"\t|\n|\r", ""
                    );
            }
            else
            {
                throw new TicketServiceException("Process response from Compliant Service failed!");
            }

        }

        public IEnumerable<Ticket> GetAllTickets()
        {
            return _ticketRepository.AllIncluding(t => t.Department, t => t.Owner, t => t.CloseBy, t => t.TaskAssignment);
        }

        public IEnumerable<Ticket> GetAllTickets(TicketStatus status)
        {
            return _ticketRepository.GetAllTicket(status);
        }

        public IEnumerable<Ticket> GetAllTicketsByDepartmentId(long departmentId)
        {
            return _ticketRepository.GetAllByDepartmentId(departmentId);
        }

        public IEnumerable<Ticket> GetAllTicketsByDepartmentId(long departmentId, TicketStatus status)
        {
            return _ticketRepository.GetAllByDepartmentId(departmentId, status);
        }

        public Ticket GetTicket(long id)
        {
            return _ticketRepository.GetSingle(t => t.Id == id, t => t.Owner, t => t.CloseBy, t => t.Department);
        }

        private Ticket PrepareModel(TicketInfo ticketInfo)
        {
            var ticket = new Ticket();

            if(ticketInfo.Type == TicketType.NotCompliant)
            {
                ticket.Title = String.Format(_ticketServiceConfig.TitleNCFormat,
                    ticketInfo.ArtifactCode, ticketInfo.TaskAssignmentId);
                ticket.Description = String.Format(_ticketServiceConfig.DescriptionNCFormat,
                    ticketInfo.Department, ticketInfo.TaskAssignmentDescription, ticketInfo.TaskAssessmentComment);

            }else if(ticketInfo.Type == TicketType.OverDue)
            {
                ticket.Title = String.Format(_ticketServiceConfig.TitleODFormat,
                    ticketInfo.ArtifactCode, ticketInfo.TaskAssignmentId);
                ticket.Description = String.Format(_ticketServiceConfig.DescriptionODFormat,
                    ticketInfo.Department, ticketInfo.TaskAssignmentDescription, ticketInfo.TaskAssessmentComment);
            }            
            ticket.Status = TicketStatus.Created;
            ticket.OwnerId = ticketInfo.OwnerId;
            ticket.DepartmentId = ticketInfo.DepartmentId;
            ticket.TaskAssignmentId = ticketInfo.TaskAssignmentId;
            ticket.ISO = ticketInfo.ISO;           
            ticket.CreateDate = DateTime.Now;

            return ticket;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

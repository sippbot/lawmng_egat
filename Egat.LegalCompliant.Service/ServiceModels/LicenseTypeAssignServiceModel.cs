﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class LicenseTypeAssignServiceModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using Egat.LegalCompliant.Model.Party;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class AssessmentInfoServiceModel
    {
        public long TaskAssignmentId { get; set; }
        public long DepartmentId { get; set; }
        public long CreatorId { get; set; }
        public bool IsCompliant { get; set; }
        public bool IsApplicable { get; set; }
        public string Detail { get; set; }

        public AssessmentInfoServiceModel(long taskAssignmentId, long departmentId, long creatorId,
            bool isApplicable, bool isCompliant, string detail)
        {
            TaskAssignmentId = taskAssignmentId;
            DepartmentId = departmentId;
            CreatorId = creatorId;
            IsApplicable = isApplicable;
            IsCompliant = isCompliant;
            Detail = detail;
        }
    }
}

﻿using Egat.LegalCompliant.Model.Party;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class AssessmentReviewServiceModel
    {
        public long TaskAssignmentId { get; set; }
        public long TaskAssessmentId { get; set; }
        public long ReviewerId { get; set; }
        public bool IsConfirm { get; set; }
        public string ReviewComment { get; set; }

        public AssessmentReviewServiceModel(long taskAssignmentId,long taskAssessmentId,
            long reviewerId, bool isConfirm, string reviewComment)
        {
            TaskAssessmentId = taskAssessmentId;
            TaskAssignmentId = taskAssignmentId;
            ReviewerId = reviewerId;
            IsConfirm = isConfirm;
            ReviewComment = reviewComment;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class ArtifactNodeServiceModel
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public int Seq { get; set; }
        public int FollowUpPeriod { get; set; }
        public bool IsRequiredLicense { get; set; }
        public bool IsAllDepartment { get; set; }
        public string Group { get; set; }

        public ICollection<LicenseTypeAssignServiceModel> RequiredLicenses { get; set; }
        public ICollection<DepartmentAssignServiceModel> Departments { get; set; }
    }
}

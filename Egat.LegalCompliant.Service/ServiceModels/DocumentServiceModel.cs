﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class DocumentServiceModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public long FileId { get; set; }
    }
}

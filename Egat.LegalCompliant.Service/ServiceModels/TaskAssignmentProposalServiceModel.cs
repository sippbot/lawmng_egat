﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class TaskAssignmentProposalServiceModel
    {
        public long ArtifactId { get; set; }
        public long CreatorId { get; set; }
        public ICollection<long> TaskAssignments { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class ArtifactServiceModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string CreateDate { get; set; }
        public string PublishedDate { get; set; }
        public string EffectiveDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? LastModified { get; set; }
        public string Note { get; set; }
        public string Introduction { get; set; }

        // Foreign Key
        public long? SourceId { get; set; }
        public long CategoryId { get; set; }
        public long LevelId { get; set; }

        // Refernce
        public ICollection<ArtifactNodeGroupServiceModel> NodeGroups { get; set; }
        public ICollection<DocumentServiceModel> Documents { get; set; }
        public string Source { get; set; }

        public string Category { get; set; }
        public string Level { get; set; }
    }
}

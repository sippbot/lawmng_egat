﻿using Egat.LegalCompliant.Model.Tasks;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class TaskAssignmentSummaryQueryServiceModel
    {
        public long ArtifactId { get; set; }
        public TaskAssignmentStatus Status { get; set; }
    }
}

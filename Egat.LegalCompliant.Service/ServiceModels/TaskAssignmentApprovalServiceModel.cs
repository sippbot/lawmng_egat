﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class TaskAssignmentApprovalServiceModel
    {
        public long ArtifactId { get; set; }
        public long ApproverId { get; set; }
        public String Signature { get; set; }
        public ICollection<long> TaskAssignments { get; set; }
    }
}

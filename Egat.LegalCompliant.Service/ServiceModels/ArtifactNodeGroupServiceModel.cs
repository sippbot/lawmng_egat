﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.ServiceModels
{
    public class ArtifactNodeGroupServiceModel
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public int Seq { get; set; }
        // Reference
        public ICollection<ArtifactNodeServiceModel> Nodes { get; set; }

    }
}

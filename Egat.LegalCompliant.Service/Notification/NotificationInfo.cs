﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.Service.Notification
{
    //
    // This is a message model that other component use to contact to this service
    // 
    public class NotificationInfo
    {
        public long RecipientId { get; set; }
        public string RecipientName { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Action { get; set; }
        public string ButtonLabel { get; set; }
        public string ButtonLink { get; set; }
        public string Excerpt { get; set; }

        // public NotificationEvent Event { get; set; }

        public NotificationInfo() { }

        public NotificationInfo(long recipientId, string recipientName, string recipient, string subject, string message, string action, string buttonLabel, string buttonLink, string excerpt)
        {
            RecipientId = recipientId;
            RecipientName = recipientName;
            Recipient = recipient;
            Subject = subject;
            Message = message;
            Action = action;
            ButtonLabel = buttonLabel;
            ButtonLink = buttonLink;
            Excerpt = excerpt;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Service.Notification
{
    public class NotificationRecipientConfiguration
    {
        public ICollection<RecipientGroup> RecipientGroups { get; set; }
        public ICollection<MailList> MailLists { get; set; }
        public ICollection<Configuration> Configurations { get; set; }
    }

    public class RecipientGroup
    {
        public string Name { get; set; }
        public long GroupId { get; set; }
        public List<long> UserList { get; set; }
    }

    public class MailList
    {
        public string Name { get; set; }
        public string RecipientName { get; set; }
        public string Email { get; set; }
    }

    public class Configuration
    {
        public string Name { get; set; }
        public string StringConfig { get; set; }
        public List<string> MailList { get; set; }
        public List<string> UserList { get; set; }
        public List<string> RecipientGroup { get; set; }
    }

    /*
     * 
     * 
     *   
  "UserGroups": [
    {
      "Name": "LAWER",
      "GroupId": null,
      "UserList": []
    }
  ],
  "Configurations": [
    {
      "Name": "ArtifactAssignmentNotification",
      "StringConfig": "DEPARTMENT",
      "MailList": [],
      "UserList": [],
      "UserGroup": []
    },
    
 */
}

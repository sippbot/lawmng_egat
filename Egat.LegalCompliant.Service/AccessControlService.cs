﻿using Egat.LegalCompliant.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using static Egat.LegalCompliant.Model.Authorizations.AccountRoles;

namespace Egat.LegalCompliant.Service
{
    public class AccessControlService : IAccessControlService, IDisposable
    {
        private readonly IUserGroupRolesRepository _groupRolesRepository;
        private readonly IUserRolesRepository _userRolesRepository;
        private readonly IUserRepository _userRepository;
        private readonly ILogger<AccessControlService> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public AccessControlService(IUserGroupRolesRepository groupRolesRepository, IUserRolesRepository userRolesRepository, IUserRepository userRepository,
            ILogger<AccessControlService> logger, IUnitOfWork unitOfWork)
        {
            _groupRolesRepository = groupRolesRepository;
            _userRolesRepository = userRolesRepository;
            _userRepository = userRepository;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public void DeleteGroupRole(long groupId, RolesConstant role)
        {
            throw new NotImplementedException();
        }

        public void DeleteRole(long groupId, RolesConstant role)
        {
            throw new NotImplementedException();
        }

        public AccountRoles GetRoles(long userId)
        {
            AccountRoles roles = new AccountRoles();
            var user = _userRepository.GetSingle(userId);
            if (user == null)
            {
                _logger.LogWarning("AuthorizationService.GetRoles() - cannot find user id {}", userId);
            }

            var _grole = _groupRolesRepository.FindBy(gr => gr.GroupId == user.GroupId);
            ICollection<RolesConstant> _groupRolesKey = _grole.Select(gr => gr.Role).ToList();
            var _urole = _userRolesRepository.FindBy(ur => ur.UserId == user.Id);
            ICollection<RolesConstant> _userRolesKey = _urole.Select(ur => ur.Role).ToList();

            HashSet<RolesConstant> _roles = new HashSet<RolesConstant>();
            HashSet<string> _roleKeys = new HashSet<string>();
            foreach (var item in _groupRolesKey)
            {
                _roles.Add(item);
                _roleKeys.Add(item.ToString());
            }
            foreach (var item in _userRolesKey)
            {
                _roles.Add(item);
                _roleKeys.Add(item.ToString());
            }
            roles.List = _roles;
            roles.KeyList = _roleKeys;
            roles.UserId = userId;

            //* create by Toey 21/03/2018
            ICollection<KeyPrivilege> _groupRoles = _grole.Select(x => new KeyPrivilege { Key = x.Role.ToString(), IsReadOnly = x.IsReadOnly }).ToList();
            roles.KeyPrivileges = _groupRoles;
            //*

            return roles;
        }

        public void SetGroupRole(long groupId, RolesConstant role)
        {
            var _role = _groupRolesRepository.FindBy(g => g.GroupId == groupId && g.Role == role);
            if (_role.Count() == 0)
            {
                var groupRole = new UserGroupRoles();
                groupRole.GroupId = groupId;
                groupRole.Role = role;
                _groupRolesRepository.Add(groupRole);

                Save();
            }
        }

        public void SetUserRole(long userId, RolesConstant role)
        {
            var _role = _userRolesRepository.FindBy(u => u.UserId == userId && u.Role == role);
            if (_role.Count() == 0)
            {
                var userRole = new UserRoles();
                userRole.UserId = userId;
                userRole.Role = role;
                _userRolesRepository.Add(userRole);

                Save();
            }
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

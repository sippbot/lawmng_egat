﻿using AutoMapper;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Service.Configurations;
using Egat.LegalCompliant.Service.Notification;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using Egat.LegalCompliant.Model.Tasks;
using System.Collections.Generic;
using Hangfire;
using Egat.LegalCompliant.Service.Exceptions;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Service.Utilities;
using System.Threading.Tasks;
using System.Linq;
using Egat.LegalCompliant.Service.ViewRenderer;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace Egat.LegalCompliant.Service
{
    public class EmailNotificationService : INotificationService
    {
        private readonly EmailNotificationServiceConfiguration _config;
        private readonly SchedulerConfiguration _schedulerConfig;
        private readonly ViewRendererServiceConfiguration _viewRendererConfig;
        private readonly NotificationRecipientConfiguration _notificationRecipientConfiguration;
        private readonly IMailNotificationRepository _mailNotificationRepository;
        private readonly IDocumentRepository _documentRepository;
        private readonly IBackgroundJobClient _backgroundJob;
        private readonly IPostRepository _postRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IViewRendererService _viewRendererService;
        private readonly IMapper _mapper;

        private readonly IConfigurationRoot Configuration;

        public EmailNotificationService(IOptions<EmailNotificationServiceConfiguration> config, IOptions<SchedulerConfiguration> schedulerConfig, IOptions<ViewRendererServiceConfiguration> viewRendererConfig,
            IUserGroupRepository userGroupRepository, IUserRepository userRepository, IViewRendererService viewRendererService, IPostRepository postRepository, IMapper mapper, IHostingEnvironment env,
            IBackgroundJobClient backgroundJob, IMailNotificationRepository mailNotificationRepository, IDocumentRepository documentRepository, IDepartmentRepository departmentRepository)
        {
            _config = config.Value;
            _schedulerConfig = schedulerConfig.Value;
            _viewRendererConfig = viewRendererConfig.Value;
            _postRepository = postRepository;
            _userGroupRepository = userGroupRepository;
            _userRepository = userRepository;
            _backgroundJob = backgroundJob;
            _mailNotificationRepository = mailNotificationRepository;
            _documentRepository = documentRepository;
            _departmentRepository = departmentRepository;
            _viewRendererService = viewRendererService;
            _mapper = mapper;

            // Handle config
            ConfigurationBuilder builder = new ConfigurationBuilder();
            if (_config.ConfigurationPath == "")
            {
                builder.SetBasePath(env.ContentRootPath);
            }
            else
            {
                builder.SetBasePath(_config.ConfigurationPath);
            }

            foreach (var fileName in _config.ConfigurationFileNames)
            {
                builder.AddJsonFile(fileName, optional: true, reloadOnChange: true);
            }

            Configuration = builder.Build();

            // Handle recipient configuration
            IConfigurationBuilder recipientBuilder;
            if (_config.ConfigurationPath == "")
            {
                recipientBuilder = new ConfigurationBuilder()
                    .SetBasePath(env.ContentRootPath);
            }
            else
            {
                recipientBuilder = new ConfigurationBuilder()
                    .SetBasePath(_config.ConfigurationPath);
            }

            recipientBuilder.AddJsonFile(_config.RecipientConfigurationFileName);

            _notificationRecipientConfiguration = recipientBuilder.Build().Get<NotificationRecipientConfiguration>();
        }
        // Usage 
        // prepare msg by resolve using ResolveNotificationInfo()
        // call this function by given info 
        //
        public void PushNotification(NotificationInfo info)
        {
            /*
            string configPath = "Artifact";
            NotificationEvent _event = info.Event;
            var eventStr = _event.ToString();
            configPath = configPath + eventStr + "Notification";

            */
            GenerateEmailNotification(CreateMailNotification(info));
        }

        public void PushNotifications(IEnumerable<NotificationInfo> notificationList)
        {
            /*
            foreach(var item in notificationList)
            {
                GenerateEmailNotification(CreateMailNotification(item, null));
            }
            */

            foreach(var item in notificationList)
            {
                GenerateEmailNotification(CreateMailNotification(item));
            }
        }

        public void PushNotificationsFromTaskAssignmentSummary(IEnumerable<TaskAssignmentSummary> taskSummary)
        {
            foreach (var item in taskSummary)
            {
                
                GenerateEmailNotification(CreateMailNotification(item, null));

                // Send Email to Posts Email
                IEnumerable<Post> _posts = _postRepository.GetAllPostByDepartment(item.DepartmentId);
                foreach (var post in _posts)
                {
                    GenerateEmailNotification(CreateMailNotification(item, post));
                }
            }
        }

        private MailNotification ResolveMailNotification(string messageConfig, params object[] models)
        {
            MailNotification resolvedModel = new MailNotification();
            var stringFormatList = Configuration.GetSection(messageConfig).GetChildren();

            foreach (var kvp in stringFormatList)
            {
                var key = kvp.Key;

                var element = kvp.Get<PlaceholderElement>();

                var template = element.StringFormat;

                if (element.Params != null && element.Params.Count != 0)
                {
                    // Resolve Params
                    List<string> args = new List<string>();

                    foreach (var param in element.Params)
                    {
                        object model;
                        var Key = FindMatchPropertyInParams(param.Key, out model, models);

                        if (Key != null)
                        {
                            var value = Key.GetValue(model);
                            if (value is DateTime)
                            {
                                args.Add(DateTimeUtil.ConvertDateTimeToStringDate((DateTime)value));
                            }
                            else
                            {
                                args.Add(value.ToString());
                            }
                        }
                        else
                        {
                            args.Add(param.Value);
                            // Default is always string
                        }
                    }
                    template = String.Format(template, args.ToArray());
                }
                resolvedModel.GetType().GetProperty(key).SetValue(resolvedModel, template, null);
            }
            return resolvedModel;
        }

        public IEnumerable<NotificationInfo> ResolveNotificationInfo(string messageConfig, params object[] models)
        {
            // Resolve recipient
            Dictionary<string, Notification.RecipientGroup> userGroupMap = new Dictionary<string, Notification.RecipientGroup>();
            Dictionary<string, Notification.MailList> mailListMap = new Dictionary<string, Notification.MailList>();
            Dictionary<string, Notification.Configuration> configMap = new Dictionary<string, Notification.Configuration>();

            // validator
            EmailValidator validator = new EmailValidator();

            foreach (var userGroup in _notificationRecipientConfiguration.RecipientGroups)
            {
                userGroupMap.Add(userGroup.Name, userGroup);
            }

            foreach (var mailList in _notificationRecipientConfiguration.MailLists)
            {
                mailListMap.Add(mailList.Name, mailList);
            }

            foreach (var config in _notificationRecipientConfiguration.Configurations)
            {
                configMap.Add(config.Name, config);
            }

            var _config = configMap[messageConfig];
            string[] configMethods = _config.StringConfig.Split(',');
            List<User> userList = new List<User>();

            for (int i = 0; i < configMethods.Length; i++)
            {
                switch (configMethods[i].ToUpper())
                {
                    case "USERGROUP":
                        foreach(var usergroup in _config.RecipientGroup)
                        {
                            if (userGroupMap[usergroup].GroupId == 0)
                            {
                                foreach(var userId in userGroupMap[usergroup].UserList)
                                {
                                    User user = _userRepository.GetSingle(userId);

                                    if(user != null)
                                    {
                                        userList.Add(user);
                                    }
                                }
                            }
                            else
                            {
                                var groupId = userGroupMap[usergroup].GroupId;
                                userList.AddRange(_userGroupRepository.GetUserInUserGroup(groupId).ToList());
                            }
                            
                        }
                        break;

                    case "MAILLIST":
                        foreach(var name in _config.MailList)
                        {
                            Notification.MailList mailList = mailListMap[name];
                            if (validator.IsValidEmail(mailList.Email))
                            {
                                // create temporary user to submit email using data mail list
                                User user = new User
                                {
                                    FirstName = mailList.RecipientName,
                                    LastName = "",
                                    Email = mailList.Email,
                                    Id = 0 // set to because it is maillist
                                };

                                userList.Add(user);
                            }
                        }
                        break;
                    case "OWNER":
                        var Key = models[0].GetType().GetProperty("DocumentId");
                        long documentId = (long)Key.GetValue(models[0]);
                        var document = _documentRepository.GetDocument(documentId);
                        User _user = document.File.Owner;
                        userList.Add(_user);
                        break;

                    case "DEPARTMENT":
                        for(int j = 0; j < models.Length; j++)
                        {
                            PropertyInfo depIdPropertyInfo;
                            depIdPropertyInfo = models[j].GetType().GetProperty("DepartmentId");
                            if (depIdPropertyInfo != null && depIdPropertyInfo.PropertyType == typeof(long))
                            {
                                long departmentId = (long)depIdPropertyInfo.GetValue(models[j]);
                                var department = _departmentRepository.GetSingle(departmentId);
                                if (department != null)
                                {
                                    User departmentUser = new User();
                                    departmentUser.FirstName = department.AliasName + " (" + department.Name + ")";
                                    departmentUser.LastName = "";
                                    departmentUser.Email = department.Email;
                                    departmentUser.Id = department.Id;

                                    userList.Add(departmentUser);

                                    // Send Email to Posts Email
                                    IEnumerable<Post> _posts = _postRepository.GetAllPostByDepartment(departmentId);
                                    foreach (var post in _posts)
                                    {
                                        userList.Add(post.User);
                                    }
                                }
                                break;
                            }
                        }
                        break;
                }
            }

            // Render message
            ICollection<NotificationInfo> returnList = new HashSet<NotificationInfo>();
            foreach(var recipient in userList)
            {
                NotificationInfo resolvedModel = new NotificationInfo();

                // Set recipient info
                resolvedModel.Recipient = recipient.Email;
                resolvedModel.RecipientId = recipient.Id;
                resolvedModel.RecipientName = recipient.ToString();

                var stringFormatList = Configuration.GetSection(messageConfig).GetChildren();

                foreach (var kvp in stringFormatList)
                {
                    var key = kvp.Key;

                    var element = kvp.Get<PlaceholderElement>();

                    var template = element.StringFormat;

                    if (element.Params != null && element.Params.Count != 0)
                    {
                        // Resolve Params
                        List<string> args = new List<string>();

                        foreach (var param in element.Params)
                        {
                            object model;
                            var Key = FindMatchPropertyInParams(param.Key, out model, models);

                            if (Key != null)
                            {
                                var value = Key.GetValue(model);
                                if (value is DateTime)
                                {
                                    args.Add(DateTimeUtil.ConvertDateTimeToStringDate((DateTime)value));
                                }
                                else
                                {
                                    args.Add(value.ToString());
                                }
                            }
                            else
                            {
                                args.Add(param.Value);
                                // Default is always string
                            }
                        }
                        template = String.Format(template, args.ToArray());
                    }
                    resolvedModel.GetType().GetProperty(key).SetValue(resolvedModel, template, null);
                }
                returnList.Add(resolvedModel);
            }
            
            return returnList;
        }

        private PropertyInfo FindMatchPropertyInParams(string key, out object model, params object[] objects)
        {
            foreach(var obj in objects)
            {
                if(obj.GetType().GetProperty(key) != null)
                {
                    model = obj;
                    return obj.GetType().GetProperty(key);
                }
                
            }
            model = null;
            return null;
        }

        private MailNotification CreateMailNotification(TaskAssignmentSummary item, Post post)
        {
            // Send Email to Department Email

            /*
            MailNotification mail = new MailNotification();
            mail.Recipient = item.Department.Email;
            mail.RecipientId = item.DepartmentId;
            
            if (post == null)
            {
                mail.RecipientName = item.Department.AliasName + " (" + item.Department.Name + ")";
            }
            else
            {
                mail.RecipientName = post.PostName + " (" + post.User.ToString() + ")";
            }
            mail.Subject = "[อยู่ในระหว่างการทดสอบ] การประเมินความเกี่ยวข้องและการปฏิบัติสอดคล้องตามกฎหมาย";
            mail.Message = "ท่านได้รับอีเมล์ฉบับนี้จากระบบส่งการแจ้งเตือนอัตโนมัติของระบบติดตามการปฏิบัติตามกฎหมาย" +
                "เนื่องจากคณะกรรมการบ่งชี้ข้อกฎหมาย โรงไฟฟ้า ได้เห็นว่าหน่วยงาน" + item.Department.Name + "ของท่าน มีความความเกี่ยวข้องกับกฎหมายเรื่อง " + item.ArtifactTitle;

            mail.Action = "โปรดประเมินความเกี่ยวข้องและการปฏิบัติสอดคล้องตามกฎหมาย เรื่อง " + item.ArtifactTitle + " ภายในวันที่ " + DateTimeUtil.ConvertDateTimeToStringDate(item.DueDate) +
                " หากมีข้อสงสัยเพิ่มเติม กรุณาติดต่อ สำนักสารสนเทศ";
            mail.ButtonLabel = "เข้าสู่ระบบเพื่อเริ่มการประเมิน";
            mail.ButtonLink = "http://localhost:4200/#/laws/artifact-assesment/" + item.ArtifactId;
            */

            var mail = ResolveMailNotification("ArtifactAssignmentNotification", item);

            if (post == null)
            {
                mail.Recipient = item.Department.Email;
                mail.RecipientId = item.DepartmentId;
                mail.RecipientName = item.Department.AliasName + " (" + item.Department.Name + ")";
            }
            else
            {
                mail.Recipient = post.User.Email;
                mail.RecipientId = post.User.Id;
                mail.RecipientName = post.User.ToString();
            }

            return mail;
        }

        public MailNotification CreateMailNotification(NotificationInfo notificationInfo)
        {
            // var mail = ResolveMailNotification(messageConfigKey, obj);
            var mail = new MailNotification();

            mail.Recipient = notificationInfo.Recipient;
            mail.RecipientId = notificationInfo.RecipientId;
            mail.RecipientName = notificationInfo.RecipientName;
            mail.Subject = notificationInfo.Subject;
            mail.Message = notificationInfo.Message;
            mail.ButtonLabel = notificationInfo.ButtonLabel;
            mail.ButtonLink = notificationInfo.ButtonLink;
            mail.Excerpt = notificationInfo.Excerpt;

            return mail;
        }

        public void SendNotification(long id)
        {
            // Step 1.
            // query notification info from database -> info
            var mailNotification = _mailNotificationRepository.GetSingle(id);

            if(mailNotification == null)
            {
                throw new EmailNotificationServiceException(String.Format("Cannot find email notification with id {0}", id));
            }

            NotificationInfo info = _mapper.Map<MailNotification, NotificationInfo>(mailNotification);

            // Step 2.
            // prepare message rendering model in message template
            var message = PrepareMessage(info);
            // Step 3.
            // Send message
            if (Send(message.Result))
            {
                // Step 4.1
                // Set IsSent to true
                mailNotification.IsSent = true;
                mailNotification.SendDate = DateTime.Now;
                _mailNotificationRepository.Update(mailNotification);
                _mailNotificationRepository.Commit();
            }
        }

        private void GenerateEmailNotification(MailNotification mailNotification)
        {
            mailNotification.CreateDate = DateTime.Now;
            mailNotification.Type = NotificationType.Mail;
            _mailNotificationRepository.Add(mailNotification);
            _mailNotificationRepository.Commit();

            if (!_schedulerConfig.IsEnable)
                return;

            if (mailNotification.Id != 0)
            {
                _backgroundJob.Enqueue<INotificationService>
                (
                    x => x.SendNotification(mailNotification.Id)
                );
            }
            else
            {
                throw new EmailNotificationServiceException("Cannot send notification!");
            }

        }

        private async Task<MimeMessage> PrepareMessage(NotificationInfo info)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_config.SenderName, _config.SenderAddress));
            message.To.Add(new MailboxAddress(info.RecipientName, info.Recipient));
            message.Subject = info.Subject;

            if (_viewRendererConfig == null || _viewRendererConfig.ViewEngine.ToLower() != "razor")
            {
                var builder = new BodyBuilder();

                string content = "<h3>[อยู่ในระหว่างการทดสอบ] การแจ้งเตือน ระบบติดตามการปฏิบัติตามกฎหมาย</h3><br>" +
                    "<hr>" +
                    "<h4>เรียน " + info.RecipientName + "</h4>" +
                    "<h4>เรื่อง " + info.Subject + "</h4><br>" +
                    "<p>" + info.Message + "</p>" +
                    "<p>" + info.Action + "<p>" +
                    "<a href=\"" + info.ButtonLink + "\">" + info.ButtonLabel + "</a>" +
                    "<br><hr>" +
                    "<p><small>ฝ่ายการจัดการและติดตามการปฏิบัติตามกฎหมาย</small></p><p><small>โรงไฟฟ้าแม่เมาะ</small></p><p><small>การไฟฟ้าฝ่ายผลิตแห่งประเทศไทย</small></p><p><small>aslawmng@egat.co.th<small></p>";

                builder.HtmlBody = content;
                message.Body = builder.ToMessageBody();

                return message;
            }
            else
            {
                // Razor Engine
                var builder = new BodyBuilder();

                builder.HtmlBody = await _viewRendererService.RenderToStringAsync("Email/Email", info);

                // message.Body = builder.ToMessageBody();
                string path = _config.EmailLogoPath;

                // create an image attachment for the file located at path
                var attachment = new MimePart("image", "jpeg")
                {
                    Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = "egat.jpg"
                };

                // now create the multipart/mixed container to hold the message text and the
                // image attachment
                var multipart = new Multipart("mixed");
                multipart.Add(builder.ToMessageBody());
                multipart.Add(attachment);

                // now set the multipart/mixed as the message body
                message.Body = multipart;

                return message;
            }

        }

        private bool Send(MimeMessage message)
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    client.Connect(_config.Host, _config.Port, _config.IsUseSSL);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(_config.Identity, _config.Credential);
                    client.Send(message);
                    client.Disconnect(true);
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public IEnumerable<MailNotification> GetAllNotifications()
        {
            var returnList = _mailNotificationRepository.GetAll();

            return returnList;
        }

        public IEnumerable<MailNotification> GetNotificationByUserId(long userId)
        {
            //var returnList = _mailNotificationRepository.FindBy(m => m.RecipientId == userId);

            var returnList = _mailNotificationRepository.GetMailNotificationByUserId(userId);

            return returnList;
        }
    }

    class RecipientInfo
    {
        long RecipientId { get; set; }
        string RecipientName { get; set; }
        string Recipient { get; set; }
    }
}

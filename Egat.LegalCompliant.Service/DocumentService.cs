﻿using Egat.LegalCompliant.Service.Abstract;
using System;
using System.Collections.Generic;
using Egat.LegalCompliant.Model.Documents;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Options;
using Egat.LegalCompliant.Service.Configurations;
using Egat.LegalCompliant.Service.Exceptions;

namespace Egat.LegalCompliant.Service
{
    public class DocumentService : IDocumentService, IDisposable
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IUserRepository _userRepository;
        private readonly IFileDataRepository _fileDataRepository;
        private readonly ILicenseRepository _licenseRepository;
        private readonly ILicenseHistoryRepository _licenseHistoryRepository;
        private readonly ILogger<DocumentService> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly FileServerConfiguration _config;

        public DocumentService(IUserRepository userRepository, IDocumentRepository documentRepository, IFileDataRepository fileDataRepository,
            ILicenseRepository licenseRepository, ILicenseHistoryRepository licenseHistoryRepositoty, IOptions<FileServerConfiguration> config, 
            IUnitOfWork unitOfWork, ILogger<DocumentService> logger)
        {
            _documentRepository = documentRepository;
            _userRepository = userRepository;
            _fileDataRepository = fileDataRepository;
            _licenseRepository = licenseRepository;
            _licenseHistoryRepository = licenseHistoryRepositoty;
            _logger = logger;
            _unitOfWork = unitOfWork;
            _config = config.Value;
        }

        public Document CreateDocument(string title, long fileDataId, bool isPublic, DocumentType type)
        {
            var file = _fileDataRepository.GetSingle(fileDataId);
            if (file == null)
            {
                _logger.LogError("DocumentService.CreateDocument() : cannot find file id {0} - throw exception.", fileDataId);
                throw new DocumentServiceException("cannot find file");
            }

            Document _document = new Document();
            _document.CreateDate = DateTime.Now;
            _document.FileId = fileDataId;
            _document.Title = title;
            _document.Type = type;
            _document.IsPulic = isPublic;

            _documentRepository.Add(_document);

            Save();

            return _document;
        }

        public Document UpdateDocument(Document document)
        {
            var _document = _documentRepository.GetSingle(document.Id);
            if (_document == null)
            {
                _logger.LogError("DocumentService.UpdateDocument() : cannot find file id {0} - throw exception.", document.Id);
                throw new DocumentServiceException("cannot find file");
            }

            _document.Title = document.Title;
            _document.IsPulic = document.IsPulic;
            _document.Type = document.Type;

            _documentRepository.Update(_document);

            Save();

            return _documentRepository.GetDocument(document.Id);
        }

        public Document GetDocument(long id)
        {
            Document document = _documentRepository.GetSingle(d => d.Id == id, d => d.File);
            if(document == null)
            {
                _logger.LogError("DocumentService.GetDocument() : cannot find document id {0} - throw exception.", id);
                throw new DocumentServiceException("cannot find document");
            }

            return document;
        }

        public IEnumerable<Document> GetDocumentsByUserId(long userId)
        {
            IEnumerable<Document> documents = _documentRepository.GetDocumentsByUserId(userId);
            if (documents == null)
            {
                _logger.LogError("DocumentService.GetDocumentsByUserId() : cannot find document of user id {0} - throw exception.", userId);
                throw new DocumentServiceException("cannot find document");
            }

            return documents;
        }

        public string GetFileExtension(long id)
        {
            Document document = _documentRepository.GetSingle(d => d.Id == id, d => d.File);
            if (document == null)
            {
                _logger.LogError("DocumentService.GetDocument() : cannot find document id {0} - throw exception.", id);
                throw new DocumentServiceException("cannot find document");
            }

            return document.File.Extension;
        }

        public string GetFileStoragePath(long id)
        {
            Document document = _documentRepository.GetSingle(d => d.Id == id, d => d.File);
            if (document == null)
            {
                _logger.LogError("DocumentService.GetDocument() : cannot find document id {0} - throw exception.", id);
                throw new DocumentServiceException("cannot find document");
            }

            return _config.Path + document.File.StoragePath;
        }


        public License CreateLicense(License license)
        {
            Document document = _documentRepository.GetSingle(license.DocumentId);
            if (document == null)
            {
                _logger.LogError("DocumentService.CreateLicense() : cannot find document id {0} - throw exception.", license.DocumentId);
                throw new DocumentServiceException("cannot find document");
            }

            _licenseRepository.Add(license);
            document.Type = DocumentType.License;
            _documentRepository.Update(document);
            _licenseRepository.Commit();

            Save();

            return _licenseRepository.GetSingle(l => l.Id == license.Id, l => l.Document,l=>l.Document.File.Owner, l => l.Document.File, l => l.LicenseType);
        }

        public License RenewLicense(long id, License license)
        {
          
            var licenseOld = _licenseRepository.GetSingle(l => l.Id == id, l=>l.LicenseHistory, l=>l.LicenseType);
            if (licenseOld == null)
            {
                _logger.LogError("DocumentService.RenewLicense() : cannot find license id {0} - throw exception.", id);
                throw new DocumentServiceException("cannot find license");
            }

            Document document = _documentRepository.GetSingle(license.DocumentId);
            if (document == null)
            {
                _logger.LogError("DocumentService.RenewLicense() : cannot find document id {0} - throw exception.", license.DocumentId);
                throw new DocumentServiceException("cannot find document");
            }

            LicenseHistory history;

            if (licenseOld.LicenseHistoryId == null)
            {
                // never have history before then create one
                history = new LicenseHistory();
                history.Name = licenseOld.LicenseType.Name;
                history.HolderName = licenseOld.HolderName;
                history.LastUpdated = DateTime.Now;
                history.CurrentLicenseId = licenseOld.Id;
                _licenseHistoryRepository.Add(history);
                _licenseHistoryRepository.Commit();

                licenseOld.LicenseHistoryId = history.Id;
            }
            else
            {
                history = licenseOld.LicenseHistory;
            }

            licenseOld.IsOverride = true;
            _licenseRepository.Update(licenseOld);
            _licenseRepository.Commit();

            license.Id = 0;
            license.LicenseTypeId = licenseOld.LicenseTypeId;
            license.LicenseHistoryId = history.Id;

            _licenseRepository.Add(license);
            document.Type = DocumentType.License;
            _documentRepository.Update(document);
            _licenseRepository.Commit();

            // Update index of current license
            history.CurrentLicenseId = license.Id;
            history.LastUpdated = DateTime.Now;

            _licenseHistoryRepository.Update(history);
            _licenseHistoryRepository.Commit();

            Save();

            return _licenseRepository.GetSingle(l => l.Id == license.Id, l => l.Document, l => l.Document.File.Owner, l => l.Document.File, l => l.LicenseType);
        }

        public License AttachLicenseToTaskAssignment(long licenseId, long taskAssignmentId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<License> GetAllLicenses(long userId)
        {
            IEnumerable<License> licenses;
            if (userId == 0)
            {
                licenses = _licenseRepository.GetLicenses();
            }
            else
            {
                licenses = _licenseRepository.GetLicensesByUserId(userId);
            }
                
            if (licenses == null)
            {
                _logger.LogInformation("DocumentService.GetAllLicenses() : cannot find document of user id {0} - throw exception.", userId);
                // throw new DocumentServiceException("cannot find document");
            }

            return licenses;
        }

        public IEnumerable<License> GetAllLicensesByDueDate(DateTime begin, DateTime end)
        {
            IEnumerable<License> licenses = _licenseRepository.GetLicensesByDueDate(begin, end);
            if (licenses == null)
            {
                _logger.LogInformation("DocumentService.GetAllLicensesByDueDate() : no license satisfy the threshold.");
                throw new DocumentServiceException("cannot find document");
            }

            return licenses;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

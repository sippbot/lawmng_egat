﻿using Egat.LegalCompliant.Service.Abstract;
using System;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Egat.LegalCompliant.Service.Configurations;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;

namespace Egat.LegalCompliant.Service
{
    public class ViewRendererService : IViewRendererService
    {
        private readonly IRazorViewEngine _razorViewEngine;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IServiceProvider _serviceProvider;

        private readonly ViewRendererServiceConfiguration _config;
        private readonly ILogger<ViewRendererService> _logger;
        

        public ViewRendererService(IRazorViewEngine razorViewEngine, ITempDataProvider tempDataProvider,
            IServiceProvider serviceProvider, IOptions<ViewRendererServiceConfiguration> config, ILogger<ViewRendererService> logger)
        {
            _razorViewEngine = razorViewEngine;
            _tempDataProvider = tempDataProvider;
            _serviceProvider = serviceProvider;
            _config = config.Value;
            _logger = logger;
        }

        public async Task<string> RenderToStringAsync(string viewName, object model)
        {
            var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

            using (var sw = new StringWriter())
            {
                var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);
                
                //var viewResult = _razorViewEngine.GetView(_config.TemplatePath , _config.TemplatePath + viewName, false);
                Console.WriteLine(viewResult.SearchedLocations);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                    sw,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }
    }
}

﻿using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Model
{
    public interface IAssessable
    {
        bool IsApplicable { get; }
        bool IsCompliant { get; }
        bool IsReady();
        TaskAssessmentStatus Status { get; }
    }
}

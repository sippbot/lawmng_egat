﻿using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Tickets
{
    public class Ticket : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ISO { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime CloseDate { get; set; }
        public TicketStatus Status { get; set; }
        public string TicketExternalId { get; set; }
        public string ActionDetail { get; set; }

        // Reference
        public long TaskAssignmentId { get; set; }
        [ForeignKey("TaskAssignmentId")]
        public TaskAssignment TaskAssignment { get; set; }

        public long DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public long OwnerId { get; set; }
        [ForeignKey("OwnerId")]
        public User Owner { get; set; }

        public long? CloseById { get; set; }
        [ForeignKey("CloseById")]
        public User CloseBy { get; set; }
    }

    public enum TicketStatus
    {
        Created     = 0,
        Submitted   = 1,
        Closed      = 2,
    }
}

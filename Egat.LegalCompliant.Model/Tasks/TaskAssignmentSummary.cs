﻿using System;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Artifacts;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskAssignmentSummary : IAssignable
    {
        public string ArtifactTitle { get; set; }
        public long ArtifactId { get; set; }
        public long DepartmentId { get; set; }
        public long GroupId { get; set; }
        public Department Department { get; set; }
        public int NumberOfTotalTask { get; set; }
        public int NumberOfInprogressTask { get; set; }
        public bool IsApplicable { get; set; }
        public bool IsCompliant { get; set; }
        public string SummaryResult { get; set; }
        public TaskAssignmentStatus Status { get; set; }
        public int Flag { get; set; }
        public long TaskGroupId { get; set; }
        public TaskGroup TaskGroup { get; }
        public DateTime DueDate { get; set; }

        public TaskAssignmentSummary() { }

        private enum InternalFlag
        {
            IsApplicable = 1,
            IsCompliant = 2,
            IsReady = 4,

            InitialValue = 0
        }

        public bool IsReady()
        {
            return (Flag & (int)InternalFlag.IsReady) == (int)InternalFlag.IsReady ? true : false;
        }
    }
}

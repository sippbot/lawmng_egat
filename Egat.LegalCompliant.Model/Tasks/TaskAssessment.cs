﻿using System;
using System.Collections.Generic;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Party;
using System.ComponentModel.DataAnnotations.Schema;
using Stateless;
using Stateless.Graph;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskAssessment : IEntityBase, IAssessable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public bool IsApplicable { get; set; }
        public bool IsCompliant { get; set; }
        public string Detail { get; set; }
        public string ReviewComment { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ReviewDate { get; set; }
        public TaskAssessmentStatus Status
        {
            get { return _state; }
            //private set { _state = value; }

            // edit by Toey 31/03/2018
            set { _state = value; }
        }

        public int Flag
        {
            get { return GetFlag(); }
        }

        // Reference
        public long CreatorId { get; set; }
        [ForeignKey("CreatorId")]
        public User Creator { get; set; }
        public long ReviewerId { get; set; }
        [ForeignKey("ReviewerId")]
        public User Reviewer { get; set; }
        public long TaskAssignmentId { get; set; }
        [ForeignKey("TaskAssignmentId")]
        public TaskAssignment TaskAssignment { get; set; }

        // State-Machine Properties
        private TaskAssessmentStatus _state = TaskAssessmentStatus.Initial;
        private StateMachine<TaskAssessmentStatus, TaskAssessmentTrigger> _machine;
        private StateMachine<TaskAssessmentStatus, TaskAssessmentTrigger>.TriggerWithParameters<bool, bool, string> _assessTrigger;
        private StateMachine<TaskAssessmentStatus, TaskAssessmentTrigger>.TriggerWithParameters<long, string> _confirmTrigger;
        private StateMachine<TaskAssessmentStatus, TaskAssessmentTrigger>.TriggerWithParameters<long, string> _rejectTrigger;
        private StateMachine<TaskAssessmentStatus, TaskAssessmentTrigger>.TriggerWithParameters<long, string> _autoCloseTrigger;

        public TaskAssessment()
        {
            SetUpStateMachine();
        }

        public TaskAssessment(long creatorId, long taskAssignmentId)
        {
            CreatorId = creatorId;
            TaskAssignmentId = taskAssignmentId;
            SetUpStateMachine();
        }

        #region State-Machine

        private void SetUpStateMachine()
        {
            _machine = new StateMachine<TaskAssessmentStatus, TaskAssessmentTrigger>(() => _state, s => _state = s);

            _assessTrigger = _machine.SetTriggerParameters<bool, bool, string>(TaskAssessmentTrigger.Assess);
            _confirmTrigger = _machine.SetTriggerParameters<long, string>(TaskAssessmentTrigger.Confirm);
            _rejectTrigger = _machine.SetTriggerParameters<long, string>(TaskAssessmentTrigger.Reject);
            _autoCloseTrigger = _machine.SetTriggerParameters<long, string>(TaskAssessmentTrigger.Close);

            _machine.Configure(TaskAssessmentStatus.Initial)
                .Permit(TaskAssessmentTrigger.Assess, TaskAssessmentStatus.Assessed)
                .Permit(TaskAssessmentTrigger.Close, TaskAssessmentStatus.Closed);

            _machine.Configure(TaskAssessmentStatus.Assessed)
                .OnEntryFrom(_assessTrigger, (isApplicable, isCompliant, detail) => OnAssess(isApplicable, isCompliant, detail))
                .Permit(TaskAssessmentTrigger.Confirm, TaskAssessmentStatus.Confirmed)
                .Permit(TaskAssessmentTrigger.Reject, TaskAssessmentStatus.Rejected)
                .Permit(TaskAssessmentTrigger.Close, TaskAssessmentStatus.Closed);

            _machine.Configure(TaskAssessmentStatus.Confirmed)
                .OnEntryFrom(_confirmTrigger, (reviewerId, detail) => OnReview(reviewerId, detail));

            _machine.Configure(TaskAssessmentStatus.Rejected)
                .OnEntryFrom(_rejectTrigger, (reviewerId, detail) => OnReview(reviewerId, detail));

            _machine.Configure(TaskAssessmentStatus.Closed)
                .OnEntryFrom(_autoCloseTrigger, (reviewerId, detail) => OnClose(reviewerId, detail)); 

        }

        private void OnClose(long reviewerId, string detail)
        {
            OnReview(reviewerId, detail);
        }

        private void OnAssess(bool isApplicable, bool isCompliant, string detail)
        {
            IsApplicable = isApplicable;
            IsCompliant = isCompliant;
            Detail = detail;
            CreateDate = DateTime.Now;
        }

        private void OnReview(long reviewerId, string detail)
        {
            ReviewerId = reviewerId;
            ReviewComment = detail;
            ReviewDate = DateTime.Now;
        }

        #endregion
        public TaskAssessment Assess(bool isApplicable, bool isCompliant, string detail)
        {
            _machine.Fire(_assessTrigger, isApplicable, isCompliant, detail);
            return this;
        }

        public TaskAssessment Review(long reviewerId, string comment, bool isConfirm = true)
        {
            if (isConfirm)
                _machine.Fire(_confirmTrigger, reviewerId, comment);
            else
                _machine.Fire(_rejectTrigger, reviewerId, comment);

            return this;
        }

        public TaskAssessment Close(long reviewerId, string comment)
        {
            if (_machine.CanFire(TaskAssessmentTrigger.Close))
            {
                _machine.Fire(_autoCloseTrigger, reviewerId, comment);
            }
            return this;
        }

        private bool Transition(TaskAssessmentTrigger trigger)
        {
            if (!_machine.CanFire(trigger))
                return false;

            _machine.Fire(trigger);

            return true;
        }

        private int GetFlag()
        {
            var isApplicable = IsApplicable ? InternalFlag.IsApplicable : InternalFlag.InitialValue;
            var isCompliant = IsCompliant ? InternalFlag.IsCompliant : InternalFlag.InitialValue;
            var isReady = (Status == TaskAssessmentStatus.Confirmed) ? InternalFlag.IsReady : InternalFlag.InitialValue;
            return (int)isApplicable | (int)isCompliant | (int)isReady;
        }

        public bool IsReady()
        {
            return (Flag & (int)InternalFlag.IsReady) == (int)InternalFlag.IsReady ? true : false;
        }

        private enum InternalFlag
        {
            // DO NOT CHANGE THE VALUE
            IsApplicable    = 1,
            IsCompliant     = 2,
            IsReady         = 4,

            InitialValue    = 0
        }

        public string ExportDotGraph()
        {
            // return _machine.ToDotGraph();
            return UmlDotGraph.Format(_machine.GetInfo());
        }

    }

    public enum TaskAssessmentStatus
    {
        Initial     = 0,
        Assessed    = 1,
        Confirmed   = 2,
        Rejected    = 3,
        Closed      = 4,

    }

    public enum TaskAssessmentTrigger
    {
        Create      = 0, // initial
        Assess      = 1, // move to asserted
        Confirm     = 2, // move to closed state
        Reject      = 3, // move to rejected state
        Close       = 4
    }
}

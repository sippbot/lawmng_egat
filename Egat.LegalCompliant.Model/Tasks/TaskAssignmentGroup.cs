﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskAssignmentGroup : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long TaskAssignmentId { get; set; }
        public long TaskGroupId { get; set; }

        [ForeignKey("TaskAssignmentId")]
        public virtual TaskAssignment TaskAssignment { get; set; }

        [ForeignKey("TaskGroupId")]
        public virtual TaskGroup TaskGroup { get; set; }

        public TaskAssignmentGroup() { }

        public TaskAssignmentGroup(long taskAssignmentId, long taskGroupId)
        {
            TaskAssignmentId = taskAssignmentId;
            TaskGroupId = taskAssignmentId;
        }
    }
}

﻿using Egat.LegalCompliant.Model.Party;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskGroup : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public long CreatorId { get; set; }

        [ForeignKey("CreatorId")]
        public virtual User Creator { get; set; }
        public virtual ICollection<TaskAssignmentGroup> Assignments { get; set; }

        public TaskGroup(){ }

        public TaskGroup(long creatorId)
        {
            CreatorId = creatorId;
            CreateDate = DateTime.Now;
        }

        public bool IsInGroup(long taskAssignmentId)
        {
            return Assignments.Any(t => t.TaskAssignmentId == taskAssignmentId);
        }
    }
}

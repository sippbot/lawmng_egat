﻿using Egat.LegalCompliant.Model.Party;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskActivity : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime LoggingTime { get; set; }
        public User Actioner { get; set; }
        public EntityType Type { get; set; }
        public string Description { get; set; }
        public int Code { get; set; }
        
        public long TaskAssignmentId { get; set; }
        [ForeignKey("TaskAssignmentId")]
        public virtual TaskAssignment TaskAssignment { get; set; }

        public TaskActivity() { }
    }

    public enum EntityType
    {
        TaskAssignment  = 0,
        TaskAssessment  = 1,
        TaskComment     = 2,
        Document        = 3,
        Miscellaneous   = 4
    }
}

﻿using System;
using System.Collections.Generic;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Artifacts;
using Stateless;
using Stateless.Graph;
using System.ComponentModel.DataAnnotations.Schema;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Tickets;
using System.Linq;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskAssignment : IEntityBase, IAssignable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime ApproveDate { get; set; }
        public DateTime LastModified { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Progress { get; set; }
        public TaskAssignmentType Type { get; set; }

        public bool IsActive { get; set; }

        public TaskAssignmentStatus Status
        {
            get { return _state; }
            private set { _state = value; }
        }
        public int Flag { get; set; }

        public bool IsApplicable
        {
            get { return (Flag & (int)InternalFlag.IsApplicable) == (int)InternalFlag.IsApplicable ? true : false; }
            private set { }
        }
        public bool IsCompliant
        {
            get { return (Flag & (int)InternalFlag.IsCompliant) == (int)InternalFlag.IsCompliant ? true : false; }
            private set { }
        }

        public long ArtifactId { get; set; }
        [ForeignKey("ArtifactId")]
        public virtual Artifact Artifact { get; set; }

        public long ArtifactNodeId { get; set; }
        [ForeignKey("ArtifactNodeId")]
        public ArtifactNode ArtifactNode { get; set; }

        public long DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public long? ApproverId { get; set; }
        [ForeignKey("ApproverId")]
        public User Approver { get; set; }

        public virtual ICollection<TaskAssessment> Assessments { get; set; }
        public virtual ICollection<TaskComment> Comments { get; set; }
        public virtual ICollection<TaskAssignmentAttachment> Documents { get; set; }
        public virtual ICollection<TaskActivity> Activities { get; set; }
        public virtual ICollection<TaskAssignmentGroup> Groups { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }

        // State-Machine Properties
        TaskAssignmentStatus _state = TaskAssignmentStatus.Initial;
        StateMachine<TaskAssignmentStatus, TaskAssignmentTrigger> _machine;
        StateMachine<TaskAssignmentStatus, TaskAssignmentTrigger>.TriggerWithParameters<ArtifactAssignment, DateTime> _initialTrigger;
        StateMachine<TaskAssignmentStatus, TaskAssignmentTrigger>.TriggerWithParameters<DateTime> _followupTrigger;
        StateMachine<TaskAssignmentStatus, TaskAssignmentTrigger>.TriggerWithParameters<TaskAssessment> _assessTrigger;
        StateMachine<TaskAssignmentStatus, TaskAssignmentTrigger>.TriggerWithParameters<long> _approveTrigger;

        public TaskAssignment()
        {
            Documents = new HashSet<TaskAssignmentAttachment>();
            Tickets = new HashSet<Ticket>();
            Activities = new HashSet<TaskActivity>();
            SetUpStateMachine();
        }

        public TaskAssignment Fork()
        {
            return new TaskAssignment
            {
                Title = Title,
                Description = Description,
                DepartmentId = DepartmentId,
                ArtifactId = ArtifactId,
                ArtifactNodeId = ArtifactNodeId
            };
        }

        #region State-Machine

        private void SetUpStateMachine()
        {
            // State Machine Configuration

            _machine = new StateMachine<TaskAssignmentStatus, TaskAssignmentTrigger>(() => _state, s => _state = s);

            _initialTrigger = _machine.SetTriggerParameters<ArtifactAssignment, DateTime>(TaskAssignmentTrigger.Create);
            //_followupTrigger = _machine.SetTriggerParameters<DateTime>(TaskAssignmentTrigger.Create);
            _assessTrigger = _machine.SetTriggerParameters<TaskAssessment>(TaskAssignmentTrigger.Assess);
            _approveTrigger = _machine.SetTriggerParameters<long>(TaskAssignmentTrigger.Approve);


            _machine.Configure(TaskAssignmentStatus.Initial)
                .OnEntry(() => OnInitial())
                .Permit(TaskAssignmentTrigger.Create, TaskAssignmentStatus.Created)
                .Permit(TaskAssignmentTrigger.Assign, TaskAssignmentStatus.Assessed);

            _machine.Configure(TaskAssignmentStatus.Created)
                .OnEntryFrom(_initialTrigger, (assignment, duedate) => OnCreate(assignment, duedate))
                //.OnEntryFrom(_followupTrigger, (duedate) => OnCreateFollowUp(duedate))
                .PermitIf(TaskAssignmentTrigger.Assign, TaskAssignmentStatus.Assigned, () => IsAssignable());

            _machine.Configure(TaskAssignmentStatus.Assigned)
                .OnEntry(() => OnAssign())
                .Permit(TaskAssignmentTrigger.Assess, TaskAssignmentStatus.Assessed);

            _machine.Configure(TaskAssignmentStatus.Assessed)
                .OnEntryFrom(_assessTrigger, assessment => OnAssess(assessment))
                .PermitReentry(TaskAssignmentTrigger.Assess)
                .PermitIf(TaskAssignmentTrigger.Confirm, TaskAssignmentStatus.Confirmed, () => IsReady())
                .PermitIf(TaskAssignmentTrigger.Reject, TaskAssignmentStatus.Assigned, () => IsReject())
                .PermitIf(TaskAssignmentTrigger.Ticket, TaskAssignmentStatus.Ticketed, () => IsNotCompliant());

            _machine.Configure(TaskAssignmentStatus.Confirmed)
                .OnEntry(() => OnConfirm())
                .Permit(TaskAssignmentTrigger.Propose, TaskAssignmentStatus.Proposed);

            _machine.Configure(TaskAssignmentStatus.Ticketed)
                .OnEntry(() => OnTicket())
                .PermitIf(TaskAssignmentTrigger.CloseTicket, TaskAssignmentStatus.Assigned, () => IsAllTicketClosed());

            _machine.Configure(TaskAssignmentStatus.Proposed)
                .OnEntry(() => OnPropose())
                .Permit(TaskAssignmentTrigger.Approve, TaskAssignmentStatus.Approved);

            _machine.Configure(TaskAssignmentStatus.Approved)
                .OnEntryFrom(_approveTrigger, user => OnApprove(user));
        }

        public void OnInitial()
        {
            LastModified = DateTime.Now;
            Progress = 0;
        }

        public void OnPropose()
        {
            Progress = 80;
            LastModified = DateTime.Now;
        }

        public void OnTicket()
        {
            Progress = 20;
            LastModified = DateTime.Now;
        }

        private void OnConfirm()
        {
            Progress = 60;
            LastModified = DateTime.Now;
        }

        private void OnAssign()
        {
            LastModified = DateTime.Now;
            Progress = 20;
        }

        private void OnCreate(ArtifactAssignment assignment, DateTime duedate)
        {
            if(assignment == null)
            {
                Type = TaskAssignmentType.FollowUp;
                DueDate = duedate;
                Progress = 0;
                LastModified = DateTime.Now;
                AssignDate = DateTime.Now;
            }
            else
            {
                AssignDate = DateTime.Now;
                // Handle configuration here for X days
                DueDate = duedate;
                DepartmentId = assignment.DepartmentId;
                ArtifactNodeId = assignment.ArtifactNodeId;
                ArtifactId = assignment.ArtifactId;
                Description = assignment.ArtifactNode.Description;
                Title = assignment.Artifact.Title;
                Type = TaskAssignmentType.Initial;
                Progress = 0;
                LastModified = DateTime.Now;
            }
            
        }

        private void OnApprove(long userId)
        {
            ApproverId = userId;
            ApproveDate = DateTime.Now;
            LastModified = DateTime.Now;
            Progress = 100;
        }

        private void OnAssess(TaskAssessment assessment)
        {
            Assessments.Add(assessment);
            Flag = GetSummaryResult();
            Progress = 40;
            LastModified = DateTime.Now;
        }

        private bool Transition(TaskAssignmentTrigger trigger)
        {
            if (!_machine.CanFire(trigger))
                return false;

            _machine.Fire(trigger);

            return true;
        }

        #endregion

        private int GetSummaryResult()
        {
            int result = 0x000;
            foreach (var assessment in Assessments)
            {
                result |= assessment.Flag;
            }
            return result;
        }

        public bool IsAssignable()
        {
            //  Should return true if dueDate will reach in 30 days
            //
            //           (Now)                        (Now + 30 Days)
            //             |---------------30----------------|
            //             |                                 |
            //  -----------X---------------------------------X---------
            //                                               |
            //                                            Due Date
            //
            // return DateTime.Compare(DueDate, DateTime.Today.AddDays(180)) < 0;
            return true;
        }

        public bool IsResultValid()
        {
            return (Flag & (int)InternalFlag.IsReady) == (int)InternalFlag.IsReady ? true : false;
        }

        public bool IsNotCompliant()
        {
            return (Flag & (int)InternalFlag.IsCompliant) == (int)InternalFlag.IsCompliant ? false : true;
        }

        public bool IsReady()
        {
            return (Flag & (int)InternalFlag.IsReady) == (int)InternalFlag.IsReady ? true : false;
        }

        public bool IsReject()
        {
            if (Assessments.Count == 0) return false;

            var _assessment = Assessments.Where(a => a.Id == Assessments.Max(aa => aa.Id)).FirstOrDefault();
            return _assessment.Status == TaskAssessmentStatus.Rejected;
        }

        public bool IsAllTicketClosed()
        {
            if (Tickets.Count == 0)
            {
                return true;
            }

            foreach (var _ticket in Tickets)
            {
                if (_ticket.Status != TicketStatus.Closed) return false;
            }
            return true;
        }

        public void Assign()
        {
            Transition(TaskAssignmentTrigger.Assign);
        }

        public void Initial(ArtifactAssignment assignment, DateTime duedate)
        {
            _machine.Fire(_initialTrigger, assignment, duedate);
        }


        public void Assess(TaskAssessment taskAssessment)
        {
            _machine.Fire(_assessTrigger, taskAssessment);
            // Because we need to automatic update if assessment is not compliant
            // However, if assessment compliant we ignore ticketing
            if (IsApplicable && !IsCompliant)
            {
                Transition(TaskAssignmentTrigger.Ticket);
            }
        }

        public void Review()
        {
            Flag = GetSummaryResult();
            // Try confirm and reject
            // It can stay in one state
            Transition(TaskAssignmentTrigger.Confirm);
            Transition(TaskAssignmentTrigger.Reject);
        }

        public void Propose()
        {
            Transition(TaskAssignmentTrigger.Propose);
        }

        public void Approve(long approverId)
        {
            _machine.Fire(_approveTrigger, approverId);
        }

        public void CloseTicket()
        {
            Transition(TaskAssignmentTrigger.CloseTicket);
        }

        public bool IsAssessable()
        {
            // return _machine.CanFire(TaskAssignmentTrigger.Assess);
            if (IsReady())
            {
                return false;
            }
            return true;
        }

        private enum InternalFlag
        {
            IsApplicable = 1,
            IsCompliant = 2,
            IsReady = 4,

            InitialValue = 0
        }

        public string ExportDotGraph()
        {
            //return _machine.ToDotGraph();
            return UmlDotGraph.Format(_machine.GetInfo());
        }

    }

    public enum TaskAssignmentStatus
    {
        Initial = 0, // next due task but not assign to any department 1
        Created = 1,
        Assigned = 2,
        Assessed = 4,
        Confirmed = 8,
        Proposed = 16,

        Approved = 32,
        Canceled = 64,

        Ticketed = 256
    }

    public enum TaskAssignmentTrigger
    {
        Create = 0,
        Assign = 1,
        Assess = 2,
        Confirm = 3,
        Reject = 4,
        Propose = 6,
        Approve = 7,
        Cancel = 8,

        Overdue = 10,
        Ticket = 11,
        CloseTicket = 12
    }

    public enum TaskAssignmentType
    {
        Initial = 1, // first time evaluation
        FollowUp = 0,
        RenewLicense = 2
    }
}

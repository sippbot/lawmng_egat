﻿using System;
using Egat.LegalCompliant.Model.Party;
using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Tasks
{
    public class TaskComment : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public string Content { get; set; }

        //Reference
        public long CreatorId { get; set; }
        public User Creator { get; set; }
        public long TaskAssignmentId { get; set; }
        [ForeignKey("TaskAssignmentId")]
        public TaskAssignment Assignment { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactCategory : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public long ExternalId { get; set; }

        public ArtifactCategory() { }
    }
}

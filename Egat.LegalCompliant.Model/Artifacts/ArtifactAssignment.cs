﻿using Egat.LegalCompliant.Model.Party;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactAssignment : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime AssignDate { get; set; }
        public bool IsActive { get; set; }
        public long ArtifactId { get; set; }
        public long ArtifactNodeId { get; set; }
        public long DepartmentId { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }

        [ForeignKey("ArtifactId")]
        public virtual Artifact Artifact { get; set; }
        [ForeignKey("ArtifactNodeId")]
        public virtual ArtifactNode ArtifactNode { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        public ArtifactAssignment()
        {
            IsActive = true;
        }
    }
}

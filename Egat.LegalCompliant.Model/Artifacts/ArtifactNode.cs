﻿using System.Collections.Generic;
using Egat.LegalCompliant.Model.Documents;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactNode : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public int Seq { get; set; }
        public int FollowUpPeriodMonth { get; set; }
        public int FollowUpPeriodYear { get; set; }
        public bool IsRequiredLicense { get; set; }
        public bool IsAllDepartment { get; set; }
        public bool IsActive { get; set; }
        public DateTime EffectiveDate { get; set; }

        //Reference
        public long ArtifactId { get; set; }
        public long GroupId { get; set; }

        [ForeignKey("ArtifactId")]
        public Artifact Artifact { get; set; }

        [ForeignKey("GroupId")]
        public ArtifactNodeGroup Group { get; set; }

        public ICollection<ArtifactNodeLicenseType> RequiredLicenses { get; set; }
        public ICollection<ArtifactAssignment> Departments { get; set; }

        public ArtifactNode()
        {
            RequiredLicenses = new HashSet<ArtifactNodeLicenseType>();
            Departments = new HashSet<ArtifactAssignment>();
            IsAllDepartment = false;
            IsActive = true;
        }

        public void Update(ArtifactNode source)
        {
            Content = source.Content;
            Seq = source.Seq;
            IsRequiredLicense = source.IsRequiredLicense;
            IsAllDepartment = source.IsAllDepartment;
            Description = source.Description;

            //* add by Toey 17/04/2018
            FollowUpPeriodMonth = source.FollowUpPeriodMonth;
            FollowUpPeriodYear = source.FollowUpPeriodYear;
        }
    }
}

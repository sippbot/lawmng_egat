﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public interface IArtifact
    {
        ArtifactType Type { get; set; }
        ArtifactStatus Status { get; set; }
    }
}

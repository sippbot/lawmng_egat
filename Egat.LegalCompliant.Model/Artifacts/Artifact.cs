﻿using Egat.LegalCompliant.Model.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Stateless;
using Stateless.Graph;
using Egat.LegalCompliant.Model.Tasks;
using System.Linq;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class Artifact : IEntityBase, IArtifact
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public ArtifactType Type { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime LastModified { get; set; }
        public string Note { get; set; }
        public string Introduction { get; set; }
        public bool IsActive { get; set; }

        public long SourceId { get; set; }
        [ForeignKey("SourceId")]
        public ArtifactSource Source { get; set; }

        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public ArtifactCategory Category { get; set; }

        public long LevelId { get; set; }
        [ForeignKey("LevelId")]
        public ArtifactLevel Level { get; set; }
        
        // Refernce
        public ICollection<ArtifactNode> Nodes { get; set; }
        public ICollection<ArtifactNodeGroup> NodeGroups { get; set; }
        public ICollection<ArtifactAttachment> Documents { get; set; }
        public virtual ICollection<TaskAssignment> TaskAssignments { get; set; }

        public ArtifactStatus Status
        {
            get { return _state; }
            set { _state = value; }
        }
        public static ArtifactStatus InProgressFlag = ArtifactStatus.Assigned | ArtifactStatus.Assessed | ArtifactStatus.Proposed | ArtifactStatus.Confirmed;

        // State Machine Properties
        private ArtifactStatus _state;
        private readonly StateMachine<ArtifactStatus, ArtifactTrigger> _machine;

        public Artifact()
        {
            Nodes = new HashSet<ArtifactNode>();
            NodeGroups = new HashSet<ArtifactNodeGroup>();
            Documents = new HashSet<ArtifactAttachment>();
            TaskAssignments = new HashSet<TaskAssignment>();
            IsActive = true;

            // State Machine Configuration

            _machine = new StateMachine<ArtifactStatus, ArtifactTrigger>(() => _state, s => _state = s);

            _machine.Configure(ArtifactStatus.Draft)
                .OnEntry(() => OnDraft())
                .Permit(ArtifactTrigger.Create, ArtifactStatus.Created)
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Created)
                .OnEntry(() => OnCreate())
                .Permit(ArtifactTrigger.Assign, ArtifactStatus.Assigned)
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Assigned)
                .OnEntry(() => OnAssign())
                .SubstateOf(ArtifactStatus.Inprogress)
                //.PermitIf(ArtifactTrigger.Assess, ArtifactStatus.Assessed, () => IsAssessComplete())
                .Permit(ArtifactTrigger.Assess, ArtifactStatus.Assessed)
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Assessed)
                .OnEntry(() => OnAssess())
                .SubstateOf(ArtifactStatus.Inprogress)
                .PermitReentry(ArtifactTrigger.Assess)
                .PermitIf(ArtifactTrigger.Confirm, ArtifactStatus.Confirmed, () => IsAssessComplete())
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Confirmed)
                .OnEntry(() => OnConfirm())
                .SubstateOf(ArtifactStatus.Inprogress)
                .PermitIf(ArtifactTrigger.Propose, ArtifactStatus.Proposed, () => IsAllTaskAssignmentConfirmed())
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Proposed)
                .OnEntry(() => OnPropose())
                .SubstateOf(ArtifactStatus.Inprogress)
                .Permit(ArtifactTrigger.Approve, ArtifactStatus.Approved)
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Approved)
                .OnEntry(() => OnApprove())
                .Permit(ArtifactTrigger.Cancel, ArtifactStatus.Canceled);

            _machine.Configure(ArtifactStatus.Canceled)
                .OnEntry(() => OnCancle());

        }

        public static Artifact Clone(Artifact source)
        {
            return new Artifact() {
                Id = source.Id, 
                Code = source.Code,
                Title = source.Title,
                Type = source.Type,
                CreateDate = source.CreateDate,
                PublishedDate = source.PublishedDate,
                EffectiveDate = source.EffectiveDate,
                ApprovedDate = source.ApprovedDate,
                ValidFrom = source.ValidFrom,
                ValidTo = source.ValidTo,
                LastModified = source.LastModified,
                Note = source.Note,
                Introduction = source.Introduction,
                SourceId = source.SourceId,
                CategoryId = source.CategoryId,
                LevelId = source.LevelId,
                Status = source.Status
            };
        }

        public void Update(Artifact source)
        {
            Code = source.Code;
            Title = source.Title;
            Type = source.Type;
            CreateDate = source.CreateDate == DateTime.MaxValue ? CreateDate : source.CreateDate;
            PublishedDate = source.PublishedDate == DateTime.MaxValue ? PublishedDate : source.PublishedDate;
            EffectiveDate = source.EffectiveDate == DateTime.MaxValue ? EffectiveDate : source.EffectiveDate;
            Note = source.Note;
            Introduction = source.Introduction;
            SourceId = source.SourceId;
            CategoryId = source.CategoryId;
            LevelId = source.LevelId;
        }

        public void Create()
        {
            Transition(ArtifactTrigger.Create);
        }

        public void Assign()
        {
            _machine.Fire(ArtifactTrigger.Assign);
        }

        public void Propose()
        {
            Transition(ArtifactTrigger.Propose);
        }

        public void Approve()
        {
            Transition(ArtifactTrigger.Approve);
        }

        public void Cancle()
        {
            Transition(ArtifactTrigger.Cancel);
        }

        public bool IsManualImport()
        {
            var str1 = (CreateDate.Year + 543).ToString().Substring(2);
            var str2 = Code.Substring(Code.IndexOf("/") + 1);
            if(str1 != str2)
            {
                return true;
            }
            return false;
        }

        private bool Transition(ArtifactTrigger trigger)
        {
            if (!_machine.CanFire(trigger))
                return false;
            _machine.Fire(trigger);

            return true;
        }

        private bool IsAssessComplete()
        {
            var departments = TaskAssignments.Select(t => t.DepartmentId).Distinct().ToList();
            foreach (var department in departments)
            {
                bool flag = true;
                foreach (var _taskAssignment in TaskAssignments.Where(t => t.DepartmentId == department).ToList())
                {
                    flag &= !_taskAssignment.IsAssessable();
                }
                if (flag)
                    return true;
            }

            return false;
        }

        private bool IsAllTaskAssignmentConfirmed()
        {
            var departments = TaskAssignments.Select(t => t.DepartmentId).Distinct().ToList();
            foreach(var department in departments)
            {
                bool flag = true;
                foreach (var _taskAssignment in TaskAssignments.Where(t=>t.DepartmentId == department).ToList())
                {
                    flag &= _taskAssignment.IsReady();
                }
                if (flag)
                    return true;
            }

            return false;
        }
        private void OnDraft()
        {
            LastModified = DateTime.Now;
        }
        private void OnCreate()
        {
            LastModified = DateTime.Now;
        }
        private void OnAssign()
        {
            LastModified = DateTime.Now;
        }
        private void OnAssess()
        {
            LastModified = DateTime.Now;
        }
        private void OnConfirm()
        {
            LastModified = DateTime.Now;
        }
        private void OnPropose()
        {
            LastModified = DateTime.Now;
        }
        private void OnApprove()
        {
            ApprovedDate = DateTime.Now;
            ValidFrom = DateTime.Now;
            LastModified = DateTime.Now;
        }
        private void OnCancle()
        {
            ValidTo = DateTime.Now;
            LastModified = DateTime.Now;
            IsActive = false;
        }

        public void UpdateState()
        {
            // TODO Check and fire all posible event return imediatly
            if (_machine.CanFire(ArtifactTrigger.Assess))
            {
                _machine.Fire(ArtifactTrigger.Assess);
            }

            if (_machine.CanFire(ArtifactTrigger.Confirm))
            {
                _machine.Fire(ArtifactTrigger.Confirm);
            }
        }

        public bool IsInProgress()
        {
            return (_state & InProgressFlag) != 0 ? true : false;
        }

        public string ExportDotGraph()
        {
            //return _machine.ToDotGraph();
            return UmlDotGraph.Format(_machine.GetInfo());
        }

    }

    public enum ArtifactTrigger
    {
        Draft   = 0,
        Create  = 1,
        Assign  = 2,
        Assess  = 2,
        Confirm = 3,
        Reject  = 4,
        Propose = 6,
        Approve = 7,
        Cancel  = 8
    }

    public enum ArtifactStatus
    {
        Initial     = 0,
        Created     = 1,
        Assigned    = 2,
        Assessed    = 4,
        Confirmed   = 8,
        Proposed    = 16,
        Approved    = 32,
        Canceled    = 64,

        Ticketed    = 256,
        Draft       = 512,
        Inprogress  = 1024 // This flag is virtual flag, we not set any artifact to this flag
    }

    public enum ArtifactType
    {
        Quality         = 9000,
        Environment     = 14000,
        Energy          = 50000,
        Safety          = 18000,
        Engineering     = 1,
        Health          = 2,
        Miscellaneous   = 0
    }
}

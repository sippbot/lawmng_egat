﻿using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactSummary : IArtifact
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public ArtifactStatus Status { get; set; }
        public ArtifactType Type { get; set; }
        public TaskAssignmentType TaskAssignmentType { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime DueDate { get; set; }
        // public string Note { get; set; }
        // public string Introduction { get; set; }

        // creat by Toey 31/03/2018
        public TaskAssignmentStatus TaskAssignmentStatus { get; set; }

        // Foreign Key
        public long SourceId { get; set; }
        public long CategoryId { get; set; }
        public long LevelId { get; set; }

        public ArtifactSource Source { get; set; }
        public ArtifactCategory Category { get; set; }
        public ArtifactLevel Level { get; set; }

        // Refernce
        public ICollection<DepartmentSummary> Departments { get; set; }

        public ArtifactSummary()
        {
            Departments = new HashSet<DepartmentSummary>();
        }

        public ArtifactSummary(Artifact artifact)
        {
            // Perform deep copy here
            Id = artifact.Id;
            Code = artifact.Code;
            Title = artifact.Title;
            Status = artifact.Status;
            Type = artifact.Type;
            CreateDate = artifact.CreateDate;
            PublishedDate = artifact.PublishedDate;
            EffectiveDate = artifact.EffectiveDate;
            ApprovedDate = artifact.ApprovedDate;
            ValidFrom = artifact.ValidFrom;
            ValidTo = artifact.ValidTo;
            LastModified = artifact.LastModified;
            // Note = artifact.Note;
            // Introduction = artifact.Introduction;

            // Foreign Key
            SourceId = artifact.SourceId;
            CategoryId = artifact.CategoryId;
            LevelId = artifact.LevelId;

            Source = artifact.Source;
            Category = artifact.Category;
            Level = artifact.Level;
            Departments = new HashSet<DepartmentSummary>();
        }
    }
}

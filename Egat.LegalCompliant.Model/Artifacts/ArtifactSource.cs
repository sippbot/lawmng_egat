﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactSource : IEntityBase
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public ArtifactSource(){}
    }
}

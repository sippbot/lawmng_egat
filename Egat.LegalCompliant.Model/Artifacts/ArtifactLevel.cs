﻿namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactLevel : IEntityBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }     
    }
}

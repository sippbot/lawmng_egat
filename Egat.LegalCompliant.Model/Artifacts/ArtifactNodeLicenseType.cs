﻿using Egat.LegalCompliant.Model.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactNodeLicenseType : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long ArtifactNodeId { get; set; }
        public long LicenseTypeId { get; set; }
        public bool IsActive { get; set; }
        // Reference
        [ForeignKey("ArtifactNodeId")]
        public ArtifactNode ArtifactNode { get; set; }
        [ForeignKey("LicenseTypeId")]
        public LicenseType LicenseType { get; set; }

        public ArtifactNodeLicenseType()
        {
            IsActive = true;
        }
    }
}
﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Artifacts
{
    public class ArtifactNodeGroup : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Content { get; set; }
        public int Seq { get; set; }
        // Reference
        public long ArtifactId { get; set; }
        [ForeignKey("ArtifactId")]
        public Artifact Artifact { get; set; }
        public ICollection<ArtifactNode> Nodes { get; set; }
        public bool IsActive { get; set; }

        public ArtifactNodeGroup()
        {
            Nodes = new HashSet<ArtifactNode>();
            IsActive = true;
        }
        
        public static ArtifactNodeGroup Clone(ArtifactNodeGroup source)
        {
            return new ArtifactNodeGroup
            {
                Id = source.Id,
                Content = source.Content,
                Seq = source.Seq,
                ArtifactId = source.ArtifactId
            };
        }

        public void Update(ArtifactNodeGroup source)
        {
            Content = source.Content;
            Seq = source.Seq;
        }
    }
}

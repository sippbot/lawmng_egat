﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Model.Authorizations
{
    public enum RolesConstant
    {
        // Access Control List
        ACLMNG = 110, // จัดการสิทธิ์ของผู้เข้าใช้งาน
        // Artifact
        ATFMNG = 220, // จัดการกฎหมายยกเลิก
        ATFMNG02 = 221, // เรียกดูกฎหมายยกเลิก
        ATFMNG03 = 222, // กฎหมายฉบับร่าง
        ATFMNG04 = 223, // ตรวจสอบกฎหมายที่รอทวนสอบ
        ATFMNG05 = 224, // จัดทำรายงานความเกี่ยวข้องกฎหมายใหม่
        ATFMNG06 = 225, // ส่งทวนสอบความเกี่ยวข้อง
        ATFMNG07 = 226, // การนำเข้ากฎหมายใหม่และจัดแผนการติดตาม
        ATFVIW = 230, // จัดทำรายงานการปฏิบัติสอดคล้องตามกฎหมาย
        ATFVIW02 = 231, // การติดตามการปฏิบัติสอดคล้องตามกฎหมาย
        ATFVIW03 = 232, // ทะเบียนแสดงกฎหมายภายในโรงไฟฟ้า
        // Department
        DEPMNG = 310, // ข้อมูลหน่วยงาน 
        // Document
        DOCMNG = 410, // จัดการเอกสารใบอนุญาต
        DOCMNG02 = 411, // เรียกดูเอกสารใบอนุญาต
        // License
        LCNMNG = 510,
        // Metadata
        METDAT = 610, // ข้อมูลหมวดหมู่กฎหมาย
        METDAT02 = 611, // ข้อมูลประเภทกฎหมาย
        // Ticket
        TCKMNG = 810, // จัดการข้อร้องเรียน
        // TaskAssignment
        TSKASM = 910, // ประเมินการปฏิบัติสอดคล้องตามกฎหมาย
        TSKASM02 = 911, // จัดการยืนยันความเกี่ยวข้อง
        TSKRVW = 930, // ตรวจสอบการประเมินการปฏิบัติสอดคล้องตามกฎหมาย
        TSKRVW02 = 931, // ตรวจสอบผลการเกี่ยวข้องและสอดคล้อง
        TSKAPP = 940, // อนุมัติและรับทราบความเกี่ยวข้องและสอดคล้อง
        // User
        USRMNG = 1010, // นำเข้าข้อมูลผู้ใช้ระบบ

        TSKRVW03 = 932, // นำเสนอผลให้ผู้แทน

        // ยังไม่ทราบเมนู
        // Artifact
        ATFAPP = 210,
        // System Setting
        SYSSET = 710,
        // TaskAssignment
        TSKVIW = 920,
    }
}

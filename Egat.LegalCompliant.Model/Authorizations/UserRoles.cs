﻿using Egat.LegalCompliant.Model.Party;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Authorizations
{
    public class UserRoles : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public RolesConstant Role { get; set; }
        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public UserRoles() { }
    }
}

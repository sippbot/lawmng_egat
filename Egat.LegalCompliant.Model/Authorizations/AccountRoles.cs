﻿using Egat.LegalCompliant.Model.Party;
using System.Collections.Generic;


namespace Egat.LegalCompliant.Model.Authorizations
{
    public class AccountRoles
    {
        public long UserId { get; set; }
        public ICollection<RolesConstant> List { get; set; }
        public ICollection<string> KeyList { get; set; }

        //* create by Toey 21/03/2018
        public ICollection<KeyPrivilege> KeyPrivileges { get; set; }
        //*

        public AccountRoles() { }

        public bool HasRole(RolesConstant role)
        {
            if (List.Contains(role))
            {
                return true;
            }
            return false;
        }

        //* create by Toey 21/03/2018
        public class KeyPrivilege
        {
            public string Key { get; set; }
            public bool IsReadOnly { get; set; }
        }
    }
}

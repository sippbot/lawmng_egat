﻿namespace Egat.LegalCompliant.Model
{
    public interface IEntityBase
    {
        long Id { get; set; }
    }
}

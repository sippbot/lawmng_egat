﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using Egat.LegalCompliant.Model.Party;
using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Documents
{
    public class FileData : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public long? Size { get; set; }
        public float? SizeKB { get; set; }
        public string StoragePath { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime UploadDate { get; }
        
        public long OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public virtual User Owner { get; set;}

        public virtual Document Document { get; set; }

        public FileData()
        {
            this.UploadDate = DateTime.Now;
        }

        public string generatePath()
        {
            if (StoragePath != null)
                return this.StoragePath;

            string hash;
            using (MD5 md5Hash = MD5.Create())
            {
                StringBuilder source = new StringBuilder();
                source.Append(FileName);
                source.Append(OwnerId);
                source.Append(UploadDate);
                hash = getMd5Hash(md5Hash, source.ToString());
            }
            string ret = String.Format("{0}\\{1}\\{2}", hash.Substring(0,2), 
                hash.Substring(2,2), hash.Substring(4));
            this.StoragePath = ret;
            return ret;
        }

        public string getUrl()
        {
            return String.Format("{0}/{1}.{2}", this.Id, this.FileName, this.Extension);
        }

        private string getMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        private bool verifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = getMd5Hash(md5Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;
            if (0 == comparer.Compare(hashOfInput, hash))
                return true;
            else
                return false;
        }

        public string getDirectory()
        {
            return this.StoragePath.Substring(0, this.StoragePath.LastIndexOf("\\") + 1);
        }
    }
}

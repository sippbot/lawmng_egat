﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Documents
{
    public class LicenseHistory : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public string HolderName { get; set; }
        public DateTime LastUpdated { get; set; }

        public List<License> License { get; set; }

        // Reference
        public long CurrentLicenseId { get; set; }
        [ForeignKey("CurrentLicenseId")]
        public virtual License CurrentLicense { get; set; }

    }
}

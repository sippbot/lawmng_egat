﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Party;

namespace Egat.LegalCompliant.Model.Documents
{
    public class TaskAssignmentAttachment : IEntityBase, IDocument
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long TaskAssignmentId { get; set; }
        public long DocumentId { get; set; }
        public long CreatorId { get; set; }
        public DateTime AttachDate { get; set; }

        [ForeignKey("DocumentId")]
        public virtual Document Document { get; set; }

        [ForeignKey("TaskAssignmentId")]
        public virtual TaskAssignment TaskAssignment { get; set; }

        [ForeignKey("CreatorId")]
        public virtual User Creator { get; set; }

        public TaskAssignmentAttachment() { }

        public TaskAssignmentAttachment(long taskAssignmentId, long documentId, long creatorId)
        {
            TaskAssignmentId = taskAssignmentId;
            DocumentId = documentId;
            CreatorId = creatorId;
            AttachDate = DateTime.Now;
        }

        public long GetDocumentId()
        {
            return DocumentId;
        }

        public string GetDocumentTitle()
        {
            return Document.File.FileName;
        }
    }

}

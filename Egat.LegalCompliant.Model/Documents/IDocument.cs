﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Model.Documents
{
    public interface IDocument
    {
        long GetDocumentId();
        string GetDocumentTitle();
    }
}

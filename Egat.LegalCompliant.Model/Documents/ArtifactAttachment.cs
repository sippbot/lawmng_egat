﻿using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Party;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Documents
{
    public class ArtifactAttachment : IEntityBase, IDocument
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long ArtifactId { get; set; }
        public long DocumentId { get; set; }
        public long CreatorId { get; set; }
        public DateTime AttachDate { get; set; }

        [ForeignKey("DocumentId")]
        public virtual Document Document { get; set; }

        [ForeignKey("ArtifactId")]
        public virtual Artifact Artifact { get; set; }

        [ForeignKey("CreatorId")]
        public virtual User Creator { get; set; }

        public ArtifactAttachment() { }

        public ArtifactAttachment(long artifactId, long documentId, long creatorId)
        {
            ArtifactId = artifactId;
            DocumentId = documentId;
            CreatorId = creatorId;
            AttachDate = DateTime.Now;
        }

        public long GetDocumentId()
        {
            return DocumentId;
        }

        public string GetDocumentTitle()
        {
            return Document.File.FileName;
        }
    }
}

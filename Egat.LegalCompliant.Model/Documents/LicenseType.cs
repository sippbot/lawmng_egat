﻿using Egat.LegalCompliant.Model.Artifacts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Egat.LegalCompliant.Model.Documents
{
    public class LicenseType : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public LicenseCategory Category { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Issuer { get; set; }
        public int FrequencyYear { get; set; }
        public int FrequencyMonth { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<ArtifactNodeLicenseType> ArtifactNodeLicenseTypes { get; set; }

        public LicenseType()
        {
            ArtifactNodeLicenseTypes = new List<ArtifactNodeLicenseType>();
        }
        override public string ToString()
        {
            return Name;
        }
    }

    public enum LicenseCategory
    {
        Organization = 1,
        Personal = 2,
        Report = 3,
        Miscellaneous = 0,
        Tool = 4,
        Machine = 5
    }
}

﻿using System;
using Egat.LegalCompliant.Model.Party;
using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Documents
{
    public class License : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string LicenseNumber { get; set; }
        public string HolderName { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime DueDate { get; set; }
        public LicenseStatus Status { get; set; }
        public LicenseAlert Alert { get; set; }
        public bool IsOverride { get; set; }

        // Reference
        public long? LicenseHistoryId { get; set; } // if history id is null then we will threat license as sigle license
        [ForeignKey("LicenseHistoryId")]
        public LicenseHistory LicenseHistory { get; set; }

        public long LicenseTypeId { get; set; }
        [ForeignKey("LicenseTypeId")]
        public LicenseType LicenseType { get; set; }

        public long DocumentId { get; set; }
        [ForeignKey("DocumentId")]
        public Document Document { get; set; }

        public License() { }

        public bool IsExpired()
        {
            return DateTime.Compare(DateTime.Today, ExpireDate) > 0;
        }
    }

    public enum LicenseStatus
    {
        Active      = 0,
        Expired     = 1
    }
    public enum LicenseAlert
    {
        Idle    = 0,
        Alert   = 1
    }

    // Set Alert to Alert to indicate that this license need someone to takecare of.
}

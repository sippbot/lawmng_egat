﻿using Egat.LegalCompliant.Model.Party;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Egat.LegalCompliant.Model.Documents
{
    public class Document : IEntityBase, IDocument
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Title { get; set; }
        public DocumentType Type { get; set; }
        public bool IsPulic { get; set; }
        public DateTime CreateDate { get; set; }

        public long FileId { get; set; }
        [ForeignKey("FileId")]
        public virtual FileData File { get; set; }

        public Document() { }

        public Document(long fileId, string title, bool isPublic, DocumentType type)
        {
            FileId = fileId;
            Title = title;
            IsPulic = isPublic;
            CreateDate = DateTime.Now;
            Type = type;
        }

        public long GetDocumentId()
        {
            return Id;
        }

        public string GetDocumentTitle()
        {
            return File.FileName;
        }
    }

    public enum DocumentType
    {
        Document = 0,
        License = 1,
        Image = 2
    }
}
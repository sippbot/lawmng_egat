﻿using Egat.LegalCompliant.Model.Artifacts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Model.Party
{
    public class DepartmentSummary : IArtifact
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public ArtifactStatus Status { get; set; }
        public ArtifactType Type { get; set; }

        public DepartmentSummary() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Egat.LegalCompliant.Model.Authorizations;


namespace Egat.LegalCompliant.Model.Party
{
    public class UserGroup : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsSetPermission { get; set; }

        // for service role
        public ICollection<UserGroupRoleConstant> Roles { get; set; }

        public UserGroup() { }

        public UserGroup(UserGroup UserGroup)
        {
            // Perform deep copy here
            Id = UserGroup.Id;
            Name = UserGroup.Name;
            IsSetPermission = UserGroup.IsSetPermission;
            Roles = new HashSet<UserGroupRoleConstant>();
        }
    }

    // create by Toey 16/03/2018
    public class UserGroupRoleConstant
    {
        public long Id { get; set; }
        public RolesConstant Role { get; set; }
        public string Key { get; set; }
        public long GroupId { get; set; }
        public bool IsReadOnly { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Egat.LegalCompliant.Model.Artifacts;

namespace Egat.LegalCompliant.Model.Party
{
    public class Department : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
        public ICollection<ArtifactAssignment> ArtifactAssignment { get; set; }

        public Department() {
            this.IsActive = true;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

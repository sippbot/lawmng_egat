﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Egat.LegalCompliant.Model.Party
{
    public class User : IEntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public long GroupId { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
        [ForeignKey("GroupId")]
        public virtual UserGroup Group { get; set; }

        public User()
        {
            this.IsActive = true;
            this.Posts = new List<Post>();
        }

        public override string ToString()
        {
            return this.FirstName + " " + this.LastName;
        }
    }
}

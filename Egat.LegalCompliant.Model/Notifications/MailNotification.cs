﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Egat.LegalCompliant.Model.Notifications
{
    /*
     *  var info = new NotificationInfo(mail.recipientId, mail.recipientName, mail.recipient, 
     *  mail.subject, mail.message, mail.action, mail.buttonLabel, mail.buttonLink);
     * 
     * 
     */
    public class MailNotification : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long RecipientId { get; set; }
        public string RecipientName { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Action { get; set; }
        public string ButtonLabel { get; set; }
        public string ButtonLink { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime? SendDate { get; set; }
        public string Excerpt { get; set; }

        public NotificationType Type { get; set; }

        public bool IsSent { get; set; }

    }

    public enum NotificationType
    {
        Mail = 0,
        Other = 1,
    }
}

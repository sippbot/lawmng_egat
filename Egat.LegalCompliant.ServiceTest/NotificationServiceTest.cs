using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Net;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Diagnostics;
using Egat.LegalCompliant.Data;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Data.Repositories;
using Egat.LegalCompliant.API.Core;
using Egat.LegalCompliant.API.Services.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc.Internal;
using Egat.LegalCompliant.API.Models.Configurations;
using Hangfire;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.API.Helpers.Filters;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Service;
using AutoMapper;
using Egat.LegalCompliant.Service.Configurations;
using Egat.LegalCompliant.Service.Utilities;
using Egat.LegalCompliant.Service.Complaint;
using Microsoft.AspNetCore.ResponseCompression;
using Hangfire.Common;
using System;
using Xunit;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.ServiceTest
{
    public class NotificationServiceTest
    {
        private static string _applicationPath = string.Empty;
        private static string _contentRootPath = string.Empty;
        public IConfigurationRoot Configuration { get; }
        private IServiceProvider serviceProvider;

        public NotificationServiceTest(IHostingEnvironment env, IServiceCollection services)
        {

            _applicationPath = env.WebRootPath;
            _contentRootPath = env.ContentRootPath;
            var builder = new ConfigurationBuilder()
                .SetBasePath(_contentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
            services.AddScoped<INotificationService, EmailNotificationService>();

            // Artifact 
            services.AddScoped<IArtifactRepository, ArtifactRepository>();
            services.AddScoped<IArtifactNodeRepository, ArtifactNodeRepository>();
            services.AddScoped<IArtifactNodeGroupRepository, ArtifactNodeGroupRepository>();
            services.AddScoped<IArtifactCategoryRepository, ArtifactCategoryRepository>();
            services.AddScoped<IArtifactLevelRepository, ArtifactLevelRepository>();
            services.AddScoped<IArtifactSourceRepository, ArtifactSourceRepository>();
            services.AddScoped<IArtifactNodeLicenseTypeRepository, ArtifactNodeLicenseTypeRepository>();
            services.AddScoped<IArtifactAssignmentRepository, ArtifactAssignmentRepository>();

            services.AddScoped<IArtifactAttachmentRepository, ArtifactAttachmentRepository>();
            // Party
            services.AddScoped<IUserGroupRepository, UserGroupRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IDocumentRepository, DocumentRepository>();
            services.AddScoped<IFileDataRepository, FileDataRepository>();
            // Document
            services.AddScoped<ILicenseTypeRepository, LicenseTypeRepository>();
            services.AddScoped<ILicenseRepository, LicenseRepository>();
            services.AddScoped<IFileDataRepository, FileDataRepository>();
            services.AddScoped<IDocumentRepository, DocumentRepository>();
            // Task
            services.AddScoped<ITaskAssignmentRepository, TaskAssignmentRepository>();
            services.AddScoped<ITaskAssessmentRepository, TaskAssessmentRepository>();
            services.AddScoped<ITaskCommentRepository, TaskCommentRepository>();
            services.AddScoped<ITaskGroupRepository, TaskGroupRepository>();
            // Notification
            services.AddScoped<IMailNotificationRepository, MailNotificationRepository>();
            // Ticket
            services.AddScoped<ITicketRepository, TicketRepository>();
            // Authorization
            services.AddScoped<IUserGroupRolesRepository, UserGroupRolesRepository>();
            services.AddScoped<IUserRolesRepository, UserRolesRepository>();


            /****************************************************************
             * 
             *                    Other configuration
             * 
             ****************************************************************/
            // Configuration
            services.Configure<LdapConfiguration>(Configuration.GetSection("AthenticationServer:ldap"));
            services.Configure<FileServerConfiguration>(Configuration.GetSection("FileServer"));
            services.Configure<EmailNotificationServiceConfiguration>(Configuration.GetSection("Notification:Mail"));
            services.Configure<TicketServiceConfiguration>(Configuration.GetSection("Complaint"));
            services.Configure<SchedulerConfiguration>(Configuration.GetSection("Scheduler"));
            services.Configure<ServiceCommonConfiguration>(Configuration.GetSection("ServiceCommon"));
            services.Configure<ArtifactServiceConfiguration>(Configuration.GetSection("ArtifactService"));
            services.Configure<ViewRendererServiceConfiguration>(Configuration.GetSection("ViewRenderer"));
            services.Configure<DailyJobExecutorServiceConfiguration>(Configuration.GetSection("DailyJob"));
            // services.Configure<ControllerConfig>(Configuration.GetSection("LAWMNG:Assignment"));
            // services.Configure<DateTimeUtilConfiguration>(Configuration.GetSection("DateTimeUtil"));

            // Services
            services.AddScoped<IAuthenticationService, LdapAuthenticationService>();
            services.AddScoped<INotificationService, EmailNotificationService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IDailyJobExecutorService, DailyJobExecutorService>();
            services.AddScoped<IAccessControlService, AccessControlService>();
            // Service Layer
            services.AddScoped<IArtifactService, ArtifactService>();
            services.AddScoped<ITaskAssignmentService, TaskAssignmentService>();
            services.AddScoped<IDocumentService, DocumentService>();
            services.AddScoped<IViewRendererService, ViewRendererService>();
            services.AddScoped<IAutoMessageGenerator, AutoMessageGenerator>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Utilities
            services.AddSingleton<IViewModelMessageUtil, ViewModelMessageUtil>();

            serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void Test1()
        {
            License license = new License();
            license.Document = new Document();
            license.Status = LicenseStatus.Active;
            license.IssueDate = new DateTime(2016, 1, 1);
            license.LicenseType = new LicenseType();
            license.HolderName = "test test";

            INotificationService _service = serviceProvider.GetService<INotificationService>();

            _service.ResolveNotificationInfo("LicenseAlertNotification", license);
        }
    }
}

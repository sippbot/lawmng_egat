﻿using CsvHelper;
using Egat.LegalCompliant.Data.Importer;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Party;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Egat.LegalCompliant.Data
{
    public class LegalCompliantDbInitializer
    {
        private static LegalCompliantContext context;
        public static void Initialize(LegalCompliantContext legalCompliantContext)
        {
            context = legalCompliantContext;
            InitArtifactContext();
        }

        public static void Initialize(IServiceProvider serviceProvider)
        {
            context = (LegalCompliantContext)serviceProvider.GetService(typeof(LegalCompliantContext));
            InitArtifactContext();
        }

        private static void InitArtifactContext()
        {
            InitDepartment(context);
            InitUserGroup(context);
            InitUser(context);
            InitPost(context);
            InitLevel(context);
            InitCategory(context);
            //InitDocumet(context);
            InitLicenseType(context);
            InitUserGroupRoles(context);

            //Temporary insert
            //InitArtifactReader(context);

        }

        private static void InitDepartment(LegalCompliantContext context)
        {
            context.Database.EnsureCreated();
            if (context.Departments.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\department.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var departments = csvReader.GetRecords<Department>().ToList();
                    context.Departments.AddRange(departments);
                    context.SaveChanges();
                }
            }
        }
        private static void InitUserGroup(LegalCompliantContext context)
        {
            context.Database.EnsureCreated();
            if (context.UserGroups.Any())
            {
                return;   // DB has been seeded
            }

            using (FileStream stream = new FileStream(@"C:\seedData\usergroup.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var usergroup = csvReader.GetRecords<UserGroup>().ToList();
                    context.UserGroups.AddRange(usergroup);
                    context.SaveChanges();
                }
            }

            {
                var autoUserGroup = new UserGroup();
                // autoUserGroup.Id = LegalCompliantDataConstant.WEBSERVICE_AUTO_USER_GROUP_ID;
                autoUserGroup.Name = LegalCompliantDataConstant.WEBSERVICE_AUTO_USER_GROUP_NAME;
                context.Add(autoUserGroup);
                context.SaveChanges();
            }
        }
        private static void InitUser(LegalCompliantContext context)
        {
            context.Database.EnsureCreated();
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            using (FileStream stream = new FileStream(@"C:\seedData\user.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var users = csvReader.GetRecords<User>().ToList();
                    context.Users.AddRange(users);
                    context.SaveChanges();
                }
            }

            {
                // TODO move this block to config
                // Add System wide Auto User
                var autoUser = new User();
                autoUser.FirstName = LegalCompliantDataConstant.WEBSERVICE_AUTO_USER_FIRSTNAME;
                autoUser.LastName = LegalCompliantDataConstant.WEBSERVICE_AUTO_USER_LASTNAME;
                // autoUser.Id = LegalCompliantDataConstant.WEBSERVICE_AUTO_USER_ID;
                autoUser.IsActive = true;
                autoUser.Email = "";
                autoUser.GroupId = context
                    .UserGroups
                    .Single(
                    g => g.Name == LegalCompliantDataConstant.WEBSERVICE_AUTO_USER_GROUP_NAME)
                    .Id;
                context.Add(autoUser);
                context.SaveChanges();
            }
        }
        private static void InitPost(LegalCompliantContext context)
        {
            context.Database.EnsureCreated();
            if (context.Posts.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\post.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    while (csvReader.Read())
                    {
                        var post = csvReader.GetRecord<Post>();
                        var departmentId = csvReader.GetField<long>("DepartmentId");
                        post.Department = context.Departments.Single(d => d.Id == departmentId);

                        var userId = csvReader.GetField<long>("UserId");
                        post.User = context.Users.Single(u => u.Id == userId);

                        context.Posts.Add(post);
                    }
                    context.SaveChanges();
                }
            }
        }
        private static void InitLevel(LegalCompliantContext context)
        {
            if (context.ArtifactLevels.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\level.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var level = csvReader.GetRecords<ArtifactLevel>().ToList();
                    context.ArtifactLevels.AddRange(level);
                    context.SaveChanges();
                }
            }
        }
        private static void InitCategory(LegalCompliantContext context)
        {
            if (context.ArtifactCategories.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\category.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var level = csvReader.GetRecords<ArtifactCategory>().ToList();
                    context.ArtifactCategories.AddRange(level);
                    context.SaveChanges();
                }
            }
        }
        private static void InitDocumet(LegalCompliantContext context)
        {
            if (context.Documents.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\document.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var docs = csvReader.GetRecords<Document>().ToList();
                    context.Documents.AddRange(docs);
                    context.SaveChanges();
                }
            }
        }
        private static void InitLicenseType(LegalCompliantContext context)
        {
            if (context.LicenseTypes.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\licensetype.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var licenseTypes = csvReader.GetRecords<LicenseType>().ToList();
                    context.LicenseTypes.AddRange(licenseTypes);
                    context.SaveChanges();
                }
            }
        }
        private static void InitUserGroupRoles(LegalCompliantContext context)
        {
            if (context.UserGroupRoles.Any())
            {
                return;   // DB has been seeded
            }
            using (FileStream stream = new FileStream(@"C:\seedData\usergroupRoles.csv", FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var groupRoles = csvReader.GetRecords<UserGroupRoles>().ToList();
                    context.UserGroupRoles.AddRange(groupRoles);
                    context.SaveChanges();
                }
            }
        }
        /*
        private static void InitArtifactReader(LegalCompliantContext context)
        {
            if (context.ArtifactReaders.Any())
            {
                return;
            }
            using(FileStream stream = new FileStream(@"c:\seedData\artifact.csv", FileMode.Open, FileAccess.Read))
            {
                using(StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    CsvReader csvReader = new CsvReader(reader);
                    csvReader.Configuration.WillThrowOnMissingField = false;
                    var artifact = csvReader.GetRecords<ArtifactReader>().ToList();
                    // TODO remove temp table from this section
                    context.ArtifactReaders.AddRange(artifact);
                    context.SaveChanges();

                }
            }
        }
        */
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Data.Importer
{
    public class ArtifactReader
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string Note { get; set; }
        public string Introduction { get; set; }
        public string Source { get; set; }
        public string Category { get; set; }
        public string Level { get; set; }
    }
}

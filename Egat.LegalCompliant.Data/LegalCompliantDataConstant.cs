﻿//---------------------------------------------------------------------
// 
//                  EGAT LEGAL COMPLIANT PROJECT
//                      WEBSERVICE CONSTANTS.
//  
//---------------------------------------------------------------------

namespace Egat.LegalCompliant.Data
{
    static class LegalCompliantDataConstant
    {
        //-------------------------------------------------------------
        // 
        //                 WEBSERVICE NAME CONSTANT.
        //             
        //-------------------------------------------------------------   
        public const string WEBSERVICE_NAME = "ระบบติดตามการปฏิบัติตามกฎหมาย";

        //-------------------------------------------------------------
        // 
        //             WEBSERVICE AUTO USER CONSTANTS.
        //             
        //-------------------------------------------------------------             
        public const string WEBSERVICE_AUTO_USER_FIRSTNAME = "ระบบอัตโนมัติ";
        public const string WEBSERVICE_AUTO_USER_LASTNAME = "";
        public const long WEBSERVICE_AUTO_USER_ID = 1;

        //-------------------------------------------------------------
        // 
        //           WEBSERVICE AUTO USER GROUP CONSTANTS.
        //             
        //-------------------------------------------------------------    
        public const string WEBSERVICE_AUTO_USER_GROUP_NAME = "ระบบติดตามการปฏิบัติตามกฎหมาย";
        public const long WEBSERVICE_AUTO_USER_GROUP_ID = 1;
    }
}

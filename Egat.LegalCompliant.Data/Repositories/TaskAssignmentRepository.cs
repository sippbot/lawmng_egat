﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class TaskAssignmentRepository : EntityBaseRepository<TaskAssignment>, ITaskAssignmentRepository
    {
        public TaskAssignmentRepository(LegalCompliantContext context)
            : base(context)
        { }

        public void AddTaskAssignments(IEnumerable<TaskAssignment> taskAssignments)
        {
            DbContext.TaskAssignments.AddRange(taskAssignments);
        }

        public IEnumerable<TaskAssignment> CreateTaskAssigment(Artifact artifact, DateTime duedate)
        {
            List<TaskAssignment> taskAssignmentList = new List<TaskAssignment>();
            foreach (ArtifactNode node in artifact.Nodes)
            {
                foreach (ArtifactAssignment assignment in node.Departments)
                {
                    var resolveValidTo = assignment.ValidTo ?? DateTime.Now.AddDays(1);
                    if (!assignment.IsActive && (assignment.ValidFrom > DateTime.Now || resolveValidTo < DateTime.Now))
                    {
                        continue;
                    }
                    TaskAssignment task = new TaskAssignment();

                    task.Initial(assignment, duedate);

                    //* add by Toey 16/04/2018
                    task.IsActive = true;

                    taskAssignmentList.Add(task);
                    DbContext.TaskAssignments.Add(task);
                }
            }
            return taskAssignmentList;
        }

        public TaskAssignment CreateTaskAssigment(TaskAssignment previousTaskAssignment, DateTime duedate, TaskAssignmentType type)
        {
            var task = previousTaskAssignment.Fork();
            task.Initial(null, duedate);

            // DbContext.TaskAssignments.Add(task);

            return task;
        }

        public TaskAssignment GetFullDetail(long id)
        {
            var taskAssignment = DbContext.TaskAssignments
                .Where(t => t.Id == id)
                .Include(t => t.Department)
                .Include(t => t.Artifact)
                .Include(t => t.ArtifactNode)
                .Include(t => t.ArtifactNode).ThenInclude(n => n.Group)
                .Include(t => t.Assessments).ThenInclude(a => a.Creator)
                .Include(t => t.Assessments).ThenInclude(a => a.Reviewer)
                .Include(t => t.Comments).ThenInclude(c => c.Creator)
                .Include(t => t.Artifact.Category)
                .Include(t => t.Artifact.Level)
                .Include(t => t.Artifact.Source)
                .Include(t => t.Documents).ThenInclude(a => a.Document).ThenInclude(d => d.File)
                .Include(t => t.Tickets)

                //* edit by Toey 27/03/2018
                .Include(t => t.Tickets).ThenInclude(a => a.Owner)
                .Include(t => t.Tickets).ThenInclude(a => a.CloseBy)
                //*
                .FirstOrDefault();

            return taskAssignment;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentByArtifactId(long id)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => t.ArtifactId == id && t.Status != TaskAssignmentStatus.Approved && t.Status != TaskAssignmentStatus.Initial && t.Status != TaskAssignmentStatus.Created)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentByArtifactId(long id, bool is_active)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => t.ArtifactId == id && t.Status != TaskAssignmentStatus.Approved && t.Status != TaskAssignmentStatus.Initial
                && t.Status != TaskAssignmentStatus.Created && t.IsActive == is_active)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentByArtifactId(long id, TaskAssignmentStatus status)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => t.ArtifactId == id && t.Status == status)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetInitialTaskAssignmentsInPeriod(DateTime begin, DateTime end)
        {
            // ensure that input value is in right order or swap it
            if (DateTime.Compare(begin, end) > 0)
            {
                var temp = begin;
                begin = end;
                end = temp;
            }

            try
            {
                var tasks = DbContext.TaskAssignments
                                .Where(t => (DateTime.Compare(t.DueDate, end) <= 0 && DateTime.Compare(t.DueDate, begin) >= 0) &&
                                    t.Status == TaskAssignmentStatus.Created)
                                .Include(t => t.Department)
                                .Include(t => t.ArtifactNode)
                                .Include(t => t.Artifact)
                                .ToList();

                return tasks;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentByStatusExclude(TaskAssignmentStatus status, long departmentId, long artifactId)
        {
            var tasks = DbContext.TaskAssignments
                //.Where(t => t.ArtifactId == artifactId && t.DepartmentId == departmentId && (t.Status & status) == 0)

                //* Edit by Toey 15/04/2018
                .Where(t => t.ArtifactId == artifactId && t.DepartmentId == departmentId && (t.Status & status) == 0 && t.IsActive == true)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Assessments)
                .ToList();
            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentByStatus(TaskAssignmentStatus status, long departmentId, long artifactId)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => t.ArtifactId == artifactId && t.DepartmentId == departmentId && (t.Status & status) != 0)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Assessments)
                .ToList();
            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignments(ICollection<long> taskAssignmentIdList)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => taskAssignmentIdList.Any(l => l == t.Id))
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Assessments)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignments(ICollection<long> taskAssignmentIdList, TaskAssignmentStatus statusFlag)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => taskAssignmentIdList.Any(l => l == t.Id) && ((t.Status & statusFlag) != 0))
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Assessments)

                // edit by Toey 31/03/2018
                .Include(t => t.Documents)
                .Include(t => t.Comments)

                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignments()
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => t.Status != TaskAssignmentStatus.Approved)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignments(TaskAssignmentType type)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => t.Status != TaskAssignmentStatus.Approved && t.Type == type)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetTaskAssignmentsExclude(ICollection<long> taskAssignmentIdList, TaskAssignmentStatus statusFlag)
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => taskAssignmentIdList.Any(l => l == t.Id) && ((t.Status & statusFlag) == 0))
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Assessments)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public IEnumerable<TaskAssignment> GetOverDueTaskAssignments()
        {
            var tasks = DbContext.TaskAssignments
                .Where(t => DateTime.Compare(t.DueDate, DateTime.Today) < 0 &&
                    t.Status != TaskAssignmentStatus.Approved &&
                    t.Status != TaskAssignmentStatus.Confirmed &&
                    t.Status != TaskAssignmentStatus.Proposed &&
                    t.Status != TaskAssignmentStatus.Ticketed)
                .Include(t => t.Department)
                .Include(t => t.ArtifactNode)
                .Include(t => t.Assessments)
                .Include(t => t.Artifact)
                .ToList();

            return tasks;
        }

        public TaskAssignmentAttachment GetTaskAssignmentAttachment(long taskAssignmentAttachmentId)
        {
            var attachment = DbContext.TaskAssignmentAttachments.Where(a => a.Id == taskAssignmentAttachmentId)
                .Include(a => a.Document)
                    .ThenInclude(d => d.File)
                        .ThenInclude(f => f.Owner)
                .FirstOrDefault();
            return attachment;
        }

        public void DeleteTaskAssignmentAttachment(long taskAssignmentId, long documentId)
        {
            var _attachments = DbContext.TaskAssignmentAttachments.Where(a => a.TaskAssignmentId == taskAssignmentId && a.DocumentId == documentId).ToList();
            DbContext.TaskAssignmentAttachments.RemoveRange(_attachments);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class UserGroupRepository : EntityBaseRepository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(LegalCompliantContext context)
            : base(context)
        { }

        public IEnumerable<User> GetUserInUserGroup(long groupId)
        {
            return DbContext.Users.Where(u => u.GroupId == groupId && u.IsActive).ToList();
        }

        public bool IsGroupEmpty(long id)
        {
            return DbContext.Users.Where(u => u.GroupId == id).Count() == 0;
        }
    }
}

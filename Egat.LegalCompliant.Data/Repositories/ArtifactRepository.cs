﻿using System;
using System.Collections.Generic;
using System.Linq;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactRepository : EntityBaseRepository<Artifact>, IArtifactRepository
    {
        LegalCompliantContext _context;
        public ArtifactRepository(LegalCompliantContext context)
            : base(context)
        {
            _context = context;
        }

        public Artifact GetFullDetail(long id)
        {
            Artifact artifact = _context.Artifacts
                .Where(t => t.Id == id)
                .Include(c => c.Category)
                .Include(c => c.Level)
                .Include(c => c.Source)
                .Include(c => c.Documents)
                    .ThenInclude(d => d.Document)
                        .ThenInclude(d => d.File)
                .FirstOrDefault();

            if (artifact != null)
            {
                _context.Entry(artifact).Collection(x => x.NodeGroups).Load();
                // _context.Entry(artifact).Collection(x => x.Documents).Load();

                foreach (ArtifactNodeGroup group in artifact.NodeGroups)
                {
                    // TODO Enable This After open close feature
                    // if (group.IsActive == false) continue;

                    _context.Entry(group).Collection(x => x.Nodes).Load();
                    foreach (ArtifactNode node in group.Nodes)
                    {
                        // TODO Enable This After open close feature
                        // if (node.IsActive == false) continue;

                        _context.Entry(node).Collection(x => x.RequiredLicenses).Load();

                        foreach (ArtifactNodeLicenseType license in node.RequiredLicenses)
                        {
                            _context.Entry(license).Reference(l => l.LicenseType).Load();
                        }

                        _context.Entry(node).Collection(x => x.Departments).Load();

                        foreach (ArtifactAssignment department in node.Departments)
                        {
                            _context.Entry(department).Reference(l => l.Department).Load();
                        }
                    }
                }
            }
            return artifact;
        }

        public Artifact GetFullDetail_isActive(long id, bool isActive)
        {
            Artifact artifact;
            if (isActive)
            {
                artifact = _context.Artifacts
                                .Where(t => t.Id == id && t.IsActive == true)
                                .Include(c => c.Category)
                                .Include(c => c.Level)
                                .Include(c => c.Source)
                                .Include(c => c.Documents)
                                    .ThenInclude(d => d.Document)
                                        .ThenInclude(d => d.File)
                                .FirstOrDefault();

                if (artifact != null)
                {
                    _context.Entry(artifact).Collection(x => x.NodeGroups).Query().Where(a => a.IsActive == true).Load();

                    foreach (ArtifactNodeGroup group in artifact.NodeGroups)
                    {
                        _context.Entry(group).Collection(x => x.Nodes).Query().Where(a => a.IsActive == true).Load();

                        foreach (ArtifactNode node in group.Nodes)
                        {
                            _context.Entry(node).Collection(x => x.RequiredLicenses).Load();

                            foreach (ArtifactNodeLicenseType license in node.RequiredLicenses)
                            {
                                _context.Entry(license).Reference(l => l.LicenseType).Load();
                            }

                            _context.Entry(node).Collection(x => x.Departments).Load();

                            foreach (ArtifactAssignment department in node.Departments)
                            {
                                _context.Entry(department).Reference(l => l.Department).Load();
                            }
                        }
                    }
                }
            }
            else
            {
                artifact = _context.Artifacts
                               .Where(t => t.Id == id)
                               .Include(c => c.Category)
                               .Include(c => c.Level)
                               .Include(c => c.Source)
                               .Include(c => c.Documents)
                                   .ThenInclude(d => d.Document)
                                       .ThenInclude(d => d.File)
                               .FirstOrDefault();

                if (artifact != null)
                {
                    _context.Entry(artifact).Collection(x => x.NodeGroups).Query().Where(a => a.Nodes.Any(n => n.IsActive == false)).Load();

                    foreach (ArtifactNodeGroup group in artifact.NodeGroups)
                    {
                        _context.Entry(group).Collection(x => x.Nodes).Query().Where(a => a.IsActive == false).Load();

                        foreach (ArtifactNode node in group.Nodes)
                        {
                            _context.Entry(node).Collection(x => x.RequiredLicenses).Load();

                            foreach (ArtifactNodeLicenseType license in node.RequiredLicenses)
                            {
                                _context.Entry(license).Reference(l => l.LicenseType).Load();
                            }

                            _context.Entry(node).Collection(x => x.Departments).Load();

                            foreach (ArtifactAssignment department in node.Departments)
                            {
                                _context.Entry(department).Reference(l => l.Department).Load();
                            }
                        }
                    }
                }
            }

            return artifact;
        }

        public Artifact GetAllArtifactNodes(long id)
        {
            Artifact artifact = _context.Artifacts
                .Where(t => t.Id == id)
                .Include(c => c.Category)
                .Include(c => c.Level)
                .Include(c => c.Source)
                .FirstOrDefault();

            if (artifact != null)
            {
                _context.Entry(artifact).Collection(x => x.Nodes).Load();

                foreach (ArtifactNode node in artifact.Nodes)
                {
                    // TODO Enable This After open close feature
                    // if (node.IsActive == false) continue;

                    _context.Entry(node).Collection(x => x.RequiredLicenses).Load();

                    foreach (ArtifactNodeLicenseType license in node.RequiredLicenses)
                    {
                        _context.Entry(license).Reference(l => l.LicenseType).Load();
                    }

                    _context.Entry(node).Collection(x => x.Departments).Load();

                    foreach (ArtifactAssignment department in node.Departments)
                    {
                        _context.Entry(department).Reference(l => l.Department).Load();
                    }
                }
            }
            return artifact;
        }

        public IEnumerable<Artifact> GetAllByStatus(ArtifactStatus status)
        {
            return _context.Artifacts
                // TODO enable this after add delete and clean db
                //.Where(t => ((t.Status & status) != 0) && t.IsActive)
                .Where(t => ((t.Status & status) != 0))
                .Include(c => c.Category)
                .Include(c => c.Level)
                .Include(c => c.Source)
                .ToList();
        }

        public IEnumerable<Artifact> GetAllArtifacts()
        {
            return _context.Artifacts
                .Include(c => c.Category)
                .Include(c => c.Level)
                .Include(c => c.Source)
                .Include(c => c.Nodes)
                    .ThenInclude(n => n.Departments)
                        .ThenInclude(d => d.Department)
                .ToList();
        }

        public IEnumerable<Artifact> GetAllCancelLaw()
        {
            return _context.Artifacts
                .Where(a => a.Nodes.Any(n => n.IsActive == false))
                .Include(c => c.Category)
                .Include(c => c.Level)
                .Include(c => c.Source)
                .Include(c => c.Nodes)
                    .ThenInclude(n => n.Departments)
                        .ThenInclude(d => d.Department)
                .ToList();
        }
    }
}

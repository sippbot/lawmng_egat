﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Artifacts;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class DepartmentRepository : EntityBaseRepository<Department>, IDepartmentRepository
    {
        private readonly LegalCompliantContext _context;
        public DepartmentRepository(LegalCompliantContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<Department> GetDepartmentByArtifactId(long id)
        {
            IEnumerable<Department> _result = new HashSet<Department>();
            Dictionary<long, Department> map = new Dictionary<long, Department>();

            var Nodes = _context.ArtifactNodes.Where(a => a.ArtifactId == id)
                .Include(a => a.Departments)
                    .ThenInclude(d => d.Department)
                .ToList();

            foreach (ArtifactNode node in Nodes)
            {
                // _context.Entry(node).Collection(x => x.Departments).Load();
                foreach (var department in node.Departments)
                {
                    if (map.ContainsKey(department.DepartmentId))
                    {
                        continue;
                    }
                    // _context.Entry(department).Reference(x => x.Department).Load();

                    map.Add(department.DepartmentId, department.Department);
                }
            }

            _result = map.Select(kvp => kvp.Value).ToList();

            return _result;
        }
    }
}

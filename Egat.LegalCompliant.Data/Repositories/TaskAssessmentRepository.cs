﻿using System;
using System.Linq;
using System.Collections.Generic;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class TaskAssessmentRepository : EntityBaseRepository<TaskAssessment>, ITaskAssessmentRepository
    {
        public TaskAssessmentRepository(LegalCompliantContext context)
            : base(context)
        { }

        public IEnumerable<TaskAssessment> GetTaskAssessmentByTaskAssignmentId(long taskAssignmentId)
        {
            return DbContext.TaskAssessments
                .Where(t => t.TaskAssignmentId == taskAssignmentId)
                .Include(t=>t.TaskAssignment)
                .Include(t=>t.Creator)
                .Include(t=>t.Reviewer)
                .ToList();
        }
    }
}

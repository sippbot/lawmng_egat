﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Tickets;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class TicketRepository : EntityBaseRepository<Ticket>, ITicketRepository
    {
        public TicketRepository(LegalCompliantContext context)
            : base(context)
        { }
        public IEnumerable<Ticket> GetAllTicket(TicketStatus status)
        {
            return DbContext.Tickets.Where(t => t.Status == status)
                .Include(t => t.Department)
                .Include(t => t.Owner)
                .Include(t => t.CloseBy)
                .Include(t => t.TaskAssignment)
                .ToList();
        }

        public IEnumerable<Ticket> GetAllByDepartmentId(long departmentId)
        {
            return DbContext.Tickets.Where(t => t.DepartmentId == departmentId)
                .Include(t => t.Department)
                .Include(t => t.Owner)
                .Include(t => t.CloseBy)
                .Include(t => t.TaskAssignment)
                .ToList();
        }

        public IEnumerable<Ticket> GetAllByDepartmentId(long departmentId, TicketStatus status)
        {
            return DbContext.Tickets.Where(t => t.DepartmentId == departmentId && t.Status == status)
                .Include(t => t.Department)
                .Include(t => t.Owner)
                .Include(t => t.CloseBy)
                .Include(t => t.TaskAssignment)
                .ToList();
        }
    }
}

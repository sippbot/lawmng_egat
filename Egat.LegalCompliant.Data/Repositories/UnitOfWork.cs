﻿using Egat.LegalCompliant.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private LegalCompliantContext _context;

        public UnitOfWork(LegalCompliantContext context)
        {
            _context = context;
        }

        public LegalCompliantContext DbContext
        {
            get { return _context; }
        }

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        #region Disposible

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}

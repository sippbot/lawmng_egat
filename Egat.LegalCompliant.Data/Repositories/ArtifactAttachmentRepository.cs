﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactAttachmentRepository : EntityBaseRepository<ArtifactAttachment>, IArtifactAttachmentRepository
    {
        public ArtifactAttachmentRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class PostRepository : EntityBaseRepository<Post>, IPostRepository
    {
        private readonly LegalCompliantContext _context;
        public PostRepository(LegalCompliantContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<Post> GetAllPostByDepartment(long id)
        {
            IEnumerable<Post> post = _context.Posts
               .Where(p => p.DepartmentId == id)
               .Include(p => p.Department)
               .Include(p => p.User)
               .ToList();

            return post;
        }
    }
}

﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactNodeGroupRepository : EntityBaseRepository<ArtifactNodeGroup>, IArtifactNodeGroupRepository
    {
        public ArtifactNodeGroupRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

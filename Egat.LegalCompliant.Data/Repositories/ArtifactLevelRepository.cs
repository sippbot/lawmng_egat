﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactLevelRepository : EntityBaseRepository<ArtifactLevel>, IArtifactLevelRepository
    {
        public ArtifactLevelRepository(LegalCompliantContext context)
            : base(context)
        { }

        public bool IsLevelEmpty(long id)
        {
            return DbContext.Artifacts.Where(a => a.LevelId == id).Count() == 0;
        }
    }
}
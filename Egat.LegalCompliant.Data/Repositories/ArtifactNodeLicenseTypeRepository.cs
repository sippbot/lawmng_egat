﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactNodeLicenseTypeRepository : EntityBaseRepository<ArtifactNodeLicenseType>, IArtifactNodeLicenseTypeRepository
    {
        public ArtifactNodeLicenseTypeRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}
﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Documents;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class DocumentRepository : EntityBaseRepository<Document>, IDocumentRepository
    {
        public DocumentRepository(LegalCompliantContext context)
            : base(context)
        { }

        public IEnumerable<Document> GetDocumentsByUserId(long userId)
        {
            return DbContext.Documents.Where(d => d.File.OwnerId == userId || d.IsPulic)
                .Include(d => d.File)
                    .ThenInclude(f => f.Owner)
                .ToList();
        }

        public Document GetDocument(long documentId)
        {
            return DbContext.Documents.Where(d => d.Id == documentId)
                .Include(d => d.File)
                    .ThenInclude(f => f.Owner)
                .FirstOrDefault();
        }
    }
}

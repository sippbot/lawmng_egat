﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactCategoryRepository : EntityBaseRepository<ArtifactCategory>, IArtifactCategoryRepository
    {
        public ArtifactCategoryRepository(LegalCompliantContext context)
            : base(context)
        { }
        public bool IsCategoryEmpty(long id)
        {
            return DbContext.Artifacts.Where(a => a.CategoryId == id).Count() == 0;
        }
    }
}
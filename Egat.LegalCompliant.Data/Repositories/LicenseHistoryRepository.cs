﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class LicenseHistoryRepository : EntityBaseRepository<LicenseHistory>, ILicenseHistoryRepository
    {
        public LicenseHistoryRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

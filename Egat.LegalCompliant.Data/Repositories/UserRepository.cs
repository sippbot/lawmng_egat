﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class UserRepository : EntityBaseRepository<User>, IUserRepository
    {
        public UserRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Tasks;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class TaskCommentRepository : EntityBaseRepository<TaskComment>, ITaskCommentRepository
    {
        public TaskCommentRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

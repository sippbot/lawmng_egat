﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class FileDataRepository : EntityBaseRepository<FileData>, IFileDataRepository
    {
        public FileDataRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

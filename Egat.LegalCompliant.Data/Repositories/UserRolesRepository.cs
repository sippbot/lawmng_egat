﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Model.Party;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class UserRolesRepository : EntityBaseRepository<UserRoles>, IUserRolesRepository
    {
        public UserRolesRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

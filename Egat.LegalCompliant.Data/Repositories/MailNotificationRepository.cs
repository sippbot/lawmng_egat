﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Notifications;
using System.Collections.Generic;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class MailNotificationRepository : EntityBaseRepository<MailNotification>, IMailNotificationRepository
    {
        public MailNotificationRepository(LegalCompliantContext context)
            : base(context)
        { }

        public IEnumerable<MailNotification> GetMailNotificationByUserId(long userId)
        {
            var notiList = DbContext.MailNotifications.Where(m => m.RecipientId == userId).OrderByDescending(m => m.CreateDate).Take(200);
            return notiList;
        }
    }
}

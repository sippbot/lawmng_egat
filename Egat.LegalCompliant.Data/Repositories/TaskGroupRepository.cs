﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Tasks;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class TaskGroupRepository : EntityBaseRepository<TaskGroup>, ITaskGroupRepository
    {
        public TaskGroupRepository(LegalCompliantContext context)
            : base(context)
        { }

        public TaskGroup GetSingleIncludeTaskAssignment(long groupId)
        {
            return DbContext.TaskGroups.Where(g => g.Id == groupId)
                .Include(g => g.Assignments)
                    .ThenInclude(a => a.TaskAssignment)
                .FirstOrDefault();

        }
    }
}

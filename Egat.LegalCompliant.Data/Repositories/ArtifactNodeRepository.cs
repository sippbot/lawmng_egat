﻿using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Data.Abstract;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class ArtifactNodeRepository : EntityBaseRepository<ArtifactNode>, IArtifactNodeRepository
    {
        public ArtifactNodeRepository(LegalCompliantContext context)
            : base(context)
        { }
        public void DeleteArtifactAssignment(long artifactNodeId)
        {
            var deleteAssignmentList = DbContext.ArtifactAssignments.Where(aa => aa.ArtifactNodeId == artifactNodeId).ToList();
            DbContext.ArtifactAssignments.RemoveRange(deleteAssignmentList);
        }
        public void DeleteRequiredLicense(long artifactNodeId)
        {
            var deleteLicesnseList = DbContext.ArtifactNodeLicenseTypes.Where(rl => rl.ArtifactNodeId == artifactNodeId).ToList();
            DbContext.ArtifactNodeLicenseTypes.RemoveRange(deleteLicesnseList);
        }
        public bool InArtifactNodeHasTaskAssignment(long artifactNodeId)
        {
            return DbContext.TaskAssignments.Where(t => t.ArtifactNodeId == artifactNodeId).Count() > 0;
            // Check law
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Documents;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class LicenseRepository : EntityBaseRepository<License>, ILicenseRepository
    {
        public LicenseRepository(LegalCompliantContext context)
            : base(context)
        { }

        public IEnumerable<License> GetLicensesByUserId(long userId)
        {
            return DbContext.Licenses.Where(l => l.Document.File.OwnerId == userId || l.Document.IsPulic)
                .Include(l => l.Document)
                    .ThenInclude(d => d.File)
                        .ThenInclude(d => d.Owner)
                .Include(l => l.LicenseType)
                .ToList();
        }

        public IEnumerable<License> GetLicenses()
        {
            return DbContext.Licenses
                .Include(l => l.Document)
                    .ThenInclude(d => d.File)
                        .ThenInclude(d => d.Owner)
                .Include(l => l.LicenseType)
                .ToList();
        }

        public IEnumerable<License> GetLicensesByDueDate(DateTime begin, DateTime end)
        {
            // ensure that input value is in right order or swap it
            if (DateTime.Compare(begin, end) > 0)
            {
                var temp = begin;
                begin = end;
                end = temp;
            }

            return DbContext.Licenses.Where(l => (DateTime.Compare(l.DueDate, end) <= 0 && DateTime.Compare(l.DueDate, begin) >= 0)
            && l.Status == LicenseStatus.Active && l.Alert == LicenseAlert.Idle)
                .Include(l => l.Document)
                    .ThenInclude(d => d.File)
                        .ThenInclude(d => d.Owner)
                .Include(l => l.LicenseType)
                .ToList();
        }

        private void UpdateExpiredFlag()
        {
            var licenseList = DbContext.Licenses.Where(l => l.Status == LicenseStatus.Active && DateTime.Compare(DateTime.Today, l.ExpireDate) > 0 ).ToList();
            foreach(var license in licenseList)
            {
                if (license.IsExpired())
                {
                    license.Status = LicenseStatus.Expired;
                    license.Alert = LicenseAlert.Idle;
                }
                    
            }

            DbContext.Licenses.UpdateRange(licenseList);
            Commit();
        }

        public IEnumerable<License> GetExpiredLicense()
        {
            UpdateExpiredFlag();

            return DbContext.Licenses.Where(l => l.Status == LicenseStatus.Expired && l.Alert == LicenseAlert.Idle)
                .Include(l => l.Document)
                    .ThenInclude(d => d.File)
                        .ThenInclude(d => d.Owner)
                .Include(l => l.LicenseType)
                .ToList();
        }
    }
}

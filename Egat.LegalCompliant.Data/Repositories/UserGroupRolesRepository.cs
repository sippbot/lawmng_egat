﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Authorizations;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class UserGroupRolesRepository : EntityBaseRepository<UserGroupRoles>, IUserGroupRolesRepository
    {
        public UserGroupRolesRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

﻿using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Documents;

namespace Egat.LegalCompliant.Data.Repositories
{
    public class LicenseTypeRepository : EntityBaseRepository<LicenseType>, ILicenseTypeRepository
    {
        public LicenseTypeRepository(LegalCompliantContext context)
            : base(context)
        { }
    }
}

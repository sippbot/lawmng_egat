﻿using Egat.LegalCompliant.Model.Artifacts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Linq;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Tickets;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Data.Importer;

namespace Egat.LegalCompliant.Data
{
    public class LegalCompliantContext : DbContext
    {
        // Artifacts
        public DbSet<Artifact> Artifacts { get; set; }
        public DbSet<ArtifactNodeGroup> ArtifactNodeGroups { get; set; }
        public DbSet<ArtifactNode> ArtifactNodes { get; set; }
        public DbSet<ArtifactLevel> ArtifactLevels { get; set; }
        public DbSet<ArtifactCategory> ArtifactCategories { get; set; }
        public DbSet<ArtifactSource> ArtifactSources { get; set; }
        public DbSet<ArtifactNodeLicenseType> ArtifactNodeLicenseTypes { get; set; }
        public DbSet<ArtifactAssignment> ArtifactAssignments { get; set; }
        // Documents
        public DbSet<License> Licenses { get; set; }
        public DbSet<LicenseType> LicenseTypes { get; set; }
        public DbSet<LicenseHistory> LicenseHistories { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<FileData> FileDatas { get; set; }
        public DbSet<TaskAssignmentAttachment> TaskAssignmentAttachments { get; set; }
        public DbSet<ArtifactAttachment> ArtifactAttachments { get; set; }
        // Party
        public DbSet<Department> Departments { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        // Notifications
        public DbSet<MailNotification> MailNotifications { get; set; }
        // Tasks
        public DbSet<TaskAssignment> TaskAssignments { get; set; }
        public DbSet<TaskAssessment> TaskAssessments { get; set; }
        public DbSet<TaskComment> TaskComments { get; set; }
        public DbSet<TaskGroup> TaskGroups { get; set; }
        // Ticket
        public DbSet<Ticket> Tickets { get; set; }
        // Authorization
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<UserGroupRoles> UserGroupRoles { get; set; }

        // Data Importer
        // public DbSet<ArtifactReader> ArtifactReaders { get; set; }

        public LegalCompliantContext(DbContextOptions options) : base(options){ }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            /*********************************************
             * 
             *                  Artifact
             * 
             *********************************************/
            modelBuilder.Entity<Artifact>()
                .ToTable("Artifact");

            modelBuilder.Entity<Artifact>()
                .Property(a => a.CategoryId)
                .IsRequired();

            modelBuilder.Entity<Artifact>()
                .Property(s => s.CreateDate)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Artifact>()
                .Property(s => s.LastModified)
                .HasDefaultValue(DateTime.Now);

            modelBuilder.Entity<Artifact>()
                .Property(s => s.Type)
                .HasDefaultValue(ArtifactType.Miscellaneous);

            modelBuilder.Entity<Artifact>()
                .Property(s => s.Status)
                .HasDefaultValue(ArtifactStatus.Draft);

            modelBuilder.Entity<Artifact>()
                .HasOne(s => s.Category);
            //    .WithMany(c => c.SchedulesCreated);

            /*********************************************
             * 
             *                  License
             * 
             *********************************************/
            modelBuilder.Entity<License>()
                .ToTable("License");

            modelBuilder.Entity<ArtifactNodeLicenseType>()
                .HasKey(a => new { a.ArtifactNodeId, a.LicenseTypeId });
            modelBuilder.Entity<ArtifactNodeLicenseType>()
                .HasOne(a => a.LicenseType)
                .WithMany(l => l.ArtifactNodeLicenseTypes)
                .HasForeignKey(a => a.LicenseTypeId);
            modelBuilder.Entity<ArtifactNodeLicenseType>()
                .HasOne(l => l.ArtifactNode)
                .WithMany(a => a.RequiredLicenses)
                .HasForeignKey(l => l.ArtifactNodeId);

            modelBuilder.Entity<License>()
                .HasOne(l => l.LicenseHistory)
                .WithMany(h => h.License)
                .HasForeignKey(l => l.LicenseHistoryId);

            /*
            modelBuilder.Entity<License>()
                .Property(u => u.Name)
                .HasMaxLength(100)
                .IsRequired();
                 
            modelBuilder.Entity<LicenseType>()
                .HasOne(a => a.User)
                .WithMany(u => u.SchedulesAttended)
                .HasForeignKey(a => a.UserId);

            modelBuilder.Entity<LicenseType>()
                .HasOne(a => a.Schedule)
                .WithMany(s => s.Attendees)
                .HasForeignKey(a => a.ScheduleId);
            */

            /*********************************************
            * 
            *                  LicenseType
            * 
            *********************************************/
            modelBuilder.Entity<LicenseType>()
                .ToTable("LicenseType");

        }
    }
}
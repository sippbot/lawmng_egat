﻿using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Tickets;
using System;
using System.Collections.Generic;

namespace Egat.LegalCompliant.Data.Abstract
{
    // Artifact

    public interface IArtifactRepository : IEntityBaseRepository<Artifact>
    {
        Artifact GetFullDetail(long id);
        Artifact GetAllArtifactNodes(long id);
        IEnumerable<Artifact> GetAllByStatus(ArtifactStatus status);
        IEnumerable<Artifact> GetAllArtifacts();

        IEnumerable<Artifact> GetAllCancelLaw();
        Artifact GetFullDetail_isActive(long id, bool isActive);
    }

    public interface IArtifactNodeGroupRepository : IEntityBaseRepository<ArtifactNodeGroup> { }

    // create by Toey 03/04/2018
    public interface IArtifactAttachmentRepository : IEntityBaseRepository<ArtifactAttachment> { }

    public interface IArtifactNodeRepository : IEntityBaseRepository<ArtifactNode>
    {
        void DeleteArtifactAssignment(long artifactNodeId);
        void DeleteRequiredLicense(long artifactNodeId);
        bool InArtifactNodeHasTaskAssignment(long artifactNodeId);
    }

    public interface IArtifactCategoryRepository : IEntityBaseRepository<ArtifactCategory>
    {
        bool IsCategoryEmpty(long id);
    }

    public interface IArtifactLevelRepository : IEntityBaseRepository<ArtifactLevel>
    {
        bool IsLevelEmpty(long id);
    }

    public interface IArtifactSourceRepository : IEntityBaseRepository<ArtifactSource> { }

    public interface IArtifactNodeLicenseTypeRepository : IEntityBaseRepository<ArtifactNodeLicenseType> { }

    public interface IArtifactAssignmentRepository : IEntityBaseRepository<ArtifactAssignment> { }
    // Document

    public interface ILicenseTypeRepository : IEntityBaseRepository<LicenseType> { }

    public interface ILicenseRepository : IEntityBaseRepository<License>
    {
        IEnumerable<License> GetLicensesByUserId(long userId);
        IEnumerable<License> GetLicenses();
        IEnumerable<License> GetLicensesByDueDate(DateTime begin, DateTime end);
        IEnumerable<License> GetExpiredLicense();
    }

    public interface ILicenseHistoryRepository : IEntityBaseRepository<LicenseHistory> { }

    public interface IDocumentRepository : IEntityBaseRepository<Document>
    {
        IEnumerable<Document> GetDocumentsByUserId(long userId);
        Document GetDocument(long documentId);
    }

    public interface IFileDataRepository : IEntityBaseRepository<FileData> { }

    // public interface ILicenseTypeRepository : IEntityBaseRepository<LicenseType> { }

    // Party

    public interface IUserRepository : IEntityBaseRepository<User> { }

    public interface IUserGroupRepository : IEntityBaseRepository<UserGroup>
    {
        IEnumerable<User> GetUserInUserGroup(long groupId);
        bool IsGroupEmpty(long id);
    }

    public interface IDepartmentRepository : IEntityBaseRepository<Department>
    {
        IEnumerable<Department> GetDepartmentByArtifactId(long id);
    }

    public interface IPostRepository : IEntityBaseRepository<Post>
    {
        IEnumerable<Post> GetAllPostByDepartment(long id);
    }

    // Task

    public interface ITaskAssignmentRepository : IEntityBaseRepository<TaskAssignment>
    {
        void AddTaskAssignments(IEnumerable<TaskAssignment> taskAssignments);
        IEnumerable<TaskAssignment> CreateTaskAssigment(Artifact artifact, DateTime duedate);
        TaskAssignment CreateTaskAssigment(TaskAssignment taskAssignment, DateTime duedate, TaskAssignmentType type);
        IEnumerable<TaskAssignment> GetTaskAssignmentByArtifactId(long id);
        IEnumerable<TaskAssignment> GetTaskAssignmentByArtifactId(long id, TaskAssignmentStatus status);

        // Create By Toey 08/04/2018
        IEnumerable<TaskAssignment> GetTaskAssignmentByArtifactId(long id, bool is_active);

        IEnumerable<TaskAssignment> GetTaskAssignmentByStatus(TaskAssignmentStatus status, long departmentId, long artifactId);
        IEnumerable<TaskAssignment> GetTaskAssignmentByStatusExclude(TaskAssignmentStatus status, long departmentId, long artifactId);
        IEnumerable<TaskAssignment> GetTaskAssignments();
        IEnumerable<TaskAssignment> GetOverDueTaskAssignments();
        IEnumerable<TaskAssignment> GetTaskAssignments(TaskAssignmentType type);
        IEnumerable<TaskAssignment> GetTaskAssignments(ICollection<long> taskAssignmentIdList);
        IEnumerable<TaskAssignment> GetTaskAssignments(ICollection<long> taskAssignmentIdList, TaskAssignmentStatus status);
        IEnumerable<TaskAssignment> GetTaskAssignmentsExclude(ICollection<long> taskAssignmentIdList, TaskAssignmentStatus status);
        IEnumerable<TaskAssignment> GetInitialTaskAssignmentsInPeriod(DateTime begin, DateTime end);
        TaskAssignment GetFullDetail(long id);
        TaskAssignmentAttachment GetTaskAssignmentAttachment(long taskAssignmentAttachmentId);
        void DeleteTaskAssignmentAttachment(long taskAssignmentId, long documentId);
    }

    public interface ITaskAssessmentRepository : IEntityBaseRepository<TaskAssessment>
    {
        IEnumerable<TaskAssessment> GetTaskAssessmentByTaskAssignmentId(long taskAssignmentId);
    }

    public interface ITaskCommentRepository : IEntityBaseRepository<TaskComment> { }

    public interface ITaskGroupRepository : IEntityBaseRepository<TaskGroup>
    {
        TaskGroup GetSingleIncludeTaskAssignment(long groupId);
    }

    // Notification

    public interface IMailNotificationRepository : IEntityBaseRepository<MailNotification>
    {
        IEnumerable<MailNotification> GetMailNotificationByUserId(long userId);
    }

    // Ticket

    public interface ITicketRepository : IEntityBaseRepository<Ticket>
    {
        IEnumerable<Ticket> GetAllTicket(TicketStatus status);
        IEnumerable<Ticket> GetAllByDepartmentId(long departmentId);
        IEnumerable<Ticket> GetAllByDepartmentId(long departmentId, TicketStatus status);
    }

    // Authorization

    public interface IUserGroupRolesRepository : IEntityBaseRepository<UserGroupRoles> { }
    public interface IUserRolesRepository : IEntityBaseRepository<UserRoles> { }
}

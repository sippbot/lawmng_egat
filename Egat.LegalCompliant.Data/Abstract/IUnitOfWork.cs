﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Data.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}

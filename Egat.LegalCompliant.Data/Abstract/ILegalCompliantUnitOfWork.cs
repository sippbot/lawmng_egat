﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Egat.LegalCompliant.Data.Abstract
{
    interface ILegalCompliantUnitOfWork : IUnitOfWork
    {
        // Artifact

        IArtifactRepository ArtifactRepository { get; }

        IArtifactNodeGroupRepository ArtifactNodeGroupRepository { get; }

        IArtifactNodeRepository ArtifactNodeRepository { get; }

        IArtifactCategoryRepository ArtifactCategoryRepository { get; }

        IArtifactLevelRepository ArtifactLevelRepository { get; }

        IArtifactSourceRepository ArtifactSourceRepository { get; }

        IArtifactNodeLicenseTypeRepository ArtifactNodeLicenseTypeRepository { get; }

        IArtifactAssignmentRepository ArtifactAssignmentRepository { get; }

        // create by Toey 03/04/2018
        IArtifactAttachmentRepository ArtifactAttachmentRepository { get; }

        // Document

        ILicenseTypeRepository LicenseTypeRepository { get; }

        ILicenseRepository LicenseRepository { get; }

        IDocumentRepository DocumentRepository { get; }

        IFileDataRepository FileDataRepository { get; }

        // Party

        IUserRepository UserRepository { get; }

        IUserGroupRepository UserGroupRepository { get; }

        IDepartmentRepository DepartmentRepository { get; }

        IPostRepository PostRepository { get; }

        // Task

        ITaskAssignmentRepository TaskAssignmentRepository { get; }

        ITaskAssessmentRepository TaskAssessmentRepository { get; }

        ITaskCommentRepository TaskCommentRepository { get; }

        // Notification

        IMailNotificationRepository MailNotificationRepository { get; }

        // Ticket

        ITicketRepository TicketRepository { get; }
    }
}

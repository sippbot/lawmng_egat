﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;

namespace Egat.LegalCompliant.API.Utilities.ViewModelMessage
{
    public class ViewModelMessageUtil : IViewModelMessageUtil
    {
        private readonly IConfigurationRoot Configuration;
        public ViewModelMessageUtil(IHostingEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("viewmodel-messages.json")
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public string GetMessage(Type type, string key)
        {
            if (type == null || key == null)
            {
                return String.Empty;
            }
            var fullname = type.FullName;
            var section = Configuration.GetSection(fullname.Substring(fullname.LastIndexOf(".")+1));
            return section.GetValue<string>(key);
        }

        public string GetMessage(Object value)
        {
            Type type = value.GetType();
            string valueString = value.ToString();
            return GetMessage(type, valueString);

        }
    }
}

﻿using System;

namespace Egat.LegalCompliant.API.Utilities.ViewModelMessage
{
    public interface IViewModelMessageUtil
    {
        string GetMessage(Type type, string key);
        string GetMessage(Object value);
    }
}

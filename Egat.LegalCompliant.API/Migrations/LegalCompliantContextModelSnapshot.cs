﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Egat.LegalCompliant.Data;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Tickets;

namespace Egat.LegalCompliant.API.Migrations
{
    [DbContext(typeof(LegalCompliantContext))]
    partial class LegalCompliantContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.Artifact", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("ApprovedDate");

                    b.Property<long>("CategoryId");

                    b.Property<string>("Code");

                    b.Property<DateTime>("CreateDate")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2018, 8, 28, 15, 13, 5, 105, DateTimeKind.Local));

                    b.Property<DateTime>("EffectiveDate");

                    b.Property<string>("Introduction");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime>("LastModified")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(new DateTime(2018, 8, 28, 15, 13, 5, 110, DateTimeKind.Local));

                    b.Property<long>("LevelId");

                    b.Property<string>("Note");

                    b.Property<DateTime>("PublishedDate");

                    b.Property<long>("SourceId");

                    b.Property<int>("Status")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(512);

                    b.Property<string>("Title");

                    b.Property<int>("Type")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(0);

                    b.Property<DateTime?>("ValidFrom");

                    b.Property<DateTime?>("ValidTo");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("LevelId");

                    b.HasIndex("SourceId");

                    b.ToTable("Artifact");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactAssignment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("ArtifactId");

                    b.Property<long>("ArtifactNodeId");

                    b.Property<DateTime>("AssignDate");

                    b.Property<long>("DepartmentId");

                    b.Property<bool>("IsActive");

                    b.Property<DateTime>("ValidFrom");

                    b.Property<DateTime?>("ValidTo");

                    b.HasKey("Id");

                    b.HasIndex("ArtifactId");

                    b.HasIndex("ArtifactNodeId");

                    b.HasIndex("DepartmentId");

                    b.ToTable("ArtifactAssignments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactCategory", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AliasName");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ArtifactCategories");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactLevel", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AliasName");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ArtifactLevels");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactNode", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("ArtifactId");

                    b.Property<string>("Content");

                    b.Property<string>("Description");

                    b.Property<DateTime>("EffectiveDate");

                    b.Property<int>("FollowUpPeriodMonth");

                    b.Property<int>("FollowUpPeriodYear");

                    b.Property<long>("GroupId");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsAllDepartment");

                    b.Property<bool>("IsRequiredLicense");

                    b.Property<int>("Seq");

                    b.HasKey("Id");

                    b.HasIndex("ArtifactId");

                    b.HasIndex("GroupId");

                    b.ToTable("ArtifactNodes");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactNodeGroup", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("ArtifactId");

                    b.Property<string>("Content");

                    b.Property<bool>("IsActive");

                    b.Property<int>("Seq");

                    b.HasKey("Id");

                    b.HasIndex("ArtifactId");

                    b.ToTable("ArtifactNodeGroups");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactNodeLicenseType", b =>
                {
                    b.Property<long>("ArtifactNodeId");

                    b.Property<long>("LicenseTypeId");

                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsActive");

                    b.HasKey("ArtifactNodeId", "LicenseTypeId");

                    b.HasIndex("LicenseTypeId");

                    b.ToTable("ArtifactNodeLicenseTypes");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactSource", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("ArtifactSources");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Authorizations.UserGroupRoles", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("GroupId");

                    b.Property<bool>("IsReadOnly");

                    b.Property<int>("Role");

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.ToTable("UserGroupRoles");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Authorizations.UserRoles", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Role");

                    b.Property<long>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.ArtifactAttachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("ArtifactId");

                    b.Property<DateTime>("AttachDate");

                    b.Property<long>("CreatorId");

                    b.Property<long>("DocumentId");

                    b.HasKey("Id");

                    b.HasIndex("ArtifactId");

                    b.HasIndex("CreatorId");

                    b.HasIndex("DocumentId");

                    b.ToTable("ArtifactAttachments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.Document", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<long>("FileId");

                    b.Property<bool>("IsPulic");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("FileId")
                        .IsUnique();

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.FileData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Extension");

                    b.Property<string>("FileName");

                    b.Property<long>("OwnerId");

                    b.Property<long?>("Size");

                    b.Property<float?>("SizeKB");

                    b.Property<string>("StoragePath");

                    b.HasKey("Id");

                    b.HasIndex("OwnerId");

                    b.ToTable("FileDatas");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.License", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Alert");

                    b.Property<long>("DocumentId");

                    b.Property<DateTime>("DueDate");

                    b.Property<DateTime>("ExpireDate");

                    b.Property<string>("HolderName");

                    b.Property<bool>("IsOverride");

                    b.Property<DateTime>("IssueDate");

                    b.Property<long?>("LicenseHistoryId");

                    b.Property<string>("LicenseNumber");

                    b.Property<long>("LicenseTypeId");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.HasIndex("LicenseHistoryId");

                    b.HasIndex("LicenseTypeId");

                    b.ToTable("License");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.LicenseHistory", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CurrentLicenseId");

                    b.Property<string>("HolderName");

                    b.Property<DateTime>("LastUpdated");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CurrentLicenseId");

                    b.ToTable("LicenseHistories");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.LicenseType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AliasName");

                    b.Property<int>("Category");

                    b.Property<int>("FrequencyMonth");

                    b.Property<int>("FrequencyYear");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Issuer");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("LicenseType");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.TaskAssignmentAttachment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AttachDate");

                    b.Property<long>("CreatorId");

                    b.Property<long>("DocumentId");

                    b.Property<long>("TaskAssignmentId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.HasIndex("DocumentId");

                    b.HasIndex("TaskAssignmentId");

                    b.ToTable("TaskAssignmentAttachments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Notifications.MailNotification", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<string>("ButtonLabel");

                    b.Property<string>("ButtonLink");

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("Excerpt");

                    b.Property<bool>("IsSent");

                    b.Property<string>("Message");

                    b.Property<string>("Recipient");

                    b.Property<long>("RecipientId");

                    b.Property<string>("RecipientName");

                    b.Property<DateTime?>("SendDate");

                    b.Property<string>("Subject");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("MailNotifications");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.Department", b =>
                {
                    b.Property<long>("Id");

                    b.Property<string>("AliasName");

                    b.Property<string>("Email");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.Post", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("DepartmentId");

                    b.Property<DateTime>("EffectiveDate");

                    b.Property<bool>("IsPrimary");

                    b.Property<string>("PostName");

                    b.Property<long>("UserId");

                    b.Property<DateTime>("ValidFrom");

                    b.Property<DateTime?>("ValidTo");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("UserId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.User", b =>
                {
                    b.Property<long>("Id");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<long>("GroupId");

                    b.Property<bool>("IsActive");

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.UserGroup", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsSetPermission");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("UserGroups");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.UserGroupRoleConstant", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("GroupId");

                    b.Property<bool>("IsReadOnly");

                    b.Property<string>("Key");

                    b.Property<int>("Role");

                    b.Property<long?>("UserGroupId");

                    b.HasKey("Id");

                    b.HasIndex("UserGroupId");

                    b.ToTable("UserGroupRoleConstant");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskActivity", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long?>("ActionerId");

                    b.Property<int>("Code");

                    b.Property<string>("Description");

                    b.Property<DateTime>("LoggingTime");

                    b.Property<long>("TaskAssignmentId");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("ActionerId");

                    b.HasIndex("TaskAssignmentId");

                    b.ToTable("TaskActivity");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskAssessment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<long>("CreatorId");

                    b.Property<string>("Detail");

                    b.Property<bool>("IsApplicable");

                    b.Property<bool>("IsCompliant");

                    b.Property<string>("ReviewComment");

                    b.Property<DateTime>("ReviewDate");

                    b.Property<long>("ReviewerId");

                    b.Property<int>("Status");

                    b.Property<long>("TaskAssignmentId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.HasIndex("ReviewerId");

                    b.HasIndex("TaskAssignmentId");

                    b.ToTable("TaskAssessments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskAssignment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ApproveDate");

                    b.Property<long?>("ApproverId");

                    b.Property<long>("ArtifactId");

                    b.Property<long>("ArtifactNodeId");

                    b.Property<DateTime>("AssignDate");

                    b.Property<long>("DepartmentId");

                    b.Property<string>("Description");

                    b.Property<DateTime>("DueDate");

                    b.Property<int>("Flag");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsApplicable");

                    b.Property<bool>("IsCompliant");

                    b.Property<DateTime>("LastModified");

                    b.Property<int>("Progress");

                    b.Property<int>("Status");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("ApproverId");

                    b.HasIndex("ArtifactId");

                    b.HasIndex("ArtifactNodeId");

                    b.HasIndex("DepartmentId");

                    b.ToTable("TaskAssignments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskAssignmentGroup", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("TaskAssignmentId");

                    b.Property<long>("TaskGroupId");

                    b.HasKey("Id");

                    b.HasIndex("TaskAssignmentId");

                    b.HasIndex("TaskGroupId");

                    b.ToTable("TaskAssignmentGroup");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskComment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTime>("CreateDate");

                    b.Property<long>("CreatorId");

                    b.Property<long>("TaskAssignmentId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.HasIndex("TaskAssignmentId");

                    b.ToTable("TaskComments");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskGroup", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<long>("CreatorId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.ToTable("TaskGroups");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tickets.Ticket", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ActionDetail");

                    b.Property<long?>("CloseById");

                    b.Property<DateTime>("CloseDate");

                    b.Property<DateTime>("CreateDate");

                    b.Property<long>("DepartmentId");

                    b.Property<string>("Description");

                    b.Property<int>("ISO");

                    b.Property<long>("OwnerId");

                    b.Property<int>("Status");

                    b.Property<long>("TaskAssignmentId");

                    b.Property<string>("TicketExternalId");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("CloseById");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("OwnerId");

                    b.HasIndex("TaskAssignmentId");

                    b.ToTable("Tickets");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.Artifact", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactCategory", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactLevel", "Level")
                        .WithMany()
                        .HasForeignKey("LevelId");

                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactSource", "Source")
                        .WithMany()
                        .HasForeignKey("SourceId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactAssignment", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.Artifact", "Artifact")
                        .WithMany()
                        .HasForeignKey("ArtifactId");

                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactNode", "ArtifactNode")
                        .WithMany("Departments")
                        .HasForeignKey("ArtifactNodeId");

                    b.HasOne("Egat.LegalCompliant.Model.Party.Department", "Department")
                        .WithMany("ArtifactAssignment")
                        .HasForeignKey("DepartmentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactNode", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.Artifact", "Artifact")
                        .WithMany("Nodes")
                        .HasForeignKey("ArtifactId");

                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactNodeGroup", "Group")
                        .WithMany("Nodes")
                        .HasForeignKey("GroupId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactNodeGroup", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.Artifact", "Artifact")
                        .WithMany("NodeGroups")
                        .HasForeignKey("ArtifactId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Artifacts.ArtifactNodeLicenseType", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactNode", "ArtifactNode")
                        .WithMany("RequiredLicenses")
                        .HasForeignKey("ArtifactNodeId");

                    b.HasOne("Egat.LegalCompliant.Model.Documents.LicenseType", "LicenseType")
                        .WithMany("ArtifactNodeLicenseTypes")
                        .HasForeignKey("LicenseTypeId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Authorizations.UserGroupRoles", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.UserGroup", "Group")
                        .WithMany()
                        .HasForeignKey("GroupId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Authorizations.UserRoles", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.ArtifactAttachment", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.Artifact", "Artifact")
                        .WithMany("Documents")
                        .HasForeignKey("ArtifactId");

                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId");

                    b.HasOne("Egat.LegalCompliant.Model.Documents.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.Document", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Documents.FileData", "File")
                        .WithOne("Document")
                        .HasForeignKey("Egat.LegalCompliant.Model.Documents.Document", "FileId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.FileData", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.License", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Documents.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentId");

                    b.HasOne("Egat.LegalCompliant.Model.Documents.LicenseHistory", "LicenseHistory")
                        .WithMany("License")
                        .HasForeignKey("LicenseHistoryId");

                    b.HasOne("Egat.LegalCompliant.Model.Documents.LicenseType", "LicenseType")
                        .WithMany()
                        .HasForeignKey("LicenseTypeId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.LicenseHistory", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Documents.License", "CurrentLicense")
                        .WithMany()
                        .HasForeignKey("CurrentLicenseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Documents.TaskAssignmentAttachment", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId");

                    b.HasOne("Egat.LegalCompliant.Model.Documents.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentId");

                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskAssignment", "TaskAssignment")
                        .WithMany("Documents")
                        .HasForeignKey("TaskAssignmentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.Post", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.Department", "Department")
                        .WithMany("Posts")
                        .HasForeignKey("DepartmentId");

                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "User")
                        .WithMany("Posts")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.User", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.UserGroup", "Group")
                        .WithMany()
                        .HasForeignKey("GroupId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Party.UserGroupRoleConstant", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.UserGroup")
                        .WithMany("Roles")
                        .HasForeignKey("UserGroupId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskActivity", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Actioner")
                        .WithMany()
                        .HasForeignKey("ActionerId");

                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskAssignment", "TaskAssignment")
                        .WithMany("Activities")
                        .HasForeignKey("TaskAssignmentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskAssessment", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId");

                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Reviewer")
                        .WithMany()
                        .HasForeignKey("ReviewerId");

                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskAssignment", "TaskAssignment")
                        .WithMany("Assessments")
                        .HasForeignKey("TaskAssignmentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskAssignment", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Approver")
                        .WithMany()
                        .HasForeignKey("ApproverId");

                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.Artifact", "Artifact")
                        .WithMany("TaskAssignments")
                        .HasForeignKey("ArtifactId");

                    b.HasOne("Egat.LegalCompliant.Model.Artifacts.ArtifactNode", "ArtifactNode")
                        .WithMany()
                        .HasForeignKey("ArtifactNodeId");

                    b.HasOne("Egat.LegalCompliant.Model.Party.Department", "Department")
                        .WithMany()
                        .HasForeignKey("DepartmentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskAssignmentGroup", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskAssignment", "TaskAssignment")
                        .WithMany("Groups")
                        .HasForeignKey("TaskAssignmentId");

                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskGroup", "TaskGroup")
                        .WithMany("Assignments")
                        .HasForeignKey("TaskGroupId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskComment", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId");

                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskAssignment", "Assignment")
                        .WithMany("Comments")
                        .HasForeignKey("TaskAssignmentId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tasks.TaskGroup", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId");
                });

            modelBuilder.Entity("Egat.LegalCompliant.Model.Tickets.Ticket", b =>
                {
                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "CloseBy")
                        .WithMany()
                        .HasForeignKey("CloseById");

                    b.HasOne("Egat.LegalCompliant.Model.Party.Department", "Department")
                        .WithMany()
                        .HasForeignKey("DepartmentId");

                    b.HasOne("Egat.LegalCompliant.Model.Party.User", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");

                    b.HasOne("Egat.LegalCompliant.Model.Tasks.TaskAssignment", "TaskAssignment")
                        .WithMany("Tickets")
                        .HasForeignKey("TaskAssignmentId");
                });
        }
    }
}

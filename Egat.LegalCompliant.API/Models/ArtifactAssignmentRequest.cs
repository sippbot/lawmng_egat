﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class ArtifactAssignmentRequest
    {
        public long ArtifactId { get; set; }
        //public DateTime DueDate { get; set; } // comment by Toey 16/03/2018

        public string DueDate { get; set; }
        
        public string Token { get; set; }
    }
}

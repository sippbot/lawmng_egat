﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class FileUploadResponse
    {
        public string filename { get; set; }
        public long id { get; set; }

        // edit by Toey 21/03/2018
        public string url { get; set; }

        public FileUploadResponse() { }
    }
}

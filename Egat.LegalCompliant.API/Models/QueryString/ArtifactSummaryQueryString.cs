﻿using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models.QueryString
{
    public class ArtifactSummaryQueryString
    {
        public ArtifactStatus Status { get; set; }
        public long DepartmentId { get; set; }
        public TaskAssignmentType TaskAssignmentType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models.QueryString
{
    public class ArtifactAssignmentQueryString
    {
        public long ArtifactId { get; set; }
        public DateTime DueDate { get; set; }
    }
}

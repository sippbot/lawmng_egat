﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models.QueryString
{
    public class ArtifactSummaryQueryStringViewModel
    {
        public string Status { get; set; }
        public long DepartmentId { get; set; }
    }
}

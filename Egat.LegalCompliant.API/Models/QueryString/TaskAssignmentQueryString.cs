﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models.QueryString
{
    public class TaskAssignmentQueryString
    {
        public long ArtifactId { get; set; }
        public long DepartmentId { get; set; }
    }
}

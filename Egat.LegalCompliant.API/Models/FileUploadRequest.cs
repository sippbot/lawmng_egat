﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class FileUploadRequestOptions
    {
        public long ownerId { get; set; }
        public long departmentId { get; set; }
        public string fileName { get; set; }
        public string type { get; set; }

        public FileUploadRequestOptions() { }
    }
}

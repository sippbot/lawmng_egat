﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TaskAssignmentApprovalRequest
    {
        public long ArtifactId { get; set; }
        public long ApproverId { get; set; }
        public ICollection<long> Departments { get; set; }
        public string Signature { get; set; }
    }
}

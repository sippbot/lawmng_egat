﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class DepartmentSummary
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Status { get; set; }
        public string DisplayText { get; set; }
    }
}

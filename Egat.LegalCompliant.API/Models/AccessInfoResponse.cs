﻿using Egat.LegalCompliant.Model.Authorizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class AccessInfoResponse
    {
        public string username { get; set; }
        public int statusCode { get; set; }
        public string message { get; set; }
        public DateTime loginTime { get; set; }
        public string token { get; set; }
        public string departmentId { get; set; }
        public string userId { get; set; }
        public string userGroup { get; set; }
        public AccountRoles roles { get; set; }

        public AccessInfoResponse() { }

        public AccessInfoResponse(string username, int statusCode, string token)
        {
            this.username = username;
            this.statusCode = statusCode;
            if (statusCode >= 200 && statusCode < 300)
            {
                this.message = "Login succedded!";
            }
            else
            {
                this.message = "Login failed!";
            }
            //TODO change to JWT
            //this.token = token;
            this.token = token;
            this.loginTime = DateTime.Now;
        }
    }
}

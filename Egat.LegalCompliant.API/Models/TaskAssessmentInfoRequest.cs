﻿using Egat.LegalCompliant.Model.Party;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TaskAssessmentInfoRequest
    {
        public long TaskAssignmentId { get; set; }
        public long DepartmentId { get; set; }
        public long CreatorId { get; set; }
        public bool IsCompliant { get; set; }
        public bool IsApplicable { get; set; }
        public string Detail { get; set; }
    }
}

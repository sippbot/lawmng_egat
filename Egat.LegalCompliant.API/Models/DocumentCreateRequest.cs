﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class DocumentCreateRequest
    {
        public string Title { get; set; }
        public bool IsPublic { get; set; }
        public long FileDataId { get; set; }
        public string Type { get; set; }
    }
}

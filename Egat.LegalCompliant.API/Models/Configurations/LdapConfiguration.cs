﻿namespace Egat.LegalCompliant.API.Models.Configurations
{
    public class LdapConfiguration
    {
        public string Url { get; set; }
        public string BindDn { get; set; }
        public string BindCredentials { get; set; }
        public string SearchBase { get; set; }
        public string SearchFilter { get; set; }
        public string AdminCn { get; set; }
        public string AdminDn { get; set; }
        public int Port { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models.Configurations
{
    public class DateTimeUtilConfiguration
    {
        public string CultureInfo { get; set; }
        public string StringFormat { get; set; }
    }
}

﻿using Egat.LegalCompliant.API.ViewModels;
using Egat.LegalCompliant.Model.Artifacts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class ArtifactMetadataResponse
    {
        public ICollection<ArtifactCategoryViewModel> Categories;
        public ICollection<ArtifactLevelViewModel> Levels;
    }
}

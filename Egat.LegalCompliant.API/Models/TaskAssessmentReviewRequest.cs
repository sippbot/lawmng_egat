﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TaskAssessmentReviewRequest
    {
        public long TaskAssignmentId { get; set; }
        public long TaskAssessmentId { get; set; }
        public long ReviewerId { get; set; }
        public bool IsConfirm { get; set; }
        public string ReviewComment { get; set; }
    }
}

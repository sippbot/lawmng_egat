﻿using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TaskAssignmentSummaryRequest
    {
        public long ArtifactId { get; set; }
        public string Status { get; set; }
    }
}

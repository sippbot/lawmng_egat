﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TaskAssignmentProposeRequest
    {
        public long ArtifactId { get; set; }
        public long CreatorId { get; set; }
        public ICollection<long> Departments { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TicketCloseRequet
    {
        public long TaskAssignmentId { get; set; }
        public long TicketId { get; set; }
        public long UserId { get; set; }
        public string ActionDetail { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Models
{
    public class TaskAssignmentAttachmentRequest
    {
        public long DocumentId { get; set; }
        public long UserId { get; set; }
        public long TaskAssignmentId { get; set; }
        public string Title { get; set; }
    }
}

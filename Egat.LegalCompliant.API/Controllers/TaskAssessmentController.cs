﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.API.Models;
using AutoMapper;
using Egat.LegalCompliant.Service.ServiceModels;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.API.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class TaskAssessmentController : Controller
    {
        private readonly ITaskAssignmentService _taskAssignmentService;
        private readonly ILogger<TaskAssessmentController> _logger;
        private readonly IMapper _mapper;
        int page = 1;
        int pageSize = 4;

        public TaskAssessmentController(ITaskAssignmentService taskAssignmentService, ILogger<TaskAssessmentController> logger,
            IMapper mapper)
        {
            _taskAssignmentService = taskAssignmentService;
            _mapper = mapper;
            _logger = logger;
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/create
        [HttpPost("create")]
        public IActionResult CreateAssessment([FromBody] TaskAssessmentInfoRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assessmentInfo = _mapper.Map<TaskAssessmentInfoRequest, AssessmentInfoServiceModel>(request);

            var taskAssessment = _taskAssignmentService.CreateTaskAssessment(assessmentInfo);
            // We return list of all assessment because to create the new assessment, data serivce will populate previous assessment
            if(taskAssessment == null)
            {
                throw new Exception("Cannot create new assessment!");
            }
            var taskAssessmentList = _taskAssignmentService.GetTaskAssessmentsByTaskAssignmentId(taskAssessment.TaskAssignmentId);
            var taskAssessmentListVM = _mapper.Map<IEnumerable<TaskAssessment>, IEnumerable<TaskAssessmentViewModel>>(taskAssessmentList);

            return new OkObjectResult(taskAssessmentListVM);
        }

        // POST api/values
        [HttpPost("review")]
        public IActionResult ReviewAssessment([FromBody] TaskAssessmentReviewRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assessmentReview = _mapper.Map<TaskAssessmentReviewRequest, AssessmentReviewServiceModel>(request);

            var taskAssessment = _taskAssignmentService.ReviewTaskAssessnment(assessmentReview);
            // We return list of all assessment because to create the new assessment, data serivce will populate previous assessment
            if (taskAssessment == null)
            {
                throw new Exception("Cannot find assessment!");
            }
            var taskAssessmentList = _taskAssignmentService.GetTaskAssessmentsByTaskAssignmentId(taskAssessment.TaskAssignmentId);
            var taskAssessmentListVM = _mapper.Map<IEnumerable<TaskAssessment>, IEnumerable<TaskAssessmentViewModel>>(taskAssessmentList);

            return new OkObjectResult(taskAssessmentListVM);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

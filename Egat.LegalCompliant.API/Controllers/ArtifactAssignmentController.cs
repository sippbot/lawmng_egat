﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.Models;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;
using System;
using Hangfire;
using Egat.LegalCompliant.Data.Abstract;
using System.Linq;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class ArtifactAssignmentController : Controller
    {
        private readonly IArtifactService _artifactService;
        private readonly ILogger<ArtifactAssignmentController> _logger;
        private readonly IBackgroundJobClient _backgroundJob;
        private readonly IArtifactRepository _artifactRepository;
        private readonly IMapper _mapper;

        public ArtifactAssignmentController(IArtifactService artifactService, IMapper mapper,
            ILogger<ArtifactAssignmentController> logger, IBackgroundJobClient backgroundJob, IArtifactRepository artifactRepository)
        {
            _artifactService = artifactService;
            _logger = logger;
            _mapper = mapper;
            _artifactRepository = artifactRepository;
            _backgroundJob = backgroundJob;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] ArtifactAssignmentRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //IEnumerable<TaskAssignment> tasks = _artifactService.CreateTaskAssignmentFromArtifactAssignment(request.ArtifactId, request.DueDate);
            if (request.Token != null)
            {
                List<long> artifactIdList = _artifactRepository.GetAll().Select(a => a.Id).ToList();
                _logger.LogCritical("ArtifactAssignmentController -  Post(): number of artifact to create task {0}", artifactIdList.Count());
                int count = 0;
                foreach(long artifactId in artifactIdList)
                {
                    _backgroundJob.Schedule(() => HandleBatchGenerationRequest(request.Token, artifactId), TimeSpan.FromSeconds(30*count));
                    count++;
                }
                return new OkObjectResult("Start generate task assignment with count value = " + count);
            }
            else
            {
                //* Edit by Toey 16/03/2018
                DateTime TaskAssignDD = _mapper.Map<string, DateTime>(request.DueDate);

                IEnumerable<TaskAssignment> tasks = _artifactService.CreateTaskAssignmentFromArtifactAssignment(request.ArtifactId, TaskAssignDD);
                //*
                var artifact = _artifactService.GetArtifact(request.ArtifactId);
                ArtifactSummaryViewModel artifactVM = _mapper.Map<Artifact, ArtifactSummaryViewModel>(artifact);

                return new OkObjectResult(artifactVM);
            }

        }

        public void HandleBatchGenerationRequest(String token, long artifactId)
        {
            int task = _artifactService.CreateTaskAssignmentFromInitialArtifactAssignment(token, artifactId);
            _logger.LogInformation("Successfully create "+ task +" task assignments for " + artifactId);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

    }
}

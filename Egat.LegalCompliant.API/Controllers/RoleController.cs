﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Authorizations;
using Egat.LegalCompliant.Model.Party;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class RoleController : Controller
    {
        private readonly IUserGroupRolesRepository _groupRolesRepository;
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<RoleController> _logger;
        private readonly IMapper _mapper;

        // GET: api/<controller>

        public RoleController(IUserGroupRolesRepository groupRolesRepository, IUserGroupRepository userGroupRepository, IUnitOfWork unitOfWork
            , ILogger<RoleController> logger, IMapper mapper)
        {
            _groupRolesRepository = groupRolesRepository;
            _userGroupRepository = userGroupRepository;
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<UserGroup> userGroups = _userGroupRepository.FindBy(p => p.IsSetPermission == true).ToList();
            ICollection<UserGroup> _userGroups = new HashSet<UserGroup>();
            foreach (var ug in userGroups)
            {
                var _ug = new UserGroup(ug);
                var grr = _groupRolesRepository.FindBy(p => p.GroupId == ug.Id).ToList();
                foreach (var groupRole in grr)
                {
                    var strRole = groupRole.Role.ToString();
                    var role = new UserGroupRoleConstant
                    {
                        Id = groupRole.Id,
                        Role = groupRole.Role,
                        GroupId = groupRole.GroupId,
                        IsReadOnly = groupRole.IsReadOnly,
                        Key = strRole
                    };
                    _ug.Roles.Add(role);
                }
                _userGroups.Add(_ug);
            }
            return new OkObjectResult(_userGroups);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]RoleViewModel rvm)
        {
            try
            {
                UserGroupRoles _userGroupRole = new UserGroupRoles();
                _userGroupRole.Role = rvm.Role;
                _userGroupRole.GroupId = rvm.GroupId;
                _userGroupRole.IsReadOnly = rvm.IsReadOnly;
                _groupRolesRepository.Add(_userGroupRole);
                Save();
                _logger.LogInformation("RoleController.Post(): Post UserGroupRoles id - {}", _userGroupRole.Id);
                return new OkObjectResult(_userGroupRole);
            }
            catch (Exception e)
            {
                _logger.LogError("RoleController.Post(): fail to Post UserGroupRoles - {}", e.Message);
                return new BadRequestResult();
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] RoleViewModel rvm)
        {
            try
            {
                UserGroupRoles _userGroupRole = _groupRolesRepository.GetSingle(id);
                if (_userGroupRole != null)
                {
                    _userGroupRole.IsReadOnly = rvm.IsReadOnly;
                    _groupRolesRepository.Update(_userGroupRole);
                    Save();
                }
                _logger.LogInformation("RoleController.Put(): Put UserGroupRoles id - {}", id);
                return new OkObjectResult(_userGroupRole);
            }
            catch (Exception e)
            {
                _logger.LogError("RoleController.Put(): fail to Put UserGroupRoles - {}", e.Message);
                return new BadRequestResult();
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                UserGroupRoles _userGroupRole = _groupRolesRepository.GetSingle(id);
                if (_userGroupRole != null)
                {
                    _groupRolesRepository.Delete(_userGroupRole);
                    Save();
                }
                _logger.LogInformation("RoleController.Delete(): delete or cancel UserGroupRoles id - {}", id);
                return new OkObjectResult(id);
            }
            catch (Exception e)
            {
                _logger.LogError("RoleController.Delete(): fail to delete UserGroupRoles - {}", e.Message);
                return new BadRequestResult();
            }
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}

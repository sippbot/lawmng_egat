﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Hangfire;
using Egat.LegalCompliant.Service.Abstract;
using System;
using Egat.LegalCompliant.Service.Complaint;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        // private readonly INotificationService _notificationService;
        private readonly ITicketService _ticketService;
        // private readonly IMailNotificationRepository _mailNotificationRepository;
        private readonly IBackgroundJobClient _backgroundJob;

        // ITicketService ticketService, IMailNotificationRepository mailNotificationRepository)
        public TestController( IBackgroundJobClient backgroundJob, ITicketService ticketService)
        {
            //_notificationService = notificationService;
            // _mailNotificationRepository = mailNotificationRepository;
            _ticketService = ticketService;
            _backgroundJob = backgroundJob;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("send/{id}")]
        public IActionResult Get(int id)
        {
            return TestTicket(id);
        }

        [HttpGet("resend/{id}")]
        public IActionResult Resend(int id)
        {
            return TestReSendTicket(id);
        }

        private IActionResult TestTicket(long id)
        {
            
            try
            {
                TicketInfo ticketInfo = new TicketInfo();
                ticketInfo.ArtifactCode = "01 05/60";
                ticketInfo.ISO = 9000;
                ticketInfo.OwnerId = 477729;
                ticketInfo.TaskAssignmentDescription = "ข้อ ๑ ให้เพิ่มความต่อไปนี้เป็น (๓) ของข้อ ๒ แห่งกฎกระทรวง ฉบับที่ ๒ (พ.ศ. ๒๕๓๕) ออกตามความในพระราชบัญญัติโรงงาน";
                ticketInfo.TaskAssessmentComment = "โปรดตรวจสอบความถูกต้อง";
                ticketInfo.TaskAssignmentId = 1;
                ticketInfo.DepartmentId = 9520020;

                _ticketService.SubmitTicket(ticketInfo);
            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }

            return new OkResult();
        }


        private IActionResult TestReSendTicket(long id)
        {

            try
            {
                _ticketService.ResubmitTicket(id);
            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }

            return new OkResult();
        }

        /// <summary>
        /// Test Send Mail with EMailNotificationService
        /// </summary>
        /// <example>
        /// Request Body Example
        /// 
        /// "recipientId":225694,
        /// "recipientName":"Tawatchai Doungbal",
        /// "recipient":"tawatchai@askme.co.th",
        /// "subject":"การทดสอบระบบแจ้งเตือนด้วยอีเมล์",
        /// "message":"ท่านได้รับอีเมล์ฉบับนี้จากระบบส่งการแจ้งเตือนอัตโนมัติของระบบติดตามการปฏิบัติตามกฎหมาย หากมีข้อสงสัยเพิ่มเติม กรุณาติดต่อ สำนักสารสนเทศ โรงไฟฟ้าแม่เมาะ",
        /// "action":"โปรดทดสอบเข้าใช้งานระบบ",
        /// "buttonLabel":"เข้าสู่ระบบ",
        /// "buttonLink":"10.249.99.238"
        /// 
        /// </example>
        /// <returns>OkResult if success</returns>
        /// 
        /*
        private IActionResult TestEmail() 
        {
            MailNotification mailNotification = new MailNotification();
            mailNotification.Recipient = "sippakorn.saeiao@gmail.com";
            mailNotification.RecipientId = 555555;
            mailNotification.RecipientName = "สิปปกร แซ่เอี่ยว";
            mailNotification.Subject = "การทดสอบระบบแจ้งเตือนด้วยอีเมล์";
            mailNotification.Message = "ท่านได้รับอีเมล์ฉบับนี้จากระบบส่งการแจ้งเตือนอัตโนมัติของระบบติดตามการปฏิบัติตามกฎหมาย หากมีข้อสงสัยเพิ่มเติม กรุณาติดต่อ สำนักสารสนเทศ โรงไฟฟ้าแม่เมาะ";
            mailNotification.Action = "โปรดทดสอบเข้าใช้งานระบบ";
            mailNotification.ButtonLabel = "เข้าสู่ระบบ";
            // TODO this link should utilize cofiguration value of email template
            mailNotification.ButtonLink = "http://localhost:4200/#/laws/review/1";

            _mailNotificationRepository.Add(mailNotification);
            _mailNotificationRepository.Commit();

            _backgroundJob.Enqueue<INotificationService>
            (
                x => x.SendNotification(mailNotification.Id)
            );

            return new OkResult();
        }
        */
        // POST api/values
        // [HttpPost]
        /*
        public void Post([FromBody] string value)
        {
            /*
             * Request Body Example
             *    
             *    "recipientId":225694,
             *    "recipientName":"Tawatchai Doungbal",
             *    "recipient":"tawatchai@askme.co.th",
             *    "subject":"การทดสอบระบบแจ้งเตือนด้วยอีเมล์",
             *    "message":"ท่านได้รับอีเมล์ฉบับนี้จากระบบส่งการแจ้งเตือนอัตโนมัติของระบบติดตามการปฏิบัติตามกฎหมาย หากมีข้อสงสัยเพิ่มเติม กรุณาติดต่อ สำนักสารสนเทศ โรงไฟฟ้าแม่เมาะ",
             *    "action":"โปรดทดสอบเข้าใช้งานระบบ",
             *    "buttonLabel":"เข้าสู่ระบบ",
             *    "buttonLink":"10.249.99.238"
             *
            MailNotification mailNoti = new MailNotification();
            mailNoti.Recipient = "sippakorn.saeiao@gmail.com";
            mailNoti.RecipientId = 555555;
            mailNoti.RecipientName = "สิปปกร แซ่เอี่ยว";
            mailNoti.Subject = "การทดสอบระบบแจ้งเตือนด้วยอีเมล์";
            mailNoti.Message = "ท่านได้รับอีเมล์ฉบับนี้จากระบบส่งการแจ้งเตือนอัตโนมัติของระบบติดตามการปฏิบัติตามกฎหมาย หากมีข้อสงสัยเพิ่มเติม กรุณาติดต่อ สำนักสารสนเทศ โรงไฟฟ้าแม่เมาะ";
            mailNoti.Action = "โปรดทดสอบเข้าใช้งานระบบ";
            mailNoti.ButtonLabel = "เข้าสู่ระบบ";
            mailNoti.ButtonLink = "http://localhost:4200/#/laws/review/1";

            _mailNotificationRepository.Add(mailNoti);
            _mailNotificationRepository.Commit();

            _backgroundJob.Enqueue<INotificationService>
            (
                x => x.SendNotification(mailNoti.Id)
            );
            //_notificationService.SendNotification(1);
        }
    */
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

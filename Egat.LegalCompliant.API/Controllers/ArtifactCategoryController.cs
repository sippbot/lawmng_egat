﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class ArtifactCategoryController : Controller
    {
        private readonly IArtifactCategoryRepository _categoryRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<ArtifactCategoryController> _logger;

        public ArtifactCategoryController(IArtifactCategoryRepository categoryRepository, IMapper mapper, ILogger<ArtifactCategoryController> logger)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
            _logger = logger;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<ArtifactCategory> categories = _categoryRepository
                .GetAll()
                .ToList();

            IEnumerable<ArtifactCategoryViewModel> categoryVM = _mapper.Map<IEnumerable<ArtifactCategory>, IEnumerable<ArtifactCategoryViewModel>>(categories);

            return new OkObjectResult(categoryVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetArtifactCategory")]
        public IActionResult Get(int id)
        {
            ArtifactCategory categories = _categoryRepository.GetSingle(id);

            ArtifactCategoryViewModel categoryVM = _mapper.Map<ArtifactCategory, ArtifactCategoryViewModel>(categories);

            return new OkObjectResult(categoryVM);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] ArtifactCategoryViewModel category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ArtifactCategory _category = _mapper.Map<ArtifactCategoryViewModel, ArtifactCategory>(category);

            _categoryRepository.Add(_category);
            _categoryRepository.Commit();

            // Add this feature to support esternal id
            _category.ExternalId = _category.Id;
            _categoryRepository.Update(_category);
            _categoryRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetArtifactCategory", new { controller = "ArtifactCategory", id = _category.Id }, _category);
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ArtifactCategoryViewModel category)
        {
            if (!ModelState.IsValid || category.Id == 0)
            {
                return BadRequest(ModelState);
            }

            //User _user = _userRepository.GetSingle(user.Id);

            if (category == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }

            ArtifactCategory _category = _mapper.Map<ArtifactCategoryViewModel, ArtifactCategory>(category);

            _categoryRepository.Update(_category);
            _categoryRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetArtifactCategory", new { controller = "ArtifactCategory", id = _category.Id }, _category);
            return result;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var category = _categoryRepository.GetSingle(id);
            if (category == null)
            {
                ModelState.AddModelError("CategoryId", "Category does not exist!");
                return BadRequest(ModelState);
            }
            // If no artifact belong to group delete it if have somebody in group return bad request
            if (!_categoryRepository.IsCategoryEmpty(id))
            {
                ModelState.AddModelError("CategoryId", "Category has artifact(s) belongs to it!");
                return BadRequest(ModelState);
            }

            _categoryRepository.Delete(category);
            _categoryRepository.Commit();

            return new OkObjectResult(id);
        }
    }
}

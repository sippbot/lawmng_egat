﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.ViewModels;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Tasks;
using AutoMapper;
using Egat.LegalCompliant.API.Models.QueryString;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Service.ServiceModels;
using Egat.LegalCompliant.Model.Documents;
using Microsoft.AspNetCore.Authorization;

namespace Egat.LegalCompliant.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TaskAssignmentController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILogger<TaskAssignmentController> _logger;
        private readonly ITaskAssignmentService _taskAssignmentService;
        private readonly IDocumentService _documentService;

        public TaskAssignmentController(ITaskAssignmentRepository taskAssignmentRepository, 
            ITaskAssignmentService taskAssignmentService, IDocumentService documentService,
            ILogger<TaskAssignmentController> logger, IMapper mapper)
        {
            _taskAssignmentService = taskAssignmentService;
            _documentService = documentService;
            _mapper = mapper;
            _logger = logger;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get([FromQuery] TaskAssignmentQueryString request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO
            // Add query by status here
            // Return TaskAssignment
            if(request.ArtifactId == 0)
            {
                return BadRequest(ModelState);
            }

            IEnumerable<TaskAssignment> tasks;
            IEnumerable<TaskAssignmentViewModel> _tasksVM;

            if (request.DepartmentId == 0)
            {
                // Forward to Get()

                tasks = _taskAssignmentService.GetTaskAssignmentsByArtifactId(request.ArtifactId);
                //_tasksVM = _mapper.Map<IEnumerable<TaskAssignment>, IEnumerable<TaskAssignmentViewModel>>(tasks);

                _tasksVM = _mapper.Map<IEnumerable<TaskAssignment>, IEnumerable<TaskAssignmentViewModel>>(tasks);

                return new OkObjectResult(_tasksVM);
            }

            tasks = _taskAssignmentService.GetTaskAssignmentsByStatusExclude(TaskAssignmentStatus.Approved, request.DepartmentId, request.ArtifactId);
            _tasksVM = _mapper.Map<IEnumerable<TaskAssignment>, IEnumerable<TaskAssignmentViewModel>>(tasks);

            return new OkObjectResult(_tasksVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetTaskAssignmentById")]
        public IActionResult Get(long id)
        {
            TaskAssignment tasks = _taskAssignmentService.GetTaskAssignmentFullDetail(id);
            TaskAssignmentViewModel _tasksVM = _mapper.Map<TaskAssignment, TaskAssignmentViewModel>(tasks);

            return new OkObjectResult(_tasksVM);
        }

        // POST api/create
        [HttpPost("approve")]
        public IActionResult ApproveTaskAssignment([FromBody] TaskAssignmentApprovalRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _taskAssignmentList = new List<long>();
            TaskAssignmentStatus flag = TaskAssignmentStatus.Proposed;
            foreach (var department in request.Departments)
            {
                var taskAssignment = _taskAssignmentService.GetTaskAssignmentIdListByArtifactId(request.ArtifactId, department, flag);
                if (taskAssignment == null)
                {
                    _logger.LogInformation("can not find task assginment with artifact id {} and department id {}", request.ArtifactId, department);
                    continue;
                }
                _taskAssignmentList.AddRange(taskAssignment);
            }
            var approval = new TaskAssignmentApprovalServiceModel();
            approval.ArtifactId = request.ArtifactId;
            approval.ApproverId = request.ApproverId;
            approval.TaskAssignments = _taskAssignmentList;
            approval.Signature = request.Signature;

            _taskAssignmentService.ApproveTaskAssignment(approval);

            var summary = _taskAssignmentService.GetTaskAssignmentsSummaryByArtifactId(request.ArtifactId);
            IEnumerable<TaskAssignmentSummaryViewModel> _tasksVM = _mapper.Map<IEnumerable<TaskAssignmentSummary>, IEnumerable<TaskAssignmentSummaryViewModel>>(summary);

            return new OkObjectResult(_tasksVM);
        }

        // POST api/create
        [HttpPost("closeticket")]
        public IActionResult CloseTicket([FromBody] TicketCloseRequet request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _taskAssignmentService.CloseTicket(request.TaskAssignmentId, request.TicketId, request.UserId, request.ActionDetail);

            // TODO return TaskAssignmentViewModel
            return new OkResult();
        }

        // POST api/create
        [HttpPost("proposal")]
        public IActionResult CreateGroup([FromBody] TaskAssignmentProposeRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //var proposal = _mapper.Map<TaskAssignmentProposeRequest, TaskAssignmentProposalServiceModel>(request);
            var _taskAssignmentList = new List<long>();
            TaskAssignmentStatus excludeFlag = TaskAssignmentStatus.Proposed | TaskAssignmentStatus.Approved;
            foreach (var department in request.Departments)
            {
                _taskAssignmentList.AddRange(_taskAssignmentService.GetTaskAssignmentIdListByArtifactIdExclude(request.ArtifactId, department, excludeFlag));
            }
            var proposal = new TaskAssignmentProposalServiceModel();
            proposal.ArtifactId = request.ArtifactId;
            proposal.CreatorId = request.CreatorId;
            proposal.TaskAssignments = _taskAssignmentList;
                
            _taskAssignmentService.CreateTaskAssignmentProposal(proposal);

            var summary = _taskAssignmentService.GetTaskAssignmentsSummaryByArtifactId(request.ArtifactId);
            IEnumerable<TaskAssignmentSummaryViewModel> _tasksVM = _mapper.Map<IEnumerable<TaskAssignmentSummary>, IEnumerable<TaskAssignmentSummaryViewModel>>(summary);

            return new OkObjectResult(_tasksVM);
        }


        // POST api/create
        [HttpPost("comment")]
        public IActionResult CreateTaskComment([FromBody] TaskCommentViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var comment = _mapper.Map<TaskCommentViewModel, TaskComment>(request);
            var _comment = _taskAssignmentService.CreateTaskComment(comment);

            TaskCommentViewModel _commentVM = _mapper.Map<TaskComment, TaskCommentViewModel>(_comment);

            return new OkObjectResult(_commentVM);
        }

        // POST api/create
        [HttpPost("attachment")]
        public IActionResult CreateTaskAttachment([FromBody] TaskAssignmentAttachmentRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            // We will rename file to name given by user
            if(request.Title != null)
            {
                var document = _documentService.GetDocument(request.DocumentId);
                document.Title = request.Title;
                _documentService.UpdateDocument(document);
            }

            var attachment = _taskAssignmentService.CreateTaskAssignmentAttachment(request.TaskAssignmentId, request.UserId, request.DocumentId);

            DocumentViewModel _commentVM = _mapper.Map<TaskAssignmentAttachment, DocumentViewModel>(attachment);

            return new OkObjectResult(_commentVM);
        }

        // POST api/create
        [HttpDelete("attachment/{taskAssignmentId}/{documentId}")]
        public IActionResult DeleteTaskAttachment(int taskAssignmentId,int documentId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _taskAssignmentService.DeleteTaskAssignmentAttachment(taskAssignmentId, documentId);

            return new OkObjectResult(documentId);
        }


        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return new BadRequestResult();
        }
    }
}

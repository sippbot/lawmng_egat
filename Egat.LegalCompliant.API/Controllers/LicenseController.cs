﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Service.Abstract;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class LicenseController : Controller
    {
        //private readonly ILicenseRepository _licenseRepository;
        private readonly IDocumentService _documentService;
        private readonly ITaskAssignmentService _taskAssignmentService;
        private readonly ILogger<LicenseController> _logger;
        private readonly IMapper _mapper;

        public LicenseController(IDocumentService documentService, ITaskAssignmentService taskAssignmentService, 
            ILogger<LicenseController> logger, IMapper mapper)
        {
            // _licenseRepository = licenseRepository;
            _documentService = documentService;
            _taskAssignmentService = taskAssignmentService;
            _logger = logger;
            _mapper = mapper;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("user/{userId}")]
        public IActionResult GetUploadLicenses(int userId)
        {
            var licenses = _documentService.GetAllLicenses(userId);

            var licensesVM = _mapper.Map<IEnumerable<License>, IEnumerable<LicenseViewModel>>(licenses);

            return new ObjectResult(licensesVM);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] LicenseViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var license = _mapper.Map<LicenseViewModel, License>(request);

            if (request.IsPublic)
            {
                // Update document to public
                var document = _documentService.GetDocument(request.DocumentId);
                if(document != null)
                {
                    document.IsPulic = true;
                    _documentService.UpdateDocument(document);
                }
            }

            var _license = _documentService.CreateLicense(license);

            if(request.TaskAssignmentId != 0 && request.UserId != 0)
            {
                // Attach license to taskassignment
                var attachment = _taskAssignmentService.CreateTaskAssignmentAttachment(request.TaskAssignmentId, request.UserId, request.DocumentId);

                DocumentViewModel _commentVM = _mapper.Map<TaskAssignmentAttachment, DocumentViewModel>(attachment);

                return new OkObjectResult(_commentVM);
            }

            var _licenseVM = _mapper.Map<License, LicenseViewModel>(license);

            return new OkObjectResult(_licenseVM);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] LicenseViewModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var license = _mapper.Map<LicenseViewModel, License>(request);

            if (request.IsPublic)
            {
                // Update document to public
                var document = _documentService.GetDocument(request.DocumentId);
                if (document != null)
                {
                    document.IsPulic = true;
                    _documentService.UpdateDocument(document);
                }
            }

            var _license = _documentService.RenewLicense(id, license);

            if (request.TaskAssignmentId != 0 && request.UserId != 0)
            {
                // Attach license to taskassignment
                var attachment = _taskAssignmentService.CreateTaskAssignmentAttachment(request.TaskAssignmentId, request.UserId, request.DocumentId);

                DocumentViewModel _commentVM = _mapper.Map<TaskAssignmentAttachment, DocumentViewModel>(attachment);

                return new OkObjectResult(_commentVM);
            }

            var _licenseVM = _mapper.Map<License, LicenseViewModel>(license);

            return new OkObjectResult(_licenseVM);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

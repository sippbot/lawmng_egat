﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Party;
using AutoMapper;
using Egat.LegalCompliant.API.ViewModels;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class DepartmentController : Controller
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly ILogger<DepartmentController> _logger;
        private readonly IMapper _mapper;

        public DepartmentController(IDepartmentRepository departmentRepository, ILogger<DepartmentController> logger, IMapper mapper)
        {
            _departmentRepository = departmentRepository;
            _logger = logger;
            _mapper = mapper;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get(DepartmentViewModel department)
        {
            IEnumerable<Department> departments;
            if (department.IsActive)
            {
                departments = _departmentRepository.FindBy(a => a.IsActive == true).ToList();
            }
            else
            {
                departments = _departmentRepository.GetAll().ToList();
            }

            IEnumerable<DepartmentViewModel> departmentsVM = _mapper.Map<IEnumerable<Department>, IEnumerable<DepartmentViewModel>>(departments);

            return new OkObjectResult(departmentsVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetDepartment")]
        public IActionResult Get(int id)
        {
            Department department = _departmentRepository.GetSingle(id);

            if (department != null)
            {
                DepartmentViewModel departmentVM = _mapper.Map<Department, DepartmentViewModel>(department);
                return new OkObjectResult(departmentVM);
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] DepartmentViewModel department)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Department _department = _mapper.Map<DepartmentViewModel, Department>(department);

            _departmentRepository.Add(_department);
            _departmentRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetDepartment", new { controller = "Department", id = _department.Id }, _department);
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] DepartmentViewModel department)
        {
            if (!ModelState.IsValid || department.Id == 0)
            {
                return BadRequest(ModelState);
            }

            //User _user = _userRepository.GetSingle(user.Id);

            if (department == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }

            Department _department = _mapper.Map<DepartmentViewModel, Department>(department);

            _departmentRepository.Update(_department);
            _departmentRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetDepartment", new { controller = "Department", id = _department.Id }, _department);
            return result;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var department = _departmentRepository.GetSingle(id);
            if (department == null)
            {
                ModelState.AddModelError("DepartmentId", "Department does not exist!");
                return BadRequest(ModelState);
            }
            // We never delete any single user we just set inactive
            department.IsActive = false;

            _departmentRepository.Update(department);
            _departmentRepository.Commit();

            return new OkObjectResult(department.Id);
        }
    }
}

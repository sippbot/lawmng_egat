﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Egat.LegalCompliant.API.ViewModels;
using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Service.Abstract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class NotificationController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILogger<NotificationController> _logger;
        private readonly INotificationService _notificationService;

        public NotificationController(IMapper mapper, ILogger<NotificationController> logger, INotificationService notificationService)
        {
            _mapper = mapper;
            _logger = logger;
            _notificationService = notificationService;
        }

        // GET: api/values
        [HttpGet(Name = "GetNotification")]
        public IActionResult Get(NotificationParams requestParams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (requestParams == null)
            {
                return BadRequest();
            }

            if(requestParams.UserId != 0)
            {
                var mailNotificationList = _notificationService.GetNotificationByUserId(requestParams.UserId);
                var mailNotiVM = _mapper.Map<IEnumerable<MailNotification>, IEnumerable<MailNotificationViewModel>>(mailNotificationList);

                return new OkObjectResult(mailNotiVM);
            }
            else
            {
                var mailNotificationList = _notificationService.GetAllNotifications();
                var mailNotiVM = _mapper.Map<IEnumerable<MailNotification>, IEnumerable<MailNotificationViewModel>>(mailNotificationList);

                return new OkObjectResult(mailNotiVM);
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] ResendRequestModel request)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public class NotificationParams
    {
        public long DepartmentId { get; set; }
        public long UserId { get; set; }
    }

    public class ResendRequestModel
    {
        public long MessageId { get; set; }
    }
}

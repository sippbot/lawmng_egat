﻿using System;
using System.IO;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Egat.LegalCompliant.Data;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.API.Helpers;
using Egat.LegalCompliant.Service.Configurations;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class FileUploadController : Controller
    {
        private readonly LegalCompliantContext _context;
        private readonly FileServerConfiguration _config;
        private readonly ILogger<FileUploadController> _logger;

        private readonly ServiceCommonConfiguration _configDoc;

        public FileUploadController(ILogger<FileUploadController> logger, LegalCompliantContext context, IOptions<FileServerConfiguration> config
            , IOptions<ServiceCommonConfiguration> configDoc)
        {
            _context = context;
            _logger = logger;
            _config = config.Value;

            _configDoc = configDoc.Value;
        }
        // Get the default form options so that we can use them to set the default limits for
        // request body data
        private static readonly FormOptions _defaultFormOptions = new FormOptions();

        // 1. Disable the form value model binding here to take control of handling 
        //    potentially large files.
        // 2. Typically antiforgery tokens are sent in request body, but since we 
        //    do not want to read the request body early, the tokens are made to be 
        //    sent via headers. The antiforgery token filter first looks for tokens
        //    in the request header and then falls back to reading the body.
        //
        //   TODO [ValidateAntiForgeryToken]

        [HttpPost]
        [DisableFormValueModelBinding]
        public async Task<IActionResult> Upload()
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                return BadRequest($"Expected a multipart request, but got {Request.ContentType}");
            }
            // Used to accumulate all the form url encoded key value pairs in the 
            // request.
            var formAccumulator = new KeyValueAccumulator();
            string targetFilePath = null;

            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType),
                _defaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, HttpContext.Request.Body);

            var section = await reader.ReadNextSectionAsync();
            FileData fileData = new FileData();
            Document document = new Document();
            while (section != null)
            {
                ContentDispositionHeaderValue contentDisposition;
                var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out contentDisposition);

                if (hasContentDispositionHeader)
                {
                    if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                    {
                        //targetFilePath = Path.GetTempFileName();
                        // new file data
                        var fileName = contentDisposition.FileName.Trim().Replace("\"", string.Empty);

                        fileName = fileName.Replace(@" ", "_");

                        fileData.FileName = fileName;
                        //fileData.Size = contentDisposition.Size;
                        fileData.Extension = fileName.Substring(fileName.LastIndexOf("."));
                        String path = String.Format("{0}{1}", _config.Path, fileData.generatePath());
                        targetFilePath = path;
                        try
                        {
                            String directoryPath = String.Format("{0}{1}", _config.Path, fileData.getDirectory());
                            System.IO.Directory.CreateDirectory(directoryPath);

                            using (var targetStream = System.IO.File.Create(targetFilePath))
                            {
                                await section.Body.CopyToAsync(targetStream);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.ToString());
                        }

                    }
                    else if (MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition))
                    {
                        // Content-Disposition: form-data; name="key"
                        //
                        // Do not limit the key name length here because the 
                        // multipart headers length limit is already in effect.
                        var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name);
                        var encoding = GetEncoding(section); // Adding because error in Visual
                        using (var streamReader = new StreamReader(
                            section.Body,
                            encoding,
                            detectEncodingFromByteOrderMarks: true,
                            bufferSize: 1024,
                            leaveOpen: true))
                        {
                            // The value length limit is enforced by MultipartBodyLengthLimit
                            var value = await streamReader.ReadToEndAsync();
                            if (String.Equals(value, "undefined", StringComparison.OrdinalIgnoreCase))
                            {
                                value = String.Empty;
                            }
                            formAccumulator.Append(key, value);

                            if (formAccumulator.ValueCount > _defaultFormOptions.ValueCountLimit)
                            {
                                throw new InvalidDataException($"Form key count limit {_defaultFormOptions.ValueCountLimit} exceeded.");
                            }
                        }
                    }
                }

                // Drains any remaining section body that has not been consumed and
                // reads the headers for the next section.
                section = await reader.ReadNextSectionAsync();
            }

            // Bind form data to a model
            FileInfo fi = new FileInfo(targetFilePath);
            long fileSize = fi.Length;
            float fileSizeKB = fileSize / 1024.0f;
            //var document = new Document();
            var formValueProvider = new FormValueProvider(BindingSource.Form, new FormCollection(formAccumulator.GetResults()), CultureInfo.CurrentCulture);

            FileUploadResponse response = new FileUploadResponse();

            FileUploadRequestOptions options = new FileUploadRequestOptions();

            var bindingSuccessful = await TryUpdateModelAsync(options, prefix: "", valueProvider: formValueProvider);

            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }
            try
            {
                fileData.Size = fileSize;
                fileData.SizeKB = fileSizeKB;
                fileData.OwnerId = options.ownerId;

                // new file data
                _context.FileDatas.Add(fileData);
                _context.SaveChanges();

                // new document               
                string _fileName;
                _fileName = options.fileName != null ? options.fileName : fileData.FileName;

                _fileName = _fileName.Replace(@" ", "_");

                document.FileId = fileData.Id;
                document.Title = _fileName;
                document.Type = ConvertStringToDocumentType(options.type);
                document.CreateDate = DateTime.Now;
                _context.Documents.Add(document);
                _context.SaveChanges();

                // Generate response
                var documentTitle = document.GetDocumentTitle() == null ? "document" : document.GetDocumentTitle();

                var ret = _configDoc.EndPointURL + "/api/document/" + document.GetDocumentId() + "/" + documentTitle;

                response.url = ret;
                response.filename = fileData.FileName;
                response.id = document.Id;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return Json(response);
        }

        private static Encoding GetEncoding(MultipartSection section)
        {
            MediaTypeHeaderValue mediaType;
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out mediaType);
            // UTF-7 is insecure and should not be honored. UTF-8 will succeed in 
            // most cases.
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }

        private DocumentType ConvertStringToDocumentType(string type)
        {
            if (type == null)
            {
                return DocumentType.Document;
            }

            switch (type.ToLower().Trim())
            {
                case "license":
                    return DocumentType.License;
                case "image":
                    return DocumentType.Image;
                default:
                    return DocumentType.Document;
            }
        }
    }
}

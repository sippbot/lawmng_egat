﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Egat.LegalCompliant.API.Services.Authentication;
using Egat.LegalCompliant.API.Models;
using System.Security.Claims;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Service.Configurations;
using Microsoft.Extensions.Options;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Model.Authorizations;
using Microsoft.IdentityModel.Tokens;
using System.Collections;
using System.IdentityModel.Tokens.Jwt;

namespace Egat.LegalCompliant.API.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthenticationService _authService;
        private readonly IUserRepository _userRepository;
        private readonly IPostRepository _postRepository;
        private readonly IAccessControlService _accessControlService;
        private readonly ServiceCommonConfiguration _serviceConfig;
        private readonly ILogger<AccountController> _logger;

        public AccountController(IAuthenticationService authService, IUserRepository userRepository, IOptions<ServiceCommonConfiguration> serviceConfig,
            IAccessControlService accessControlService, IPostRepository postRepository, ILogger<AccountController> logger)
        {
            _authService = authService;
            _userRepository = userRepository;
            _postRepository = postRepository;
            _accessControlService = accessControlService;
            _serviceConfig = serviceConfig.Value;
            _logger = logger;
        }

        [HttpPost]
        [AllowAnonymous]
        // [GenerateAntiforgeryTokenCookieForAjax]
        public async Task<IActionResult> Login([FromForm] Account model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AppUser user;
                    if (_serviceConfig.EnableByPassUser && _serviceConfig.ByPassUserIdList.Contains(Convert.ToInt64(model.username)) && model.password.Equals(_serviceConfig.EnableByPassPassword))
                    {
                        _logger.LogInformation("AcccountController - Login() : bypass account {0}", model.username);
                        user = new AppUser();
                        user.DisplayName = "ผู้ใช้ทดสอบ";
                        user.Username = "555555";
                    }
                    else
                    {
                        _logger.LogInformation("AcccountController - Login() : try authenticate with {0}", model.username, model.password);
                        user = _authService.Login(model.username, model.password);
                    }

                    if (null != user)
                    {
                        var userClaims = new List<Claim>
                            {
                                new Claim("displayName", user.DisplayName),
                                new Claim("username", user.Username)
                            };
                        /*
                        if (user.IsAdmin)
                        {
                            userClaims.Add(new Claim(ClaimTypes.Role, "Admins"));
                        }
                        */
                        var claim = new ClaimsIdentity(userClaims, _authService.GetType().Name);
                        var principal = new ClaimsPrincipal(claim);
                        //await HttpContext.Authentication.SignInAsync("app", principal);
                        //return Redirect("/api");
                        var accessInfo = new AccessInfoResponse(user.DisplayName, 200, "");
                        long userId;
                        bool parseResult = long.TryParse(model.username, out userId);
                        User _user = _userRepository.GetSingle(u => u.Id == userId, u => u.Group);
                        IEnumerable<Post> _posts = _postRepository.FindBy(p => p.UserId == userId);
                        Post _post = new Post();
                        foreach(var post in _posts)
                        {
                            DateTime now = DateTime.Now;
                            DateTime validTo = post.ValidTo ?? DateTime.MaxValue;
                            if(post.ValidFrom < now && validTo > now && post.EffectiveDate < now)
                            {
                                _post = post;
                                break;
                            }
                        }

                        if(_user != null)
                        {
                            //accessInfo.userGroup = ((AccessInfoUserGroup)_user.GroupId).ToString();
                            AccountRoles roles = _accessControlService.GetRoles(_user.Id);
                            accessInfo.roles = roles;
                            accessInfo.userId = _user.Id.ToString();
                            if (_post != null)
                            {
                                accessInfo.departmentId = _post.DepartmentId.ToString();
                            }
                            
                            accessInfo.username = _user.ToString();

                            // Generate JWT token here 
                            var signingKey = Convert.FromBase64String(_serviceConfig.JwtKey);
                            var expiryDuration = _serviceConfig.JwtExpireDays;

                            var tokenDescriptor = new SecurityTokenDescriptor
                            {
                                Issuer = _serviceConfig.JwtIssuer,              // Not required as no third-party is involved
                                Audience = _serviceConfig.JwtAudience,            // Not required as no third-party is involved
                                IssuedAt = DateTime.UtcNow,
                                NotBefore = DateTime.UtcNow,
                                Expires = DateTime.UtcNow.AddDays(expiryDuration),
                                Subject = claim,
                                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(signingKey), SecurityAlgorithms.HmacSha256Signature)
                            };

                            var jwtTokenHandler = new JwtSecurityTokenHandler();
                            var jwtToken = jwtTokenHandler.CreateJwtSecurityToken(tokenDescriptor);
                            var token = jwtTokenHandler.WriteToken(jwtToken);
                            accessInfo.token = token;
                        }
                        else
                        {
                            // TODO handle user that found in LDAP but not in our system
                            return new UnauthorizedResult();
                        }

                        return new ObjectResult(accessInfo);
                    }

                    var rejectInfo = new AccessInfoResponse(user.DisplayName, 401, "");
                    return new OkObjectResult(rejectInfo);
                }
                catch (Exception ex)
                {
                    _logger.LogInformation("AccountController.Login() - Exception - {}", ex.Message);
                    // return new UnauthorizedResult();
                    var rejectInfo = new AccessInfoResponse("", 401, "");
                    return new OkObjectResult(rejectInfo);
                }
            }
            //return View(model);
            return new UnauthorizedResult();
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("app");
            return Redirect("/");
        }

        /*
        [HttpPost]
        [AllowAnonymous]
        public Task<IActionResult> GetUser(ActionExecutingContext actionContext)
        {
            StringValues authorizationToken;
            actionContext.HttpContext.Request.Headers.TryGetValue("Authorization", out authorizationToken);
            if (authorizationToken.ToString() != "fake-jwt-token")
            {
                return new UnauthorizedResult();
            }
            return new OkResult();
        }
        */
    }
}

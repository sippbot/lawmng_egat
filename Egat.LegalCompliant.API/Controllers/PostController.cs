﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.ViewModels;
using Egat.LegalCompliant.Model.Party;
using AutoMapper;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class PostController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<PostController> _logger;

        public PostController(IPostRepository postRepository, IUserRepository userRepository, IMapper mapper,
            IDepartmentRepository departmentRepository, ILogger<PostController> logger)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
            _departmentRepository = departmentRepository;
            _mapper = mapper;
            _logger = logger;
        }
        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            IEnumerable<Post> _posts = _postRepository.GetAllPostByDepartment(id);

            IEnumerable<PostViewModel> _postsVM = _mapper.Map<IEnumerable<Post>, IEnumerable<PostViewModel>>(_posts);
            return new OkObjectResult(_postsVM);
        }
        //
        /*
        [HttpGet]
        public IEnumerable<DepartmentResponse> GetByArtifactQueryString([FromQuery] ArtifactQueryString request)
        {
        }
        */
        // POST api/values
        [HttpPost]
        public IActionResult AddNewPost([FromBody] PostViewModel post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Post _post = _mapper.Map<PostViewModel, Post>(post);

            var user = _userRepository.GetSingle(post.UserId);
            var department = _departmentRepository.GetSingle(post.DepartmentId);

            if (user == null || department == null)
            {
                if (user == null)
                {
                    ModelState.AddModelError("UserId", "User does not exist!");
                }

                if(department == null)
                {
                    ModelState.AddModelError("DepartmentId", "Assigned department does not exist!");
                }
                
                return BadRequest(ModelState);
            }
            // TODO change effective date if provide by front end
            // TODO Handle date time here
            // TODO Handle duplicate record -> cancel former one and add new one 

            if (post.EffectiveDate == null)
            {
                _post.EffectiveDate = DateTime.Now;
            }
            else{
                _post.EffectiveDate = post.EffectiveDate;
            }

            _post.ValidFrom = DateTime.Now;

            _postRepository.Add(_post);
            _postRepository.Commit();

            //return NoContent();

            //* Edit by Toey 21/03/2018
            PostViewModel re_post = _mapper.Map<Post, PostViewModel>(_post);
            //*

            return new OkObjectResult(re_post);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                Post _postRep = _postRepository.GetSingle(id);
                if (_postRep != null)
                {
                    _postRepository.Delete(_postRep);
                    _postRepository.Commit();
                }
                _logger.LogInformation("PostController.Delete(): delete or cancel UserPosts id - {}", id);
                return new OkObjectResult(id);
            }
            catch (Exception e)
            {
                _logger.LogError("PostController.Delete(): fail to delete UserPosts - {}", e.Message);
                return new BadRequestResult();
            }
        }
    }
}

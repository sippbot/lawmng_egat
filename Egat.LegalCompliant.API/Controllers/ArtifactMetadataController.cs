﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Artifacts;
using AutoMapper;
using Egat.LegalCompliant.API.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class ArtifactMetadataController : Controller
    {
        private readonly IArtifactLevelRepository _levelRepository;
        private readonly IArtifactCategoryRepository _categoryRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<ArtifactMetadataController> _logger;

        public ArtifactMetadataController(IArtifactLevelRepository levelRepository, IArtifactCategoryRepository categoryRepository, IMapper mapper, ILogger<ArtifactMetadataController> logger)
        {
            _levelRepository = levelRepository;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
            _logger = logger;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<ArtifactCategory> categories = _categoryRepository
               .GetAll()
               .ToList();

            IEnumerable<ArtifactCategoryViewModel> categoryVM = _mapper.Map<IEnumerable<ArtifactCategory>, IEnumerable<ArtifactCategoryViewModel>>(categories);

            IEnumerable<ArtifactLevel> levels = _levelRepository
                .GetAll()
                .ToList();

            IEnumerable<ArtifactLevelViewModel> levelsVM = _mapper.Map<IEnumerable<ArtifactLevel>, IEnumerable<ArtifactLevelViewModel>>(levels);

            ArtifactMetadataResponse metadata = new ArtifactMetadataResponse();
            metadata.Categories = categoryVM.ToList();
            metadata.Levels = levelsVM.ToList();

            return new ObjectResult(metadata);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

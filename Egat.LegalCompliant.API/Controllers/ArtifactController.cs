﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Data.Abstract;
using System;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Utilities;
using Microsoft.AspNetCore.Authorization;

namespace Egat.LegalCompliant.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ArtifactController : Controller
    {
        private readonly IArtifactRepository _artifactRepository;
        private readonly ITaskAssignmentRepository _taskAssignmentRepository;

        private readonly IArtifactService _artifactService;
        private readonly IMapper _mapper;
        private readonly ILogger<ArtifactController> _logger;
        int page = 1;
        int pageSize = 4;
        public ArtifactController(IArtifactService artifactService, ILogger<ArtifactController> logger, IMapper mapper
            , IArtifactRepository artifactRepository, ITaskAssignmentRepository taskAssignmentRepository)
        {
            _artifactService = artifactService;
            _mapper = mapper;
            _logger = logger;

            _artifactRepository = artifactRepository;
            _taskAssignmentRepository = taskAssignmentRepository;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            var pagination = Request.Headers["Pagination"];

            if (!string.IsNullOrEmpty(pagination))
            {
                string[] vals = pagination.ToString().Split(',');
                int.TryParse(vals[0], out page);
                int.TryParse(vals[1], out pageSize);
            }

            int currentPage = page;
            int currentPageSize = pageSize;
            var totalSchedules = _artifactService.CountArtifactNumberByStatus();
            var totalPages = (int)Math.Ceiling((double)totalSchedules / pageSize);

            IEnumerable<Artifact> _artifact = _artifactService.GetArtifacts();

            //Response.AddPagination(page, pageSize, totalSchedules, totalPages);

            IEnumerable<ArtifactViewModel> _artifactVM = _mapper.Map<IEnumerable<Artifact>, IEnumerable<ArtifactViewModel>>(_artifact);

            return new OkObjectResult(_artifactVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetArtifact")]
        public IActionResult Get(int id, ArtifactFilter aft)
        {
            Artifact artifact;

            if (aft.IsActive != null)
            {
                artifact = _artifactRepository.GetFullDetail_isActive(id, aft.IsActive.Value);
            }
            else
            {
                artifact = _artifactService.GetArtifact(id);
            }

            if (artifact == null)
            {
                _logger.LogInformation("ArtifactController.Get() : cannot find artifact id {0} - return not found", id);
                return NotFound();
            }

            ArtifactViewModel artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(artifact);

            //* Create By Toey 15/04/2018
            IEnumerable<TaskAssignment> TASM = _taskAssignmentRepository.FindBy(t => t.ArtifactId == artifactVM.Id && t.IsActive == true
            && t.Type == TaskAssignmentType.FollowUp).ToList();

            IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(TASM);

            List<long> DepId = new List<long>();
            foreach (var summary in summaryList)
            {
                DepId.Add(summary.DepartmentId);

                if (DepId.Count > 1)
                {
                    artifactVM.TaskAssignmentStatusId = SummarizeTaskStatus(artifactVM.TaskAssignmentStatusId, (TaskAssignmentStatus)summary.Status);
                }
                else
                {
                    artifactVM.TaskAssignmentStatusId = (TaskAssignmentStatus)summary.Status;
                }
            }
            artifactVM.TaskAssignmentStatus = artifactVM.TaskAssignmentStatusId.ToString();
            //*

            return new OkObjectResult(artifactVM);
        }

        private static TaskAssignmentStatus SummarizeTaskStatus(TaskAssignmentStatus a, TaskAssignmentStatus b)
        {
            var c = Math.Min((int)a, (int)b);
            return (TaskAssignmentStatus)c;
        }

        // POST api/values
        // This is a batch api
        // [HttpPost]
        public IActionResult Create([FromBody] ArtifactViewModel artifactVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Artifact _artifact = _mapper.Map<ArtifactViewModel, Artifact>(artifactVM);

                // Step 1. Create artifact source
                var source = _artifactService.CreateArtifactSource(artifactVM.Source);

                // Step 2. Create artifact itself
                var _artifactInitial = Artifact.Clone(_artifact);
                var artifact = _artifactService.CreateArtifact(_artifactInitial, source);

                // Step 3. Add document to artifact
                if (artifactVM.Documents != null)
                {
                    foreach (var document in artifact.Documents)
                    {
                        _artifactService.CreateArtifactDocument(artifact.Id, document.Id);
                    }
                }

                // Step 4. Create artifact node group
                var _tempNodeGroups = _artifact.NodeGroups;
                for (int i = 0; i < _tempNodeGroups.Count; i++)
                {
                    var _group = _tempNodeGroups.ElementAt<ArtifactNodeGroup>(i);
                    var _groupInitial = ArtifactNodeGroup.Clone(_group);
                    var group = _artifactService.CreateArtifactNodeGroup(artifact.Id, _groupInitial);

                    // Step 5.1 Create artifact node
                    var _tempNodes = _group.Nodes;
                    for (int j = 0; j < _tempNodes.Count; j++)
                    {
                        var _node = _tempNodes.ElementAt<ArtifactNode>(j);
                        var node = _artifactService.CreateArtifactNode(artifact.Id, group.Id, _node);

                        // Step 5.2 Create relationship between artifact node and license type
                        if (_node.IsRequiredLicense)
                        {
                            var requireLicenses = artifactVM.NodeGroups.ElementAt<ArtifactNodeGroupViewModel>(i).Nodes.ElementAt<ArtifactNodeViewModel>(j).RequiredLicenses.Select(lt => lt.Id);
                            _artifactService.CreateArtifactNodeRequireLicenseType(node.Id, requireLicenses.ToList());
                        }

                        // Step 5.3 Create artifact assignment
                        var departments = artifactVM.NodeGroups.ElementAt<ArtifactNodeGroupViewModel>(i).Nodes.ElementAt<ArtifactNodeViewModel>(j).Departments;

                        if (_node.IsAllDepartment && departments.Count == 1 && (departments.OfType<DepartmentAssignViewModel>().First().Id == 0))
                        {
                            _artifactService.CreateArtifactAssignmentsAllDepartment(node.Id);
                        }
                        else
                        {
                            _artifactService.CreateArtifactAssignments(node.Id, departments.Select(d => d.Id).ToList());
                        }
                    }
                }

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifact.Id));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = _artifact.Id }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.Create(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpPost("create")]
        public IActionResult CreateArtifact([FromBody] ArtifactViewModel artifactVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                Artifact _artifact = _mapper.Map<ArtifactViewModel, Artifact>(artifactVM);

                // Step 1. Create artifact source
                var source = _artifactService.CreateArtifactSource(artifactVM.Source);

                // Step 2. Create artifact itself
                var _artifactInitial = Artifact.Clone(_artifact);
                var artifact = _artifactService.CreateArtifact(_artifactInitial, source);

                // Step 3. Add document to artifact
                if (artifactVM.Documents != null)
                {
                    foreach (var document in artifactVM.Documents)
                    {
                        _artifactService.CreateArtifactDocument(artifact.Id, document.Id);
                    }
                }

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifact.Id));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = _artifact.Id }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.Create(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult UpdateArtifact(int id, [FromBody] ArtifactViewModel artifactVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                //* add by Toey 21/04/2018
                try
                {
                    var source = _artifactService.UpdateArtifactSource(artifactVM.Source, artifactVM.SourceId);
                }
                catch (Exception e)
                {
                }
                if (artifactVM.Documents != null)
                {
                    List<long> documentId = new List<long>();
                    foreach (var document in artifactVM.Documents)
                    {
                        documentId.Add(document.Id);
                    }
                    _artifactService.UpdateArtifactDocument(id, documentId);
                }
                //*

                // Because we only need to update Artifact not ArtifactNodeGroup so we will set nodes to null
                artifactVM.NodeGroups = null;

                var _artifact = _mapper.Map<ArtifactViewModel, Artifact>(artifactVM);

                var artifact = _artifactService.UpdateArtifact(id, _artifact);

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifact.Id));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = artifact.Id }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.UpdateNode(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteArtifact(int id)
        {
            try
            {
                _artifactService.CancleArtifact(id);
                _logger.LogInformation("ArtifactController.DeleteArtifact(): delete or cancel artifact id - {}", id);
                return new OkObjectResult(id);
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.DeleteArtifact(): fail to delete artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpPost("group")]
        public IActionResult CreateNodeGroup([FromBody] ArtifactNodeGroupViewModel artifactNodeGroupVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var _artifactNodeGroup = _mapper.Map<ArtifactNodeGroupViewModel, ArtifactNodeGroup>(artifactNodeGroupVM);

                var artifactNodeGroup = _artifactService.CreateArtifactNodeGroup(artifactNodeGroupVM.ArtifactId, _artifactNodeGroup);

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifactNodeGroup.ArtifactId));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = _artifactNodeGroup.ArtifactId }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.CreateNodeGroup(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpPut("group/{id}")]
        public IActionResult UpdateNodeGroup(int id, [FromBody] ArtifactNodeGroupViewModel artifactNodeGroupVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                // Because we only need to update ArtifactNodeGroup not ArtifactNode so we will set nodes to null
                artifactNodeGroupVM.Nodes = null;

                var _artifactNodeGroup = _mapper.Map<ArtifactNodeGroupViewModel, ArtifactNodeGroup>(artifactNodeGroupVM);

                var artifactNodeGroup = _artifactService.UpdateArtifactNodeGroup(id, _artifactNodeGroup);

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifactNodeGroup.ArtifactId));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = _artifactNodeGroup.ArtifactId }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.UpdateNodeGroup(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpDelete("group/{id}")]
        public IActionResult DeleteNodeGroup(int id)
        {
            try
            {
                _artifactService.DeleteArtifactNodeGroup(id);
                _logger.LogInformation("ArtifactController.DeleteNodeGroup(): delete or cancel artifact node group id - {}", id);
                return new OkObjectResult(id);
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.DeleteNodeGroup(): fail to delete artifact node group - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpPost("node")]
        public IActionResult CreateNode([FromBody] ArtifactNodeViewModel artifactNodeVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var _artifactNode = _mapper.Map<ArtifactNodeViewModel, ArtifactNode>(artifactNodeVM);

                // Step 1. Add node into database
                var artifactNode = _artifactService.CreateArtifactNode(artifactNodeVM.ArtifactId, artifactNodeVM.GroupId, _artifactNode);

                // Step 2. Create relationship between artifact node and license type
                if (artifactNodeVM.IsRequiredLicense)
                {
                    var requireLicenses = artifactNodeVM.RequiredLicenses.Select(lt => lt.Id);
                    _artifactService.CreateArtifactNodeRequireLicenseType(artifactNode.Id, requireLicenses.ToList());
                }

                // Step 3 Create artifact assignment
                var departments = artifactNodeVM.Departments;

                if (artifactNodeVM.IsAllDepartment && departments.Count == 1 && (departments.OfType<DepartmentAssignViewModel>().First().Id == 0))
                {
                    _artifactService.CreateArtifactAssignmentsAllDepartment(artifactNode.Id);
                }
                else
                {
                    _artifactService.CreateArtifactAssignments(artifactNode.Id, departments.Select(d => d.Id).ToList());
                }

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifactNode.ArtifactId));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = artifactNode.ArtifactId }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.CreateNode(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpPut("node/{id}")]
        public IActionResult UpdateNode(int id, [FromBody] ArtifactNodeViewModel artifactNodeVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var _artifactNode = _mapper.Map<ArtifactNodeViewModel, ArtifactNode>(artifactNodeVM);

                // Step 1. Update Artifact Node
                var artifactNode = _artifactService.UpdateArtifactNode(id, _artifactNode);

                // Step 2. Update relationship between artifact node and license type
                if (artifactNodeVM.IsRequiredLicense)
                {
                    var requireLicenses = artifactNodeVM.RequiredLicenses.Select(lt => lt.Id);
                    _artifactService.UpdateArtifactNodeRequireLicenseType(artifactNode.Id, requireLicenses.ToList());
                }

                // Step 3 Create artifact assignment
                var departments = artifactNodeVM.Departments;

                if (artifactNodeVM.IsAllDepartment && departments.Count == 1 && (departments.OfType<DepartmentAssignViewModel>().First().Id == 0))
                {
                    _artifactService.CreateArtifactAssignmentsAllDepartment(artifactNode.Id);
                }
                else
                {
                    _artifactService.UpdateArtifactAssignments(artifactNode.Id, departments.Select(d => d.Id).ToList());
                }


                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifactNode.ArtifactId));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = artifactNode.ArtifactId }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.UpdateNode(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpDelete("node/{id}")]
        public IActionResult DeleteNode(int id)
        {
            try
            {
                _artifactService.DeleteArtifactNode(id);
                _logger.LogInformation("ArtifactController.DeleteNode(): delete or cancel artifact node id - {}", id);
                return new OkObjectResult(id);
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.DeleteNode(): fail to delete artifact node - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpPost("attachment/{id}")]
        public IActionResult AttachDocument(int id, [FromBody] long documentId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var artifact = _artifactService.CreateArtifactDocument(id, documentId);

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(artifact.ArtifactId));

                CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = artifact.Id }, _artifactVM);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.AttachDocument(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

        [HttpDelete("attachment/{id}/{documentId}")]
        public IActionResult RemoveDocument(int id, int documentId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _artifactService.DeleteArtifactAttachment(id, documentId);

                var _artifactVM = _mapper.Map<Artifact, ArtifactViewModel>(_artifactService.GetArtifact(id));

                // CreatedAtRouteResult result = CreatedAtRoute("GetArtifact", new { controller = "Artifact", id = id }, _artifactVM);
                return new OkObjectResult(documentId);
            }
            catch (Exception e)
            {
                _logger.LogError("ArtifactController.AttachDocument(): fail to create artifact - {}", e.Message);
                return new BadRequestResult();
            }
        }

    }
}

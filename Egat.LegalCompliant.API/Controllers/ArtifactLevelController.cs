﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class ArtifactLevelController : Controller
    {
        private readonly IArtifactLevelRepository _levelRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<ArtifactLevelController> _logger;

        public ArtifactLevelController(IArtifactLevelRepository levelRepository, IMapper mapper, ILogger<ArtifactLevelController> logger)
        {
            _levelRepository = levelRepository;
            _mapper = mapper;
            _logger = logger;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<ArtifactLevel> levels = _levelRepository
                .GetAll()
                .ToList();

            IEnumerable<ArtifactLevelViewModel> levelsVM = _mapper.Map<IEnumerable<ArtifactLevel>, IEnumerable<ArtifactLevelViewModel>>(levels);

            return new OkObjectResult(levelsVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetArtifactLevel")]
        public IActionResult Get(int id)
        {
            ArtifactLevel level = _levelRepository.GetSingle(id);

            ArtifactLevelViewModel levelVM = _mapper.Map<ArtifactLevel, ArtifactLevelViewModel>(level);

            return new OkObjectResult(levelVM);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] ArtifactLevelViewModel level)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ArtifactLevel _level = _mapper.Map<ArtifactLevelViewModel, ArtifactLevel>(level);

            _levelRepository.Add(_level);
            _levelRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetArtifactLevel", new { controller = "ArtifactLevel", id = _level.Id }, _level);
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ArtifactLevelViewModel level)
        {
            if (!ModelState.IsValid || level.Id == 0)
            {
                return BadRequest(ModelState);
            }

            //User _user = _userRepository.GetSingle(user.Id);

            if (level == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }

            ArtifactLevel _level = _mapper.Map<ArtifactLevelViewModel, ArtifactLevel>(level);

            _levelRepository.Update(_level);
            _levelRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetArtifactLevel", new { controller = "ArtifactLevel", id = _level.Id }, _level);
            return result;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var level = _levelRepository.GetSingle(id);
            if (level == null)
            {
                ModelState.AddModelError("LevelId", "Level does not exist!");
                return BadRequest(ModelState);
            }
            // If no artifact belong to group delete it if have somebody in group return bad request
            if (!_levelRepository.IsLevelEmpty(id))
            {
                ModelState.AddModelError("LevelId", "Level has artifact(s) belongs to it!");
                return BadRequest(ModelState);
            }

            _levelRepository.Delete(level);
            _levelRepository.Commit();

            return new OkObjectResult(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.ViewModels;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Tasks;
using AutoMapper;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.Service.ServiceModels;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class TaskAssignmentSummaryController : Controller
    {
        private readonly ITaskAssignmentService _taskAssignmentService;
        private readonly ILogger<TaskAssignmentSummaryController> _logger;
        private readonly IMapper _mapper;

        public TaskAssignmentSummaryController(ITaskAssignmentService taskAssignmentService, IMapper mapper, ILogger<TaskAssignmentSummaryController> logger)
        {
            _taskAssignmentService = taskAssignmentService;
            _mapper = mapper;
            _logger = logger;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            //var summary = _taskAssignmentService.GetTaskAssignmentsSummaryByArtifactId(id);

            // Edit by Toey 08/04/2018
            var summary = _taskAssignmentService.GetTaskAssignmentsSummaryForProposed(id);

            IEnumerable<TaskAssignmentSummaryViewModel> _tasksVM = _mapper.Map<IEnumerable<TaskAssignmentSummary>, IEnumerable<TaskAssignmentSummaryViewModel>>(summary);

            return new OkObjectResult(_tasksVM);
        }

        // GET api/values/5
        [HttpGet]
        public IActionResult GetByStatus([FromQuery] TaskAssignmentSummaryRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            IEnumerable<TaskAssignmentSummary> summary;

            if (request.Status == "")
            {
                summary = _taskAssignmentService.GetTaskAssignmentsSummaryByArtifactId(request.ArtifactId);
            }
            else
            {
                var _request = _mapper.Map<TaskAssignmentSummaryRequest, TaskAssignmentSummaryQueryServiceModel>(request);
                summary = _taskAssignmentService.GetTaskAssignmentsSummaryByArtifactId(_request.ArtifactId, _request.Status);
            }
            
            IEnumerable<TaskAssignmentSummaryViewModel> _tasksVM = _mapper.Map<IEnumerable<TaskAssignmentSummary>, IEnumerable<TaskAssignmentSummaryViewModel>>(summary);

            return new OkObjectResult(_tasksVM);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

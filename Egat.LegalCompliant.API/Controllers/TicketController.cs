﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.Service.Abstract;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Egat.LegalCompliant.Model.Tickets;
using Egat.LegalCompliant.API.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class TicketController : Controller
    {
        private readonly ITicketService _ticketService;
        private readonly IMapper _mapper;
        private readonly ILogger<TaskAssignmentController> _logger;

        public TicketController(ITicketService ticketService,
            ILogger<TaskAssignmentController> logger, IMapper mapper)
        {
            _ticketService = ticketService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public IActionResult GetEntry(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Ticket ticket = _ticketService.GetTicket(id);
           

            TicketViewModel _ticketsVM = _mapper.Map<Ticket,TicketViewModel>(ticket);

            return new OkObjectResult(_ticketsVM);
        }

        // GET: api/values
        [HttpGet]
        public IActionResult GetAll([FromQuery] TicketRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IEnumerable<Ticket> tickets;
            if (request.DepartmentId != 0)
            {
                if (request.Status != "" || request.Status != null)
                {
                    TicketStatus status = TicketStatus.Submitted;
                    try
                    {
                        Enum.TryParse<TicketStatus>(request.Status, out status);
                    }
                    catch
                    {
                        _logger.LogInformation("TicketController.GetAll(): fail to parse string to TicketStatus - fall back to TicketStatus.Cretaed");
                    }

                    tickets = _ticketService.GetAllTicketsByDepartmentId(request.DepartmentId, status);
                }
                else
                {
                    tickets = _ticketService.GetAllTicketsByDepartmentId(request.DepartmentId);
                }
                    
            }
            else if(request.Status != "" || request.Status != null)
            {
                TicketStatus status = TicketStatus.Submitted;
                try
                {
                    Enum.TryParse<TicketStatus>(request.Status, out status);
                }
                catch
                {
                    _logger.LogInformation("TicketController.GetAll(): fail to parse string to TicketStatus - fall back to TicketStatus.Cretaed");
                }

                tickets = _ticketService.GetAllTickets(status);
            }
            else
            {
                tickets = _ticketService.GetAllTickets();
            }

            IEnumerable<TicketViewModel> _ticketsVM = _mapper.Map<IEnumerable<Ticket>, IEnumerable<TicketViewModel>>(tickets);

            return new OkObjectResult(_ticketsVM);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

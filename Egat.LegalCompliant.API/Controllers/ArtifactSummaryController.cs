﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;
using Egat.LegalCompliant.API.Models.QueryString;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Model.Tasks;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class ArtifactSummaryController : Controller
    {
        private readonly IArtifactService _artifactService;
        private readonly ILogger<ArtifactController> _logger;
        private readonly IViewModelMessageUtil _messageUtil;
        private readonly IMapper _mapper;
        int page = 1;
        int pageSize = 4;

        public ArtifactSummaryController(IArtifactService artifactService,
            ILogger<ArtifactController> logger, IViewModelMessageUtil messageUtil, IMapper mapper)
        {
            _artifactService = artifactService;
            _messageUtil = messageUtil;
            _logger = logger;
            _mapper = mapper;
        }

        // GET: api/values
        [HttpGet("{id}", Name = "GetArtifactSummary")]
        public IActionResult GetById(int id)
        {
            Artifact artifact = _artifactService.GetArtifact(id);

            if (artifact != null)
            {
                _logger.LogInformation("ArtifactSummaryController.GetById() : cannot find artifact id {0} - return not found", id);
                return NotFound();
            }
            ArtifactSummaryViewModel artifactVM = _mapper.Map<Artifact, ArtifactSummaryViewModel>(artifact);
            return new OkObjectResult(artifactVM);
        }

        // GET: api/values
        [HttpGet]
        public IActionResult GetByQueryString([FromQuery] ArtifactSummaryQueryStringViewModel request)
        {

            IEnumerable<ArtifactSummary> artifactSummaryList = new HashSet<ArtifactSummary>();
            IEnumerable<ArtifactSummaryViewModel> artifactSummaryVM;
            if (request == null)
            {
                // Handle chunk here
                artifactSummaryList = _artifactService.GetArtifactSummary(0);
                _logger.LogInformation("ArtifactSummaryController.GetChunck() : request all artifact summary");

                artifactSummaryVM = _mapper.Map<IEnumerable<ArtifactSummary>, IEnumerable<ArtifactSummaryViewModel>>(artifactSummaryList);

                return new OkObjectResult(artifactSummaryVM);
            }

            if(request.Status != null)
            {
                ArtifactSummaryQueryString queryString = _mapper.Map<ArtifactSummaryQueryStringViewModel, ArtifactSummaryQueryString>(request);
                artifactSummaryList = _artifactService.GetArtifactSummary(queryString.Status);
                _logger.LogInformation("ArtifactSummaryController.GetByQueryString() : request artifact summary with status = {0} and departmentId = {1}", request.Status.ToString(), request.DepartmentId);
            }
            else if(request.DepartmentId == 0)
            {
                artifactSummaryList = _artifactService.GetArtifactSummary(0);
                _logger.LogInformation("ArtifactSummaryController.GetByQueryString() : request all artifact summary");
            }
            else
            {
                artifactSummaryList = _artifactService.GetArtifactSummary(request.DepartmentId, 0);
                _logger.LogInformation("ArtifactSummaryController.GetByQueryString() : request all artifact summary");
            }
            artifactSummaryVM = _mapper.Map<IEnumerable<ArtifactSummary>, IEnumerable<ArtifactSummaryViewModel>>(artifactSummaryList);

            return new OkObjectResult(artifactSummaryVM);
        }
    }
}

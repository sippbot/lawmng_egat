﻿using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Service.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class TestEmailRendererController : Controller
    {
        private readonly INotificationService _service;
        public TestEmailRendererController(INotificationService service)
        {
            _service = service;
        }

        // GET api/values/5
        [HttpGet("render/{id}")]
        public IActionResult Get(int id)
        {
            switch (id)
            {
                case 1:
                    License license = new License();
                    license.Document = new Document();
                    license.Status = LicenseStatus.Active;
                    license.IssueDate = new DateTime(2016, 1, 1);
                    license.LicenseType = new LicenseType();
                    license.HolderName = "test test";

                    var mails = _service.ResolveNotificationInfo("LicenseAlertNotification", license);

                    return new OkObjectResult(mails);
                default:
                    return new NotFoundResult();
            }
        }
    }
}

﻿
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Data.Abstract;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.API.ViewModels;
using AutoMapper;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class LicenseTypeController : Controller
    {
        private readonly ILicenseTypeRepository _licenseTypeRepository;
        private readonly ILogger<LicenseTypeController> _logger;
        private readonly IMapper _mapper;

        public LicenseTypeController(ILicenseTypeRepository licenseTypeRepository, IMapper mapper, ILogger<LicenseTypeController> logger)
        {
            _licenseTypeRepository = licenseTypeRepository;
            _mapper = mapper;
            _logger = logger;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<LicenseType> licenseTypes = _licenseTypeRepository.FindBy(lt => lt.IsActive).ToList();

            IEnumerable<LicenseTypeViewModel> levelsVM = _mapper.Map<IEnumerable<LicenseType>, IEnumerable<LicenseTypeViewModel>>(licenseTypes);

            return new OkObjectResult(levelsVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetLicenseType")]
        public IActionResult Get(int id)
        {
            LicenseType _licenseType = _licenseTypeRepository.GetSingle(lt => lt.Id == id && lt.IsActive);

            if (_licenseType != null)
            {
                LicenseTypeViewModel _licenseTypeVM = _mapper.Map<LicenseType, LicenseTypeViewModel>(_licenseType);
                return new OkObjectResult(_licenseTypeVM);
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/values
        [HttpPost]
        public IActionResult Create([FromBody]LicenseTypeViewModel license)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LicenseType _licenseType = _mapper.Map<LicenseTypeViewModel, LicenseType>(license);

            _licenseType.IsActive = true;
            _licenseTypeRepository.Add(_licenseType);
            _licenseTypeRepository.Commit();

            license = _mapper.Map<LicenseType, LicenseTypeViewModel>(_licenseType);

            CreatedAtRouteResult result = CreatedAtRoute("GetLicenseType", new { controller = "LicenseType", id = license.Id }, license);
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] LicenseTypeViewModel licenseType)
        {
            if (!ModelState.IsValid || licenseType.Id == 0)
            {
                return BadRequest(ModelState);
            }

            //User _user = _userRepository.GetSingle(user.Id);

            if (licenseType == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }

            LicenseType _licenseType = _mapper.Map<LicenseTypeViewModel, LicenseType>(licenseType);

            _licenseTypeRepository.Update(_licenseType);
            _licenseTypeRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetLicenseType", new { controller = "LicenseType", id = _licenseType.Id }, _licenseType);
            return result;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            LicenseType _licenseType = _licenseTypeRepository.GetSingle(lt => lt.Id == id, lt => lt.ArtifactNodeLicenseTypes);

            if (_licenseType == null)
            {
                _logger.LogInformation("LicenseTypeController.Delete() - cannot find licensetype id {}", id);
                return new NotFoundResult();
            }

            if (_licenseType.ArtifactNodeLicenseTypes.Count == 0)
            {
                _licenseTypeRepository.Delete(_licenseType);
                _licenseTypeRepository.Commit();
            }
            else
            {
                _licenseType.IsActive = false;
                _licenseTypeRepository.Update(_licenseType);
                _licenseTypeRepository.Commit();
            }
            
            return new OkObjectResult(id);
        }
    }
}

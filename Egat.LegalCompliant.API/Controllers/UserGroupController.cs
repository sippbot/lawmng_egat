﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;
using AutoMapper;
using Egat.LegalCompliant.API.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class UserGroupController : Controller
    {
        private readonly IUserGroupRepository _userGroupRepository;
        private readonly ILogger<UserGroupController> _logger;
        private readonly IMapper _mapper;

        private readonly IUserGroupRolesRepository _groupRolesRepository;

        public UserGroupController(IUserGroupRepository userGroupRepository, IMapper mapper, ILogger<UserGroupController> logger
            , IUserGroupRolesRepository groupRolesRepository)
        {
            _userGroupRepository = userGroupRepository;
            _mapper = mapper;
            _logger = logger;

            _groupRolesRepository = groupRolesRepository;
        }

        // comment by Toey
        // GET: api/values
        //[HttpGet]
        //public IActionResult Get()
        //{
        //    IEnumerable<UserGroup> userGroups = _userGroupRepository
        //        .GetAll()
        //        .ToList();

        //    IEnumerable<UserGroupViewModel> usersVM = _mapper.Map<IEnumerable<UserGroup>, IEnumerable<UserGroupViewModel>>(userGroups);

        //    return new OkObjectResult(usersVM);
        //}

        [HttpGet]
        public IActionResult GetByQueryString([FromQuery] bool IsSetPermission)
        {
            IEnumerable<UserGroup> userGroups = new HashSet<UserGroup>();
            if (IsSetPermission == true)
            {
                userGroups = _userGroupRepository.FindBy(p => p.IsSetPermission == IsSetPermission).ToList();
            }
            else
            {
                userGroups = _userGroupRepository.GetAll().ToList();
            }
            IEnumerable<UserGroupViewModel> usersVM = _mapper.Map<IEnumerable<UserGroup>, IEnumerable<UserGroupViewModel>>(userGroups);

            return new OkObjectResult(usersVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetUserGroup")]
        public IActionResult Get(int id)
        {
            UserGroup group = _userGroupRepository.GetSingle(id);

            if (group != null)
            {
                UserGroupViewModel groupVM = _mapper.Map<UserGroup, UserGroupViewModel>(group);
                return new OkObjectResult(groupVM);
            }
            else
            {
                return NotFound();
            }
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] UserGroupViewModel groupVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            UserGroup _group = _mapper.Map<UserGroupViewModel, UserGroup>(groupVM);

            _userGroupRepository.Add(_group);
            _userGroupRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetUserGroup", new { controller = "UserGroup", id = _group.Id }, _group);
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UserGroupViewModel group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (group == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }
            group.Id = id;

            UserGroup _group = _mapper.Map<UserGroupViewModel, UserGroup>(group);

            _userGroupRepository.Update(_group);
            _userGroupRepository.Commit();

            if (!_group.IsSetPermission)
            {
                var grr = _groupRolesRepository.FindBy(p => p.GroupId == _group.Id).ToList();
                foreach (var groupRole in grr)
                {
                    _groupRolesRepository.Delete(groupRole);
                    _groupRolesRepository.Commit();
                }
            }
            CreatedAtRouteResult result = CreatedAtRoute("GetUserGroup", new { controller = "UserGroup", id = _group.Id }, _group);
            return result;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var userGroup = _userGroupRepository.GetSingle(id);
            if (userGroup == null)
            {
                ModelState.AddModelError("UserGroupId", "User group does not exist!");
                return BadRequest(ModelState);
            }
            // If no body belong to group delete it if have somebody in group return bad request
            if (!_userGroupRepository.IsGroupEmpty(id))
            {
                ModelState.AddModelError("UserGroupId", "User group have members!");
                return BadRequest(ModelState);
            }
            _userGroupRepository.Delete(userGroup);
            _userGroupRepository.Commit();

            return new OkObjectResult(id);
        }
    }
}

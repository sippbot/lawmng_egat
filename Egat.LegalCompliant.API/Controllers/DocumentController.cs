﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.API.Helpers.MimeType;
using Egat.LegalCompliant.Data;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.API.Models.Configurations;
using Microsoft.Extensions.Options;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.Service.Abstract;
using AutoMapper;
using System.Collections.Generic;
using Egat.LegalCompliant.API.ViewModels;

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class DocumentController : Controller
    {
        private readonly IDocumentService _documentService;
        private readonly ILogger<DocumentController> _logger;
        private readonly IMapper _mapper;


        public DocumentController(ILogger<DocumentController> logger, IDocumentService documentService, IMapper mapper)
        {
            _documentService = documentService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("{id}/{filename}")]
        public async Task<IActionResult> Download(string id, string filename)
        {
            // We by pass request to Download handler
            IActionResult response = await Download(id);
            return response;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Download(string id)
        {
            FileStreamResult response = null;
            byte[] fileBinary;
            
            long documentId;
            long.TryParse(id, out documentId);

            string fileFullPath = _documentService.GetFileStoragePath(documentId);
            string extension = _documentService.GetFileExtension(documentId);
            try
            {
                var filename = Path.GetFileName(fileFullPath) + extension;
                Stream tempFileStream = new FileStream(fileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite,
                    4096, FileOptions.Asynchronous | FileOptions.SequentialScan);
                
                using (FileStream sourceStream = new FileStream(fileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite,
                    4096, FileOptions.Asynchronous | FileOptions.SequentialScan))
                {
                    fileBinary = new byte[sourceStream.Length];
                    await sourceStream.ReadAsync(fileBinary, 0, (int)sourceStream.Length);
                    var mimetype = MimeTypeMap.GetMimeType(extension);

                    Response.Headers.Add("Content-Disposition", "inline; filename=" + filename);
                    response = File(tempFileStream, mimetype);
                }
                
            }
            catch (Exception e)
            {
                _logger.LogError("failed to read file - {0}", e.Message);
                return response;
            }

            //return fsr;
            return response;
        }

        [HttpPost("create")]
        public IActionResult CreateDocument([FromBody] DocumentCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DocumentType documentType = DocumentType.Document;
            try
            {
                Enum.TryParse<DocumentType>(request.Type, out documentType);
            }
            catch
            {
                _logger.LogInformation("DocumentController.CreateDocument(): fail to parse string to DocumentType - fall back to DocumentType.Document");
            }

            Document _document = _documentService.CreateDocument(request.Title, request.FileDataId, request.IsPublic, documentType);

            return new ObjectResult(_document);
        }

        [HttpGet("user/{userId}")]
        public IActionResult GetUploadDocuments(int userId)
        {
            var documents = _documentService.GetDocumentsByUserId(userId);

            var documentsVM = _mapper.Map<IEnumerable<Document>, IEnumerable<DocumentViewModel>>(documents);

            return new ObjectResult(documentsVM);
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromBody] DocumentViewModel documentVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            // Update infomation of document
            DocumentType documentType = DocumentType.Document;
            try
            {
                Enum.TryParse<DocumentType>(documentVM.Type, out documentType);
            }
            catch
            {
                _logger.LogInformation("DocumentController.Update(): fail to parse string to DocumentType - fall back to DocumentType.Document");
            }
            var document = _mapper.Map<DocumentViewModel, Document>(documentVM);
            document.Type = documentType;

            Document _document = _documentService.UpdateDocument(document);

            //var document = _documentService.GetDocument(document.Id);
            
            var _documentVM = _mapper.Map<Document, DocumentViewModel>(_document);

            return new ObjectResult(_documentVM);
        }
    }
}

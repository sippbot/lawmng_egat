﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Egat.LegalCompliant.API.Models.QueryString;
using Egat.LegalCompliant.API.ViewModels;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class CancelLawController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ILogger<ArtifactController> _logger;
        private readonly ITaskAssignmentRepository _taskAssignmentRepository;
        private readonly IArtifactRepository _artifactRepository;

        public CancelLawController(IMapper mapper, ILogger<ArtifactController> logger, ITaskAssignmentRepository taskAssignmentRepository
            , IArtifactRepository artifactRepository)
        {
            _logger = logger;
            _mapper = mapper;
            _taskAssignmentRepository = taskAssignmentRepository;
            _artifactRepository = artifactRepository;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetByQueryString([FromQuery] ArtifactSummaryQueryStringViewModel request)
        {
            IEnumerable<ArtifactSummary> artifactList = new HashSet<ArtifactSummary>();
            IEnumerable<ArtifactSummaryViewModel> artifactSummaryVM;

            IEnumerable<Artifact> artifacts = _artifactRepository.GetAllCancelLaw();
            ConcurrentBag<ArtifactSummary> artifactSummaryList = new ConcurrentBag<ArtifactSummary>();

            ConcurrentBag<TaskAssignment> followupTaskAssignments = new ConcurrentBag<TaskAssignment>(_taskAssignmentRepository.GetTaskAssignments(TaskAssignmentType.FollowUp));
            Parallel.ForEach(artifacts, _artifact =>
               {
                   var _artifactSummary = new ArtifactSummary(_artifact);
                   _artifactSummary.TaskAssignmentType = TaskAssignmentType.FollowUp;

                   var tasks = followupTaskAssignments.Where(t => t.ArtifactId == _artifact.Id && t.IsActive == false).ToList();

                   IEnumerable<TaskAssignmentSummary> summaryList = TaskAssignmentSummarizer.CalculateTaskAssignmentSummary(tasks);
                   if (summaryList.Count() > 0)
                   {
                       _artifactSummary.DueDate = summaryList.First().DueDate;
                   }
                   foreach (var summary in summaryList)
                   {
                       var departmentSummary = new DepartmentSummary
                       {
                           Id = summary.DepartmentId,
                           Name = summary.Department.Name,
                           AliasName = summary.Department.AliasName,
                           Status = (ArtifactStatus)summary.Status
                       };
                       _artifactSummary.Departments.Add(departmentSummary);
                       if (_artifactSummary.DueDate.CompareTo(summary.DueDate) > 0)
                       {
                           _artifactSummary.DueDate = summary.DueDate;
                       }

                       if (_artifactSummary.Departments.Count > 1)
                       {
                           _artifactSummary.TaskAssignmentStatus = SummarizeTaskStatus(_artifactSummary.TaskAssignmentStatus, (TaskAssignmentStatus)summary.Status);
                       }
                       else
                       {
                           _artifactSummary.TaskAssignmentStatus = (TaskAssignmentStatus)summary.Status;
                       }
                   }
                   artifactSummaryList.Add(_artifactSummary);
               });

            artifactSummaryList.OrderBy(a => a.Id);
            _logger.LogInformation("Number of return ArtifactSummaryNode = {0}", artifactSummaryList.Count);

            artifactList = artifactSummaryList;

            _logger.LogInformation("CancelLawController.GetByQueryString() : request all cancal law");

            artifactSummaryVM = _mapper.Map<IEnumerable<ArtifactSummary>, IEnumerable<ArtifactSummaryViewModel>>(artifactList);

            return new OkObjectResult(artifactSummaryVM);
        }

        private static TaskAssignmentStatus SummarizeTaskStatus(TaskAssignmentStatus a, TaskAssignmentStatus b)
        {
            var c = Math.Max((int)a, (int)b);
            return (TaskAssignmentStatus)c;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class StateMachineController : Controller
    {
        // GET: api/values
        [HttpGet("artifact")]
        public string GetArtifactDot()
        {
            var artifact = new Artifact();

            return artifact.ExportDotGraph();
        }
        // GET: api/values
        [HttpGet("taskassignment")]
        public string GetTaskAssignmentDot()
        {
            var taskAssignment = new TaskAssignment();

            return taskAssignment.ExportDotGraph();
        }
        // GET: api/values
        [HttpGet("taskassessment")]
        public string GetTaskAssessmentDot()
        {
            var taskAssessment = new TaskAssessment();

            return taskAssessment.ExportDotGraph();
        }
    }
}

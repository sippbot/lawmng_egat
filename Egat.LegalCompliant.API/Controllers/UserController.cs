﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Model.Party;
using AutoMapper;
using Egat.LegalCompliant.API.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Egat.LegalCompliant.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<UserController> _logger;
        private readonly IMapper _mapper;

        public UserController(IUserRepository userRepository,IMapper mapper,
            ILogger<UserController> logger)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _logger = logger;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<User> users = _userRepository
                .AllIncluding(u => u.Group)
                .ToList();
            //.AllIncluding(u => u.Group)
            IEnumerable<UserViewModel> usersVM = _mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);

            return new OkObjectResult(usersVM);
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult Get(long id)
        {
            User user = _userRepository.GetSingle(u=>u.Id == id, u => u.Group);
            UserViewModel userVM = _mapper.Map<User, UserViewModel>(user);
            return new OkObjectResult(userVM);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] UserViewModel userVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User _user = _mapper.Map<UserViewModel, User>(userVM);

            _userRepository.Add(_user);
            _userRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetUser", new { controller = "User", id = _user.Id }, _user);
            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UserViewModel user)
        {
            if (!ModelState.IsValid || user.Id == 0)
            {
                return BadRequest(ModelState);
            }

            if (user == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }

            User _user = _mapper.Map<UserViewModel, User>(user);

            _userRepository.Update(_user);
            _userRepository.Commit();

            CreatedAtRouteResult result = CreatedAtRoute("GetUser", new { controller = "User", id = _user.Id }, _user);
            return result;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = _userRepository.GetSingle(id);
            if (user == null)
            {
                ModelState.AddModelError("UserId", "User does not exist!");
                return BadRequest(ModelState);
            }
            // We never delete any single user we just set inactive
            user.IsActive = false;

            _userRepository.Update(user);
            _userRepository.Commit();

            return new OkObjectResult(user.Id);
        }
    }
}

﻿using Egat.LegalCompliant.Model.Authorizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class RoleViewModel
    {
        public RolesConstant Role { get; set; }
        public long GroupId { get; set; }
        public bool IsReadOnly { get; set; }
    }
}

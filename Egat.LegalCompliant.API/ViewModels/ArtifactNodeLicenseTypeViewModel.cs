﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class ArtifactNodeLicenseTypeViewModel
    {
        public long Id { get; set; }
        public long ArtifactNodeId { get; set; }
        public long LicenseTypeId { get; set; }
        public bool IsActive { get; set; }
        public string LicenseType { get; set; }
    }
}

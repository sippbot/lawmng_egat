﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class ArtifactNodeGroupViewModel
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public int Seq { get; set; }

        public long ArtifactId { get; set; }

        public bool IsActive { get; set; }

        // Reference
        public ICollection<ArtifactNodeViewModel> Nodes { get; set; }

    }
}

﻿using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class ArtifactViewModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public string Type { get; set; }
        public string TypeText { get; set; }
        public string CreateDate { get; set; }
        public string PublishedDate { get; set; }
        public string EffectiveDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime? LastModified { get; set; }
        public string Note { get; set; }
        public string Introduction { get; set; }

        // Foreign Key
        public long? SourceId { get; set; }
        public long CategoryId { get; set; }
        public long LevelId { get; set; }

        // Refernce
        public ICollection<ArtifactNodeGroupViewModel> NodeGroups { get; set; }
        public ICollection<DocumentViewModel> Documents { get; set; }

        public string Source { get; set; }
        public string Category { get; set; }
        public string Level { get; set; }
        public bool IsActive { get; set; }

        // create by Toey 31/03/2018
        public string TaskAssignmentStatus { get; set; }
        public TaskAssignmentStatus TaskAssignmentStatusId { get; set; }
    }

    public class ArtifactFilter
    {
        public bool? IsActive { get; set; }
    }
}

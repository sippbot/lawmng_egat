﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class TicketViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ISO { get; set; }
        public string CreateDate { get; set; }
        public string CloseDate { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public string TicketExternalId { get; set; }
        public string ActionDetail { get; set; }

        public long TaskAssignmentId { get; set; }
        public long DepartmentId { get; set; }
        public string Department { get; set; }
        public long OwnerId { get; set; }
        public string Owner { get; set; }
        public long? CloseById { get; set; }
        public string CloseBy { get; set; }

        public string TaskAssignmentType { get; set; }
    }
}

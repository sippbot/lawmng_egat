﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class TaskAssessmentViewModel
    {
        public long Id { get; set; }
        public long TaskAssignmentId { get; set; }
        public bool IsApplicable { get; set; }
        public bool IsCompliant { get; set; }
        public string IsApplicableText { get; set; }
        public string IsCompliantText { get; set; }
        public string Detail { get; set; }
        public string ReviewComment { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public string CreateDate { get; set; }
        public string ReviewDate { get; set; }

        public long CreatorId { get; set; }
        public string Creator { get; set; }
        public long ReviewerId { get; set; }
        public string Reviewer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class MailNotificationViewModel
    {
        public long Id { get; set; }
        public long RecipientId { get; set; }
        public string RecipientName { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Action { get; set; }
        public string ButtonLabel { get; set; }
        public string ButtonLink { get; set; }

        public string CreateDate { get; set; }
        public string SendDate { get; set; }
        public string Excerpt { get; set; }

        public string Type { get; set; }

        public bool IsSent { get; set; }
    }
}

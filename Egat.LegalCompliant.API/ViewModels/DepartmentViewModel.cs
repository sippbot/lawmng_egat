﻿namespace Egat.LegalCompliant.API.ViewModels
{
    public class DepartmentViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class LicenseTypeViewModel
    {
        public long Id { get; set; }
        public string Category { get; set; }
        public string CategoryText { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Issuer { get; set; }
        public int FrequencyYear { get; set; }
        public int FrequencyMonth { get; set; }
        public bool IsActive { get; set; }
    }

}

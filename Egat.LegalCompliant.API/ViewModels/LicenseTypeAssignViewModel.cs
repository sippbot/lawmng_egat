﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class LicenseTypeAssignViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}

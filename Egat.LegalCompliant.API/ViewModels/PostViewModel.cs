﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class PostViewModel
    {
        public long Id { get; set; }
        public string PostName { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool IsPrimary { get; set; }
        public long DepartmentId { get; set; }
        public long UserId { get; set; }
        public string Department { get; set; }
        public string User { get; set; }
    }
}

﻿using AutoMapper;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.API.ViewModels.Resolvers;
using Egat.LegalCompliant.Service.ServiceModels;

namespace Egat.LegalCompliant.API.ViewModels.Mappings
{
    public class ViewModelToServiceModelMappingProfile : Profile
    {
        public ViewModelToServiceModelMappingProfile()
        {
            // TaskAssignmentService
            CreateMap<TaskAssessmentInfoRequest, AssessmentInfoServiceModel>();
            CreateMap<TaskAssessmentReviewRequest, AssessmentReviewServiceModel>();
            CreateMap<TaskAssignmentProposeRequest, TaskAssignmentProposalServiceModel>();
            CreateMap<TaskAssignmentSummaryRequest, TaskAssignmentSummaryQueryServiceModel>()
                .ForMember(t => t.Status, opt => opt.ResolveUsing<StringInRequestToTaskAssignmentStatusResolver>());
        }
    }
}

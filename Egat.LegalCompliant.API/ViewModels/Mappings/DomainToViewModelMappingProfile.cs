﻿using AutoMapper;
using Egat.LegalCompliant.API.ViewModels.Resolvers;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Notifications;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Tickets;
using Egat.LegalCompliant.Service.Notification;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Mappings
{
    /*
     * This class is aim for map request from client to internal model class
     */
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            // Type level convertion
            CreateMap<DateTime, string>().ConvertUsing<DateTimeToStringConverter>();

            // Artifacts
            CreateMap<Artifact, ArtifactViewModel>()
                .ForMember(vm => vm.Category, map => map.MapFrom(a => a.Category.Name))
                .ForMember(vm => vm.Level, map => map.MapFrom(a => a.Level.Name))
                .ForMember(vm => vm.Source, map => map.MapFrom(a => a.Source.Name))
                .ForMember(vm => vm.Status, map => map.MapFrom(a => a.Status.ToString()))
                .ForMember(vm => vm.StatusText, opt => opt.ResolveUsing<ArtifactTypeToStringResolver<Artifact, ArtifactViewModel>>())
                .ForMember(vm => vm.Type, map => map.MapFrom(a => a.Type.ToString()))
                .ForMember(vm => vm.TypeText, opt => opt.ResolveUsing<ArtifactTypeToStringResolver<Artifact, ArtifactViewModel>>());


            CreateMap<ArtifactNodeGroup, ArtifactNodeGroupViewModel>()
                .ForMember(vm => vm.Nodes, map => map.MapFrom(a => a.Nodes));

            CreateMap<ArtifactNode, ArtifactNodeViewModel>()
                .ForMember(vm => vm.RequiredLicenses, map => map.MapFrom(a => a.RequiredLicenses))
                .ForMember(vm => vm.Group, map => map.MapFrom(a => a.Group.Content));

            CreateMap<ArtifactNodeLicenseType, LicenseTypeAssignViewModel>()
                .ForMember(vm => vm.Name, map => map.MapFrom(a => a.LicenseType.Name))
                .ForMember(vm => vm.Id, map => map.MapFrom(a => a.LicenseType.Id));

            CreateMap<ArtifactAssignment, DepartmentAssignViewModel>()
                .ForMember(vm => vm.Id, map => map.MapFrom(a => a.DepartmentId))
                .ForMember(vm => vm.Name, map => map.MapFrom(a => a.Department.Name))
                .ForMember(vm => vm.AliasName, map => map.MapFrom(a => a.Department.AliasName));

            CreateMap<ArtifactAttachment, DocumentViewModel>()
                .ForMember(vm => vm.Id, map => map.MapFrom(s => s.DocumentId))
                .ForMember(vm => vm.Type, map => map.MapFrom(s => s.Document.Type))
                .ForMember(vm => vm.FileId, map => map.MapFrom(s => s.Document.FileId))
                .ForMember(vm => vm.Owner, map => map.MapFrom(s => s.Document.File.Owner.ToString()))
                .ForMember(vm => vm.Title, map => map.MapFrom(s => s.Document.Title))
                .ForMember(vm => vm.URL, opt => opt.ResolveUsing<DocumentIdToURLStringResolver<ArtifactAttachment>>());

            CreateMap<Document, DocumentViewModel>()
                .ForMember(s => s.Id, map => map.MapFrom(d => d.Id))
                .ForMember(s => s.Title, map => map.MapFrom(d => d.Title))
                .ForMember(s => s.Owner, map => map.MapFrom(f => f.File.Owner.ToString()))
                .ForMember(s => s.URL, opt => opt.ResolveUsing<DocumentIdToURLStringResolver<Document>>());

            CreateMap<Artifact, ArtifactSummaryViewModel>()
                .ForMember(vm => vm.Category, map => map.MapFrom(a => a.Category.Name))
                .ForMember(vm => vm.Level, map => map.MapFrom(a => a.Level.Name))
                .ForMember(vm => vm.Source, map => map.MapFrom(a => a.Source.Name))
                .ForMember(vm => vm.Status, map => map.MapFrom(a => a.Status.ToString()))
                .ForMember(vm => vm.Type, opt => opt.ResolveUsing<ArtifactTypeToStringResolver<Artifact, ArtifactSummaryViewModel>>());

            CreateMap<ArtifactSummary, ArtifactSummaryViewModel>()
                .ForMember(vm => vm.Category, map => map.MapFrom(a => a.Category.Name))
                .ForMember(vm => vm.Level, map => map.MapFrom(a => a.Level.Name))
                .ForMember(vm => vm.Source, map => map.MapFrom(a => a.Source.Name))
                .ForMember(vm => vm.Status, map => map.MapFrom(a => a.Status.ToString()))

                // create by Toey 31/03/2018
                .ForMember(vm => vm.TaskAssignmentStatus, map => map.MapFrom(a => a.TaskAssignmentStatus.ToString()))

                .ForMember(vm => vm.StatusText, opt => opt.ResolveUsing<ArtifactStatusToStringResolver<ArtifactSummary, ArtifactSummaryViewModel>>())
                .ForMember(vm => vm.Type, map => map.MapFrom(a => a.Type.ToString()))
                .ForMember(vm => vm.TypeText, opt => opt.ResolveUsing<ArtifactTypeToStringResolver<ArtifactSummary, ArtifactSummaryViewModel>>())
                .ForMember(vm => vm.TaskAssignmentType, map => map.MapFrom(a => a.TaskAssignmentType.ToString()));

            CreateMap<ArtifactCategory, ArtifactCategoryViewModel>();

            CreateMap<ArtifactLevel, ArtifactLevelViewModel>();


            // Party

            CreateMap<Department, DepartmentViewModel>();

            CreateMap<DepartmentSummary, DepartmentSummaryViewModel>()
                .ForMember(vm => vm.StatusText, opt => opt.ResolveUsing<ArtifactStatusToStringResolver<DepartmentSummary, DepartmentSummaryViewModel>>());


            CreateMap<User, UserViewModel>()
                .ForMember(vm => vm.Group, map => map.MapFrom(u => u.Group.Name));

            CreateMap<UserGroup, UserGroupViewModel>();

            // Documents

            CreateMap<LicenseType, LicenseTypeViewModel>()
                .ForMember(vm => vm.CategoryText, opt => opt.ResolveUsing<LicenseCategoryToStringResolver>());

            CreateMap<Post, PostViewModel>()
                .ForMember(vm => vm.Department, map => map.MapFrom(p => p.Department.Name))
                .ForMember(vm => vm.User, map => map.MapFrom(p => p.User.ToString()));

            CreateMap<TaskAssignmentAttachment, DocumentViewModel>()
                .ForMember(vm => vm.Id, map => map.MapFrom(s => s.DocumentId))
                .ForMember(vm => vm.Type, map => map.MapFrom(s => s.Document.Type))
                .ForMember(vm => vm.FileId, map => map.MapFrom(s => s.Document.FileId))
                .ForMember(vm => vm.Title, map => map.MapFrom(s => s.Document.Title))
                .ForMember(vm => vm.URL, opt => opt.ResolveUsing<DocumentIdToURLStringResolver<TaskAssignmentAttachment>>());

            // Tasks

            // comment 224/03/2018
            //CreateMap<TaskAssignment, TaskAssignmentViewModel>()
            //    //.ForMember(vm => vm.ArtifactNode, map => map.MapFrom(a => a.ArtifactNode.Content))
            //    .ForMember(vm => vm.IsCompliantText, opt => opt.ResolveUsing<IsCompliantBooleanToStringOrDefaultResolver<TaskAssignment, TaskAssignmentViewModel>>())
            //    .ForMember(vm => vm.IsApplicableText, opt => opt.ResolveUsing<IsApplicableBooleanToStringOrDefaultResolver<TaskAssignment, TaskAssignmentViewModel>>())
            //    .ForMember(vm => vm.StatusText, opt => opt.ResolveUsing<TaskAssignmentStatusToStringResolver>())
            //    .ForMember(vm => vm.Department, map => map.MapFrom(s => s.Department.Name))
            //    .ForMember(vm => vm.Group, map => map.MapFrom(s => s.ArtifactNode.Group.Content));

            CreateMap<TaskAssignment, TaskAssignmentViewModel>()
                //.ForMember(vm => vm.ArtifactNode, map => map.MapFrom(a => a.ArtifactNode.Content))
                .ForMember(vm => vm.IsCompliantText, opt => opt.ResolveUsing<IsCompliantTaskAssignmentBooleanToStringResolver<TaskAssignment, TaskAssignmentViewModel>>())
                .ForMember(vm => vm.IsApplicableText, opt => opt.ResolveUsing<IsApplicableTaskAssignmentBooleanToStringResolver<TaskAssignment, TaskAssignmentViewModel>>())
                .ForMember(vm => vm.StatusText, opt => opt.ResolveUsing<TaskAssignmentStatusToStringResolver>())
                .ForMember(vm => vm.Department, map => map.MapFrom(s => s.Department.Name))
                .ForMember(vm => vm.Group, map => map.MapFrom(s => s.ArtifactNode.Group.Content));

            CreateMap<TaskAssignmentSummary, TaskAssignmentSummaryViewModel>()
                .ForMember(vm => vm.Department, map => map.MapFrom(s => s.Department.Name))
                .ForMember(vm => vm.IsCompliantText, opt => opt.ResolveUsing<IsCompliantBooleanToStringOrDefaultResolver<TaskAssignmentSummary, TaskAssignmentSummaryViewModel>>())
                .ForMember(vm => vm.IsApplicableText, opt => opt.ResolveUsing<IsApplicableBooleanToStringOrDefaultResolver<TaskAssignmentSummary, TaskAssignmentSummaryViewModel>>())
                .ForMember(vm => vm.SummaryResult, opt => opt.ResolveUsing<TaskAssignmentSummaryStatusToStringResolver>());

            CreateMap<TaskAssessment, TaskAssessmentViewModel>()
                .ForMember(vm => vm.Creator, map => map.MapFrom(s => s.Creator.ToString()))
                .ForMember(vm => vm.Reviewer, map => map.MapFrom(s => s.Reviewer.ToString()))
                .ForMember(vm => vm.IsCompliantText, opt => opt.ResolveUsing<IsCompliantTaskAssessmentBooleanToStringResolver<TaskAssessment, TaskAssessmentViewModel>>())
                .ForMember(vm => vm.IsApplicableText, opt => opt.ResolveUsing<IsApplicableTaskAssessmentBooleanToStringResolver<TaskAssessment, TaskAssessmentViewModel>>())
                .ForMember(vm => vm.StatusText, opt => opt.ResolveUsing<TaskAssessmentStatusToStringResolver>());

            CreateMap<TaskComment, TaskCommentViewModel>()
                .ForMember(vm => vm.Creator, map => map.MapFrom(c => c.Creator.ToString()));

            // Notification

            CreateMap<MailNotification, NotificationInfo>();

            CreateMap<MailNotification, MailNotificationViewModel>()
                .ForMember(vm => vm.Type, map => map.MapFrom(a => a.Type.ToString()));

            // Ticket

            CreateMap<Ticket, TicketViewModel>()
                .ForMember(t => t.StatusText, opt => opt.ResolveUsing<TicketStatusToStringResolver>())
                .ForMember(t => t.Status, map => map.MapFrom(t => t.Status.ToString()))
                .ForMember(t => t.Department, map => map.MapFrom(t => t.Department.AliasName))
                .ForMember(t => t.Owner, map => map.MapFrom(t => t.Owner.ToString()))
                .ForMember(t => t.CloseBy, map => map.MapFrom(t => t.CloseBy.ToString()))
                .ForMember(t => t.TaskAssignmentType, map => map.MapFrom(t => t.TaskAssignment.Type.ToString()));

            // License

            CreateMap<License, LicenseViewModel>()
                .ForMember(vm => vm.Status, map => map.MapFrom(l => l.Status.ToString()))
                .ForMember(vm => vm.Alert, map => map.MapFrom(l => l.Alert.ToString()));
            //.ForMember(vm => vm.LicenseType, map => map.MapFrom(l => l.LicenseType.Name));
            //.ForMember(vm => vm.Document, map => map.MapFrom(d => d.Document.Title));
        }
    }
}
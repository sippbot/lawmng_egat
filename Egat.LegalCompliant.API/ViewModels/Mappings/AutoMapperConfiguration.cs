﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDomainMappingProfile>();

                // Intermediat Model Presentation-to-ServiceLayer
                x.AddProfile<ViewModelToServiceModelMappingProfile>();
            });
        }
    }
}

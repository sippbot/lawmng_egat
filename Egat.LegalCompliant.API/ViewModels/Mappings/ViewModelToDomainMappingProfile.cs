﻿using AutoMapper;
using Egat.LegalCompliant.API.Models.QueryString;
using Egat.LegalCompliant.API.ViewModels.Resolvers;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Party;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            // Type level convertion
            CreateMap<string, DateTime>().ConvertUsing<StringToDateTimeConverter>();

            // Document
            CreateMap<LicenseTypeViewModel, LicenseType>()
               .ForMember(s => s.Category, opt => opt.ResolveUsing<StringToLicenseCategoryResolver>());

            CreateMap<DocumentViewModel, Document>()
                .ForMember(s => s.Id, map => map.MapFrom(d => d.Id));

            CreateMap<DocumentViewModel, ArtifactAttachment>()
                .ForMember(s => s.Id, map => map.MapFrom(d => d.Id));

            // Artifact
            CreateMap<ArtifactViewModel, Artifact>()
                .ForMember(a => a.Status, opt => opt.ResolveUsing<StringToArtifactStatusResolver>())
                .ForMember(a => a.Type, opt => opt.ResolveUsing<StringToArtifactTypeResolver>())
                .ForMember(a => a.Source, map => map.UseValue(new ArtifactSource()))
                .ForMember(a => a.Category, map => map.UseValue(new ArtifactCategory()))
                .ForMember(a => a.Documents, map => map.UseValue(new HashSet<ArtifactAttachment>()))
                .ForMember(a => a.Level, map => map.UseValue(new ArtifactLevel()));

            CreateMap<ArtifactNodeViewModel, ArtifactNode>()
                .ForMember(a => a.RequiredLicenses, map => map.UseValue(new List<LicenseType>()))
                .ForMember(a => a.Group, map => map.UseValue(new ArtifactNodeGroup()));

            CreateMap<ArtifactNodeGroupViewModel, ArtifactNodeGroup>();

            CreateMap<LicenseTypeAssignViewModel, ArtifactNodeLicenseType>()
                .ForMember(al => al.LicenseTypeId, map => map.MapFrom(lt => lt.Id));

            CreateMap<ArtifactLevelViewModel, ArtifactLevel>();

            CreateMap<ArtifactCategoryViewModel, ArtifactCategory>();

            // Party

            CreateMap<DepartmentAssignViewModel, ArtifactAssignment>()
                .ForMember(da => da.DepartmentId, map => map.MapFrom(a => a.Id));

            CreateMap<DepartmentViewModel, Department>();

            CreateMap<PostViewModel, Post>();

            CreateMap<UserViewModel, User>();

            CreateMap<UserGroupViewModel, UserGroup>();

            // Query String Mapper
            CreateMap<ArtifactSummaryQueryStringViewModel, ArtifactSummaryQueryString>();
            // TODO should add auto map here

            // Task

            CreateMap<TaskCommentViewModel, TaskComment>();

            // License

            CreateMap<LicenseViewModel, License>();
        }
    }
}

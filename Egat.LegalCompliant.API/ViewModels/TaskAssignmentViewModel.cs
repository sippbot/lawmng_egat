﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class TaskAssignmentViewModel
    {
        public long Id { get; set; }
        public long DepartmentId { get; set; }
        public string AssignDate { get; set; }
        public string DueDate { get; set; }
        public string ApproveDate { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public bool IsApplicable { get; set; }
        public bool IsCompliant { get; set; }
        public string IsApplicableText { get; set; }
        public string IsCompliantText { get; set; }
        public string Description { get; set; }
        public DateTime LastModified { get; set; }
        public int Progress { get; set; }
        public string Title { get; set; }
        public string Group { get; set; }

        public ArtifactNodeViewModel ArtifactNode { get; set; }
        public long ArtifactNodeId { get; set; }

        public ArtifactSummaryViewModel Artifact { get; set; }
        public long ArtifactId { get; set; }

        public string Department { get; set; }
        public ICollection<TaskAssessmentViewModel> Assessments { get; set; }
        public ICollection<TaskCommentViewModel> Comments { get; set; }
        public ICollection<DocumentViewModel> Documents { get; set; }
        public ICollection<TicketViewModel> Tickets { get; set; }

        public TaskAssignmentViewModel()
        {
            Assessments = new HashSet<TaskAssessmentViewModel>();
            Comments = new HashSet<TaskCommentViewModel>();
        }
    }
}

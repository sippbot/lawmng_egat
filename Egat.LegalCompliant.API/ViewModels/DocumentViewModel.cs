﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class DocumentViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public long FileId { get; set; }
        public string URL { get; set; }
        public string CreateDate { get; set; }
        public string Owner { get; set; }
    }
}

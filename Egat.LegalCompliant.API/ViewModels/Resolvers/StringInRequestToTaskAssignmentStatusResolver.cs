﻿using AutoMapper;
using Egat.LegalCompliant.API.Models;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.ServiceModels;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class StringInRequestToTaskAssignmentStatusResolver : IValueResolver<TaskAssignmentSummaryRequest, TaskAssignmentSummaryQueryServiceModel, TaskAssignmentStatus>
    {

        public TaskAssignmentStatus Resolve(TaskAssignmentSummaryRequest source, TaskAssignmentSummaryQueryServiceModel destination, TaskAssignmentStatus destMember, ResolutionContext context)
        {
            string status = source.Status as string;
            if (!string.IsNullOrWhiteSpace(status))
            {
                status = status.Replace(" ", "");
                return (TaskAssignmentStatus)Enum.Parse(typeof(TaskAssignmentStatus), status, true);
            }
            return TaskAssignmentStatus.Proposed;
        }
    }
}

﻿using AutoMapper;
using Egat.LegalCompliant.Model.Artifacts;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class StringToArtifactStatusResolver : IValueResolver<ArtifactViewModel, Artifact, ArtifactStatus>
    {
        public ArtifactStatus Resolve(ArtifactViewModel source, Artifact destination, ArtifactStatus destMember, ResolutionContext context)
        {
            string status = source.Status as string;
            if (!string.IsNullOrWhiteSpace(status))
            {
                status = status.Replace(" ", "");
                return (ArtifactStatus)Enum.Parse(typeof(ArtifactStatus), status, true);
            }
            return ArtifactStatus.Draft;
        }

    }
}

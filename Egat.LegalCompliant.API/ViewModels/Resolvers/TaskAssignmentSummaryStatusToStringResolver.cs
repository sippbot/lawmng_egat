﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class TaskAssignmentSummaryStatusToStringResolver : IValueResolver<TaskAssignmentSummary, TaskAssignmentSummaryViewModel, string>
    {
        //TaskAssessmentStatus
        private readonly IViewModelMessageUtil _messageUtil;

        public TaskAssignmentSummaryStatusToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(TaskAssignmentSummary source, TaskAssignmentSummaryViewModel destination, string destMember, ResolutionContext context)
        {
            var interval = source.Status.ToString();
            var ret = _messageUtil.GetMessage(typeof(TaskAssignmentStatus), interval);
            return ret;
        }
    }
}

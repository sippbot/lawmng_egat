﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class IsApplicableTaskAssessmentBooleanToStringResolver<T,U> : IValueResolver<T,U,string> where T : IAssessable
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public IsApplicableTaskAssessmentBooleanToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(T source, U destination, string destMember, ResolutionContext context)
        {
            if (((IAssessable)source).IsApplicable)
            {
                return _messageUtil.GetMessage(source.GetType(), "IsApplicable:true");
            }
            else
            {
                return _messageUtil.GetMessage(source.GetType(), "IsApplicable:false");
            }
        }
    }
}

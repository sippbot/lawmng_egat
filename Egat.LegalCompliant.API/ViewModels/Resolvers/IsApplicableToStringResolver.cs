﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class IsApplicableToStringResolver : IValueResolver<TaskAssessment,TaskAssessmentViewModel, string>
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public IsApplicableToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(TaskAssessment source, TaskAssessmentViewModel destination,string destMember, ResolutionContext context)
        {
            if (source.IsApplicable)
            {
                return _messageUtil.GetMessage(source.GetType(), "IsApplicable:true");
            }
            else
            {
                var ret =  _messageUtil.GetMessage(source.GetType(), "IsApplicable:false");
                return ret;
            }
        }
    }
}

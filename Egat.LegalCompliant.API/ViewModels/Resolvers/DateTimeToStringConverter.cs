﻿using AutoMapper;
using Egat.LegalCompliant.Service.Utilities;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class DateTimeToStringConverter : ITypeConverter<DateTime, string>
    {
        public string Convert(DateTime source, string destination, ResolutionContext context)
        {
            return DateTimeUtil.ConvertDateTimeToStringDate(source);
        }

    }
}

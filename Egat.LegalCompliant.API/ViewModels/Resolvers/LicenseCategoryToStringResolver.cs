﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class LicenseCategoryToStringResolver : IValueResolver<LicenseType, LicenseTypeViewModel, string>
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public LicenseCategoryToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(LicenseType source, LicenseTypeViewModel destination, string member, ResolutionContext context)
        {
            LicenseCategory category = source.Category;

            return _messageUtil.GetMessage(typeof(LicenseCategory), category.ToString());
        }
    }
}

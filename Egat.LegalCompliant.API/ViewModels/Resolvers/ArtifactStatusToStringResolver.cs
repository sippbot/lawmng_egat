﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class ArtifactStatusToStringResolver<T,U> : IValueResolver<T, U, string> where T : IArtifact
    {
        //TaskAssessmentStatus
        private readonly IViewModelMessageUtil _messageUtil;

        public ArtifactStatusToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(T source, U destination, string destMember, ResolutionContext context)
        {
            var key = source.Status.ToString();
            var ret =  _messageUtil.GetMessage(typeof(ArtifactStatus), key);
            return ret;
        }
    }
}

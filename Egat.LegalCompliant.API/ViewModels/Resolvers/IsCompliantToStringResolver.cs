﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class IsCompliantToStringResolver : IValueResolver<TaskAssessment,TaskAssessmentViewModel, string>
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public IsCompliantToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(TaskAssessment source, TaskAssessmentViewModel destination,string destMember, ResolutionContext context)
        {
            if (source.IsCompliant)
            {
                return _messageUtil.GetMessage(source.GetType(), "IsCompliant:true");
            }
            else
            {
                return _messageUtil.GetMessage(source.GetType(), "IsCompliant:false");
            }
        }
    }
}

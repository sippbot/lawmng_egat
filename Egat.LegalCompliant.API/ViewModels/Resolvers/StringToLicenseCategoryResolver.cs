﻿using AutoMapper;
using Egat.LegalCompliant.Model.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class StringToLicenseCategoryResolver : IValueResolver<LicenseTypeViewModel, LicenseType, LicenseCategory>
    {
        public LicenseCategory Resolve(LicenseTypeViewModel source, LicenseType destination, LicenseCategory member, ResolutionContext context)
        {

            string category = source.Category as string;

            if (!string.IsNullOrWhiteSpace(category))
            {
                category = category.Replace(" ", "");
                return (LicenseCategory)Enum.Parse(typeof(LicenseCategory), category, true);
            }

            return LicenseCategory.Miscellaneous;
        }
    }
}

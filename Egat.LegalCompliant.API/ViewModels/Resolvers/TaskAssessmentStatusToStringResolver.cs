﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class TaskAssessmentStatusToStringResolver : IValueResolver<TaskAssessment, TaskAssessmentViewModel, string>
    {
        //TaskAssessmentStatus
        private readonly IViewModelMessageUtil _messageUtil;

        public TaskAssessmentStatusToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(TaskAssessment source, TaskAssessmentViewModel destination, string destMember, ResolutionContext context)
        {
            var interval = source.Status.ToString();
            var ret =  _messageUtil.GetMessage(typeof(TaskAssessmentStatus), interval);
            return ret;
        }
    }
}

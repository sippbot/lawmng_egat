﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class IsApplicableBooleanToStringOrDefaultResolver<T, U> : IValueResolver<T, U, string> where T : IAssignable
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public IsApplicableBooleanToStringOrDefaultResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(T source, U destination, string destMember, ResolutionContext context)
        {
            //if (((IAssignable)source).IsReady())
            if (((IAssignable)source).IsReady() || ((IAssignable)source).Status == TaskAssignmentStatus.Assessed
                || ((IAssignable)source).Status == TaskAssignmentStatus.Ticketed)
            {
                if (((IAssignable)source).IsApplicable)
                {
                    return _messageUtil.GetMessage(source.GetType(), "IsApplicable:true");
                }
                else
                {
                    return _messageUtil.GetMessage(source.GetType(), "IsApplicable:false");
                }
            }

            return _messageUtil.GetMessage(source.GetType(), "IsApplicable:default");
        }
    }
}

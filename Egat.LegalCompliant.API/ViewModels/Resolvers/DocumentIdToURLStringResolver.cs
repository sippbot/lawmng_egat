﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Artifacts;
using Egat.LegalCompliant.Model.Documents;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Service.Configurations;
using Microsoft.Extensions.Options;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class DocumentIdToURLStringResolver<T> : IValueResolver<T, DocumentViewModel, string> where T : IDocument
    {
        private readonly ServiceCommonConfiguration _config;

        public DocumentIdToURLStringResolver(IOptions<ServiceCommonConfiguration> config)
        {
            _config = config.Value;
        }

        public string Resolve(T source, DocumentViewModel destination, string destMember, ResolutionContext context)
        {
            var documentTitle = source.GetDocumentTitle() == null ? "document" : source.GetDocumentTitle();

            var ret = _config.EndPointURL + "/api/document/" + source.GetDocumentId() + "/" + documentTitle;
            return ret;
        }
    }
}

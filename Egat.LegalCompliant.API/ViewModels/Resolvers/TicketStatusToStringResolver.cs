﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Tasks;
using Egat.LegalCompliant.Model.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class TicketStatusToStringResolver : IValueResolver<Ticket, TicketViewModel, string>
    {
        //TaskAssessmentStatus
        private readonly IViewModelMessageUtil _messageUtil;

        public TicketStatusToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(Ticket source, TicketViewModel destination, string destMember, ResolutionContext context)
        {
            var interval = source.Status.ToString();
            var ret =  _messageUtil.GetMessage(typeof(TicketStatus), interval);
            return ret;
        }
    }
}

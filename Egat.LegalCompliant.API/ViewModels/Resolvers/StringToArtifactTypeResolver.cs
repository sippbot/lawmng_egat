﻿using AutoMapper;
using Egat.LegalCompliant.Model.Artifacts;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class StringToArtifactTypeResolver : IValueResolver<ArtifactViewModel, Artifact, ArtifactType>
    {
        public ArtifactType Resolve(ArtifactViewModel source, Artifact destination, ArtifactType destMember, ResolutionContext context)
        {
            string type = source.Type as string;
            if (!string.IsNullOrWhiteSpace(type))
            {
                type = type.Replace(" ", "");
                return (ArtifactType)Enum.Parse(typeof(ArtifactType), type, true);
            }
            return ArtifactType.Miscellaneous;
        }

    }
}
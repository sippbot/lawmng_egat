﻿using AutoMapper;
using Egat.LegalCompliant.Service.Utilities;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class StringToDateTimeConverter : ITypeConverter<string, DateTime>
    {
        public DateTime Convert(string source, DateTime destination, ResolutionContext context)
        {
            if (!string.IsNullOrWhiteSpace(source))
            {
                source = source.Replace(" ", "");
                return DateTimeUtil.ConvertStringToDateTime(source);
            }
            return DateTime.MaxValue;
            //return System.Convert.ToDateTime(source);
        }
    }
}

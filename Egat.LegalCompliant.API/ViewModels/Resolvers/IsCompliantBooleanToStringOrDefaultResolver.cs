﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model;
using Egat.LegalCompliant.Model.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class IsCompliantBooleanToStringOrDefaultResolver<T, U> : IValueResolver<T, U, string> where T : IAssignable
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public IsCompliantBooleanToStringOrDefaultResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(T source, U destination, string destMember, ResolutionContext context)
        {
            //if (((IAssignable)source).IsReady())
            if (((IAssignable)source).IsReady() || ((IAssignable)source).Status == TaskAssignmentStatus.Assessed
                || ((IAssignable)source).Status == TaskAssignmentStatus.Ticketed)
            {
                if (((IAssignable)source).IsCompliant)
                {
                    return _messageUtil.GetMessage(source.GetType(), "IsCompliant:true");
                }
                else
                {
                    return _messageUtil.GetMessage(source.GetType(), "IsCompliant:false");
                }
            }
            return _messageUtil.GetMessage(source.GetType(), "IsCompliant:default");
        }
    }
}

﻿using AutoMapper;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.Model.Artifacts;
using System;

namespace Egat.LegalCompliant.API.ViewModels.Resolvers
{
    public class ArtifactTypeToStringResolver<T,U> : IValueResolver<T, U, string> where T : IArtifact
    {
        private readonly IViewModelMessageUtil _messageUtil;

        public ArtifactTypeToStringResolver(IViewModelMessageUtil messageUtil)
        {
            _messageUtil = messageUtil;
        }

        public string Resolve(T source, U destination, string destMember, ResolutionContext context)
        {
            ArtifactType _type = source.Type;
            return _messageUtil.GetMessage(typeof(ArtifactType), _type.ToString());
        }

    }
}
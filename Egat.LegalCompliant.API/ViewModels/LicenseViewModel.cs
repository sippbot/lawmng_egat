﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class LicenseViewModel
    {
        public long Id { get; set; }
        public string LicenseNumber { get; set; }
        public string HolderName { get; set; }
        public string IssueDate { get; set; }
        public string ExpireDate { get; set; }
        public string DueDate { get; set; }
        public string Status { get; set; }
        public string Alert { get; set; }

        public long LicenseTypeId { get; set; }
        public LicenseTypeViewModel LicenseType { get; set; }

        public long DocumentId { get; set; }
        public DocumentViewModel Document { get; set; }

        public long TaskAssignmentId { get; set; }
        public long UserId { get; set; }

        public bool IsPublic { get; set; }
        public bool IsOverride { get; set; }
        public long? LicenseHistoryId { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class UserGroupViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }

        //* create by Toey 02/04/2018
        public bool IsSetPermission { get; set; }
    }
}

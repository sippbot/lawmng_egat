﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class TaskAssignmentSummaryViewModel
    {
        public long DepartmentId { get; set; }
        public string Department { get; set; }
        public int NumberOfTotalTask { get; set; }
        public int NumberOfInprogressTask { get; set; }
        public bool IsApplicable { get; set; }
        public bool IsCompliant { get; set; }
        public string IsApplicableText { get; set; }
        public string IsCompliantText { get; set; }
        public string Status { get; set; }
        public string SummaryResult { get; set; }
        public long TaskGroupId { get; set; }
    }
}

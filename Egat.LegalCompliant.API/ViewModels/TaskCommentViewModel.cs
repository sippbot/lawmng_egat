﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class TaskCommentViewModel
    {
        public long Id { get; set; }
        public string CreateDate { get; set; }
        public string Content { get; set; }
        public long TaskAssignmentId { get; set; }

        //Reference
        public long CreatorId { get; set; }
        public string Creator { get; set; }
    }
}

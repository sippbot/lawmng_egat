﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class ArtifactSummaryViewModel
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }

        // create by Toey 31/03/2018
        public string TaskAssignmentStatus { get; set; }

        public string TaskAssignmentType { get; set; }
        public string Type { get; set; }
        public string TypeText { get; set; }
        public string CreateDate { get; set; }
        public string PublishedDate { get; set; }
        public string EffectiveDate { get; set; }
        public string ApprovedDate { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string LastModified { get; set; }
        public string DueDate { get; set; }
        // public string Note { get; set; }
        // public string Introduction { get; set; }

        // Foreign Key
        public long SourceId { get; set; }
        public long CategoryId { get; set; }
        public long LevelId { get; set; }

        // Refernce
        public ICollection<DepartmentSummaryViewModel> Departments { get; set; }
        public string Source { get; set; }

        public string Category { get; set; }
        public string Level { get; set; }

        public ArtifactSummaryViewModel()
        {
            Departments = new HashSet<DepartmentSummaryViewModel>();
        }
    }
}

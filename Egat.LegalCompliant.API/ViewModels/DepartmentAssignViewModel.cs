﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class DepartmentAssignViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
    }
}

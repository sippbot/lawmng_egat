﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.ViewModels
{
    public class ArtifactNodeViewModel
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public int Seq { get; set; }
        public int FollowUpPeriodMonth { get; set; }
        public int FollowUpPeriodYear { get; set; }
        public bool IsRequiredLicense { get; set; }
        public bool IsAllDepartment { get; set; }
        public string Group { get; set; }
        public long ArtifactId { get; set; }
        public long GroupId { get; set; }

        public bool IsActive { get; set; }

        //Reference
        //public long ArtifactId { get; set; }
        //public string Artifact { get; set; }
        //public long GroupId { get; set; }
        //public ArtifactNodeGroup Group { get; set; }

        public ICollection<LicenseTypeAssignViewModel> RequiredLicenses { get; set; }
        public ICollection<DepartmentAssignViewModel> Departments { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Egat.LegalCompliant.API.Helpers
{
    public class GenerateAntiforgeryTokenCookieForAjaxAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var antiforgery = context.HttpContext.RequestServices.GetService<IAntiforgery>();

            // We can send the request token as a JavaScript-readable cookie, 
            // and Angular will use it by default.
            if(context.HttpContext.Response.StatusCode == 200)
            {
                var tokens = antiforgery.GetAndStoreTokens(context.HttpContext);
                var tokensString = tokens.RequestToken;
                context.HttpContext.Response.Cookies.Append(
                    "XSRF-TOKEN",
                    tokensString,
                    new CookieOptions() { HttpOnly = false });
            }
        }
    }
}

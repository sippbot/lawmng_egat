﻿using Hangfire.Dashboard;

namespace Egat.LegalCompliant.API.Helpers.Filters
{
    public class AllowAllDashboardAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}

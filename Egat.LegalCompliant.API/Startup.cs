﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Net;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Diagnostics;
using Egat.LegalCompliant.Data;
using Egat.LegalCompliant.Data.Abstract;
using Egat.LegalCompliant.Data.Repositories;
using Egat.LegalCompliant.API.Core;
using Egat.LegalCompliant.API.Services.Authentication;
using Egat.LegalCompliant.API.Models.Configurations;
using Hangfire;
using Egat.LegalCompliant.API.Utilities.ViewModelMessage;
using Egat.LegalCompliant.API.Helpers.Filters;
using Egat.LegalCompliant.Service.Abstract;
using Egat.LegalCompliant.Service;
using AutoMapper;
using Egat.LegalCompliant.Service.Configurations;
using Egat.LegalCompliant.Service.Utilities;
using Egat.LegalCompliant.Service.Complaint;
using Microsoft.AspNetCore.ResponseCompression;
using Hangfire.Common;
using System;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Egat.LegalCompliant.API
{
    public class Startup
    {
        private static string _applicationPath = string.Empty;
        private static string _contentRootPath = string.Empty;
        public string sqlConnectionString = string.Empty;
        public IConfigurationRoot Configuration { get; }
        private bool useInMemoryProvider = false;

        public Startup(IHostingEnvironment env)
        {
            _applicationPath = env.WebRootPath;
            _contentRootPath = env.ContentRootPath;
            // Setup configuration sources.

            var builder = new ConfigurationBuilder()
                .SetBasePath(_contentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            /*
            if (env.IsDevelopment())
            {
                // This reads the configuration keys from the secret store.
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();
            }
            */

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration["Database:ConnectionString"];
            //services.AddDbContext<LegalCompliantContext>(options => 
            //    options.UseSqlServer(connection,
            //    b => b.MigrationsAssembly("Egat.LegalComplient.API")));

            //string sqlConnectionString = Configuration.GetConnectionString("DefaultConnection");

            try
            {
                useInMemoryProvider = bool.Parse(Configuration["AppSettings:InMemoryProvider"]);
            }
            catch { }

            services.AddDbContext<LegalCompliantContext>(options =>
            {
                switch (useInMemoryProvider)
                {
                    case true:
                        options.UseInMemoryDatabase();
                        break;
                    default:
                        options.UseSqlServer(connection,
                        b => b.MigrationsAssembly("Egat.LegalCompliant.API"));
                        break;
                }
            });
            /****************************************************************
             * 
             *                      Repositories
             * 
             ****************************************************************/
            // Artifact 
            services.AddScoped<IArtifactRepository, ArtifactRepository>();
            services.AddScoped<IArtifactNodeRepository, ArtifactNodeRepository>();
            services.AddScoped<IArtifactNodeGroupRepository, ArtifactNodeGroupRepository>();
            services.AddScoped<IArtifactCategoryRepository, ArtifactCategoryRepository>();
            services.AddScoped<IArtifactLevelRepository, ArtifactLevelRepository>();
            services.AddScoped<IArtifactSourceRepository, ArtifactSourceRepository>();
            services.AddScoped<IArtifactNodeLicenseTypeRepository, ArtifactNodeLicenseTypeRepository>();
            services.AddScoped<IArtifactAssignmentRepository, ArtifactAssignmentRepository>();

            services.AddScoped<IArtifactAttachmentRepository, ArtifactAttachmentRepository>();
            // Party
            services.AddScoped<IUserGroupRepository, UserGroupRepository>();
            services.AddScoped<IDepartmentRepository, DepartmentRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IDocumentRepository, DocumentRepository>();
            services.AddScoped<IFileDataRepository, FileDataRepository>();
            // Document
            services.AddScoped<ILicenseTypeRepository, LicenseTypeRepository>();
            services.AddScoped<ILicenseHistoryRepository, LicenseHistoryRepository>();
            services.AddScoped<ILicenseRepository, LicenseRepository>();
            services.AddScoped<IFileDataRepository, FileDataRepository>();
            services.AddScoped<IDocumentRepository, DocumentRepository>();
            // Task
            services.AddScoped<ITaskAssignmentRepository, TaskAssignmentRepository>();
            services.AddScoped<ITaskAssessmentRepository, TaskAssessmentRepository>();
            services.AddScoped<ITaskCommentRepository, TaskCommentRepository>();
            services.AddScoped<ITaskGroupRepository, TaskGroupRepository>();
            // Notification
            services.AddScoped<IMailNotificationRepository, MailNotificationRepository>();
            // Ticket
            services.AddScoped<ITicketRepository, TicketRepository>();
            // Authorization
            services.AddScoped<IUserGroupRolesRepository, UserGroupRolesRepository>();
            services.AddScoped<IUserRolesRepository, UserRolesRepository>();

            /****************************************************************
             * 
             *                            CORS
             * 
             ****************************************************************/
            // Angular's default header name for sending the XSRF token.
            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");

            /****************************************************************
             * 
             *                    Other configuration
             * 
             ****************************************************************/
            // Configuration
            services.Configure<LdapConfiguration>(Configuration.GetSection("AthenticationServer:ldap"));
            services.Configure<FileServerConfiguration>(Configuration.GetSection("FileServer"));
            services.Configure<EmailNotificationServiceConfiguration>(Configuration.GetSection("Notification:Mail"));
            services.Configure<TicketServiceConfiguration>(Configuration.GetSection("Complaint"));
            services.Configure<SchedulerConfiguration>(Configuration.GetSection("Scheduler"));
            services.Configure<ServiceCommonConfiguration>(Configuration.GetSection("ServiceCommon"));
            services.Configure<ArtifactServiceConfiguration>(Configuration.GetSection("ArtifactService"));
            services.Configure<ViewRendererServiceConfiguration>(Configuration.GetSection("ViewRenderer"));
            services.Configure<DailyJobExecutorServiceConfiguration>(Configuration.GetSection("DailyJob"));
            // services.Configure<ControllerConfig>(Configuration.GetSection("LAWMNG:Assignment"));
            // services.Configure<DateTimeUtilConfiguration>(Configuration.GetSection("DateTimeUtil"));

            // Services
            services.AddScoped<IAuthenticationService, LdapAuthenticationService>();
            services.AddScoped<INotificationService, EmailNotificationService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IDailyJobExecutorService, DailyJobExecutorService>();
            services.AddScoped<IAccessControlService, AccessControlService>();
            // Service Layer
            services.AddScoped<IArtifactService, ArtifactService>();
            services.AddScoped<ITaskAssignmentService, TaskAssignmentService>();
            services.AddScoped<IDocumentService, DocumentService>();
            services.AddScoped<IViewRendererService, ViewRendererService>();
            services.AddScoped<IAutoMessageGenerator, AutoMessageGenerator>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Utilities
            services.AddSingleton<IViewModelMessageUtil, ViewModelMessageUtil>();

            /****************************************************************
             * 
             *                 Third Party Configuration
             * 
             ****************************************************************/
            // HangFire 
            // We config connection string to the same one that we use by other components.
            services.AddHangfire(config => config.UseSqlServerStorage(connection));
            // services.AddHangfire(config.UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection")));

            // Automapper Configuration
            services.AddAutoMapper(typeof(Startup));
            // AutoMapperConfiguration.Configure();

            /****************************************************************
             * 
             *                 Web Service Configuration
             * 
             ****************************************************************/

            // Enable Cors
            services.AddCors();
            services.AddMvc()
                .AddJsonOptions(opts =>
                {
                    // Force Camel Case to JSON
                    opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            // Response Compression
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IRecurringJobManager recurringJobManager)
        {
            // Response Compression
            app.UseResponseCompression();

            app.UseStaticFiles();

            app.UseJwtBearerAuthentication(JWTAccessTokenOption());

            // Add MVC to the request pipeline.
            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseExceptionHandler(
              builder =>
              {
                  builder.Run(
                    async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                        //context.Response.Headers.Add("Access-Control-Expose-Headers","XSRF-TOKEN");

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            context.Response.AddApplicationError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(false);
                        }
                    });
              });

            // loggerFactory.AddDebug();
            loggerFactory.AddLog4Net();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "login",
                    template: "login",
                    defaults: new { controller = "Account", action = "Login" }
                );
                routes.MapRoute(
                    name: "logout",
                    template: "logout",
                    defaults: new { controller = "Account", action = "Logout" }
                );

            });

            LegalCompliantDbInitializer.Initialize(app.ApplicationServices);

            // Enable Hangfire server and dashboard
            // To access dashboard use http://<host-name>/hangfire
            // app.UseHangfireDashboard();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                Authorization = new[] { new AllowAllDashboardAuthorizationFilter() }
            });
            app.UseHangfireServer();
            string cronStringAutoAssign = Configuration["DailyJob:CronStringAutoAssign"];
            string cronStringTicketOverDue = Configuration["DailyJob:CronStringTicketOverDue"];
            string cronStringLicenseExpired = Configuration["DailyJob:CronStringLicenseExpired"];
            bool dailyTaskEnable;
            Boolean.TryParse(Configuration["DailyJob:Enable"], out dailyTaskEnable);

            if (dailyTaskEnable)
            {

                recurringJobManager.AddOrUpdate("AutoAssignTaskAssigment", Job.FromExpression<IDailyJobExecutorService>(
                    dailyTask => dailyTask.SendAssigndTaskAssignment()), cronStringAutoAssign
                    );

                recurringJobManager.AddOrUpdate("AutoTicketOverDueTaskAssignment", Job.FromExpression<IDailyJobExecutorService>(
                    dailyTask => dailyTask.SendOverdueTicket()), cronStringTicketOverDue
                    );

                recurringJobManager.AddOrUpdate("AutoAlertLicenseExpired", Job.FromExpression<IDailyJobExecutorService>(
                    dailyTask => dailyTask.SendLicenseExpireAlert()), cronStringLicenseExpired
                    );

                recurringJobManager.AddOrUpdate("AutoSendNotificationLicenseExpired", Job.FromExpression<IDailyJobExecutorService>(
                    dailyTask => dailyTask.SendLicenseExpiredNotification()), cronStringLicenseExpired
                    );
            }
            else
            {
                recurringJobManager.RemoveIfExists("AutoAssignTaskAssigment");
                recurringJobManager.RemoveIfExists("AutoTicketOverDueTaskAssignment");
                recurringJobManager.RemoveIfExists("AutoAlertLicenseExpired");
                recurringJobManager.RemoveIfExists("AutoSendNotificationLicenseExpired");
            }
        }

        private JwtBearerOptions JWTAccessTokenOption()
        {
            var secretKey = Configuration.GetSection("ServiceCommon:JwtKey").Value;
            var issuer = Configuration.GetSection("ServiceCommon:JwtIssuer").Value;
            var audience = Configuration.GetSection("ServiceCommon:JwtAudience").Value;
            var signingKey = new SymmetricSecurityKey(Convert.FromBase64String(secretKey));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = false,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = false
            };

            return new JwtBearerOptions
            {
                TokenValidationParameters = tokenValidationParameters
            };

        }
    }
}

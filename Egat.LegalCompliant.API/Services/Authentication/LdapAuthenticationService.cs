﻿using Egat.LegalCompliant.API.Models.Configurations;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Novell.Directory.Ldap;
using System;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Services.Authentication
{
    public class LdapAuthenticationService : IAuthenticationService
    {
        private const string MemberOfAttribute = "memberOf";
        private const string DisplayNameAttribute = "displayName";
        private const string SAMAccountNameAttribute = "sAMAccountName";

        private readonly LdapConfiguration _config;
        private readonly LdapConnection _connection;
        private readonly ILogger<LdapAuthenticationService> _logger;

        public LdapAuthenticationService(IOptions<LdapConfiguration> config, ILogger<LdapAuthenticationService> logger)
        {
            _config = config.Value;
            _connection = new LdapConnection
            {
                SecureSocketLayer = false
            };
            _logger = logger;
        }

        public AppUser Login(string username, string password)
        {
            try
            {
                var task = Task.Run(() =>
                {
                    _connection.Connect(_config.Url, _config.Port);
                    _connection.Bind(_config.AdminDn, _config.BindCredentials);
                    _logger.LogInformation("LdapAuthenticationService - login() : binding with connection success");

                    var searchFilter = string.Format(_config.SearchFilter, username);
                    var result = _connection.Search(
                        _config.SearchBase,
                        LdapConnection.SCOPE_SUB,
                        searchFilter,
                        new[] { MemberOfAttribute, DisplayNameAttribute, SAMAccountNameAttribute },
                        false
                    );
                    var user = result.next();
                    if (user != null)
                    {
                        _connection.Bind(user.DN, password);
                        if (_connection.Bound)
                        {
                            return new AppUser
                            {
                                DisplayName = user.getAttribute(DisplayNameAttribute).StringValue,
                                Username = user.getAttribute(SAMAccountNameAttribute).StringValue,
                            };
                        }
                        _logger.LogInformation("LdapAuthenticationService - login() : cannot bound connection");
                        throw new Exception("Login failed. - cannot bound connection");
                    }
                    _logger.LogInformation("LdapAuthenticationService - login() : user is null");
                    throw new Exception("Login failed. - user is null");
                }
                );

                if (task.Wait(TimeSpan.FromSeconds(10)))
                {
                    return task.Result;
                }
                else
                {
                    throw new Exception("Timed out");
                }

            }
            catch (Exception e)
            {
                _logger.LogInformation("LdapAuthenticationService - login() : login failed cause by {}", e.Message + e.StackTrace);
                throw new Exception("Login failed. - other exception ", e);

            }
            finally
            {
                _connection.Disconnect();
            }
        }
    }
}

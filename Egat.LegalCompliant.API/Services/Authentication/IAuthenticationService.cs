﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Services.Authentication
{
    public interface IAuthenticationService
    {
        AppUser Login(string username, string password);
    }
}

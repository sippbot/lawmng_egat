﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Egat.LegalCompliant.API.Services.Authentication
{
    public class AppUser
    {
        public string DisplayName { get; set; }
        public string Username { get; set; }
    }
}
